# -------------------------------------------------
# Project created by QtCreator 2010-08-31T13:13:48
# -------------------------------------------------
QT += sql \
    network \
    svg
CONFIG  += debug qwt qxt warn_on exceptions rtti
CONFIG  -= release

unix {
    QT += dbus
    INCLUDEPATH += /usr/local/qwt/include/
    LIBS += -L/usr/local/qwt/lib -lqwt
}
win32 {
    INCLUDEPATH += c:\qt\qwt-6.0\src
    CONFIG(debug) {
        LIBS += -L"C:\Qt\Qwt-6.0.0-svn\lib" -lqwt6
    }
    else {
        LIBS += -L"C:\Qt\Qwt-6.0.0-svn\lib" -lqwt6
    }
}


QXT += core

TARGET = EnchSales
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    qwbrdb.cpp \
    logindialog.cpp \
    threadexec.cpp \
    QProgressIndicator.cpp \
    enchitem.cpp \
    test.cpp \
    qwt_plot_price.cpp \
    qwt_move_series_data.cpp \
    enchtreewidget.cpp
HEADERS += mainwindow.h \
    qwbrdb.h \
    qwbrplot.h \
    logindialog.hpp \
    threadexec.h \
    QProgressIndicator.h \
#    enchitem.hpp \ # no needs MOCing -- no QObjects
    qwt_price_move.h \
    qwt_plot_price.h \
    qwt_move_series_data.h \
    enchtreewidget.hpp \
    dbinterface.hxx
FORMS += mainwindow.ui \
    logindialog.ui

RESOURCES += \
    resource.qrc
