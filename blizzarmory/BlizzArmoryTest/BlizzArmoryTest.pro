#-------------------------------------------------
#
# Project created by QtCreator 2011-08-04T11:47:41
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = BlizzArmoryTest
CONFIG   += console debug
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L/home/weber/lib -lQBlizzArmory

SOURCES += \
    main.cxx
