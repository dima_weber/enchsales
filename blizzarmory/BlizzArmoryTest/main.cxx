#include <QtCore/QCoreApplication>
#include "../armory.hxx"
#include "../realmresource.hxx"

using namespace QwbrBlizzardArmory;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Armory armory;

    armory.setUseAuth (false);
    armory.setUseSsl (true);
    armory.setPrivateKey ("private");
    armory.setPublicKey ("public");

    armory.setDefaultRealm (QString::fromUtf8 ("Азурегос"));
    armory.setDefaultRegion (EU);

    RealmResource*  realm = armory.realmResource();
    RealmStatusInfo status = realm->getRealmInfo("Азурегос");

    return a.exec();
}
