#-------------------------------------------------
#
# Project created by QtCreator 2011-08-03T23:51:20
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = QBlizzArmory
TEMPLATE = lib

CONFIG += debug qxt

QXT += core

DEFINES += QBLIZZARMORY_LIBRARY

SOURCES += \
    hmac_sha1.cxx \
    armory.cxx \
    realmresource.cxx \
    resource.cxx

HEADERS +=\
    hmac_sha1.hxx \
    guildresource.hxx \
    characterresource.hxx \
    QBlizzArmory_global.hxx \
    armory.hxx \
    resource.hxx \
    realmresource.hxx \
    auctionresource.hxx \
    dataresource.hxx

symbian {
    #Symbian specific definitions
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE55380C4
    TARGET.CAPABILITY = 
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = QBlizzArmory.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /home/weber/lib
    }
    INSTALLS += target
}
