#include "armory.hxx"
#include <QDateTime>
#include "hmac_sha1.hxx"
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QxtJSON>
#include <QxtSignalWaiter>

#include "characterresource.hxx"
#include "guildresource.hxx"
#include "realmresource.hxx"
#include "dataresource.hxx"
#include "auctionresource.hxx"
#include <QNetworkAccessManager>

namespace QwbrBlizzardArmory
{
    Armory::Armory()
        : QObject()
    {
        pManager = new QNetworkAccessManager(this);
    }

    QString Armory::getAuthHeader(QString urlPath, QString httpVerb)
    {
        QString ret = "";
        QString stringToSign = QString("%1\n%2\n%3\n").arg(httpVerb).arg(QDateTime::currentDateTime().toString()).arg(urlPath);
        QString signature = hmacSha1(_privateKey.toUtf8(), stringToSign.toUtf8());
        ret = QString("%1 %2:%3").arg("BNET").arg(_publicKey).arg(signature);
        return ret;
    }

    void Armory::setPrivateKey (const QString & key)
    {
        _privateKey = key;
    }

    void Armory::setPublicKey (const QString & key)
    {
        _publicKey = key;
    }

    void  Armory::setKeys (const QString& publicKey, const QString& privatKey)
    {
        setPrivateKey (privatKey);
        setPublicKey (publicKey);
    }

    QString Armory::regionAddress (Region r)
    {
        switch(r)
        {
            case DEFAULT:
            case US: return "us.battle.net";
            case EU: return "eu.battle.net";
            case KR: return "kr.battle.net";
            case TW: return "tw.battle.net";
            case CN: return "battlenet.com.cn";
        }
        return "localhost";
    }

    void Armory::setUseAuth (bool use)
    {
        _useAuth = use;
    }

    bool Armory::useAuth () const
    {
        return _useAuth;
    }

    CharacterResource* Armory::characterResource (const QString &characterName, const QString &realm, const Region &region)
    {
        CharacterResource* resource = new CharacterResource (this, characterName, (realm.isNull ()?defaultRealm ():realm), (region==DEFAULT?defaultRegion ():region));
        return resource;
    }

    GuildResource* Armory::guildResource (const QString &guildName, const QString &realm, const Region &region)
    {
        GuildResource* resource = new GuildResource (this, guildName, (realm.isNull ()?defaultRealm ():realm), (region==DEFAULT?defaultRegion ():region));
        return resource;
    }

    AuctionResource* Armory::auctionResource (const QString &realm, const Region &region)
    {
        AuctionResource* resource = new AuctionResource(this, realm.isNull ()?defaultRealm ():realm, region==DEFAULT?defaultRegion ():region);
        return resource;
    }

    RealmResource* Armory::realmResource (const Region &region)
    {
        RealmResource* resource = new RealmResource(this, region==DEFAULT?defaultRegion ():region);
        return resource;
    }

    DataResource* Armory::dataResource()
    {
        return new DataResource(this);
    }

    void Armory::setDefaultRealm (const QString &realm)
    {
        _defaultRealm = realm;
    }

    void Armory::setDefaultRegion (Region region)
    {
        _defaultRegion = region;
    }

    QString Armory::defaultRealm () const
    {
        return _defaultRealm;
    }

    Region Armory::defaultRegion () const
    {
        return _defaultRegion;
    }

    void Armory::setUseSsl (bool use)
    {
        _useSsl = use;
    }

    bool Armory::useSsl () const
    {
        return _useSsl;
    }

    QVariantMap Armory::onRequestDone (QNetworkReply* reply)
    {
        //QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
        QByteArray buf = reply->readAll ();
        QString jsonStr = QString::fromUtf8 (buf.data (), buf.size ());
        reply->deleteLater ();

        QVariant var = QxtJSON::parse (jsonStr);

        QVariantMap map;
        if (var.canConvert (QVariant::Map))
        {
            map = var.toMap ();
        }
        return map;
    }

    void Armory::onHttpError (QNetworkReply::NetworkError e)
    {
        Q_UNUSED(e)
        QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender ());
        qDebug() << reply->errorString ();
    }

    #ifndef QT_NO_OPENSSL
    void Armory::onSslError (QList<QSslError> errList)
    {
        foreach(QSslError e, errList)
        {
            qDebug() << e.errorString();
        }
    }
    #endif

    QVariantMap Armory::doRequest(QString address, Region region, QString method, QList<QPair<QString, QString> > params)
    {
        QString regionUrl = regionAddress (region == DEFAULT?defaultRegion ():region);
        QUrl url;

        url.setHost (regionUrl);
        url.setPath (address);
        url.setScheme ( useSsl ()?"https":"http");
        url.setQueryItems (params);

        QNetworkRequest request;
        request.setUrl (url);
        if (useAuth ())
            request.setRawHeader ("Authorization", getAuthHeader (address).toUtf8 ());

        qDebug() << url.toString();

        QNetworkReply* reply = pManager->get (request);
        //connect (pManager, SIGNAL(finished(QNetworkReply*)), SLOT(onRequestDone(QNetworkReply* )));
        connect (reply, SIGNAL(error(QNetworkReply::NetworkError)), SLOT(onHttpError(QNetworkReply::NetworkError)));
    #ifndef QT_NO_OPENSSL
        connect (reply, SIGNAL(sslErrors(QList<QSslError>)), SLOT(onSslError(QList<QSslError>)));
    #endif
        QxtSignalWaiter::wait (reply, SIGNAL(finished()));
        return onRequestDone (reply);
    }
}
