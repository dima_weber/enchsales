#ifndef QBLIZZARMORY_HPP
#define QBLIZZARMORY_HPP

#include "QBlizzArmory_global.hxx"
#include <QObject>
#include <QString>

#include <QtNetwork/QNetworkReply>
#ifndef QT_NO_OPENSSL
#   include <QtNetwork/QSslError>
#endif

class QNetworkAccessManager;
class QNetworkReply;
namespace QwbrBlizzardArmory
{
    class CharacterResource;
    class AuctionResource;
    class GuildResource;
    class DataResource;
    class RealmResource;

    enum Region {DEFAULT = -1, US, EU, KR, TW, CN};

    struct QBLIZZARMORYSHARED_EXPORT RaceInfo
    {
        int id;
        uint mask;
        int side;
        QString name;
    };


    class QBLIZZARMORYSHARED_EXPORT Armory : public QObject
    {
        Q_OBJECT
    public:
        // Keys
        void setPublicKey(const QString& key);
        void setPrivateKey(const QString& key);
        void setKeys(const QString& publicKey, const QString& privateKey);

        // Auth & Crypt
        void setUseAuth(bool use = true);
        bool useAuth() const;
        void setUseSsl (bool use = true);
        bool useSsl() const;

        // Defaults
        void setDefaultRealm(const QString& realm);
        void setDefaultRegion (Region region);
        QString defaultRealm() const;
        Region  defaultRegion() const;

        // Character Resources
        CharacterResource* characterResource  (const QString &characterName,  const QString &realm = QString::null, const Region& region = DEFAULT);
        GuildResource*     guildResource      (const QString &guildName,      const QString &realm = QString::null, const Region& region = DEFAULT);
        RealmResource*     realmResource      (const Region& region = DEFAULT);
        AuctionResource*   auctionResource    (const QString& realm = QString::null, const Region& region = DEFAULT);
        DataResource*      dataResource();

        Armory();
        QVariantMap doRequest(QString address, Region region = DEFAULT, QString method = "GET", QList<QPair<QString, QString> > params = QList<QPair<QString,QString> >());

        static QString regionAddress (Region r);

    private slots:
        QVariantMap onRequestDone(QNetworkReply* );
        void onHttpError(QNetworkReply::NetworkError);
    #ifndef QT_NO_OPENSSL
        void onSslError(QList<QSslError>);
    #endif

    private:
        QString getAuthHeader(QString urlPath, QString httpVerb = "GET");

        bool _useAuth;
        bool _useSsl;

        QString _defaultRealm;
        Region  _defaultRegion;

        QString _privateKey;
        QString _publicKey;

        QNetworkAccessManager* pManager;
    };
}
#endif // QBLIZZARMORY_HPP
