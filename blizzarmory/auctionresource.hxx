#ifndef AUCTIONRESOURCE_HXX
#define AUCTIONRESOURCE_HXX
#include "armory.hxx"
namespace QwbrBlizzardArmory
{
    class QBLIZZARMORYSHARED_EXPORT AuctionResource : public Resource
    {
        public:
            AuctionResource(Armory* pArmory, const QString& realm, const Region& region)
                :Resource (pArmory, region)
            {}
    };
}
#endif // AUCTIONRESOURCE_HXX
