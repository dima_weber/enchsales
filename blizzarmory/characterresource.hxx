#ifndef CHARACTERRESOURCE_H
#define CHARACTERRESOURCE_H

#include "resource.hxx"

namespace QwbrBlizzardArmory
{
    class QBLIZZARMORYSHARED_EXPORT CharacterResource : public Resource
    {
        QString _characterName;
        QString _characterRealm;
        Region  _characterRegion;
        bool    infoLoaded;

        CharacterResource(Armory* pArmory, const QString& characterName, const QString& characterRealm, const Region& characterRegion)
            :Resource (pArmory, characterRegion)
        {}

    public:
        QString characterName() const;
        QString characterRealm() const;
        Region characterRegion() const;


        friend class Armory;
    };
}
#endif // CHARACTERRESOURCE_H
