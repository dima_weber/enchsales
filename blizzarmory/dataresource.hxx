#ifndef DATARESOURCE_HXX
#define DATARESOURCE_HXX

#include "armory.hxx"
#include "resource.hxx"
namespace QwbrBlizzardArmory
{
    class QBLIZZARMORYSHARED_EXPORT DataResource : public Resource
    {
    public:
        DataResource(Armory* pArmory)
            : Resource (pArmory, DEFAULT)
        {}
        QList<RaceInfo> getCharacterRaces();

        friend class Armory;
    };

}
#endif // DATARESOURCE_HXX
