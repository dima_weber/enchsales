#ifndef GUILDRESOURCE_HXX
#define GUILDRESOURCE_HXX

#include "armory.hxx"

namespace QwbrBlizzardArmory
{
    class QBLIZZARMORYSHARED_EXPORT GuildResource : public Resource
    {
    public:
        GuildResource(Armory* pArmory, const QString& guildName, const QString& relam, const Region& region)
            :Resource(pArmory, region)
        {}//;
    };
}
#endif // GUILDRESOURCE_HXX
