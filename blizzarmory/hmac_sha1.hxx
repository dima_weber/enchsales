#ifndef HMAC_SHA1_HXX
#define HMAC_SHA1_HXX

#include <QString>
#include <QByteArray>

QString hmacSha1(QByteArray key, QByteArray baseString);

#endif // HMAC_SHA1_HXX
