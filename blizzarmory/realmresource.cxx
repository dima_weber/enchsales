#include "realmresource.hxx"

namespace QwbrBlizzardArmory
{
    RealmStatusInfo& RealmStatusInfo::fromVariant(QVariantMap map)
    {
        type = stringToRealmType(map["type"].toString());
        population = stringToPopulation(map["population"].toString());
        queue = map["queue"].toBool();
        status = map["status"].toBool();
        name = map["name"].toString();
        slug = map["slug"].toString();

        return *this;
    }

    RealmStatusInfo::RealmStatusInfo()
    {}

    RealmStatusInfo::RealmStatusInfo(QVariantMap map)
    {
        fromVariant(map);
    }

    RealmType RealmStatusInfo::stringToRealmType(const QString &string)
    {
        if (string.compare("pve", Qt::CaseInsensitive) == 0)
            return PvE;
        if (string.compare("pvp", Qt::CaseInsensitive) == 0)
            return PvP;
        if (string.compare("rp", Qt::CaseInsensitive) == 0)
            return RP;
        if (string.compare("RPPVP", Qt::CaseInsensitive) == 0)
            return RPPvP;
        return UnknownType;
    }

    QString RealmStatusInfo::realmTypeToString(RealmType type)
    {
        switch (type)
        {
        case PvE: return "PvE";
        case PvP: return "PvP";
        case RP: return "RP";
        case RPPvP: return "RP-PvP";
        case UnknownType:
        default: return "unknown realm type";
        }
    }

    RealmPopulation RealmStatusInfo::stringToPopulation(const QString &string)
    {
        if (string.compare("high", Qt::CaseInsensitive) == 0)
            return High;
        if (string.compare("low", Qt::CaseInsensitive) == 0)
            return Low;
        if (string.compare("medium", Qt::CaseInsensitive) == 0)
            return Medium;
        return UnknownPopulation;
    }

    QString RealmStatusInfo::populationToString(RealmPopulation population)
    {
        switch (population)
        {
        case Low: return "low";
        case High: return "high";
        case Medium: return "medium";
        case UnknownPopulation:
        default: return "unknown realm population";
        }
    }

    RealmResource::RealmResource(Armory* pArmory, const Region& region)
        :Resource(pArmory, region)
    {
        _dataAccessString = "/api/wow/realm/status";
    }

    RealmStatusInfo RealmResource::getRealmInfo(QString realmName)
    {
        RealmStatusInfo status;
        QList<QPair<QString, QString> > params;
        params.append(QPair<QString, QString>("realms", realmName));

        QVariantMap result = doRequest(params);
        if (!isError())
            status.fromVariant(result);

        return status;
    }
}
