#ifndef REALMRESOURCE_HXX
#define REALMRESOURCE_HXX
#include "armory.hxx"
#include "resource.hxx"

namespace QwbrBlizzardArmory
{
    enum RealmType {UnknownType = -1, PvP, PvE, RP, RPPvP};
    enum RealmPopulation {UnknownPopulation = -1, Low, Medium, High};

    struct RealmStatusInfo
    {
        RealmType type;
        bool    queue;
        bool    status;
        RealmPopulation population;
        QString name;
        QString slug;

        RealmStatusInfo& fromVariant(QVariantMap map);
        RealmStatusInfo();
        RealmStatusInfo(QVariantMap map);

        static RealmType stringToRealmType(const QString& string);
        static QString realmTypeToString(RealmType type);
        static RealmPopulation stringToPopulation(const QString& string);
        static QString populationToString(RealmPopulation type);
    };

    class QBLIZZARMORYSHARED_EXPORT RealmResource : public Resource
    {
    public:
        RealmResource(Armory* pArmory, const Region& region);
              RealmStatusInfo  getRealmInfo(QString realmName);
        QList<RealmStatusInfo> getRealmsByName(QStringList names);
        QList<RealmStatusInfo> getRealmsByType(RealmType type);
        QList<RealmStatusInfo> getRealmsByPopulation(RealmPopulation population);
        QList<RealmStatusInfo> getRealmsByQueue(bool queue);
        QList<RealmStatusInfo> getRealmsByStatus(bool status);
    };
}
#endif // REALMRESOURCE_HXX
