#include "resource.hxx"

namespace QwbrBlizzardArmory
{
    Resource::Resource(Armory *pArmory, const Region &region)
    {
        _pArmory = pArmory;
        _region = region;
    }

    QVariantMap Resource::doRequest(QList<QPair<QString, QString> > params)
    {
        QVariantMap result = _pArmory->doRequest(_dataAccessString, _region, "GET", params);
        if (result.isEmpty())
        {
            _isError = true;
            _errorReason = "Non JSON return.";
        }
        else if (result["status"] == "nok")
        {
            _isError = true;
            _errorReason = result["reason"].toString();
        }
        else
            _isError = false;

        return result;
    }

    bool Resource::isError() const
    {
        return _isError;
    }

    QString Resource::errorString() const
    {
        return _errorReason;
    }
}
