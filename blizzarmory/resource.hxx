#ifndef RESOURCE_HXX
#define RESOURCE_HXX

#include "armory.hxx"

namespace QwbrBlizzardArmory
{
    class Resource
    {
        Armory* _pArmory;
        Region  _region;

        bool    _isError;
        QString _errorReason;
    protected:
        QString _dataAccessString;

        Resource (Armory* pArmory, const Region& region );

        QVariantMap doRequest(QList<QPair<QString, QString> > params = QList<QPair<QString,QString> >());
    public:
        bool isError() const;
        QString errorString() const;
    };
}
#endif // RESOURCE_HXX
