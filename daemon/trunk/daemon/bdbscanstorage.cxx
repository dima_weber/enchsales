#include "bdbscanstorage.hxx"
#include  <QxtLogger>
#include "exception.hxx"

#include <db_cxx.h>
#include <stdlib.h>
#include <time.h>
#include <QDir>
#include <QMutex>
#include <QCoreApplication>

#define USE_TRANSACTIONS
//# undef USE_TRANSACTIONS

#define USE_LOCKS
//# undef USE_LOCKS

#define log_entry_header QString("[%1] %2 (%3:%4)").arg((long)pthread_self).arg(__func__).arg(__FILE__).arg(__LINE__)

/**
  Store pointer to cursor,
  cursor operation,
  pointer to transaction;
  */
struct DbExtraInfo
{
    Dbc*        pCursor;
    DbTxn*      pTransaction;
    u_int32_t   nextFetchOperation;

    DbExtraInfo()
    {
        pCursor = NULL;
        pTransaction = NULL;
        nextFetchOperation = DB_SET;
    }
};

/*
class Lock
{
    private:
    pthread_mutex_t* p_mutex;
    Lock(){}
    Lock& operator=(Lock&){return *this;}
    Lock(Lock&){}
    public:
    Lock(pthread_mutex_t* p)
    {
        p_mutex = p;
        pthread_mutex_lock(p_mutex);
    }
    ~Lock()
    {
        pthread_mutex_unlock(p_mutex);
    }
};

#define //LOCK_ON  {Lock lock(&writeDBMutex);
#define //LOCK_OFF }
*/

void errCallback(const DbEnv* env, const char* prefix, const char* errMsg)
{
    Q_UNUSED(env);
    qxtLog->error () << QString("BDB error: %1 %2 [%3]").arg (prefix).arg (errMsg).arg(log_entry_header);
}


int get_group_state(       Db* pSecondaryDb,
                    const Dbt* pPKey,
                    const Dbt* pPData,
                          Dbt* pSKey)
{
    Q_UNUSED(pSecondaryDb)
    Q_UNUSED(pPKey)

    GroupHandler group;
    group.fromMem (pPData->get_data (), pPData->get_size ());
    GroupState* pState = (GroupState*)malloc(sizeof(GroupState));
    *pState = group.state;
    pSKey->set_data (pState);
    pSKey->set_size (sizeof(GroupState));
    pSKey->set_ulen (sizeof(GroupState));
    pSKey->set_flags (DB_DBT_APPMALLOC);

    return 0;
}

int get_vault_groupid(       Db* pSecondaryDb,
                      const Dbt* pPKey,
                      const Dbt* pPData,
                            Dbt* pSKey)
{
    Q_UNUSED(pSecondaryDb)
    Q_UNUSED(pPKey)

    AucData  aucdata;
    aucdata.fromMem (pPData->get_data (), pPData->get_size ());
    char* pId = NULL;
    size_t size = 0;
    pId = (char*)aucdata.groupid.toMem (pId, size);
    char* ptr = (char*)malloc(size);
    memcpy (ptr, pId, size);
    delete [] pId;
    pSKey->set_data (ptr);
    pSKey->set_size (size);
    pSKey->set_ulen (size);
    pSKey->set_flags (DB_DBT_APPMALLOC);

    return 0;
}


void db_feedback_func(Db* dbp, int opcode, int percent)
{
    Q_UNUSED(dbp)

    QString proc_name;
    switch(opcode)
    {
    case DB_UPGRADE:
        proc_name = "Upgrade"; break;
    case DB_VERIFY:
        proc_name = "Verify"; break;
    default:
        proc_name = "Unknown"; break;
    }
    qxtLog->info() << QString("%1 %2: %3% done").arg(log_entry_header).arg(proc_name).arg(percent);
}

void env_feedback_func(DbEnv* dbenv, int opcode, int percent)
{
    Q_UNUSED (dbenv)

    QString proc_name;
    switch(opcode)
    {
    case DB_RECOVER:
        proc_name = "Recover"; break;
    default:
        proc_name = "Unknown"; break;
    }
    qxtLog->info() << QString("%1 %2: %3% done").arg(log_entry_header).arg(proc_name).arg(percent);
}

BDBScanStorage::BDBScanStorage(QString _datafolder, QObject* parent)
    :ScanStorageInterface(parent), datafolder(_datafolder)
{
    env = NULL;
    catalog = NULL;
    vault = NULL;
    catalog_state_index = NULL;
    vault_groupid_index = NULL;
}

bool BDBScanStorage::initialize ()
{
    int ret;

    u_int32_t envOpenFlags = DB_CREATE | DB_THREAD | DB_INIT_MPOOL  ;
    u_int32_t  dbOpenFlags = DB_CREATE | DB_THREAD;

#ifdef USE_TRANSACTIONS
#   define USE_LOCKS
    envOpenFlags |= DB_INIT_TXN | DB_INIT_LOG;
    dbOpenFlags |= DB_AUTO_COMMIT;
#endif

#ifdef USE_LOCKS
    envOpenFlags |= DB_INIT_LOCK;
#endif

    try
    {
        if (datafolder.isEmpty ())
            datafolder = QCoreApplication::applicationDirPath ();
        QDir dir (datafolder);
        if (!dir.exists ("data"))
             dir.mkdir ("data");
        QDir envFolder (dir.absoluteFilePath ("data"));
        qxtLog->debug () << QString("Use BDB env folder: %1").arg (envFolder.absolutePath ());
        bool retryOpen;
        do
        {
            retryOpen = false;
            try
            {
                env = new DbEnv(0);
                ret = env->set_cachesize (1,0,0);

                ret = env->set_lk_max_lockers (10000);
                ret = env->set_lk_max_locks ( 10000);
                ret = env->set_lk_max_objects (10000);

                env->set_errcall ( errCallback );
                env->set_errpfx ("Berkeley DB Envirompment");

                ret = env->set_lk_detect (DB_LOCK_DEFAULT);
                env->set_feedback(env_feedback_func);

                ret = env->open (envFolder.absolutePath ().toAscii(),  envOpenFlags, 0600 );
            }
            catch(DbException& e)
            {
                ret = env->close (0);
                if (e.get_errno() == DB_VERSION_MISMATCH
                        || e.get_errno() == DB_RUNRECOVERY
                   )
                {
                    qxtLog->warning() << QString(tr("%1 Env Version mismatch")).arg(log_entry_header);
                    delete env;
                    envOpenFlags |= DB_RECOVER | DB_CREATE;
                    retryOpen = true;

                    //env->remove(envFolder.absolutePath ().toAscii(), 0);

                 }
                 else
                     throw e;
            }
        } while (retryOpen);



        catalog = new Db(env, 0);
        catalog_state_index = new Db(env, 0);
        try
        {
             catalog->open (NULL, "catalog.db", "catalog", DB_BTREE, dbOpenFlags, 0600);
        }
        catch (DbException& e)
        {
            if (e.get_errno() == DB_OLD_VERSION)
            {
                delete catalog;

                Db db(NULL, 0);
                db.set_feedback(db_feedback_func);

                ret = db.upgrade(envFolder.absoluteFilePath ("catalog.db").toAscii(), 0);
                if (ret)
                 qxtLog->fatal() << log_entry_header << tr("Fail to upgrade catalog.db");

                catalog = new Db(env, 0);
                catalog->open (NULL, "catalog.db", "catalog", DB_BTREE, dbOpenFlags, 0600);
            }
            else
                throw e;
        }

        catalog_state_index->set_flags (DB_DUP | DB_DUPSORT);
        catalog_state_index->open (NULL, "catalog.db", "state_index", DB_BTREE, dbOpenFlags, 0600);

        catalog->associate (NULL, catalog_state_index, get_group_state, 0);

        vault   = new Db(env, 0);
        vault_groupid_index = new Db(env, 0);

        try
        {
            vault->open (NULL, "vault.db", "vault", DB_RECNO, dbOpenFlags, 0600);
        }
        catch (DbException& e)
        {
            if (e.get_errno() == DB_OLD_VERSION)
            {
                delete vault;

                Db db(NULL, 0);
                db.set_feedback(db_feedback_func);

                ret = db.upgrade(envFolder.absoluteFilePath ("vault.db").toAscii(), 0);
                if (ret)
                 qxtLog->fatal() << log_entry_header << tr("Fail to upgrade vault.db");

                vault = new Db(env, 0);
                vault->open (NULL, "vault.db", "vault", DB_RECNO, dbOpenFlags, 0600);
            }
            else
                throw e;
        }

        vault_groupid_index->set_flags (DB_DUP | DB_DUPSORT);
        vault_groupid_index->open (NULL, "vault.db", "groupid_index", DB_BTREE, dbOpenFlags, 0600);
        vault->associate (NULL, vault_groupid_index, get_vault_groupid, 0);

        catalog->associate_foreign (vault_groupid_index, NULL, DB_FOREIGN_CASCADE);

    }
    catch(DbException& e)
    {
        qxtLog->fatal() << log_entry_header << tr("fail to open DB Enviropment");
        qxtLog->fatal() << log_entry_header << QString("[%1]: %2").arg (e.get_errno()).arg (e.what());
    }

    stopThread = false;
    pthread_mutex_init(&waitMutex,    NULL);
    pthread_mutex_init(&writeDBMutex, NULL);
    pthread_cond_init (&threadSignal, NULL);
    pthread_create(&threadHandler,           NULL, BDBScanStorage::cleanThreadFunc, this);
#ifdef USE_LOCKS
    pthread_create(&lockDetectThreadHandler, NULL, BDBScanStorage::lockDetectFunc,  this);
#endif
    printState();
    onOpen();
    printState();

    return ScanStorageInterface::initialize ();
}

void BDBScanStorage::purgeGroup(GroupState state)
{
    Dbc* cursor;
    int ret;
    Dbt key;
    Dbt primkey;
    Dbt value;
    int purged;
    u_int32_t records_count;
    GroupID id;
    GroupHandler group;

    qxtLog->info () << log_entry_header << QString(tr("Check for %1 groups.")).arg(groupStateToString(state));
    catalog_state_index->cursor(NULL, &cursor, 0);
    //LOCK_ON
            bool redo;
            do
            {
                redo = false;

                try
                {
                        ret = cursor->pget(&key, &primkey, &value, DB_SET);
                }
                catch (DbDeadlockException& e)
                {
                    qxtLog->trace() << "DeadLock detected: redo operation";
                    redo = true;
                }
            }while (redo);
    //LOCK_OFF
    purged = 0;
    if (ret == 0)
    {
        cursor->count(&records_count, 0);
        qxtLog->warning() << log_entry_header << QString(tr("%1 invalid groups found. Please don't Ctrl-C / kill programm to prevent this. Use DBus shutdown call instead. Delete groups")).arg(records_count);
        do
        {
            group.fromDbt(&value, true);
            id.fromDbt (&primkey, true);
            qxtLog->trace() << log_entry_header << QString("deleting group #%1. ItemId: %2, Time: %3").arg(id.id).arg(group.meta.itemid).arg(group.meta.scantime.toString());
            //LOCK_ON
            bool redo;
            do
            {
                redo = false;

                try
                {
                    ret = cursor->del(0);
                }
                catch (DbDeadlockException& e)
                {
                    qxtLog->trace() << "DeadLock detected: redo operation";
                    redo = true;
                }
            }while (redo);
            //LOCK_OFF
            purged ++;
            //LOCK_ON
                    //bool redo;
                    do
                    {
                        redo = false;

                        try
                        {
                            ret = cursor->pget(&key, &primkey, &value, DB_NEXT_DUP);
                        }
                        catch (DbDeadlockException& e)
                        {
                            qxtLog->trace() << "DeadLock detected: redo operation";
                            redo = true;
                        }
                    }while (redo);

            //LOCK_OFF
        } while  (ret == 0);
        qxtLog->info() << log_entry_header << QString(tr("Done. %1 group(s) purged.")).arg(purged);
    }
    cursor->close();
}

/**
  Delete all commited groups.  -- normal state
  Delete all invalid groups.   -- broken state
  Make all locked groups Valid -- broken state
*/
void BDBScanStorage::onOpen()
{
    Dbt key;
    Dbt primkey;
    Dbt value;
    Dbc* cursor;
    u_int32_t records_count;
    int ret;
    GroupID id;
    GroupHandler group;
    GroupState state;
    int purged;

    key.set_data (&state);
    key.set_size (sizeof(GroupState));
    key.set_ulen (sizeof(GroupState));
    key.set_flags (DB_DBT_USERMEM);

    value.set_flags   (DB_DBT_MALLOC);
    primkey.set_flags (DB_DBT_MALLOC);

    clearCommited();
    purgeGroup(Invalid);
    purgeGroup(Corrupted);

    state = Locked;
    qxtLog->info () << log_entry_header << "Check for Locked groups.";
    catalog_state_index->cursor(NULL, &cursor, 0);
    //LOCK_ON
            bool redo;
            do
            {
                redo = false;

                try
                {
                    ret = cursor->pget(&key, &primkey, &value, DB_SET);
                }
                catch (DbDeadlockException& e)
                {
                    qxtLog->trace() << "DeadLock detected: redo operation";
                    redo = true;
                }
            }while (redo);

    //LOCK_OFF
    purged = 0;
    if (ret == 0)
    {
        cursor->count(&records_count, 0);
        qxtLog->warning()<< log_entry_header << QString("%1 locked groups found. Please don't Ctrl-C / kill programm to prevent this. Use DBus shutdown call instead. Validate groups").arg(records_count);
        do
        {
            group.fromMem(value.get_data(), value.get_size());
            group.state = Valid;
            group.pos = NULL;
            size_t size = value.get_size();
            group.toMem(value.get_data(), size);
            //LOCK_ON
            bool redo;
            do
            {
                redo = false;

                try
                {
                    ret = catalog->put (NULL, &primkey, &value, 0);
                }
                catch (DbDeadlockException& e)
                {
                    qxtLog->trace() << "DeadLock detected: redo operation";
                    redo = true;
                }
            }while (redo);
            //LOCK_OFF
            purged++;
            qxtLog->trace() << log_entry_header << QString("Updating group #%1. ItemId: %2, Time: %3").arg(id).arg(group.meta.itemid).arg(group.meta.scantime.toString());
            free (value.get_data());
            //LOCK_ON
            //bool redo;
            do
            {
                redo = false;

                try
                {
                    ret = cursor->pget(&key, &primkey, &value, DB_NEXT_DUP);
                }
                catch (DbDeadlockException& e)
                {
                    qxtLog->trace() << "DeadLock detected: redo operation";
                    redo = true;
                }
            }while (redo);
            //LOCK_OFF
        } while  (ret == 0);
        qxtLog->info() << log_entry_header << QString("Done. %1 group(s) Validated.").arg(purged);
    }
    cursor->close();
}

void BDBScanStorage::printState()
{
    Dbt key;
    Dbt value;
    Dbc* cursor;
    GroupState state;
    int states_count = 6;
    GroupState statesToShow[] = {Invalid, Valid, Locked, Commited, Blocked, Corrupted};
    u_int32_t records_count;
    int ret;

    key.set_data(&state);
    key.set_size(sizeof(GroupState));
    key.set_ulen(sizeof(GroupState));
    key.set_flags(DB_DBT_USERMEM);

    value.set_flags(DB_DBT_MALLOC);

    for (int i=0; i< states_count; i++)
    {
        state = statesToShow[i];
        catalog_state_index->cursor(NULL, &cursor, 0);
        //LOCK_ON
        bool redo;
        do
        {
            redo = false;

            try
            {
                ret = cursor->get(&key, &value, DB_SET);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);
        //LOCK_OFF
        if (!ret)
        {
            cursor->count(&records_count, 0);
            qxtLog->info() << log_entry_header << QString("%1 : %2").arg(groupStateToString(state)).arg(records_count);
            free (value.get_data());
        }
        else
        {
            if (ret != DB_NOTFOUND)
                qxtLog->error () << log_entry_header << "fail to set cursor";
            else
                qxtLog->info () << log_entry_header << QString("%1 : %2").arg(groupStateToString(state)).arg("not found");
        }
        cursor->close();
    }
}

BDBScanStorage::~BDBScanStorage ()
{
    stopThread = true;
    pthread_cond_signal(&threadSignal);
    pthread_join(threadHandler, NULL);
    qxtLog->trace () << log_entry_header << "clearThread should be finished by now";

#ifdef USE_LOCKS
    pthread_join (lockDetectThreadHandler, NULL);
    qxtLog->trace () << log_entry_header << "lockDetectThread should be finished by now";
#endif

    pthread_mutex_destroy(&waitMutex);
    pthread_cond_destroy (&threadSignal);
    int ret;

    if (catalog_state_index)
    {
        ret = catalog_state_index->close (0);
        if (ret)
            qxtLog->critical () << log_entry_header << "Fail to close catalog_state_index db";
        delete catalog_state_index;
    }
    if (catalog)
    {
        ret = catalog->close (0);
        if (ret)
            qxtLog->critical () << log_entry_header << "Fail to close catalog db";
        delete catalog;
    }
    if (vault_groupid_index)
    {
        ret = vault_groupid_index->close (0);
        if (ret)
            qxtLog->critical () << log_entry_header << "Fail to close vault_groupid_index db";
        delete vault_groupid_index;
    }
    if (vault)
    {
        ret = vault->close (0);
        if (ret)
            qxtLog->critical () << log_entry_header << "Fail to close vault db";
        delete vault;
    }
    if (env)
    {
        ret = env->close (DB_FORCESYNC);
        if (ret)
            qxtLog->critical () << log_entry_header << "Fail to close environment";
        delete env;
    }
    pthread_mutex_destroy(&writeDBMutex);
}

GroupID         BDBScanStorage::startGroup(MetaType metadata)
{
    GroupID      group_id = GroupID::null;
    DbTxn*       hTransaction = NULL;
    GroupHandler hGroup;

    DbExtraInfo* pExtraInfo =  new DbExtraInfo;
    hGroup.pos   = pExtraInfo;
    hGroup.meta  = metadata;
    hGroup.state = Invalid;

    try
    {
#ifdef USE_TRANSACTIONS
        env->txn_begin (NULL, &hTransaction, 0);
#endif
        pExtraInfo->pTransaction = hTransaction;
        int ret;
        do
        {
            //group_id = qrand() + 1;
            group_id = QDateTime::currentMSecsSinceEpoch ();
            //LOCK_ON
            bool redo;
            do
            {
                redo = false;

                try
                {
                    ret = catalog->put (hTransaction, group_id.toDbt (), hGroup.toDbt (), 0);
                }
                catch (DbDeadlockException& e)
                {
                    qxtLog->trace() << "DeadLock detected: redo operation";
                    redo = true;
                }
            }while (redo);
            //LOCK_OFF
        } while (ret == DB_KEYEXIST);

        if (ret != 0)
        {
            group_id = GroupID::null;
        }
        if (hTransaction)
            hTransaction->commit (0);
    }
    catch(DbException& e)
    {
        qxtLog->error () << log_entry_header << "fail to start new group";
        qxtLog->error () << log_entry_header << QString("%1 [%2]: %3").arg(__func__).arg(e.get_errno()).arg (e.what());
        if (hTransaction)
            hTransaction->abort ();
        group_id = GroupID::null;
    }

    if (group_id.isValid())
        emit groupStarted(group_id);

    return group_id;
}

bool BDBScanStorage::addItemToGroup(GroupID  id,
                                   DataType data)
{
    AucData aucdata(data);
    DbTxn* pTransaction = NULL;
    DbExtraInfo* pExtraInfo = NULL;
    aucdata.groupid = id;
    QString str = (QString)aucdata;
    QString msg = QString("new item retrieved: %1").arg (str);
    qxtLog -> trace() << "put item:" << msg;

    Dbt vaultkey;
    Dbt catalogvalue;

    GroupHandler group;
    db_recno_t recno;

    vaultkey.set_data (&recno);
    vaultkey.set_size (sizeof(recno));
    vaultkey.set_ulen (sizeof(recno));
    vaultkey.set_flags (DB_DBT_USERMEM);

    catalogvalue.set_flags (DB_DBT_MALLOC);

    int ret;
    try
    {
        //LOCK_ON
        bool redo;
        do
        {
            redo = false;

            try
            {
                ret = catalog->get (pTransaction, id.toDbt (), &catalogvalue, 0);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);

        //LOCK_OFF
        if (ret)
            THROW_EXCEPTION("can't get group handle");

        if(!group.fromDbt (&catalogvalue, true))
            THROW_EXCEPTION("Fail to construct group from Dbt");

        pExtraInfo = reinterpret_cast<DbExtraInfo*>(group.pos);
        if (!pExtraInfo)
            THROW_EXCEPTION("ExtraInfo NULL pointer")
        pTransaction = pExtraInfo->pTransaction;
        if (group.state != Invalid)
            THROW_EXCEPTION (QString("not Invalid group. Group [%1] state: %2").arg (id).arg (groupStateToString (group.state))) ;

        recno = 1;
        //LOCK_ON
        //bool redo;
        do
        {
            redo = false;

            try
            {
                ret = vault->put (pTransaction, &vaultkey, aucdata.toDbt (), DB_APPEND);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);

        //LOCK_OFF
        qxtLog->trace () << log_entry_header << QString ("record #%1 added").arg(recno);
        if (ret != 0)
        {
            switch(ret)
            {
                case DB_KEYEXIST: qxtLog->error() << log_entry_header << "key already exists"; break;
            }
            THROW_EXCEPTION("fail to put item");
        }
    }
    catch (DbException& e)
    {
        qxtLog->error() << log_entry_header << QString("Fail to addItemToGroup [%1]").arg(id);
        qxtLog->error() << log_entry_header << QString("[%1]: %2").arg(e.get_errno()).arg(e.what());
        ret = -1;
    }
    catch (Exception& e)
    {
        qxtLog->error () << log_entry_header << QString("Fail to addItemToGroup: %1").arg(e.getErrorMsg());
        ret = -1;
    }

    if (ret && pTransaction)
    {
        qxtLog->error()<< log_entry_header << "Abort transaction";
        pTransaction->abort();
        delete pExtraInfo;
    }

    if (ret == 0)
        emit groupItemAdded(id, data);

    return ret == 0;
}

bool BDBScanStorage::finishGroup(GroupID id)
{
    Dbt value;
    DbTxn* pTransaction = NULL;
    DbExtraInfo* pExtraInfo = NULL;
    GroupHandler group;
    int   ret;

    value.set_flags (DB_DBT_MALLOC);
    try
    {
        //LOCK_ON
        bool redo;
        do
        {
            redo = false;

            try
            {
                ret = catalog->get (pTransaction, id.toDbt (), &value, 0);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);
        //LOCK_OFF
        if (ret != 0)
            THROW_EXCEPTION("finishGroup: can't get group status");
        if (!group.fromDbt (&value, true))
            THROW_EXCEPTION("cant construct group from Dbt");
        pExtraInfo = reinterpret_cast<DbExtraInfo*>(group.pos);
        pTransaction = pExtraInfo->pTransaction;
        if (group.state != Invalid)
            THROW_EXCEPTION(QString("finishGroup: not Invalid group. Group [%1] state: %2").arg (id).arg(groupStateToString (group.state)));
        group.state = Valid;
        group.pos = NULL;

        //LOCK_ON
        //bool redo;
        do
        {
            redo = false;

            try
            {
                ret = catalog->put (pTransaction, id.toDbt (), group.toDbt (), 0);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);
        //LOCK_OFF
        if (ret != 0)
        {
            THROW_EXCEPTION("finishGroup: fail to update group state.");
        }
    }
    catch (DbException& e)
    {
        qxtLog->error()<< log_entry_header << QString("Fail to finishGroup [%1]").arg(id);
        qxtLog->error()<< log_entry_header << QString("[%1]: %2").arg(e.get_errno()).arg(e.what());
        ret = -1;
    }
    catch (Exception& e)
    {
        qxtLog->error ()<< log_entry_header << e.getErrorMsg();
        ret = -1;
    }

    if (ret && pTransaction)
        pTransaction->abort();
    if (!ret && pTransaction)
        pTransaction->commit(0);

    delete pExtraInfo;

    if (ret == 0)
        emit groupFinished(id);
    return ret == 0;
}

QMutex get_first_avaliable_group__mutex;
GroupID BDBScanStorage::getFirstAvaliableId()
{
    Dbt sec_key;
    Dbt key;
    Dbt value;
    GroupID gId = GroupID::null;
    GroupHandler group;

    GroupState state = Valid;
    sec_key.set_data (&state);
    sec_key.set_size (sizeof(GroupState));
    sec_key.set_ulen (sizeof(GroupState));
    sec_key.set_flags (DB_DBT_USERMEM);

    key.set_flags (DB_DBT_MALLOC);
    value.set_flags (DB_DBT_MALLOC);

    get_first_avaliable_group__mutex.lock ();
    try
    {
        int ret;
        //LOCK_ON
        bool redo;
        do
        {
            redo = false;

            try
            {
                ret = catalog_state_index->pget (NULL, &sec_key, &key, &value, 0);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);
        //LOCK_OFF
        if (ret != 0)
        {
            free(key.get_data ());
            free(value.get_data());
            if (ret == DB_NOTFOUND)
                gId = GroupID::null;
            else
                THROW_EXCEPTION("fail to get group id/state");
        }
        else
        {
            if (!group.fromDbt (&value, true))
                THROW_EXCEPTION("can't build group from dbt");
            if (group.state != Valid)
                THROW_EXCEPTION(QString("not Valid group state. Group [%1] state: %2").arg(gId).arg(groupStateToString (group.state)));
            if(!gId.fromDbt (&key, true))
                THROW_EXCEPTION("can't build groupid from dbt");
        }
    }
    catch (DbException& e)
    {
        qxtLog->error ()<< log_entry_header << QString("getFirstAvaliableId: [%1] %2").arg (e.get_errno()).arg (e.what());
        gId = GroupID::null;
    }
    catch (Exception& e)
    {
        qxtLog->error ()<< log_entry_header << QString("getFirstAvaliableId: %1").arg (e.getErrorMsg());
        gId = GroupID::null;
    }

    get_first_avaliable_group__mutex.unlock ();
    return gId;
}

QMutex lock_group_mutex;
GroupID BDBScanStorage::lockGroup (GroupID id)
{
    int ret = 0;
    DbTxn* pTransaction = NULL;
    GroupHandler group;

    lock_group_mutex.lock ();
    if (!id.isValid ())
        id = getFirstAvaliableId();
    lock_group_mutex.unlock ();
    if (id.isValid ())
    {
        try
        {
            Dbt groupvalue;
            groupvalue.set_flags (DB_DBT_MALLOC);

            //LOCK_ON
            bool redo;
            do
            {
                redo = false;

                try
                {
                    ret = catalog->get (NULL, id.toDbt (), &groupvalue, 0);
                }
                catch (DbDeadlockException& e)
                {
                    qxtLog->trace() << "DeadLock detected: redo operation";
                    redo = true;
                }
            }while (redo);

            //LOCK_OFF
            if (ret != 0)
                THROW_EXCEPTION("lockGroup: can't get group Handle");

            if(!group.fromDbt(&groupvalue, true))
                THROW_EXCEPTION("can't build group from dbt");
            if (group.state != Valid)
                THROW_EXCEPTION(QString("Try to lock not valid group. Group [%2] state: %1").arg (groupStateToString (group.state)).arg (id));
            group.state = Locked;

            Dbc* cursor;
            Dbt  vaultvalue;
            vaultvalue.set_flags (DB_DBT_MALLOC);
            ret = vault_groupid_index->cursor (NULL, &cursor, 0);
            if (ret)
                THROW_EXCEPTION ("fail to get cursor");

            DbExtraInfo* pExtraInfo = new DbExtraInfo;
            pExtraInfo->pCursor = cursor;
            pExtraInfo->nextFetchOperation = DB_SET;
            pExtraInfo->pTransaction = pTransaction;
            group.pos = pExtraInfo;
            //LOCK_ON
            //bool redo;
            do
            {
                redo = false;

                try
                {
                    ret = catalog->put (pTransaction, id.toDbt (), group.toDbt (), 0);
                }
                catch (DbDeadlockException& e)
                {
                    qxtLog->trace() << "DeadLock detected: redo operation";
                    redo = true;
                }
            }while (redo);
            //LOCK_OFF
            if (ret)
                THROW_EXCEPTION("fail to update groupstate");
        }
        catch(DbException& e)
        {
            qxtLog->error ()<< log_entry_header << QString("lockGroup: [%1] %2").arg (e.get_errno()).arg (e.what());
            id = GroupID::null;
        }
        catch(Exception& e)
        {
            qxtLog->error ()<< log_entry_header << QString("lockGroup: %1").arg (e.getErrorMsg());
            id = GroupID::null;
        }
    }
    //lock_group_mutex.unlock ();

    if (id.isValid())
    {
        emit groupLocked(id);
    }
    return id;
 }

bool  BDBScanStorage::getNextGroupItem(GroupID   id,
                                         DataType& item)
{
    Dbt catalogvalue;
    Dbt vaultvalue;
    Dbc* cursor;
    AucData aucData;
    GroupHandler group;
    int ret;
    bool ok = true;
    catalogvalue.set_flags (DB_DBT_MALLOC);
    vaultvalue.set_flags (DB_DBT_MALLOC);

    try
    {
        //LOCK_ON
        bool redo;
        do
        {
            redo = false;

            try
            {
                ret = catalog->get(NULL, id.toDbt (), &catalogvalue, 0);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);

        //LOCK_OFF
        if (ret)
            THROW_EXCEPTION("getNextGroupItem: can't get group handler");
        if (!group.fromDbt (&catalogvalue, true))
            THROW_EXCEPTION("can't build group from dbt");
        if (group.state != Locked )
            THROW_EXCEPTION(QString("getNextGroupItem: Read from not locked group! Group [%2] state: %1").arg (groupStateToString (group.state)).arg (id));
        DbExtraInfo* pExtraInfo =  reinterpret_cast<DbExtraInfo*>(group.pos);
        if (!pExtraInfo)
            THROW_EXCEPTION("ExtraInfo NULL pointer")
        u_int32_t getFlags = pExtraInfo->nextFetchOperation;
        pExtraInfo->nextFetchOperation = DB_NEXT_DUP;
        cursor = pExtraInfo->pCursor;
        //LOCK_ON
                //bool redo;
                do
                {
                    redo = false;

                    try
                    {
                        ret = cursor->get(id.toDbt (), &vaultvalue, getFlags);
                    }
                    catch (DbDeadlockException& e)
                    {
                        qxtLog->trace() << "DeadLock detected: redo operation";
                        redo = true;
                    }
                }while (redo);

        //LOCK_OFF
        switch(ret)
        {
            case 0:
                if (!aucData.fromDbt (&vaultvalue, true))
                    THROW_EXCEPTION("can't build aucdata from dbt");
                item = aucData;
                qxtLog -> trace() << "got item:" << (QString)aucData;
                break;
            case DB_NOTFOUND:
                ok = false;
                break;
            default:
                THROW_EXCEPTION("Fail to get next group item!");
                break;
         }
    }
    catch(DbException& e)
    {
        qxtLog->error ()<< log_entry_header << QString("getNextGroupItem: [%1] %2").arg (e.get_errno()).arg (e.what());
        id = GroupID::null;
        ok = false;
    }
    catch(Exception& e)
    {
        qxtLog->error ()<< log_entry_header << QString("getNextGroupItem: %1").arg (e.getErrorMsg());
        id = GroupID::null;
        ok = false;
    }

    return ok;
}

bool  BDBScanStorage::releaseGroup(GroupID id)
{
     Dbt value;
     value.set_flags (DB_DBT_MALLOC);
     int ret;
     GroupHandler group;

    try
    {
        //LOCK_ON
        bool redo;
        do
        {
            redo = false;

            try
            {
                ret = catalog->get (NULL, id.toDbt(), &value, 0);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);
        //LOCK_OFF
        if (ret)
            THROW_EXCEPTION("releaseGroup: fail to get group handle");
        if (!group.fromDbt (&value, true))
            THROW_EXCEPTION("fail to construct from dbt");
        if (group.state != Locked)
            THROW_EXCEPTION(QString("releaseGroup: not locked group. Group [%2] state is %1").arg (groupStateToString (group.state)).arg (id));
        group.state = Commited;
        DbExtraInfo* pExtraInfo = reinterpret_cast<DbExtraInfo*>(group.pos);
        if (!pExtraInfo)
            THROW_EXCEPTION("ExtraInfo NULL pointer")
        Dbc* cursor = pExtraInfo->pCursor;
        cursor->close();
        delete pExtraInfo;
        group.pos = NULL;
        //LOCK_ON
        //bool redo;
        do
        {
            redo = false;

            try
            {
                ret = catalog->put (NULL, id.toDbt(), group.toDbt(), 0);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);
        //LOCK_OFF
        if (ret != 0)
            THROW_EXCEPTION("releaseGroup: can't update group handler");
        qxtLog->debug()<< log_entry_header << "send signal";
        pthread_cond_broadcast(&threadSignal);
     }
     catch(DbException& e)
     {
        qxtLog->error () << log_entry_header<< QString("releaseGroup: [%1] %2").arg (e.get_errno()).arg (e.what());
        ret = -1;
     }
     catch(Exception& e)
     {
        qxtLog->error ()<< log_entry_header << QString("releaseGroup: %1").arg (e.getErrorMsg());
        ret = -1;
     }

    if (ret == 0)
        emit groupReleased(id);
    return ret == 0;
}

 bool            BDBScanStorage::rollbackGroup(GroupID id)
{
    Dbt value;
    int ret;
    GroupHandler group;
    value.set_flags (DB_DBT_MALLOC);

    try
    {
        //LOCK_ON
        bool redo;
        do
        {
            redo = false;

            try
            {
                ret = catalog->get (NULL, id.toDbt(), &value, 0);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);
        //LOCK_OFF
        if (ret)
            THROW_EXCEPTION("releaseGroup: can't get group handler");
        if (!group.fromDbt(&value, true))
            THROW_EXCEPTION("Fail to construct group from Dbt");
        if (group.state != Locked)
            THROW_EXCEPTION(QString("releaseGroup: not Locked group. Group [%2] state: %1").arg(groupStateToString (group.state)).arg (id));
        DbExtraInfo* pExtraInfo = reinterpret_cast<DbExtraInfo*>(group.pos);
        if (!pExtraInfo)
            THROW_EXCEPTION("ExtraInfo NULL pointer")
        Dbc* cursor = pExtraInfo->pCursor;
        cursor->close();
        delete pExtraInfo;
        group.pos = NULL;
        group.rollbackcount++;
        if (group.rollbackcount >= 10)
        {
            group.state = Blocked;
            qxtLog->warning ()<< log_entry_header << "group #%1 marked as blocked";
        }
        else
            group.state = Valid;

        //LOCK_ON
        //bool redo;
        do
        {
            redo = false;

            try
            {
                ret = catalog->put (NULL, id.toDbt(), group.toDbt(), 0);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);
        //LOCK_OFF
        if (ret)
            THROW_EXCEPTION("releaseGroup: can't update group handler");
    }
    catch(DbException& e)
    {
       qxtLog->error () << log_entry_header<< QString("rollbackGroup: [%1] %2").arg (e.get_errno()).arg (e.what());
       ret = -1;
    }
    catch(Exception& e)
    {
       qxtLog->error () << log_entry_header<< QString("rollbackGroup: %1").arg (e.getErrorMsg());
       ret = -1;
    }
    if (ret == 0)
        emit groupRolledBack(id);
    return ret == 0;
}

void BDBScanStorage::clearCommited()
{
    Dbt key;
    Dbt primkey;
    Dbt value;
    Dbc* cursor;
    u_int32_t records_count;
    int ret;
    GroupID id;
    GroupHandler group;
    GroupState state;
    int purged = 0;

    key.set_data (&state);
    key.set_size (sizeof(GroupState));
    key.set_ulen (sizeof(GroupState));
    key.set_flags (DB_DBT_USERMEM);

    value.set_flags (DB_DBT_MALLOC);
    primkey.set_flags (DB_DBT_MALLOC);

    try
    {
        state = Commited;
        qxtLog->debug () << log_entry_header<< "Purge all commited groups (if any)";
        ret = catalog_state_index->cursor(NULL, &cursor, 0);
        if (ret)
            THROW_EXCEPTION("Fail to get cursor");
        //LOCK_ON
        bool redo;
        do
        {
            redo = false;

            try
            {
                ret = cursor->pget(&key, &primkey, &value, DB_SET);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);
        //LOCK_OFF
        if (ret && ret != DB_NOTFOUND)
        {
            THROW_EXCEPTION("fail to get record");
        }
        else if (ret != DB_NOTFOUND)
        {
            ret = cursor->count(&records_count, 0);
            if (ret)
                THROW_EXCEPTION("can't get commited records count");
            qxtLog->debug() << log_entry_header<< QString("%1 commited groups found. delete them").arg(records_count);
            do
            {
                if (!group.fromDbt(&value, true))
                    THROW_EXCEPTION("Fail to construct group from Dbt");
                if (!id.fromDbt(&primkey, true))
                    THROW_EXCEPTION("Fail to construct groupid from Dbt");
                qxtLog->trace() << log_entry_header << QString("deleting group #%1. ItemId: %2, Time: %3").arg(id).arg(group.meta.itemid).arg(group.meta.scantime.toString());
                //LOCK_ON
                bool redo;
                do
                {
                    redo = false;

                    try
                    {
                        ret = cursor->del(0);
                    }
                    catch (DbDeadlockException& e)
                    {
                        qxtLog->trace() << "DeadLock detected: redo operation";
                        redo = true;
                    }
                }while (redo);
                //LOCK_OFF
                if (ret)
                    THROW_EXCEPTION("Fail to delete record");
                qxtLog->trace () << log_entry_header << QString("group deleted successfully");
                purged ++;
                //LOCK_ON
                //bool redo;
                do
                {
                    redo = false;

                    try
                    {
                        ret = cursor->pget(&key, &primkey, &value, DB_NEXT_DUP);
                    }
                    catch (DbDeadlockException& e)
                    {
                        qxtLog->trace() << "DeadLock detected: redo operation";
                        redo = true;
                    }
                }while (redo);
                //LOCK_OFF
                if (ret && ret != DB_NOTFOUND)
                    THROW_EXCEPTION("Fail to get record");
            } while (ret == 0);
            qxtLog->debug()<< log_entry_header << QString("Done. %1 group(s) purged.").arg(purged);
        }
        ret = cursor->close();
        if (ret)
            THROW_EXCEPTION("Fail to close cursor");
    }
    catch(DbException& e)
    {
       qxtLog->error ()<< log_entry_header << QString("clearCommited: [%1] %2").arg (e.get_errno()).arg (e.what());
    }
    catch(Exception& e)
    {
       qxtLog->error ()<< log_entry_header << QString("clearCommited: %1").arg (e.getErrorMsg());
    }
}

void * BDBScanStorage::cleanThreadFunc(void * arg)
{
    BDBScanStorage* pStorage = reinterpret_cast<BDBScanStorage*>(arg);
    while (!pStorage->stopThread)
    {
        qxtLog->trace()<< log_entry_header << "cleanThreadFunc: waiting signal";
        pthread_cond_wait(&pStorage->threadSignal, &pStorage->waitMutex);
        qxtLog->trace()<< log_entry_header << "cleanThreadFunc: signal recieved";
        if (pStorage->stopThread)
        {
            break;
        }
        qxtLog->trace()<< log_entry_header << "cleanThreadFunc: start clear";

        pStorage->clearCommited();

        qxtLog->trace()<< log_entry_header << "cleanThreadFunc: clear finished";

        pthread_mutex_unlock(&pStorage->waitMutex);
    }
    qxtLog->info()<< log_entry_header << "cleanThreadFunc: quit";
    pthread_exit(NULL);
    return NULL;
}

void * BDBScanStorage::lockDetectFunc (void* arg)
{
    BDBScanStorage* pStorage = reinterpret_cast<BDBScanStorage*>(arg);
    timespec req;
    timespec rem;
    req.tv_sec = 3;
    req.tv_nsec = 0;
    qxtLog->trace () << log_entry_header << QString("lock detect thread startted");
    while (!pStorage->stopThread)
    {
        int rejected = 0;
        pStorage->env->lock_detect (0, DB_LOCK_DEFAULT, &rejected);
        if (rejected > 0)
        {
            qxtLog->info() << log_entry_header << QString("%1 locks detected and removed").arg(rejected);
        }
        nanosleep(&req, &rem);
    }
    qxtLog->trace () << log_entry_header << QString("lock detect thread finished");
    pthread_exit (NULL);
    return NULL;
}

int BDBScanStorage::size()
{
    return getCount("Valid");
}

int BDBScanStorage::getCount(QString state)
{
    GroupState st = stringToGtoupState(state);
    Dbt key;
    Dbt value;
    Dbc* cursor = NULL;
    u_int32_t records_count = 0;
    int ret;

    key.set_data (&st);
    key.set_size (sizeof(GroupState));
    key.set_ulen (sizeof(GroupState));
    key.set_flags (DB_DBT_USERMEM);

    value.set_flags (DB_DBT_MALLOC);

    ret = catalog_state_index->cursor(NULL, &cursor, 0);
    //LOCK_ON
    bool redo;
    do
    {
        redo = false;

        try
        {
            ret = cursor->get(&key, &value, DB_SET);
        }
        catch (DbDeadlockException& e)
        {
            qxtLog->trace() << "DeadLock detected: redo operation";
            redo = true;
        }
    }while (redo);
    //LOCK_OFF
    if (ret == 0)
    {
        free (value.get_data());
        cursor->count(&records_count, 0);
    }
    cursor->close();

    return ret == 0 ? (int)records_count:-1;
}

bool BDBScanStorage::corruptGroup(GroupID id)
{
    Dbt value;
    DbTxn* pTransaction = NULL;
    DbExtraInfo* pExtraInfo = NULL;
    GroupHandler group;
    int   ret;

    value.set_flags (DB_DBT_MALLOC);
    try
    {
        //LOCK_ON
        bool redo;
        do
        {
            redo = false;

            try
            {
                ret = catalog->get (pTransaction, id.toDbt (), &value, 0);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);

        //LOCK_OFF
        if (ret != 0)
            THROW_EXCEPTION("Can't get group status");
        if (!group.fromDbt (&value, true))
            THROW_EXCEPTION("Cant construct group from Dbt");
        pExtraInfo = reinterpret_cast<DbExtraInfo*>(group.pos);
        pTransaction = pExtraInfo->pTransaction;
        if (group.state != Invalid)
            THROW_EXCEPTION(QString("Not Invalid group. Group [%1] state: %2").arg (id).arg (groupStateToString (group.state)));
        group.state = Corrupted;
        group.pos = NULL;

        //LOCK_ON
        //bool redo;
        do
        {
            redo = false;

            try
            {
                ret = catalog->put (pTransaction, id.toDbt (), group.toDbt (), 0);
            }
            catch (DbDeadlockException& e)
            {
                qxtLog->trace() << "DeadLock detected: redo operation";
                redo = true;
            }
        }while (redo);

        //LOCK_OFF
        if (ret)
            THROW_EXCEPTION("Fail to update group state.");
    }
    catch (DbException& e)
    {
        qxtLog->error()<< log_entry_header << QString("corruptGroup: Fail to finishGroup [%1]").arg(id);
        qxtLog->error()<< log_entry_header << QString("corruptGroup: [%1] %2").arg(e.get_errno()).arg(e.what());
        ret = -1;
    }
    catch (Exception& e)
    {
        qxtLog->error()<< log_entry_header << QString("corruptGroup: %2").arg(e.getErrorMsg());
        ret = -1;
    }

    if (ret && pTransaction)
        pTransaction->abort();
    if (!ret && pTransaction)
        pTransaction->commit(0);

    delete pExtraInfo;

    if (ret == 0)
        emit groupCorrupted(id);
    return ret == 0;
}

/*
void BDBScanStorage::aquireLock ()
{
    qxtLog->trace(QString("ThreadID: %1. Try to lock").arg((long)pthread_self ()));
    int lock_res = pthread_mutex_trylock (&writeDBMutex);
    if (lock_res)
    {
        qxtLog->trace (QString("ThreadID: %1. Can't aquire lock: already locked").arg((long)pthread_self ()));
        pthread_mutex_lock(&writeDBMutex);
    }
    qxtLog->trace (QString("ThreadID: %1. Lock aquired").arg((long)pthread_self ()));
}

void BDBScanStorage::releaseLock ()
{
    pthread_mutex_unlock(&writeDBMutex);
    qxtLog->trace(QString("ThreadID: %1. Lock released").arg((long)pthread_self ()));
}
*/

void BDBScanStorage::setDataFolder (const QString &str)
{
    datafolder = str;
}

QString BDBScanStorage::getDataFolder ()
{
    return datafolder;
}

#ifdef IS_PLUGIN
#include <QtPlugin>

Q_EXPORT_PLUGIN2(bdbscanstorageplugin, BDBScanStorage)
#endif
