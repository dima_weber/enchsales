#ifndef BDBSCANSTORAGE_H
#define BDBSCANSTORAGE_H

#include "scanstorageinterface.hxx"
#include <pthread.h>

class DbEnv;
class Db;

class BDBScanStorage :  //public QObject,
                        public ScanStorageInterface

{
    Q_OBJECT
    Q_PROPERTY (QString datafolder READ getDataFolder WRITE setDataFolder)
    Q_INTERFACES(ScanStorageInterface)
            DbEnv*          env;
            Db*             catalog;
            Db*             catalog_state_index;
            Db*             vault_groupid_index;
            Db*             vault;

            pthread_t       threadHandler;
            pthread_t       lockDetectThreadHandler;
            pthread_cond_t  threadSignal;
            pthread_mutex_t waitMutex;
            pthread_mutex_t writeDBMutex;
            bool            stopThread;
          QString           datafolder;
     static void*           cleanThreadFunc(void*);
     static void*           lockDetectFunc (void*);
            //void            aquireLock();
            //void            releaseLock();
            void            purgeGroup(GroupState state);
            void            onOpen();
            void            printState();
    virtual GroupID         getFirstAvaliableId();          /// return first avaliable valid group id
    virtual    void         clearCommited();
            QString         getDataFolder();
               void         setDataFolder(const QString& str);
public:
                            BDBScanStorage(QString _datafolder=QString::null, QObject* parent = NULL);
    virtual                 ~BDBScanStorage();
    virtual bool            initialize ();
    virtual GroupID         startGroup(MetaType metadata); /// createNew group, mark it as invalid
                                                           /// @return group id
    virtual bool            addItemToGroup(GroupID  id,    /// add item to invalid group
                                           DataType data); /// @return false for closed group or if add failed.
    virtual bool            finishGroup(GroupID id);       ///
    virtual bool            corruptGroup(GroupID id);       ///
    virtual GroupID         lockGroup (GroupID = 0);        /// 0 - first avaliable
    virtual bool            getNextGroupItem(GroupID   id,
                                             DataType& item);/// Alternative style: iteration
    virtual bool            releaseGroup(GroupID );         /// mark group as commited,
                                                            /// group can be deleted at anytime;
    virtual bool            rollbackGroup(GroupID);
    virtual  int            size();
public slots:
    virtual int             getCount(QString state);
};

#endif // BDBSCANSTORAGE_H
