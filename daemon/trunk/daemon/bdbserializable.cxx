#include "bdbserializable.hxx"
#include <stdlib.h>
#include <stdio.h>
#include <QDataStream>
#include <QxtLogger>

BDBSerializableException::BDBSerializableException(ErrCodes code, const char* file, int line, const char* msg)
    : Exception(file, line, msg)
{
    errCode = code;
    if (msg == NULL)
        switch(code)
        {
            case NULLPTR:   error_msg += "NULL pointer destination"; break;
            case DESTSMALL: error_msg += "Buffer too short"; break;
            case SIGFAIL:   error_msg += "Signature check fail"; break;
            case VERFAIL:   error_msg += "Version check fail"; break;
            case UNINIT:    error_msg += "Uninitialized instance"; break;
            case MEMFAIL:   error_msg += "Memory (re)allocation fail"; break;
            case BROKEN:    error_msg += "Wrong data format";
        }
}

BDBSerializableException::ErrCodes BDBSerializableException::getErrCode()
{
    return errCode;
}

void* BDBSerializable::toMem(void *p_mem, size_t &sz)
{
    QByteArray buffer;
    QDataStream stream(&buffer, QIODevice::WriteOnly);
    stream.writeRawData(getSig(), BDBSerializable::SIG_LENGTH);
    stream.writeRawData(getVer(), BDBSerializable::VER_LENGTH);

    pack(stream);

    size_t real_size = buffer.size ();
    if (!p_mem)
        p_mem = new char[real_size];
    memcpy (p_mem, buffer.data (), real_size);
    if (sz != real_size && sz > 0)
        qxtLog->warning() << "data size changed!";
    sz = real_size;

    return p_mem;
}

bool BDBSerializable::fromMem(void *p_mem, size_t sz)
{
    if (!p_mem)
    {
        THROW_BDBEXCEPTION( BDBSerializableException::NULLPTR);
        return false;
    }
    if (sz < BDBSerializable::SIG_LENGTH + BDBSerializable::VER_LENGTH)
    {
        THROW_BDBEXCEPTION(BDBSerializableException::DESTSMALL);
        return false;
    }

    QByteArray buffer((char*)p_mem, sz);
    QDataStream stream(buffer);
    char sig[BDBSerializable::SIG_LENGTH];
    char ver[BDBSerializable::VER_LENGTH];
    size_t read = 0;

    read = stream.readRawData(sig, BDBSerializable::SIG_LENGTH);
    if (read != BDBSerializable::SIG_LENGTH
            || memcmp (sig, getSig(), BDBSerializable::SIG_LENGTH) != 0)
    {
        THROW_BDBEXCEPTION( BDBSerializableException::SIGFAIL);
        return false;
    }

    read = stream.readRawData(ver, BDBSerializable::VER_LENGTH);
    if ( read != BDBSerializable::SIG_LENGTH
            || memcmp (ver, getVer(), BDBSerializable::VER_LENGTH) != 0)
    {
        THROW_BDBEXCEPTION( BDBSerializableException::VERFAIL);
        return false;
    }

    unpack(stream);

    if (stream.status() != QDataStream::Ok)
    {
        THROW_BDBEXCEPTION( BDBSerializableException::BROKEN);
        return false;
    }

    return true;
}

Dbt* BDBSerializable::toDbt ()
{
    if (pDbt)
        delete [] reinterpret_cast<char*>(pDbt->get_data ());
    delete pDbt;
    pDbt = new Dbt;
    char* buff = NULL;
    size_t size = 0;
    buff = reinterpret_cast<char*>(toMem (buff, size));
    pDbt->set_data (buff);
    pDbt->set_size (size);
    pDbt->set_ulen (size);
    pDbt->set_flags (DB_DBT_USERMEM);

    return pDbt;
}

bool BDBSerializable::fromDbt (Dbt* dbt, bool clearDbtData)
{
    bool ok = false;
    if (dbt)
    {
        ok = fromMem (dbt->get_data (), dbt->get_size ());
        if (clearDbtData
                && (dbt->get_flags () & (DB_DBT_MALLOC | DB_DBT_REALLOC)))
            free (dbt->get_data ());
    }
    return ok;
}

BDBSerializable::BDBSerializable()
    :pDbt(NULL)
{
}

BDBSerializable::BDBSerializable(const BDBSerializable &)
    :pDbt(NULL)
{
}

BDBSerializable::~BDBSerializable()
{
    if (pDbt)
        delete [] reinterpret_cast<char*>(pDbt->get_data ());
    delete pDbt;
}

BDBSerializable& BDBSerializable::operator =(const BDBSerializable& )
{
    if (pDbt)
        delete [] reinterpret_cast<char*>(pDbt->get_data ());
    delete pDbt;
    pDbt = NULL;
    return *this;
}
