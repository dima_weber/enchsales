#ifndef BDBSERIALIZABLE_HPP
#define BDBSERIALIZABLE_HPP

#include <db_cxx.h>
#include "exception.hxx"
class BDBSerializableException : public Exception
{
public:
    enum ErrCodes{NULLPTR = 1, DESTSMALL, SIGFAIL, VERFAIL, UNINIT, MEMFAIL, BROKEN};
    BDBSerializableException(ErrCodes code, const char* file, int line, const char* msg = NULL);
    ErrCodes getErrCode();
protected:
    ErrCodes errCode;
};

#define THROW_BDBEXCEPTION(code) throw BDBSerializableException(code, __FILE__,__LINE__, NULL);

class BDBSerializable
{
public:
    BDBSerializable();
    BDBSerializable(const BDBSerializable& copy);
    virtual ~BDBSerializable();
    virtual  BDBSerializable& operator=(const BDBSerializable& copy);

    virtual void*  toMem (void* p_mem, size_t& sz);
    virtual bool fromMem (void* p_mem, size_t sz);

            Dbt*   toDbt();
            bool fromDbt(Dbt*, bool clearDbtData = false);
private:
            Dbt*   pDbt;
protected:
    virtual const char* getSig() = 0;
    virtual const char* getVer() = 0;
    virtual void   pack(QDataStream& stream) =0;
    virtual void unpack(QDataStream& stream) =0;

static const size_t SIG_LENGTH = 2;
static const size_t VER_LENGTH = 2;
};
#endif // BDBSERIALIZABLE_HPP
