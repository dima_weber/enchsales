#include "bnetconnection.hxx"
#include "exception.hxx"

#include <QxtLogger>
#include <QxtSignalWaiter>
#include <QxtJSON>

#include <QByteArray>
#include <QUrl>
#include <QFile>

BNetConnection::BNetConnection(QObject *parent) :
    QObject(parent), config ()
{
    loggedAucAs.reset ();
    connection_state = Disconnected;
#if QT_VERSION >= 0x040700
    connect (&connection_manager, SIGNAL(networkAccessibleChanged( QNetworkAccessManager::NetworkAccessibility)),
             SLOT(onAccessibleChange( QNetworkAccessManager::NetworkAccessibility)));
#endif

#ifndef QT_NO_OPENSSL
    connect (&connection_manager, SIGNAL (sslErrors (QNetworkReply *, const QList < QSslError > &)),
             SLOT (slotSslErrors (QNetworkReply *, const QList < QSslError > &)));
#endif
    connect (this, SIGNAL(disconnectedBNet()), SLOT(connectBNet()));
}

BNetConnection::~BNetConnection()
{
}

AucLogin BNetConnection::aucLogin()
{
    return loggedAucAs;
}

bool        BNetConnection::isConnectedBNet()
{
    return connection_state == Connected;
}

QByteArray BNetConnection::getPage(QString surl)
{
    QUrl url(surl);
    return getPage (url);
}

QByteArray  BNetConnection::getPage(QUrl url)
{
    QNetworkRequest *req = new QNetworkRequest (url);

    QNetworkReply *reply = connection_manager.get (*req);
        connect (reply, SIGNAL (error (QNetworkReply::NetworkError)),
              this, SLOT (onError (QNetworkReply::NetworkError)));
        /*
        connect (reply, SIGNAL(readyRead()),
                    SLOT(readReplyData(QNetworkReply*)));
        */
    if (reply->error() != QNetworkReply::NoError)
    {
        sleep(3);
        qxtLog->error() << reply->errorString();
    }

    bool timedout = false;
    QByteArray buff;
    if (reply->isRunning())
    {
        qxtLog->debug() << "wait for page retrieve";
        timedout = !QxtSignalWaiter::wait (&connection_manager, SIGNAL (finished (QNetworkReply*)),20000);
    }
    if (timedout)
    {
        qxtLog->error() << tr("getPage timed out");
        THROW_EXCEPTION(tr("BNetConnection::getPage timed out"));
    }
    else
    {
        buff = readReplyData (reply);
    }
    delete reply;
    delete req;
    return buff;
}


bool  BNetConnection::connectBNet()
{
    connection_state = Connecting;
    qxtLog->info (tr("connecting to BNet: start"));
    QNetworkRequest *req = new QNetworkRequest (QUrl ("https://eu.battle.net/login/en/login.xml?ref=http://eu.wowarmory.com/auctionhouse/index.xml&app=armory&cr=true#search"));
    req->setHeader (QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    QByteArray content =
        QString ("accountName=%1&password=%2").arg (config["BNet/BNetLogin"].toString ()).
        arg (config["BNet/BNetPassword"].toString ()).toAscii ();

    QNetworkReply *reply = connection_manager.post (*req, content);

    connect (reply, SIGNAL (error   (QNetworkReply::NetworkError)),
             this,  SLOT   (onError (QNetworkReply::NetworkError)));

    if (QxtSignalWaiter::wait (reply, SIGNAL (finished ()),5 * 60 * 1000))
    {
        QString url_s = "http://eu.wowarmory.com/auctionhouse/search.json?&sk=";

        foreach (QByteArray a, reply->rawHeaderList ())
        {
            qxtLog->trace () << QString ("%1: %2").arg (a.data ()).arg (reply->rawHeader (a).
                                        data ());
            if (a == "Location")
            url_s = reply->rawHeader (a);
        }

        QUrl url (url_s);
        if (reply->error () != QNetworkReply::NoError)
        {
            qxtLog->error (reply->errorString ());
            qxtLog->info(tr("Sleep for 3 seconds"));
            sleep(3);
        }
        else if (url.path ().contains ("maintenance.html"))
        {
            qxtLog->info () << tr("Server maintenance. Wait a bit.");
        }
        else
        {
            readReplyData (reply);
            QNetworkRequest req (url);
            QNetworkReply *reply = connection_manager.get (req);
            connect (reply, SIGNAL (error (QNetworkReply::NetworkError)),
                 this,  SLOT (onError (QNetworkReply::NetworkError)));
            if (QxtSignalWaiter::wait (reply, SIGNAL (finished ()),1000 * 60))
            {
                if (reply->error () != QNetworkReply::NoError)
                {
                    qxtLog->error (reply->errorString ());
                    qxtLog->info(tr("Sleep for 3 seconds again"));
                }
                else
                {
                   qxtLog->info (tr("connecting to BNet: done"));
                   connection_state = Connected;
                }
            }
            delete reply;
        }

    }
    if (connection_state == Connected)
        emit connectedBNet ();
    else
        connection_state = Disconnected;

    delete reply;
    delete req;

    return connection_state == Connected;
}

bool        BNetConnection::logAuc(AucLogin as)
{
    bool logged = true;
    if (connection_state != Connected)
        if (!connectBNet ())
            return false;

    if (   as != loggedAucAs)
    {
        loggedAucAs =  as;
        qxtLog->info () << QString (tr("Logging character %1, server %2"))
                            .arg (as.charname)
                            .arg (as.srv);
        QUrl url ("http://eu.wowarmory.com/vault/character-select-submit.json");
        QNetworkRequest *request = new QNetworkRequest (url);
        QByteArray content = QString ("cn=%1&r=%2")
                                .arg (as.charname)
                                .arg (as.srv)
                                .toUtf8 ();
        request->setHeader (QNetworkRequest::ContentTypeHeader,
                            "application/x-www-form-urlencoded");

        QNetworkReply *reply = connection_manager.post (*request, content);
        connect (reply, SIGNAL (error (QNetworkReply::NetworkError)),
                 this, SLOT (onError (QNetworkReply::NetworkError)));

    if (!QxtSignalWaiter::wait (reply, SIGNAL (finished ()), 10000)) // finished
    {
        delete request;
        delete reply;
        loggedAucAs.reset ();
        THROW_EXCEPTION(tr("BNetConnection::logAuc timed out"));
    }

    QByteArray buf = reply->readAll ();
    qxtLog->trace () << buf;
    QVariantMap log_result = QxtJSON::parse (buf).toMap ();
    if (log_result.contains("success") )
    {
        if(log_result["success"].toBool ())
        {
        qxtLog->info () << tr("character logged");
        logged = true;
        }
        else
        {
        loggedAucAs.reset ();
        qxtLog->error () << tr("Unable to log character. See trace for details");
        logged = false;
        }
    }
    else if (buf.isEmpty())
    {
        qxtLog->warning() << tr("empty result. Dunno if we logged. let's hope we did");
        logged = true;
    }
    else
    {
        loggedAucAs.reset ();
        qxtLog->error() << tr("Error while trying to log character.");
        logged = false;
    }
    delete reply;
    delete request;
    }
    emit loggedAuc (loggedAucAs);
    return logged;
}

void        BNetConnection::onError(QNetworkReply::NetworkError)
{
    QNetworkReply *reply = dynamic_cast < QNetworkReply * >(sender ());
    qxtLog->error (reply->errorString ());
    qxtLog->info(tr("Sleep for another 3 seconds"));
    sleep(3);
}

void BNetConnection::slotSslErrors (QNetworkReply * r, const QList < QSslError > &list)
{
#ifndef QT_NO_OPENSSL
    foreach (QSslError er, list)
    {
        qxtLog->error (er.errorString ());
    }
    r->ignoreSslErrors ();
#endif
}

QByteArray  BNetConnection::readReplyData (QNetworkReply * reply)
{
    qxtLog->debug () << QString (tr("%1  byte(s) read")).arg (reply->bytesAvailable ());
    QByteArray buf = reply->readAll ();
    QFile file ("out.html");
    file.open (QFile::WriteOnly);
    file.write (buf);
    file.close ();
    qxtLog->trace () << buf;
    return buf;
}

#if QT_VERSION >= 0x040700
void BNetConnection::onAccessibleChange (QNetworkAccessManager::NetworkAccessibility accessible)
{
    if (accessible == QNetworkAccessManager::Accessible)
    {
        qxtLog->info()  << tr("Network is back now. Reconnecting");
        connectBNet ();
    }
    else
    {
        qxtLog->error()  << tr("Network is down");
        connection_state = Disconnected;
        emit disconnectedBNet ();
    }
}
#endif

bool BNetConnection::postPage (QUrl url, QByteArray& content)
{
    QNetworkRequest *request = new QNetworkRequest (url);
    request->setHeader (QNetworkRequest::ContentTypeHeader,
                        "application/x-www-form-urlencoded");

    QNetworkReply *reply = connection_manager.post (*request, content);
    connect (reply, SIGNAL (error   (QNetworkReply::NetworkError)),
             this,  SLOT   (onError (QNetworkReply::NetworkError)));

    if (!QxtSignalWaiter::wait (reply, SIGNAL (finished ()), 10000)) // finished
    {
        content = reply->readAll ();
        return true;
    }
    else
    {
        qxtLog->error () << tr("Request timed out");
        return false;
    }
    delete reply;
    delete request;
}

void BNetConnection::disconnectDetected ()
{
    connection_state = Disconnected;
    emit disconnectedBNet ();
}
