#ifndef BNETCONNECTION_H
#define BNETCONNECTION_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QSslError>
#include <QSslConfiguration>
#include <QList>

#include "datatypes.hxx"
#include "settings.hxx"

class BNetConnection : public QObject
{
    Q_OBJECT
    AucLogin loggedAucAs;
    QNetworkAccessManager connection_manager;
    enum ConnectionState {Disconnected, Connecting, Connected};
    ConnectionState connection_state;
    ConfigClient     config;
public:
    explicit    BNetConnection(QObject *parent = 0);
    ~BNetConnection();
    AucLogin    aucLogin();
    bool        isConnectedBNet();

    QByteArray  getPage(QUrl);
    QByteArray  getPage(QString);
private:
    bool postPage(QUrl, QByteArray&);
signals:
    void        connectedBNet();
    void        disconnectedBNet();
    void        loggedAuc(AucLogin);

public slots:
    bool        connectBNet();
    bool        logAuc(AucLogin);
    void        disconnectDetected();
private slots:
    void        onError(QNetworkReply::NetworkError);
    void        slotSslErrors (QNetworkReply * r, const QList < QSslError > &list);
    QByteArray  readReplyData (QNetworkReply * reply);
#if QT_VERSION >= 0x040700
    void        onAccessibleChange( QNetworkAccessManager::NetworkAccessibility);
#endif
};

#endif // BNETCONNECTION_H
