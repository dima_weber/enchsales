#include "core.hxx"
#include "emaillogengine.hxx"
#include "filelog.hxx"
#include "exception.hxx"
#include "qtscanstorage.hxx"
#include "storagelistener.hxx"
#include "updateiteminfothread.hxx"
#include "manager.hxx"
#ifdef Q_OS_LINUX
#   include "signalhandler.hxx"
#endif
#ifdef USE_BDB
#   include "bdbscanstorage.hxx"
#endif
#ifdef USE_DBUS
#   include <QDBusConnection>
#   include "dbus/coreadaptor.h"
#   include "dbus/coreproxy.h"
#   include "dbus/managerproxy.h"
#   include <QDBusServiceWatcher>
#endif
#include <QTranslator>

#include <QxtBasicFileLoggerEngine>
#include <QxtLoggerEngine>
#include <QxtSignalWaiter>
#include <QxtJSON>

#include <QSharedMemory>
#include <QBuffer>
#include <QDataStream>
#include <QDebug>

#ifdef USE_DBUS
#   define SERVICE_NAME "net.softwarium.weber.EnchSaleDaemon"
#endif


ScanStorageInterface* scan_map = NULL;

Core::Core(int & argc, char ** argv) :
    QCoreApplication(argc, argv), config(), storagePluginLoader(this)
{
    loglevel = QxtLogger::DebugLevel;
    startLogging ();
    setupTranslate ();
    setupUnixSignals ();

    connect(this, SIGNAL(aboutToQuit()), SLOT(onFinalCleanUp()));

    QVariantMap map;

    isTakeOver = false;
    shmem_key = "EnchSaleSharedData";
    p_shmem = new QSharedMemory(shmem_key,this);

#ifdef  USE_DBUS
    QDBusConnection connection = QDBusConnection::sessionBus ();
    bool ret = connection.registerService (SERVICE_NAME);
    if (!ret)
    {
        if (connection.lastError().type() != QDBusError::NoError)
        {
            qxtLog->warning() << QString("%1[%2]: %3").arg(connection.lastError().name())
                                                      .arg(connection.lastError().type())
                                                      .arg(connection.lastError().message());
        }
        else if (config["takeover"].toBool())
        {
            qxtLog->info (tr("Daemon is already running: take over."));
            CoreProxy coreproxy(SERVICE_NAME, "/Core",  QDBusConnection::sessionBus (), NULL);
            ManagerProxy managerproxy(SERVICE_NAME, "/Manager",  QDBusConnection::sessionBus (), NULL);
            qxtLog->debug() << tr("NewOne: pause him");
            if (managerproxy.isScanning())
            {
                managerproxy.pauseScan();
                if (!QxtSignalWaiter::wait(&managerproxy, SIGNAL(scanStopped() ), 60 * 1000 ) )
                {
                    qxtLog->debug() << tr("NewOne: crap... he's ignore me... exit now");
                    THROW_EXCEPTION("Core::Core timeouted");
                }
            }
            qxtLog->debug() << tr("NewOne: ok he is paused now");
            QString shmemkey = coreproxy.prepareShMem();
            qxtLog->debug() << tr("NewOne: we've got secret key: ") << shmemkey;
            QVariant v = readShMem(shmemkey);
            if (v.canConvert<QVariantMap>() && !v.toMap().isEmpty())
            {
                qxtLog->debug() << tr("NewOne: and all his bases belong to us!");
                map = v.toMap();
                isTakeOver = true;
            }
            else
            {
                qxtLog->info (tr("NewOne: Fail to retrieve data: exit now."));
                managerproxy.resumeScan();
                THROW_EXCEPTION(tr("Core::Core broken data recieved"));
            }
            qxtLog->debug() << tr("NewOne: shut him down");
            coreproxy.shutdown();
            QDBusServiceWatcher p_watcher(SERVICE_NAME, QDBusConnection::sessionBus(), QDBusServiceWatcher::WatchForOwnerChange, this);
            if (!QxtSignalWaiter::wait(&p_watcher, SIGNAL(serviceUnregistered(QString)), 60 * 1000))
            {
                qxtLog->debug() << tr("NewOne: crap... he's ignore me... exit now");
                THROW_EXCEPTION(tr("Core::Core timeouted"));
            }
            qxtLog->debug() << tr("NewOne: his dead now!");
            bool ret = connection.registerService (SERVICE_NAME);
            if (!ret)
            {
                qxtLog->info (tr("Fail to acquire dbus: exit now."));
                THROW_EXCEPTION(QString("Core::Core: ") + connection.lastError().message());
            }
            qxtLog->debug() << tr("I'm the One!");
        }
        else
        {
            qxtLog->info (tr("Daemon is already running and no --takeover : exit now."));
            THROW_EXCEPTION(QString("Core::Core: ") + connection.lastError().message());
        }
    }

    new CoreAdaptor (this);
    connection.registerObject ("/Core", this);
#endif
#ifdef USE_PLUGINS
    QDir pluginsDir = QDir(qApp->applicationDirPath());
    pluginsDir.cd("plugins");
    QString pluginName =
#   ifdef USE_BDB
        "libbdbscanstorageplugin.so";
#   else
        "libqtscanstorageplugin.so";
#   endif
    qxtLog->debug () << QString("trying to load plugin: %1").arg (pluginsDir.absoluteFilePath (pluginName));
    //if (pluginsDir.exists(pluginName))
    {
        storagePluginLoader.setFileName(pluginsDir.absoluteFilePath (pluginName));
        if (storagePluginLoader.load())
        {
            QObject* storagePlugin = storagePluginLoader.instance();
            if (storagePlugin)
            {
                ScanStorageInterface* pStorage = qobject_cast<ScanStorageInterface*>(storagePlugin);
                if (pStorage)
                {
                    scan_map = pStorage;
                    qxtLog->debug() << QString("Using scan storage from plugin: %1")
                                .arg (pluginsDir.absoluteFilePath (pluginName));
                }
                else
                {
                    qxtLog->debug() << tr("Fail to cast.");
                }
            }
            else
            {
                qxtLog->debug () << tr("Fail to instancenate");
            }
        }
        else
        {
            qxtLog->debug () << tr("Fail to load plugin")
                             << storagePluginLoader.errorString ();
        }
    }

    //else
    //{
    //    qDebug () << tr("Not library");
    //}

#endif
    if (!scan_map)
    {
        qDebug() << "using compiled-in scan storage";
#   ifdef USE_BDB
       scan_map = new BDBScanStorage(config["datadir"].toString (), this);
#   else
       scan_map = new QtScanStorage(this);
#   endif
    }
    scan_map->setProperty ("datafolder", config["datadir"]);
    scan_map->initialize ();
    new StorageListener(scan_map, this);
    p_update_icon_thread = new UpdateItemInfoThread(this);

    p_man = new Manager(this);
    connect (p_man, SIGNAL(finished()), SLOT(onManagerStopped()));

    updThread = new UpdateDBThread(this);

    if (isTakeOver)
        p_man->setWatchedItems(map);
}

Core::~Core()
{
    p_shmem->detach();
    /*
    delete p_shmem ;
    delete p_man;
    delete p_update_icon_thread;
    */
#ifndef USE_PLUGINS
    //delete scan_map;
    //scan_map = NULL;
#else
    if (storagePluginLoader.isLoaded())
        storagePluginLoader.unload();
#endif
#ifdef USE_DBUS
    QDBusConnection connection = QDBusConnection::sessionBus ();
    connection.unregisterObject ("/Core");
    connection.unregisterService (SERVICE_NAME);
#endif
    stopLogging ();
}

QString Core::logLevel()
{
    return QxtLogger::logLevelToString(loglevel);
}

void Core::setLogLevel(QString level)
{
    loglevel = QxtLogger::stringToLogLevel(level);
    qxtLog->setMinimumLevel("file", loglevel);
    qxtLog->setMinimumLevel("DEFAULT", loglevel);
}

void Core::startLogging ()
{
    QString logfilepath = config["logfile"].toString ();
    if (logfilepath.isEmpty ())
        logfilepath = QDir(QCoreApplication::applicationDirPath ()).absoluteFilePath ("daemon.log");
    if (config["trunclogfile"].toBool ())
        truncateLogFile(logfilepath);
    QxtBasicFileLoggerEngine *eng = new FileLog ( logfilepath );
    qxtLog->addLoggerEngine (QString ("file"), eng);
    qxtLog->enableLoggerEngine (QString ("file"));

    if (!config["consolelog"].toBool ())
        qxtLog->disableLoggerEngine (QString ("DEFAULT"));

    setLogLevel(config["loglevel"].toString ());

    if (config["mail/use_mail_log"].toBool ())
    {
        QxtLoggerEngine *email_eng = new QwbrEmailLogEngine ();
        qxtLog->addLoggerEngine (QString ("email"), email_eng);
        qxtLog->initLoggerEngine ("email");
        qxtLog->enableLoggerEngine (QString ("email"));
        qxtLog->setMinimumLevel ("email", QxtLogger::ErrorLevel);
    }

    qxtLog->info() << "\n"
                   << QString("---- %1 ----").arg(QDateTime::currentDateTime().toString());

    qxtLog->installAsMessageHandler ();
}

void Core::stopLogging ()
{
    //qDebug() << "stopLogging start";
    foreach(QString logEngineName, qxtLog->allLoggerEngines ())
    {
        if (logEngineName != "DEFAULT")
        {
    //      QxtLoggerEngine* pEngine = qxtLog->engine (logEngineName);
            qxtLog->disableLoggerEngine (logEngineName);
            qxtLog->removeLoggerEngine (logEngineName);
            qxtLog->killLoggerEngine (logEngineName);
    //      delete pEngine;
    //      qDebug() << QString("Log engine %1 stopped").arg(logEngineName);
        }
    }
    //qDebug() << "stopLogging end";
}

void Core::shutdown()
{
    qxtLog->info() << "quiting now";

    if (p_shmem->isAttached())
        p_shmem->detach();

    if (p_man->isScanning ())
    {
        qxtLog->trace () << tr("scan is in progress -- stop it");
        p_man->stopScan();
        qxtLog->trace () << tr("and wait for scanStoppedsignal");
        QxtSignalWaiter::wait(p_man, SIGNAL(scanStopped() ) );
        qxtLog->trace () << tr("scanStopped recieved, proceed");
    }

    qxtLog->trace () << tr("call manager shutdown slot");
    p_man->shutdown ();
 }


void Core::onManagerStopped()
{
    qxtLog->debug() << tr("Manager is stopped now.");
    if (updThread->isRunning())
    {
        qxtLog->debug() << tr("Stop UpDBThread now");
        updThread->stop();
        if (updThread->isRunning())
        {
            updThread->wait();
        }
    }

    if (p_update_icon_thread->isRunning ())
    {
        p_update_icon_thread->stop();
        p_update_icon_thread->wait ();
    }

    emit shuttingDown();

    QCoreApplication::quit();
}

QString Core::prepareShMem()
{
    QString key = p_shmem->key();
    if (p_shmem->isAttached())
        p_shmem->detach();
    QVariant v = p_man->watchItems();
    QBuffer buffer;
    buffer.setData(QxtJSON::stringify(v).toUtf8());
    int size = buffer.size();
    try
    {
        if (!p_shmem->create(size)) throw 1;
        if (!p_shmem->lock())       throw 2;
        char * dest = (char*) p_shmem->data();
        char* src = buffer.buffer().data();
        memcpy(dest, src, qMin(p_shmem->size(), size));
        if (!p_shmem->unlock())     throw 3;
    }
    catch(int e)
    {
        key = QString::null;
        QString msg = QString("Core::prepareShMem: %1").arg(p_shmem->errorString());
        THROW_EXCEPTION(msg);
    }
    return key;
}


QVariant Core::readShMem(QString key)
{
    QVariant res;
    if (key == QString::null)
        return res;
    QBuffer buffer;
    int size = 0;
    try
    {
        p_shmem->setKey(key);
        if (!p_shmem->attach()) throw 1;
        if (!p_shmem->lock()) throw 2;
        size = p_shmem->size();
        char* ptr = (char*)p_shmem->data();
        buffer.setData(ptr, size);;
        if (!p_shmem->unlock()) throw 3;
        if (!p_shmem->detach()) throw 4;
    }
    catch(int e)
    {
        qxtLog->error() << tr("Fail to read shared memory: ") << p_shmem->errorString();
        return res;
    }
    QString jsonstr = QString::fromUtf8(buffer.data().data(), size);
    qxtLog->trace() << tr("Data from SharedMemmory:") << jsonstr;
    res = QxtJSON::parse(jsonstr);
    return res;
}

void Core::runServer()
{
    p_update_icon_thread->start ();
    updThread->start ();
    if(config["BNet/BNetEnabled"].toBool())
    {
        if(isTakeOver)
        {
            p_man->pauseScan ();
            qxtLog->debug () << tr("resuming previous scan");
            p_man->resumeScan();
        }
        else
        {
            qxtLog->debug () << tr("starting new scan");
            p_man->startScan ();
        }
    }
    else
    {
        qxtLog->info() << tr("Bnet connection disabled. Will only send data from cash to DB.");
    }
}

void Core::setupTranslate ()
{
    QString locale = QLocale::system().name();
    pTranslator = new QTranslator(this);
    QDir transdir(config["i18ndir"].toString ());
    QString i18nfilename = transdir.absoluteFilePath ("daemon_" + locale);
    if (!pTranslator->load(i18nfilename))
    {
        qxtLog->warning () << QString("Fail to load %1").arg (i18nfilename);
    }
    QCoreApplication::installTranslator(pTranslator);
}

void Core::setupUnixSignals ()
{
#ifdef Q_OS_LINUX
    pUnixSignalHandler = new SignalHandler(this);
    SignalHandler::setup_unix_signal_handlers ();
    connect (pUnixSignalHandler, SIGNAL(CtrlCPressed()), SLOT(shutdown()) );
#endif
}

void Core::onFinalCleanUp ()
{
    qxtLog->info() << tr("quit.");
}

void Core::truncateLogFile (const QString &logfilepath)
{
    QFile file(logfilepath);
    file.open (QFile::Truncate | QFile::WriteOnly);
    file.write ("-- file truncated by user request --\n");
    file.close ();
}
