#ifndef CORE_HPP
#define CORE_HPP

#include <QCoreApplication>
#include <QxtLogger>
#include <QPluginLoader>
#include "settings.hxx"
#include "updatedbthread.hxx"
#include "watchlist.hxx"

class QSharedMemory;
class UpdateItemInfoThread;
class SignalHandler;
class Manager;
class QTranslator;

class Core : public QCoreApplication
{
    Q_OBJECT
public:
    explicit Core(int & argc, char ** argv);
    ~Core();
    Q_PROPERTY (QString loglevel READ logLevel WRITE setLogLevel)
signals:
   void shuttingDown();
public slots:
    void shutdown();
    QString prepareShMem();
    void runServer();
private slots:
    void onManagerStopped();
    void onFinalCleanUp();
private:
    QVariant readShMem(QString key);
    QString  logLevel();
    void setLogLevel(QString);
    QxtLogger::LogLevel loglevel;

    void truncateLogFile(const QString& logfilepath);
    void startLogging();
    void stopLogging();
    void setupTranslate();
    void setupUnixSignals();

    bool isTakeOver;
    ConfigClient config;
    UpdateItemInfoThread *p_update_icon_thread;
    UpdateDBThread* updThread;
    Manager *p_man;
    QString shmem_key;
    QSharedMemory* p_shmem;
    QPluginLoader storagePluginLoader;
    SignalHandler* pUnixSignalHandler;
    QTranslator* pTranslator;
};

#endif // CORE_HPP
