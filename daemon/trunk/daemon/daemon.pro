TRANSLATIONS = ../translates/daemon_en_US.ts \
               ../translates/daemon_ru_RU.ts

CODECFORTR      = UTF-8
CODECFORSRC     = UTF-8

QT       += core sql network

QT       -= gui

DESTDIR = ../bin
MOC_DIR = ../build
OBJECTS_DIR = ../build

TARGET = enchSale_daemon
VERSION = 1.0.0

CONFIG   += exceptions sse mmx sse2 sse3 sse4 \
        console \
#            use_service \
            qxt \
            bdb \
            use_dbus \
            use_plugins
#            release
#            debug


CONFIG   -= app_bundle

QXT += core network

TEMPLATE = app

SOURCES += main.cxx \
    emaillogengine.cxx \
    manager.cxx \
    updatedbthread.cxx \
    watchlist.cxx \
    settings.cxx \
    core.cxx \
    filelog.cxx \
    bnetconnection.cxx \
    exception.cxx \
    scanaucthread.cxx \
    scanstate.cxx \
    datatypes.cxx \
    updateiteminfothread.cxx \
    scanstorageinterface.cxx \
    storagelistener.cxx \
    qtscanstorage.cxx \
    db.cxx

unix {
    HEADERS += signalhandler.hxx
    SOURCES += signalhandler.cxx
}


HEADERS += \
    manager.hxx\
    emaillogengine.hxx\
    updatedbthread.hxx\
    watchlist.hxx\
    settings.hxx\
    core.hxx \
    filelog.hxx \
    bnetconnection.hxx\
    updateiteminfothread.hxx\
    exception.hxx\
    scanaucthread.hxx\
    scanstate.hxx\
    datatypes.hxx\
    scanstorageinterface.hxx \
    storagelistener.hxx \
    qtscanstorage.hxx \
    watchlistinterface.hxx \
    db.hxx

bdb {
    DEFINES += USE_BDB
    HEADERS +=  bdbserializable.hxx \
                bdbscanstorage.hxx
    SOURCES += 	bdbscanstorage.cxx \
                bdbserializable.cxx
    unix {
    INCLUDEPATH += /opt/bdb/include
        LIBS+= -L/opt/bdb/lib -ldb_cxx
    }
    win32 {
        LIBS += -LC:\Qt\2010.05\mingw\lib -ldb-4.5
    }
}

use_dbus:unix {
    HEADERS += dbus/manageradaptor.h\
        dbus/coreadaptor.h\
        dbus/updatedbadaptor.h\
        dbus/settingsadaptor.h\
        dbus/managerproxy.h\
        dbus/coreproxy.h \
        dbus/scanstorageadaptor.h

    SOURCES += dbus/manageradaptor.cpp \
        dbus/coreadaptor.cpp \
        dbus/updatedbadaptor.cpp \
        dbus/settingsadaptor.cpp \
        dbus/managerproxy.cpp \
        dbus/coreproxy.cpp \
        dbus/scanstorageadaptor.cpp

    QT +=  dbus

    DEFINES += USE_DBUS

    system(qdbuscpp2xml manager.hxx       | sed "s/local./net.softwarium.weber.EnchSaleDaemon./g" | qdbusxml2cpp -a dbus/manageradaptor )
    system(qdbuscpp2xml manager.hxx       | sed "s/local./net.softwarium.weber.EnchSaleDaemon./g" | qdbusxml2cpp -p dbus/managerproxy -c ManagerProxy)
    system(qdbuscpp2xml updatedbthread.hxx| sed "s/local./net.softwarium.weber.EnchSaleDaemon./g" | qdbusxml2cpp -a dbus/updatedbadaptor)
    system(qdbuscpp2xml core.hxx          | sed "s/local./net.softwarium.weber.EnchSaleDaemon./g" | qdbusxml2cpp -a dbus/coreadaptor)
    system(qdbuscpp2xml core.hxx          | sed "s/local./net.softwarium.weber.EnchSaleDaemon./g" | qdbusxml2cpp -p dbus/coreproxy -c CoreProxy)
    system(qdbuscpp2xml scanstorageinterface.hxx | sed "s/local./net.softwarium.weber.EnchSaleDaemon./g" | qdbusxml2cpp -a dbus/scanstorageadaptor -c ScanStorageAdaptor)
    # Don't uncomment! will unmask passwords!
    #system(qdbuscpp2xml settings.hxx      | sed "s/local./net.softwarium.weber.EnchSaleDaemon./g" | qdbusxml2cpp -a dbus/settingsadaptor)
}
use_dbus:win32 {
        #DEFINES -= USE_DBUS
}

use_plugins  {
    DEFINES += USE_PLUGINS
}
OTHER_FILES +=

use_service {
    include (qtservice/src/qtservice.pri)
    DEFINES += USE_SERVICE
    SOURCES += service.cxx
    HEADERS += service.hxx
}
