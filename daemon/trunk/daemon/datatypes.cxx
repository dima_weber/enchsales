#include "datatypes.hxx"
#include <QxtJSON>

bool GroupID::typeRegistered = false;

void GroupID::_register ()
{
    if(!GroupID::typeRegistered)
    {
        typeRegistered = true;
        qRegisterMetaType<GroupID>("GroupID");
    }
}

#ifdef USE_BDB
const char* GroupID::getSig ()
{
    static char sig[BDBSerializable::SIG_LENGTH] = {'G','I'};
    return sig;
}

const char*  GroupID::getVer()
{
    static char ver[BDBSerializable::VER_LENGTH] = {1,0};
    return ver;
}


void GroupID::pack (QDataStream & stream)
{
    stream << id;
}

void GroupID::unpack (QDataStream & stream)
{
    stream >> id;
}
#endif

GroupID::GroupID()
#ifdef USE_BDB
    : BDBSerializable()
#endif
{
    _register ();
    id = 0;
}

GroupID::GroupID(quint64 _id)
#ifdef USE_BDB
    : BDBSerializable()
#endif
{
    _register ();
    id = _id;
}

GroupID::GroupID(const GroupID & copy)
#ifdef USE_BDB
    : BDBSerializable(copy)
#endif
{
    _register ();
    id = copy.id;
}

GroupID& GroupID::operator= (const GroupID& copy)
{
    id = copy.id;
    return *this;
}

GroupID::~GroupID ()
{}

bool GroupID::operator == (const GroupID& copy) const
{
    return id == copy.id;
}

bool GroupID::operator < (const GroupID& copy) const
{
    return id < copy.id;
}

GroupID::operator bool() const
{
    return isValid ();
}

bool GroupID::isValid () const
{
    return id != GroupID::null;
}

QString GroupID::toString()
{
    return QString::number(id);
}

QDataStream& operator<<(QDataStream& stream, const GroupID& id)
{
    stream << id.id;
    return stream;
}

QDataStream& operator>> (QDataStream& stream, GroupID& id)
{
    stream >> id.id;
    return stream;
}

#ifdef USE_BDB
const char*  MetaType::getSig()
{
    static char sig[BDBSerializable::SIG_LENGTH] = {'M','T'};
    return sig;
}

const char*  MetaType::getVer()
{
    static char ver[BDBSerializable::VER_LENGTH] = {1,0};
    return ver;
}

void MetaType::pack(QDataStream& stream)
{
    stream << itemid
           << scantime;
}

void MetaType::unpack(QDataStream& stream)
{
    stream >> itemid
           >> scantime;
}
#endif

MetaType::MetaType()
{
    itemid = 0;
    scantime = QDateTime::currentDateTime();
}

MetaType::MetaType(const MetaType& a)
#ifdef USE_BDB
    :BDBSerializable(a)
#endif
{
    itemid = a.itemid;
    scantime = a.scantime;
}

MetaType::~MetaType()
{

}

#ifdef USE_BDB
const char*  AucData::getSig()
{
    static char sig[BDBSerializable::SIG_LENGTH] = {'A','D'};
    return sig;
}

const char*  AucData::getVer()
{
    static char ver[BDBSerializable::VER_LENGTH] = {1,0};
    return ver;
}


void AucData::pack(QDataStream& stream)
{
    stream << groupid << itemid << auc << quan << buy
           << nbid << ppuBid << time
           << srv << faction
           << itemmods << seller
           << timestmp << icon;
}

void AucData::unpack(QDataStream& stream)
{
    stream >> groupid >> itemid >> auc >> quan >> buy
           >> nbid >> ppuBid >> time
           >> srv >> faction >> itemmods
           >> seller >> timestmp >> icon;
}
#endif

bool AucData::typeRegistered = false;

void AucData::_register ()
{
    if(!typeRegistered)
    {
        qRegisterMetaType<AucData>();
        qRegisterMetaType<DataType>("DataType");
        typeRegistered = true;
    }
}

AucData::AucData(const QVariant& v)
{
    _register ();
    QVariantMap map = v.toMap ();
    groupid=  map["groupid"].toInt();
    itemid =  map["id"].toInt();
    auc    =  map["auc"].toInt();
    quan   =  map["quan"].toInt();
    buy    =  map["buy"].toInt();
    nbid   =  map["nbid"].toInt();
    ppuBid =  map["ppuBid"].toInt();
    time   =  map["time"].toInt();
    srv    =  map["srv"].toString();
    faction=  map["faction"].toString();
    itemmods= map["itemmods"].toString();
    seller =  map["seller"].toString();
    timestmp= map["timestmp"].toDateTime();
    icon   =  map["icon"].toString();
}

AucData::AucData()
#ifdef USE_BDB
    : BDBSerializable()
#endif
{
    _register ();
    groupid = 0;
    itemid = 0;
    auc = 0;
    quan = 0;
    buy = 0;
    nbid = 0;
    ppuBid = 0;
    time = 0;
    srv = QString::null;
    faction = QString::null;
    seller = QString::null;
    itemmods = QString::null;
    timestmp = QDateTime();
    icon = QString::null;
}

AucData::AucData(const AucData& c)
#ifdef USE_BDB
    : BDBSerializable(c)
#endif
{
    _register ();
    groupid= c.groupid;
    itemid = c.itemid;
    auc    = c.auc;
    quan   = c.quan;
    buy    = c.buy;
    nbid   = c.nbid;
    ppuBid = c.ppuBid;
    time   = c.time;
    srv    = c.srv;
    faction= c.faction;
    itemmods=c.itemmods;
    seller = c.seller;
    timestmp=c.timestmp;
    icon   = c .icon;
}

AucData& AucData::operator=(const AucData& c)
{
    groupid= c.groupid;
    itemid = c.itemid;
    auc    = c.auc;
    quan   = c.quan;
    buy    = c.buy;
    nbid   = c.nbid;
    ppuBid = c.ppuBid;
    time   = c.time;
    srv    = c.srv;
    faction= c.faction;
    itemmods=c.itemmods;
    seller = c.seller;
    timestmp=c.timestmp;
    icon   = c.icon;
    return *this;
}

AucData::operator QVariantMap()
{
  QVariantMap map;
  map["groupid"].setValue(groupid);
  map["id"] = itemid;
  map["auc"] = auc;
  map["quan"] = quan;
  map["buy"] = buy;
  map["nbid"] = nbid;
  map["ppuBid"] = ppuBid;
  map["time"] = time;
  map["srv"] = srv;
  map["faction"]= faction;
  map["itemmods"]=itemmods;
  map["seller"] = seller;
  map["timestmp"]=timestmp;
  map["icon"] = icon;
  return map;
}

AucData::operator QString ()
{
    QString msg;
    msg = QxtJSON::stringify ((QVariantMap)*this);
    return msg;
}


    GroupHandler::GroupHandler()
    {
        state = Undefined;
        meta.itemid = 0;
        meta.scantime = QDateTime::currentDateTime ();
        pos = NULL;
        count = 0;
        rollbackcount = 0;
    }

    GroupHandler::GroupHandler(const GroupHandler& copy)
#ifdef USE_BDB
    : BDBSerializable(copy)
#endif
    {
        state = copy.state;
        meta = copy.meta;
        pos = copy.pos;
        count = copy.count;
        rollbackcount = copy.rollbackcount;
    }

#ifdef USE_BDB
    const char*  GroupHandler::getSig()
    {
        static char sig[BDBSerializable::SIG_LENGTH] = {'G','H'};
        return sig;
    }

    const char*  GroupHandler::getVer()
    {
        static char ver[BDBSerializable::VER_LENGTH] = {1,0};
        return ver;
    }

    void GroupHandler::pack(QDataStream& stream)
    {
        int s = (int)state;
        quint64 p = (quint64)pos;
        stream << s
               << meta.itemid
               << meta.scantime
               << p
               << count
               << rollbackcount;
    }

    void       GroupHandler::unpack(QDataStream& stream)
    {
        int s;
        quint64 p;
        stream >> s
               >> meta.itemid
               >> meta.scantime
               >> p
               >> count
               >> rollbackcount;
        state = (GroupState)s;
        pos   = (void*)p;
    }
#endif

    AucLogin::AucLogin(QString a, QString b)
    {
        srv = a;
        charname = b;
    }

    bool AucLogin::operator== (AucLogin a)
    {
        return a.srv == srv && a.charname == charname;
    }
    bool AucLogin::operator != (AucLogin a)
    {
        return !(*this == a);
    }
    AucLogin& AucLogin::operator=(AucLogin a)
    {
        srv = a.srv;
        charname = a.charname;
        return *this;
    }
    void AucLogin::reset()
    {
        srv = charname = QString::null;
    }

    WatchItem::WatchItem(const WatchItem& wi)
    {
        srv = wi.srv;
        chname = wi.chname;
        itemid = wi.itemid;
    }
    WatchItem::WatchItem()
    {
        srv = chname = QString::null;
        itemid = -1;
    }
    WatchItem& WatchItem::operator =(const WatchItem& c)
    {
        srv = c.srv;
        chname = c.chname;
        itemid = c.itemid;
        return *this;
    }

    bool WatchItem::fromQVariant(QVariant map)
    {
        if (map.toMap().isEmpty())
            return false;
        srv = map.toMap()["srv"].toString();
        chname = map.toMap()["chname"].toString();
        itemid = map.toMap()["itemid"].toInt();
        return true;
    }

    QVariant WatchItem::toQVariant()
    {
        QVariantMap itemmap;
        itemmap ["srv"] = srv;
        itemmap ["chname"] = chname;
        itemmap ["itemid"] = itemid;
        return itemmap;
    }

    AucLogin WatchItem::login()
    {
        return AucLogin (srv, chname);
    }

QString groupStateToString(GroupState state)
{
    switch (state)
    {
    case Invalid: return "Invalid";
    case Valid: return "Valid";
    case Locked: return "Locked";
    case Commited: return "Commited";
    case Blocked: return "Blocked";
    case Corrupted: return "Corrupted";
    case Undefined:
    default: return "Undefined";
    }
}

GroupState stringToGtoupState(QString state)
{
    if (state.toLower() == "invalid")
        return Invalid;
    if (state.toLower() == "valid")
        return Valid;
    if (state.toLower() == "locked")
        return Locked;
    if (state.toLower() == "commited")
        return Commited;
    if (state.toLower() == "blocked")
        return Blocked;
    if (state.toLower() == "corrupted")
        return Corrupted;
    return Undefined;
}

