#ifndef DATATYPES_H
#define DATATYPES_H

#ifdef USE_BDB
#	include <db_cxx.h>
#	include "bdbserializable.hxx"
#endif
#include <QString>
#include <QDateTime>
#include <QVariantMap>
//#include <QxtLogger>
//#include <QxtJSON>


class MetaType
#ifdef USE_BDB
    : public BDBSerializable
#endif
{
#ifdef USE_BDB
protected:
    virtual const char* getSig();
    virtual const char* getVer();
    virtual void pack (QDataStream&);
    virtual void unpack(QDataStream&);
#endif
public:
    int         itemid;
    QDateTime   scantime;

    MetaType();
    ~MetaType();
    MetaType(const MetaType&);

};

struct GroupID
#ifdef USE_BDB
    : public BDBSerializable
#endif
{
private:
    static bool typeRegistered;
    static void _register();
public:
    quint64  id;
    const static quint64 null = 0;

    GroupID();
    GroupID(quint64);
    GroupID(const GroupID&);
    GroupID& operator=(const GroupID&);
    ~GroupID();

    bool operator==(const GroupID&) const;
    bool operator <(const GroupID&) const;
    operator bool() const;
    bool isValid() const;
    QString toString();
#ifdef USE_BDB
protected:
    virtual const char* getSig();
    virtual const char* getVer();
    virtual void pack (QDataStream&);
    virtual void unpack(QDataStream&);
#endif
};

QDataStream& operator << (QDataStream&, const GroupID& id);
QDataStream& operator >> (QDataStream&, GroupID& id);

Q_DECLARE_METATYPE(GroupID)

struct AucData
#ifdef USE_BDB
    : public BDBSerializable
#endif
{
    GroupID     groupid;
    int         itemid;
    int         auc;
    int         quan;
    int         buy;
    int         nbid;
    int         ppuBid;
    int         time;
    QString     srv;
    QString     faction;
    QString     itemmods;
    QString     seller;
    QDateTime   timestmp;
    QString     icon;

    AucData(const QVariant& v);
    AucData();
    AucData(const AucData& c);
    AucData& operator=(const AucData& c);
    operator QVariantMap();
    operator QString ();
#ifdef USE_BDB
protected:
    virtual const char* getSig();
    virtual const char* getVer();
    virtual void pack (QDataStream&);
    virtual void unpack(QDataStream&);
#endif
private:
    static bool typeRegistered;
    static void _register();
};
Q_DECLARE_METATYPE(AucData)

typedef AucData DataType;

enum GroupState {Undefined,
                 Invalid,  /// created  with open, not closed yet.
                           /// can't be listed as avaliable
                 Valid,    ///
                 Locked,
                 Commited,
                 Blocked, /// 10 rollbacks -- seems sql-server blocks it.
                 Corrupted}; /// Corrupted: not all items added.
QString groupStateToString(GroupState state);
GroupState stringToGtoupState(QString state);

struct GroupHandler
#ifdef USE_BDB
    : public BDBSerializable
#endif
{
    GroupState  state;
    MetaType    meta;
    //quint64     pos;
    void*       pos; // Clients can reinterpret this field as they want
                     // Base scanstorage use it as quint64 number of already posted items
                     // bdb scanstorage use it as pointer to cursor;
    quint64     count;
    quint16     rollbackcount;

    GroupHandler();
    GroupHandler(const GroupHandler&);
#ifdef USE_BDB
protected:
    virtual const char* getSig();
    virtual const char* getVer();
    virtual void pack (QDataStream&);
    virtual void unpack(QDataStream&);
#endif
};
Q_DECLARE_METATYPE(GroupHandler)

typedef QList<DataType>             Group;
typedef QMap<GroupID, Group>        GroupVault;
typedef QMap<GroupID, GroupHandler> GroupCatalog;

typedef QList<DataType>             ScanStorage_Vault;

struct AucLogin
{
    QString srv;
    QString charname;

    AucLogin(QString a = QString::null, QString b = QString::null);
    bool operator== (AucLogin a);
    bool operator != (AucLogin a);
    AucLogin& operator=(AucLogin a);
    void reset();
};
Q_DECLARE_METATYPE(AucLogin)

struct WatchItem
{
    QString srv;
    QString chname;
    int itemid;

    WatchItem(const WatchItem& wi);
    WatchItem();
    WatchItem& operator =(const WatchItem& c);
    bool fromQVariant(QVariant map);
    QVariant toQVariant();
    AucLogin login();
};
Q_DECLARE_METATYPE(WatchItem)

#endif // DATATYPES_H
