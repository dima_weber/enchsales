#include "db.hxx"
#include <QxtLogger>

int dbg_QSqlQuery::counter = 0;
int dbg_QSqlDatabase::counter = 0;

dbg_QSqlQuery::dbg_QSqlQuery(QSqlDatabase& db )
    : QSqlQuery(db)
{
    name = QString("QSqlQuery_%1").arg (dbg_QSqlQuery::counter++);

    qxtLog->trace() << QString("[%1] %2").arg (name).arg ("created");
}

dbg_QSqlQuery::~dbg_QSqlQuery()
{
    qxtLog->trace() << QString("[%1] %2").arg (name).arg ("destroyed");
}

bool dbg_QSqlQuery::exec()
{
    bool res;
    res = QSqlQuery::exec ();
    QString msg = QString("[%1] execute: %2").arg (name).arg (executedQuery ());
    QString values = "";
    QList<QVariant> list = boundValues().values();
    for (int i = 0; i < list.size(); ++i)
        values +=  QString("%1 : %2").arg (i).arg (list.at(i).toString().toAscii().data());
    if (!values.isEmpty ())
        msg = QString("%1 with values %2").arg (msg).arg (values);
    qxtLog->trace() << msg;
    return res;
}

bool dbg_QSqlQuery::exec(const QString& q)
{
    bool res;
    res = QSqlQuery::exec (q);
    QString msg = QString("[%1] execute: %2").arg (name).arg (executedQuery ());
    QString values = "";
    QList<QVariant> list = boundValues().values();
    for (int i = 0; i < list.size(); ++i)
        values +=  QString("%1 : %2").arg (i).arg (list.at(i).toString().toAscii().data());
    if (!values.isEmpty ())
        msg = QString("%1 with values %2").arg (msg).arg (values);
    qxtLog->trace() << msg;
    return res;
}



dbg_QSqlDatabase::dbg_QSqlDatabase(const QSqlDatabase& db)
    : QSqlDatabase(db)
{
    name = QString("QSqlDatabase_%1").arg (dbg_QSqlDatabase::counter++);

    qxtLog->trace() << QString("[%1] %2").arg (name).arg ("created");
}

dbg_QSqlDatabase::~dbg_QSqlDatabase()
{
    qxtLog->trace() << QString("[%1] %2").arg (name).arg ("destroyed");
}

bool dbg_QSqlDatabase::transaction ()
{
   qxtLog->trace() << QString("[%1]: %2").arg (name).arg("transaction started");
   return  QSqlDatabase::transaction ();
}

bool dbg_QSqlDatabase::commit ()
{
    qxtLog->trace() << QString("[%1] %2").arg (name).arg ("transaction commited");
    return QSqlDatabase::commit ();
}

bool dbg_QSqlDatabase::rollback ()
{
    qxtLog->trace() << QString("[%1] %2").arg (name).arg ("transaction rolled back");
    return QSqlDatabase::rollback ();
}

bool dbg_QSqlDatabase::open ()
{
    qxtLog->trace() << QString("[%1] %2").arg (name).arg ("db opened");
    return QSqlDatabase::open ();
}

void dbg_QSqlDatabase::close ()
{
    qxtLog->trace() << QString("[%1] %2").arg (name).arg ("db closed");
    QSqlDatabase::close ();
}

/*
QVariant MySQLDBStorage::getItemsList ()
{

}
*/
