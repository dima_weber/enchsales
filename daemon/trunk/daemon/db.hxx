#ifndef DB_HXX
#define DB_HXX
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QString>
#include  "settings.hxx"

class dbg_QSqlQuery : public QSqlQuery
{
    QString name;
    static int counter;
public:
    dbg_QSqlQuery(QSqlDatabase& db );
    ~dbg_QSqlQuery();
    virtual bool exec();
    virtual bool exec(const QString& q);
};


class dbg_QSqlDatabase : public QSqlDatabase
{
    QString name;
    static int counter;
public:
    dbg_QSqlDatabase(const QSqlDatabase& db);
    virtual ~dbg_QSqlDatabase();
    virtual bool transaction ();
    virtual bool commit ();
    virtual bool rollback ();
    virtual bool open ();
    virtual void close ();
};

/*
class DBStorageInterface
{
    public:
    virtual QVariant getItemsList() = 0;
    virtual void addItemInfo(int id, const QString& path) =0;
    virtual void setItemIcon(QPixmap& icon, const QString& path) = 0;
    virtual void addScan(AucData& scan) =0;
};

class MySQLDBStorage : public DBStorageInterface
{
public:
    virtual QVariant getItemsList();
    virtual void addItemInfo(int id, const QString& path);
    virtual void setItemIcon(QPixmap& icon, const QString& path);
    virtual void addScan(AucData& scan);
};

class BDBStorage : public DBStorageInterface
{
public:
    virtual QVariant getItemsList();
    virtual void addItemInfo(int id, const QString& path);
    virtual void setItemIcon(QPixmap& icon, const QString& path);
    virtual void addScan(AucData& scan);
};
*/

#endif // DB_HXX
