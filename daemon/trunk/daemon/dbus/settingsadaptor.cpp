/*
 * This file was generated by qdbusxml2cpp version 0.7
 * Command line was: qdbusxml2cpp -a settingsadaptor
 *
 * qdbusxml2cpp is Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#include "settingsadaptor.h"
#include <QtCore/QMetaObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

/*
 * Implementation of adaptor class ConfigAdaptor
 */

ConfigAdaptor::ConfigAdaptor(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    // constructor
    setAutoRelaySignals(true);
}

ConfigAdaptor::~ConfigAdaptor()
{
    // destructor
}

QVariantMap ConfigAdaptor::config() const
{
    // get the value of property config
    QVariantMap map = qvariant_cast< QVariantMap >(parent()->property("config"));
    QString s;
    foreach (s, map.keys())
    {
        if (s.contains ("password", Qt::CaseInsensitive))
        {
            map[s] = QString("*").repeated (map[s].toString().length ());
        }
    }

    return map;
}

QString ConfigAdaptor::getParam(const QString &key)
{
    // handle method call net.softwarium.weber.EnchSaleDaemon.Config.getParam
    QString out0;
    QMetaObject::invokeMethod(parent(), "getParam", Q_RETURN_ARG(QString, out0), Q_ARG(QString, key));
    if (key.contains ("password", Qt::CaseInsensitive))
    {
        out0 = QString("*").repeated (out0.length ());
    }
    return out0;
}

void ConfigAdaptor::readConfig()
{
    // handle method call net.softwarium.weber.EnchSaleDaemon.Config.readConfig
    QMetaObject::invokeMethod(parent(), "readConfig");
}

void ConfigAdaptor::setParam(const QString &key, const QString &value)
{
    // handle method call net.softwarium.weber.EnchSaleDaemon.Config.setParam
    QMetaObject::invokeMethod(parent(), "setParam", Q_ARG(QString, key), Q_ARG(QString, value));
}

void ConfigAdaptor::writeConfig()
{
    // handle method call net.softwarium.weber.EnchSaleDaemon.Config.writeConfig
    QMetaObject::invokeMethod(parent(), "writeConfig");
}

