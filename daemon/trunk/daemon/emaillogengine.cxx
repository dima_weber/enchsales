#include "emaillogengine.hxx"
#include <qxtsmtp.h>
#include <qxtmailmessage.h>
#include <QSettings>
#include <QCoreApplication>
#include <QTimer>

QwbrEmailLogEngine::QwbrEmailLogEngine()
    : QxtLoggerEngine()
{
    pThread = new SMTPThread();
}

SMTPThread::SMTPThread(QObject *parent)
    : QThread(parent), config()
{
    p_smtp= new QxtSmtp(this);
    connect(p_smtp, SIGNAL(authenticated()), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(authenticationFailed()),this,  SLOT(onSMTPDisconnected()));
    connect(p_smtp, SIGNAL(authenticationFailed ( const QByteArray &)), this, SLOT(smtpError(QByteArray)));
    connect(p_smtp, SIGNAL(connected()), this, SLOT(onSMTPConnected()));
    //connect(p_smtp, SIGNAL(connectionFailed()), this, SLOT(onSMTPDisconnected()));
    connect(p_smtp, SIGNAL(connectionFailed ( const QByteArray &)), this, SLOT(smtpError(QByteArray)));
    connect(p_smtp, SIGNAL(disconnected()), this, SLOT(onSMTPDisconnected()));

    //connect(p_smtp, SIGNAL(encrypted ()), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(encryptionFailed ()), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(encryptionFailed ( const QByteArray & )), this, SLOT(smtpError(QByteArray)));
    //connect(p_smtp, SIGNAL(finished ()), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(mailFailed ( int , int )), this, SLOT(dummySlot()));
    connect(p_smtp, SIGNAL(mailFailed ( int , int , const QByteArray & )), this, SLOT(onMailFail(int,int,const QByteArray&)));
    connect(p_smtp, SIGNAL(mailSent ( int)), this, SLOT(onMailSent(int)));
    //connect(p_smtp, SIGNAL(recipientRejected ( int, const QString & )), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(recipientRejected ( int, const QString & , const QByteArray & )), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(senderRejected ( int, const QString & )), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(senderRejected ( int, const QString &, const QByteArray & )), this, SLOT(dummySlot()));


    connected = false;
}

QwbrEmailLogEngine::~QwbrEmailLogEngine()
{
    if (pThread->isRunning ())
        pThread->terminate ();
    delete pThread;
}

SMTPThread::~SMTPThread ()
{
    delete p_smtp;
}

void QwbrEmailLogEngine::initLoggerEngine ()
{
    pThread->start ();
}

bool QwbrEmailLogEngine::isInitialized () const
{
    return pThread->isRunning ();
}

void QwbrEmailLogEngine::killLoggerEngine ()
{
    pThread->stop();
    if(!pThread->wait (60000))
    {
        qDebug() << "Email logger thread stop timed out. Terminate it";
        pThread->terminate ();
        pThread->wait();
    }
//  qDebug("Email logger thread stoppped 100%");
}

void QwbrEmailLogEngine::writeFormatted ( QxtLogger::LogLevel level, const QList<QVariant> & messages )
{
    if(isLogLevelEnabled(level) && level > QxtLogger::WarningLevel)
    {
        /*
        QString message_text = QString("This mail was generated automatically.\n"
                                       "Enchsale daemon have some problems:\n"
                                       "\n[%1] %2").arg(QxtLogger::logLevelToString(level)).arg(messages[0].toString());
        */
        foreach (QVariant var, messages)
            pThread->addMsg (var.toString ());
    }
}

void SMTPThread::run ()
{
//    qDebug() << "Email logger thread started";
    QTimer timer;
    connect(&timer, SIGNAL(timeout()), SLOT(mailBuffer()));
    timer.start (60000);
    exec();
//    qDebug() << "Email logger thread stopped";
}

void SMTPThread::stop ()
{
    exit();
}

void SMTPThread::addMsg (QString msg)
{
    msgBuffAccessMutex.lock();
    msgBuff << msg;
    msgBuffAccessMutex.unlock();
}

void SMTPThread::mailBuffer ()
{
    if (msgBuff.isEmpty ())
        return;
    QxtMailMessage msg;
    QByteArray login = config["mail/login"].toByteArray();
    p_smtp->setUsername(login);
    QByteArray password = config["mail/password"].toByteArray();
    p_smtp->setPassword(password);
    bool tls = config["startTLS"].toBool();
    p_smtp->setStartTlsDisabled(!tls);

    msgBuffAccessMutex.lock();
    msg.setBody(msgBuff.join("\n"));
    msgBuff.clear ();
    msgBuffAccessMutex.unlock();
    msg.setSubject("enchsale daemon problems");
    msg.addRecipient(config["mail/to"].toString());
    msg.setSender(config["mail/from"].toString());
    QString server = config["mail/server"].toString();
    int port = config["mail/port"].toInt();
#ifndef QT_NO_OPENSSL
    if(config["mail/useSSL"].toBool())
        p_smtp->connectToSecureHost(server, port);
    else
#endif
        p_smtp->connectToHost(server, port);
    p_smtp->send(msg);
    //p_smtp->disconnectFromHost();
}

void SMTPThread::onSMTPConnected()
{
    qDebug() << "SMTP connected";
}

void SMTPThread::onSMTPDisconnected()
{
    qDebug() << "SMTP disconnected";
}

void SMTPThread::dummySlot()
{
    qDebug() << QString("smtp auth ok");
}

void SMTPThread::smtpError(QByteArray msg)
{
    qDebug() << QString("smtp error: %1").arg(msg.data());
}

void SMTPThread::onMailSent(int id)
{
    qDebug() << QString("mail with id %1 sent.").arg(id);
    p_smtp->disconnectFromHost();
}

void SMTPThread::onMailFail(int mailid, int errCode, const QByteArray& msg)
{
    qDebug() << QString("fail to send mail %1: [%2] %3").arg(mailid).arg(errCode).arg(QString(msg));
    p_smtp->disconnectFromHost();
}
