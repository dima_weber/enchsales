#ifndef EMAILLOGENGINE_H
#define EMAILLOGENGINE_H

#include "settings.hxx"
#include <QxtLoggerEngine>
#include <QThread>
#include <QMutex>
class QxtSmtp;

class SMTPThread : public QThread
{
    Q_OBJECT

    ConfigClient config;
    bool connected;
    QxtSmtp* p_smtp;
    QStringList msgBuff;
    QMutex msgBuffAccessMutex;
public:
    SMTPThread(QObject* parent = 0);
    ~SMTPThread();
    void addMsg(QString msg);
protected:
    void run();
public slots:
    void stop();
    void mailBuffer();
private slots:
    void onSMTPConnected();
    void onSMTPDisconnected();
    void smtpError(QByteArray);
    void dummySlot();
    void onMailSent(int);
    void onMailFail(int mailid, int errCode, const QByteArray& msg);
};

class QwbrEmailLogEngine : public QxtLoggerEngine
{
    SMTPThread* pThread;
public:
    QwbrEmailLogEngine();
    ~QwbrEmailLogEngine();
    void initLoggerEngine ();
    bool isInitialized () const;
    void killLoggerEngine ();
    void writeFormatted ( QxtLogger::LogLevel level, const QList<QVariant> & messages );
};

#endif // EMAILLOGENGINE_H
