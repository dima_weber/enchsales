#include "exception.hxx"

Exception::Exception(const char* filename, int codeline, const char* msg)
{
    throw_codeline = codeline;
    throw_filename = QString::fromUtf8 (filename);
    error_msg = QString::fromUtf8 (msg);
}

Exception::Exception(const char* filename, int codeline, const QString& msg)
{
    throw_codeline = codeline;
    throw_filename = QString::fromUtf8 (filename);
    error_msg = msg;
}


QString Exception::getErrorMsg()
{
    return QString ("[%1:%2]: %3").arg(throw_filename).arg(throw_codeline).arg(error_msg);
}
