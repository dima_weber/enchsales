#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <QString>

class Exception
{
protected:
    QString error_msg;
    QString throw_filename;
    int     throw_codeline;
public:
    Exception(const char* filename, int codeline, const QString& msg);
    Exception(const char* filename, int codeline, const char* msg);
    QString getErrorMsg();

};

#define THROW_EXCEPTION(msg) throw Exception(__FILE__, __LINE__, msg);

#endif // EXCEPTION_H
