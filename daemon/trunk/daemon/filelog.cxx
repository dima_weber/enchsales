#include "filelog.hxx"
#include <QDateTime>
#include <QCoreApplication>
#include <QThread>

FileLog::FileLog(QString filename) :
    QxtBasicFileLoggerEngine(filename)
{
}

void FileLog::writeFormatted(QxtLogger::LogLevel level, const QList<QVariant> &messages)
{
    if (messages.isEmpty()) return;
    QString header = QString("[%1] [%2] [%3] ")
                    .arg(QDateTime::currentDateTime().toString(dateFormat()))
                    .arg(QCoreApplication::applicationPid())
                    .arg(QxtLogger::logLevelToString(level).remove("Level"))
                    ;
    QString padding;
    QIODevice* file = device();
    Q_ASSERT(file);
    file->write(header.toUtf8());
    padding.fill (' ', header.size ());
    int count = 0;
    Q_FOREACH(const QVariant& out, messages)
    {
        if (!out.isNull())
        {
            if (count != 0) file->write(padding.toUtf8());
            file->write(out.toString().toUtf8());
            file->write("\n");
        }
        count++;
    }
}
