#ifndef FILELOG_HPP
#define FILELOG_HPP

#include <QxtBasicFileLoggerEngine>

class FileLog : public QxtBasicFileLoggerEngine
{
public:
    explicit FileLog(QString filename);
public:
    void writeFormatted ( QxtLogger::LogLevel level, const QList<QVariant> & messages );
};

#endif // FILELOG_HPP
