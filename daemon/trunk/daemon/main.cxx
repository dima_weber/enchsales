//#include <stdlib.h>
#include "exception.hxx"

//#include <QSqlQuery>
//#include <QSqlError>
//#include <QList>
//#include <QMap>
//#include <QString>
//#include <QVariant>
//#include <QVariantMap>
//#include <QStringList>
//#include <QTimer>
//#include <QDir>

#include <QxtLogger>

#include "core.hxx"
//#include "bdbscanstorage.hxx"
#ifdef USE_SERVICE
#   include "service.hxx"
#endif

int main (int argc, char *argv[])
{

    QCoreApplication::setOrganizationName ("dimaweber");
    QCoreApplication::setOrganizationDomain ("dimaweber.homeip.net");
    QCoreApplication::setApplicationName ("enchsales_daemon");

    ConfigClient config;

    if (config.processCommandLine(argc, argv) < 0)
        return 0;

    int retCode = 0xFF;
    try
    {
        if (config["daemon"].toBool ())
        {
#ifdef USE_SERVICE
            QwbrService service(argc, argv, "ench_sale_daemon");
            retCode = service.exec ();
#else
            qFatal ( "Daemon build without service support. Recompile or use as application.");
#endif
        }
        else
        {
            Core app(argc, argv);
            QTimer::singleShot (10, &app, SLOT(runServer()));

            retCode =  app.exec ();
        }
    }
    catch(Exception& e)
    {
        qxtLog->fatal() << e.getErrorMsg();
    }

    return retCode;
}
