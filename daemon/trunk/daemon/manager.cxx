#include "manager.hxx"
#include "updatedbthread.hxx"
#include "qtscanstorage.hxx"
#include "settings.hxx"
#include "exception.hxx"
#include "scanstate.hxx"

#include <QTimer>
#include <QFile>
#include <QNetworkCookieJar>
#include <QFinalState>
#include <QxtLogger>
#include <QxtSignalWaiter>
#include <QxtJSON>

#ifdef USE_DBUS
#    include "dbus/manageradaptor.h"
#endif


Manager::Manager (QObject * parent):QStateMachine(parent), config()
{
    timer = new QTimer (this);
    int delay = config["Timers/scan_sleep"].toInt ();
    timer->setInterval(delay * 1000);
    connect (timer, SIGNAL (timeout ()), this, SLOT (startScan ()));

    p_thread = new ScanAucThread(this);
    connect (p_thread, SIGNAL(nextAucStarted()), SIGNAL(nextAucStarted()));

    initStateMachine ();
    //connect (this, SIGNAL(finished ()), SIGNAL(machineStopped()));
#ifdef USE_DBUS
    new ManagerAdaptor (this);
    QDBusConnection connection = QDBusConnection::sessionBus ();
    connection.registerObject ("/Manager", this);
#endif
    connect (this, SIGNAL(stopped()), SLOT(onMachineStop()));
}

Manager::~Manager ()
{
    //delete timer;
    //delete p_thread;
#ifdef USE_DBUS
    QDBusConnection connection = QDBusConnection::sessionBus ();
    connection.unregisterObject ("/Manager");
#endif
}

int Manager::getScanMapSize ()
{
    return scan_map->size ();
}

QVariantMap Manager::getStatus ()
{
    QVariantMap map;

    map  = p_thread->property ("status").toMap ();
    map["state"]            = getCurStateString();
    map["items in memmory"] = getScanMapSize ();
    map["next scan in"]     = getTimeToNextScan ().toString ("mm'm' ss's'");
    return map;
}

QTime Manager::getTimeToNextScan ()
{
    QDateTime cur_time = QDateTime::currentDateTime ();
    int sec_past_start = p_thread->getScanStartTime().secsTo (cur_time);

    int scan_delay = config["Timers/scan_sleep"].toInt ();
    int sec_till_new = scan_delay - sec_past_start;

    QTime time (0, 0);
    time = time.addSecs (sec_till_new);
    return time;
}

QVariantMap Manager::watchItems()
{
    return p_thread->getWatchList ().toMap ();
}

void Manager::stopScan()
{
    timer->stop();
    emit stopRequest ();
}

void Manager::startScan ()
{
    timer->start ();
    emit startRequest ();
}

void Manager::resumeScan()
{
    timer->start();
    emit resumeRequest ();
}

void Manager::pauseScan()
{
    timer->stop();
    emit pauseRequest ();
}


void Manager::shutdown ()
{
    qxtLog->trace () << tr("Manager::shutdown() called");
    timer->stop ();
    qxtLog->trace () << tr("timer stopped. Send shutdownRequest signal");
    emit shutdownRequest ();
}

bool Manager::setWatchedItems(QVariantMap map)
{
    if (p_thread->setWatchList (map))
    {
        pauseScan ();
        return true;
    }
    else
        return false;
}

bool Manager::isScanning ()
{
    return p_thread && p_thread->isRunning ();
}

void Manager::onMachineStop()
{
    qxtLog->debug () << tr("machine stopped");
}

void Manager::initStateMachine ()
{
    /**
        @see graph.dot / graph.svg files for graphic view of
        states/transitions.
    */
    ScanAucThread* pExecuter = p_thread;
    Manager* pController = this;

    QState* pIdleState     = new ScannerState(Idle,    pController, this);
    QState* pStoppedState  = new ScannerState(Stopped, pController, this);
    QState* pPausedState   = new ScannerState(Paused,  pController, this);
    QState* pScanningState = new ScannerState(Scanning,pController, this);
    QState* pStartingState = new ScannerState(Starting,pController, this);
    QState* pPausingState  = new ScannerState(Pausing, pController, this);
    QState* pStoppingState = new ScannerState(Stopping,pController, this);
    QState* pResumingState = new ScannerState(Resuming,pController, this);
    QFinalState* pShutdownState = new QFinalState(this);

    connect (pStartingState, SIGNAL(entered()), pExecuter, SLOT(startScan()));
    connect (pPausingState,  SIGNAL(entered()), pExecuter, SLOT(pauseScan()));
    connect (pStoppingState,  SIGNAL(entered()), pExecuter, SLOT(stopScan()));
    connect (pResumingState, SIGNAL(entered()), pExecuter, SLOT(resumeScan()));

    connect (pStoppedState, SIGNAL(entered()), pController, SIGNAL(scanStopped()));
    connect (pPausedState, SIGNAL(entered()), pController, SIGNAL(scanStopped()));
    connect (pScanningState, SIGNAL(entered()), pController, SIGNAL(scanStarted()));
    connect (pIdleState, SIGNAL(entered()), pController, SIGNAL(waiting()));

    pIdleState->addTransition     (pController, SIGNAL(startRequest()), pStartingState);
    pIdleState->addTransition     (pController, SIGNAL(stopRequest()),  pStoppedState);
    pIdleState->addTransition     (pController, SIGNAL(pauseRequest()), pPausedState);
    pIdleState->addTransition     (pController, SIGNAL(shutdownRequest()), pShutdownState);

    pStartingState->addTransition (pExecuter,   SIGNAL(started()),      pScanningState);

    pStoppedState->addTransition  (pController, SIGNAL(startRequest()), pStartingState);
    pStoppedState->addTransition  (pController, SIGNAL(shutdownRequest()), pShutdownState);
    pStoppedState->addTransition (pController, SIGNAL(stopRequest()), pStoppedState);

    pStoppingState->addTransition (pExecuter,   SIGNAL(finished()),     pStoppedState);

    pPausedState->addTransition   (pController, SIGNAL(stopRequest()),  pStoppedState);
    pPausedState->addTransition   (pController, SIGNAL(resumeRequest()),pResumingState);
    pPausedState->addTransition   (pController, SIGNAL(shutdownRequest()), pShutdownState);
    pPausedState->addTransition (pController, SIGNAL(pauseRequest()), pPausedState);

    pResumingState->addTransition (pExecuter,   SIGNAL(started()),      pScanningState);

    pScanningState->addTransition (pController, SIGNAL(stopRequest()),  pStoppingState);
    pScanningState->addTransition (pExecuter,   SIGNAL(finished()),     pIdleState);
    pScanningState->addTransition (pController, SIGNAL(pauseRequest()), pPausingState);

    pPausingState->addTransition  (pExecuter,   SIGNAL(finished()),     pPausedState);


    setInitialState (pIdleState);

    start ();
}
