#ifndef MANAGER_H
#define MANAGER_H
#include <QVariantMap>
#include <QStateMachine>

#include "settings.hxx"
#include "scanaucthread.hxx"

#include <QxtLogger>

/*
   This credentials are only allowed for my IPs
   use --host --user --password --name --port
   command line arguments. Or edit config file.
   Use --help command line argument for more info.
 */
#define DB_USER "remote"
#define DB_PWD  "remote"
#define DB_HOST "dimaweber.homeip.net"
#define DB_NAME "enchsales"
#define DB_PORT 3306

class QTimer;

class Manager : public QStateMachine /* public QNetworkAccessManager */
{
    Q_OBJECT
    Q_PROPERTY(QVariantMap status         READ getStatus)
    Q_PROPERTY(QTime       timeToNextScan READ getTimeToNextScan)
    Q_PROPERTY(QString     state          READ getCurStateString WRITE setCurStateString )
    Q_PROPERTY(QVariantMap watchitems     READ watchItems WRITE setWatchedItems)
    Q_PROPERTY(bool        isScanning     READ isScanning)

public:
    Manager(QObject* parent = NULL);
    ~Manager();
    bool setWatchedItems(QVariantMap);
    QVariantMap watchItems();
    bool     isScanning();

public slots:
    void startScan ();
    void stopScan  ();
    void pauseScan ();
    void resumeScan();
    void shutdown ();

signals:
    void startRequest();
    void stopRequest();
    void pauseRequest();
    void resumeRequest();
    void shutdownRequest();

    void waiting();
    void scanStopped();
    void scanStarted();

    //void machineStopped();
    void nextAucStarted();
private:
    QString     cur_state_name;
    QString     getCurStateString(){return cur_state_name;}
    void        setCurStateString(QString name){cur_state_name = name;}

    void        initStateMachine();

    QTimer*     timer;
    ConfigClient     config;

    int         getScanMapSize();
    ScanAucThread* p_thread;

    QVariantMap getStatus();
    QTime       getTimeToNextScan();

private slots:
    void        onMachineStop();
};

#endif // MANAGER_H
