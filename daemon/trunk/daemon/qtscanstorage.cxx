#include "qtscanstorage.hxx"

QtScanStorage::QtScanStorage(QObject* parent)
    :ScanStorageInterface(parent)
{
//    p_th = new SaveLoadThread(&data_vault);
    totalSize  = 0;
}

//void  QtScanStorage::append(QVariantMap v)
//{
//    data_vault.append(v);
//    if (data_vault.size() > 2000)
//        save_to_disk(1000);
//}

//bool QtScanStorage::isEmpty()
//{
//    return data_vault.isEmpty();
//}

int QtScanStorage::size()
{
    return totalSize;
}

//QVariantMap QtScanStorage::takeFirst()
//{
//    if (size() < 100)
//        load_from_disk(200);
//    return data_vault.takeFirst();
//}

//void QtScanStorage::save_to_disk (int recordsNum)
//{
//    p_th->save(recordsNum);
//}

//void QtScanStorage::load_from_disk(int recordsNum)
//{
//    p_th->load(recordsNum);
//}

QtScanStorage::~QtScanStorage()
{
//    save_to_disk(-1);
}

bool QtScanStorage::initialize ()
{
    return ScanStorageInterface::initialize();
}

//QtScanStorage::SaveLoadThread::SaveLoadThread(QtScanStorage_Vault* p, QObject* parent)
//    :QThread(parent)
//{
//    p_vault = p;
//    batchSize = 100;
//}

//void QtScanStorage::SaveLoadThread::save(int recNum)
//{
//    recordsNum = recNum;
//    op = Save;
//    start();
//}

//void QtScanStorage::SaveLoadThread::load(int recNum)
//{
//    recordsNum = recNum;
//    op = Load;
//    start();
//}

//void QtScanStorage::SaveLoadThread::run()
//{
//    if (op == Save)
//    {
//        QVariantMap scan;
//        qxtLog->warning () << "Saving  data to disk";
//        QDir directory (".");
//        directory.mkdir ("datacash");
//        QString fname;
//        int saved = 0;
//        while (!p_vault->isEmpty () && recordsNum / batchSize > saved++)
//        {
//            qsrand (QDateTime::currentDateTime ().toTime_t ());
//            do
//            {
//                fname =
//                    QString ("datacash%1es%2.dat").arg (QDir::separator ()).
//                    arg ( qrand ());
//                qxtLog->trace () << QString ("Try temp file name: %1").arg (fname);
//            }
//            while (QFile::exists (fname));
//            QFile data (fname);
//            int cnt = 0;
//            if (data.open (QFile::WriteOnly | QFile::Append))
//            {
//                QDataStream out (&data);
//                while (cnt++ < batchSize+1 && !p_vault->isEmpty ())
//                {
//                    scan = p_vault->takeFirst ();
//                    out << scan;
//                }
//                data.close ();
//            }
//        }
//    }
//    else
//    {
//        int recordsLoaded = 0;
//        int file_index = 0;
//        QDir directory ("datacash");
//        if (directory.exists () )
//        {
//            QStringList files = directory.entryList (QDir::Files | QDir::Readable);
//            if (!files.isEmpty())
//            {
//                while (recordsLoaded < recordsNum && file_index < files.size ())
//                {
//                    QString fname = files[file_index];
//                    qxtLog->info () << QString ("load backup file %1").arg (fname);
//                    QFile data (directory.filePath (fname));
//                    if (data.open (QFile::ReadOnly))
//                    {
//                        QDataStream in (&data);
//                        QVariantMap record;
//                        while (!in.atEnd())
//                        {
//                            in >> record;
//                            p_vault->append(record);
//                            recordsLoaded++;
//                        }
//                        data.close();
//                        if (data.remove ())
//                            qxtLog->info () << QString ("Backup file %1 removed").arg (fname);
//                        else
//                            qxtLog->error () << QString ("Fail to remove backup file %1").arg (fname);
//                    }
//                    file_index++;
//                }
//            }
//        }
//    }
//}

//void QtScanStorage::SaveLoadThread::start()
//{
//    mutex.lock ();
//    QThread::start();
//    mutex.unlock ();
//}

GroupID QtScanStorage::startGroup(MetaType metadata)
{
    qsrand(QDateTime::currentDateTime().toTime_t());
    GroupHandler handler;
    handler.pos = NULL;
    handler.meta = metadata;
    handler.state = Invalid;
    int id;
    do
    {
        id = qrand() + 1;
    }while (groupCatalog.contains(id));
    groupCatalog[id] = handler;

    emit groupStarted(id);

    return id;
}

bool QtScanStorage::addItemToGroup(GroupID id, DataType data)
{
    if (groupCatalog[id].state != Invalid)
        return false;
    Group& group = groupData[id];
    data.groupid = id;
    group.append(data);
    emit groupItemAdded(id, data);
    return true;
}

bool QtScanStorage::finishGroup(GroupID id)
{
    if (groupCatalog[id].state != Invalid)
        return false;
    groupCatalog[id].state = Valid;
    totalSize += groupData[id].size();
    emit groupFinished(id);
    return true;
}

//QList<GroupID> QtScanStorage::listAvaliableGroups()
//{
//    QList<GroupID> list;
//    foreach(GroupID id, groupCatalog.keys())
//    {
//        if (groupCatalog[id].state  == Valid)
//            list.append(id);
//    }
//    return list;
//}

GroupID QtScanStorage::lockGroup(GroupID id)
{
    if (!id.isValid())
    {
        id = getFirstAvaliableId ();
    }

    if (id.isValid())
    {
        if (groupCatalog[id].state != Valid)
            return GroupID::null;

        groupCatalog[id].state = Locked;
        quint64* pos = new quint64;
        *pos = 0;
        groupCatalog[id].pos = pos;
        groupCatalog[id].count = groupData[id].size();
    }
    emit groupLocked(id);
    return id;
}

//QList<DataType> QtScanStorage::getGroupItems(GroupID id)
//{
//    if (groupCatalog[id].state == Locked)
//        return groupData[id];
//    else
//        return QList<DataType>();
//}

bool QtScanStorage::getNextGroupItem(GroupID id, DataType &item)
{
    if (groupCatalog[id].state != Locked)
        return false;
    quint64* position = reinterpret_cast<quint64*>(groupCatalog[id].pos);
    if (*position >= groupCatalog[id].count)
        return false;

    item = groupData[id][*position];
    (*position)++;
    return true;
}

bool QtScanStorage::releaseGroup(GroupID id)
{
    if (groupCatalog[id].state != Locked)
        return false;
    groupCatalog[id].state = Commited;
    delete (quint64*)groupCatalog[id].pos;
    groupCatalog[id].pos = NULL;
    totalSize -= groupData[id].size();
    emit groupReleased(id);
    return true;
}

GroupID QtScanStorage::getFirstAvaliableId ()
{
    foreach (GroupID id, groupCatalog.keys ())
    {
        if (groupCatalog[id].state == Valid)
            return id;
    }
    return 0;
}

bool QtScanStorage::rollbackGroup (GroupID id)
{
    if (groupCatalog[id].state != Locked)
        return false;
    groupCatalog[id].state = Valid;
    delete (quint64*)groupCatalog[id].pos;
    groupCatalog[id].pos = NULL;
    emit groupRolledBack(id);
    return true;
}

void QtScanStorage::clearCommited()
{

}

bool QtScanStorage::corruptGroup(GroupID id)
{
    groupCatalog[id].state = Corrupted;
    delete (quint64*)groupCatalog[id].pos;
    groupCatalog[id].pos = NULL;
    totalSize -= groupData[id].size();
    emit groupCorrupted(id);
    return true;
}

int QtScanStorage::getCount(QString sState)
{
    Q_UNUSED(sState)
    return 0;
}

#ifdef IS_PLUGIN
#include <QtPlugin>

Q_EXPORT_PLUGIN2(qtscanstorageplugin, QtScanStorage)
#endif
