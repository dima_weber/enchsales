#ifndef SCANSTORAGE_H
#define SCANSTORAGE_H

#include "datatypes.hxx"
#include "scanstorageinterface.hxx"

class QtScanStorage : public ScanStorageInterface
{
    Q_OBJECT
    Q_INTERFACES (ScanStorageInterface)
    GroupVault    groupData;
    GroupCatalog  groupCatalog;
protected:
    int  totalSize;
    virtual GroupID         getFirstAvaliableId();          /// return first avaliable valid group id
    virtual    void         clearCommited();
public:
    QtScanStorage(QObject* parent = NULL);
    virtual ~QtScanStorage();
    virtual bool            initialize();
    /* GroupAPI */
    virtual GroupID         startGroup(MetaType metadata);              /// createNew group, mark it as invalid
                                                                        /// @return group id
    virtual bool            addItemToGroup(GroupID id,                  /// add item to invalid group
                                   DataType data);                      /// @return false for closed group or if add failed.
    virtual bool            finishGroup(GroupID id);                    ///
    virtual bool            corruptGroup(GroupID id);
    virtual GroupID         lockGroup (GroupID = GroupID::null);        /// 0 - first avaliable
    virtual bool            getNextGroupItem(GroupID id, DataType& item);  /// Alternative style: iteration
    virtual bool            releaseGroup(GroupID );                     /// mark group as commited,
                                                                        /// group can be deleted at anytime;
    virtual bool            rollbackGroup(GroupID);

    virtual int                     size();                             /// Number of Valid items
public slots:
    virtual int             getCount(QString state);
};

#endif // SCANSTORAGE_H
