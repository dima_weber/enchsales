#include "scanaucthread.hxx"
#include "exception.hxx"
#include "qtscanstorage.hxx"
#include "datatypes.hxx"

#include <QxtLogger>
#include <QxtSignalWaiter>
#include <QxtJSON>
#include <QDir>
#include <QCoreApplication>

void ScanAucThread::nextAuc ()
{
    aucsTaken = 0;
    int logCharAttempts= 3;
    while (pWatchlist->next() && !quit_required)
    {
        try
        {
            if(p_bnetconnection->logAuc (pWatchlist->value ().login ()))
            {
                logCharAttempts = 3; // set new attempts for next one;
                int start, end, total, size;
                int itemid = pWatchlist->value().itemid;
                int cur = 0;
                bool ok = true;
                size = 50;
                QDateTime scantime = QDateTime::currentDateTime ();
                MetaType meta;
                meta.itemid = itemid;
                meta.scantime = scantime;
                GroupID gr_id = scan_map->startGroup (meta);
                emit nextAucStarted();
                do
                {
                    QString url =
                        QString
                        ("http://eu.wowarmory.com/auctionhouse/search.json?start=%2&id=%1&pageSize=%3").
                        arg (itemid).arg (cur).arg (size);

                    qxtLog->debug(QString(tr("request page with url %1")).arg(url));
                    QString data;
                    data = p_bnetconnection->getPage (url);
                    qxtLog->trace(tr("parse page content"));
                    ok = parse (data, scantime, gr_id, start, end, total, size);
                    if (ok)
                    {
                        aucsTaken += end - start;
                        msleep (1000 * config["Timers/page_sleep"].toInt ());
                        cur = end;
                    }
                } while (start != end && ok);
                if (ok)
                    scan_map->finishGroup (gr_id);
                else
                    scan_map->corruptGroup(gr_id);
            }
            else
            {
              logCharAttempts--;
              if (logCharAttempts > 0)
                  pWatchlist->prev ();
            }
        }
        catch (Exception e)
        {
            qxtLog->error () << e.getErrorMsg();
        }

        msleep (1000 * config["Timers/auc_sleep"].toInt ());
    }
}

bool ScanAucThread::parse (QString data, QDateTime scantime, GroupID gr_id, int &start, int &end, int &total, int &pageSize)
{
    bool ret = true;
    if (data.isEmpty ())
    {
        qxtLog->error(tr("Empty data recieved"));
        ret = false;
        return ret;
    }
    QVariantMap t = QxtJSON::parse (data).toMap ();
    QVariantMap error_map = t["error"].toMap ();
    QVariantMap aucSearch_map = t["auctionSearch"].toMap ();
    QVariantMap command_map = t["command"].toMap ();
    QVariantMap pra_map = t["pra"].toMap ();

    if (!error_map.isEmpty () && error_map["error"].toBool ())
    {
        int code = error_map["code"].toInt ();
        QString message = QString::fromUtf8(error_map["message"].toByteArray());
        qxtLog->warning() << QString(tr("error when parsing [%1]: %2")).arg(code).arg(message);
        if (code == 10005)
        {
            p_bnetconnection->disconnectDetected();
            qxtLog->info () << tr("Connection to BNet lost");
        }
        else if (code == 114)
        {
            qxtLog->info(tr("sleep for 1 minute to prevent extra spam to server"));
            sleep(60);
        }
        ret = false;
    }
    else
    {
        start = aucSearch_map["start"].toInt ();
        end = aucSearch_map["end"].toInt ();
        total = aucSearch_map["total"].toInt ();
        pageSize = command_map["pageSize"].toInt ();
        int faction = command_map["f"].toInt();
        QString srv = command_map["r"].toString ();

        QVariantList aucs_list = aucSearch_map["auctions"].toList ();
        QString itemname = "";
        if (aucs_list.size () > 0)
        {
            itemname = QString::fromUtf8 (aucs_list[0].toMap()["n"].toByteArray ());
            qxtLog->info () <<
            QString (tr("New scan. item: %7 (%1) srv: %5 faction:%6 start: %2 end: %3 total: %4 ")).
                arg (command_map["id"].toInt ()).arg (start).arg (end).arg (total).
                arg (QString::fromUtf8 (command_map["r"].toByteArray ())).arg (faction).
                arg(itemname);
            foreach (QVariant auc, aucs_list)
            {
                QVariantMap auc_map = auc.toMap ();

                auc_map["timestmp"] = scantime;
                switch (faction)
                {
                case 0:
                    auc_map["faction"] = "Альянс";
                    break;
                case 1:
                    auc_map["faction"] = "Орда";
                    break;
                default:
                    auc_map["faction"] = "Нейтральный";
                    break;
                }
                auc_map["srv"] = srv;
                auc_map["itemmods"] = "0:0:0:0:0:0";
                AucData aucdata(auc_map);
                ret = scan_map->addItemToGroup (gr_id, aucdata);
                if (!ret)
                    break;
            }
        }

    }
    return ret;
}

ScanAucThread::ScanAucThread(QObject* parent)
    :QThread(parent), config()
{
    quit_required = false;
    p_bnetconnection = NULL;
    pWatchlist = NULL;
#ifdef USE_PLUGINS
    QDir pluginsDir = QDir(qApp->applicationDirPath());
    pluginsDir.cd("plugins");
    QString pluginName = "libdbwatchlistplugin.so";
    qxtLog->debug () << QString("trying to load plugin: %1").arg (pluginsDir.absoluteFilePath (pluginName));
    //if (pluginsDir.exists(pluginName))
    {
        watchlistPluginLoader.setFileName(pluginsDir.absoluteFilePath (pluginName));
        if (watchlistPluginLoader.load())
        {
            QObject* watchlistPlugin = watchlistPluginLoader.instance();
            if (watchlistPlugin)
            {
                WatchListInterface* plugin = qobject_cast<WatchListInterface*>(watchlistPlugin);
                if (plugin)
                {
                    pWatchlist = plugin;
                    qxtLog->debug() << QString("Using watch list from plugin: %1").arg (pluginsDir.absoluteFilePath (pluginName));
                }
                else
                {
                    qxtLog->debug() << tr("Fail to cast.");
                }
            }
            else
            {
                qxtLog->debug () << tr("Fail to instancenate");
            }
        }
        else
        {
            qxtLog->debug () << tr("Fail to load plugin");
            qxtLog->debug () << watchlistPluginLoader.errorString ();
        }
    }

    //else
    //{
    //    qDebug () << tr("Not library");
    //}

#endif
    if (!pWatchlist)
    {
        qDebug() << "Using compiled-in watch list";
        pWatchlist = new WatchList(this);
    }
}

ScanAucThread::~ScanAucThread()
{
    delete p_bnetconnection;
    watchlistPluginLoader.unload ();
    qxtLog->trace() << tr("ScanAucThread destroyed");
}

bool ScanAucThread::isBNetConnected()
{
    if (!p_bnetconnection)
        return false;
    return p_bnetconnection->isConnectedBNet ();
}

AucLogin ScanAucThread::loggedAs()
{
    AucLogin l;
    if (p_bnetconnection)
    {
        l = p_bnetconnection->aucLogin ();
    }
    return l;
}

void ScanAucThread::stopScan ()
{
    quit_required = true;
}

void ScanAucThread::startScan ()
{
    pWatchlist->buildItemsList ();
    pWatchlist->resetIndex ();
    quit_required = false;
    start();
}

void ScanAucThread::pauseScan ()
{
    quit_required = true;
}

void ScanAucThread::resumeScan ()
{
    quit_required = false;
    start();
}

void ScanAucThread::run()
{
    startscantime = QDateTime::currentDateTime ();
    if (p_bnetconnection)
        delete p_bnetconnection;
    p_bnetconnection = new BNetConnection;
    quit_required = false;
    nextAuc();
}


QDateTime ScanAucThread::getScanStartTime()
{
    return startscantime;
}

int ScanAucThread::curItemId ()
{
    int itemid = -1;
    if (!pWatchlist->atEnd ())
    {
         itemid =  pWatchlist->value ().itemid;
    }
    return itemid;
}

int ScanAucThread::itemsDone ()
{
    return pWatchlist->currentIndex ();
}

int ScanAucThread::itemsLeft ()
{
    return pWatchlist->size () - itemsDone ();
}

QVariant ScanAucThread::getWatchList ()
{
    return pWatchlist->toVariant ();
}

bool ScanAucThread::setWatchList (QVariant m)
{
    return pWatchlist->fromVariant (m);
}

QVariantMap ScanAucThread::getStatus()
{
    QVariantMap map;

    map["BNet connection"]   = isBNetConnected ()? tr("online" ): tr("offline");
    map["current Srv"]       = loggedAs ().srv;
    map["current Character"] = loggedAs ().charname;
    map["current Item"]      = curItemId();
    map["aucs done"]         = itemsDone();
    map["aucs left"]         = itemsLeft();
    map["download speed"] = getDownloadSpeed ();
    return map;

}

double ScanAucThread::getDownloadSpeed ()
{
    double res = 0;
    if (isRunning ())
    {
        res = aucsTaken * 1.0 / getScanStartTime ().secsTo (QDateTime::currentDateTime ());
    }
    return res;
}
