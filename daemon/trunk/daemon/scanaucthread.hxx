#ifndef SCANAUCTHREAD_H
#define SCANAUCTHREAD_H

#include "bnetconnection.hxx"
#include "watchlist.hxx"
#include <QThread>
#include <QDateTime>
#include <QPluginLoader>

class ScanAucThread : public QThread
{
    Q_OBJECT
    BNetConnection*     p_bnetconnection;
    bool                quit_required;
    ConfigClient        config;
    QDateTime           startscantime;
    QPluginLoader       watchlistPluginLoader;
    WatchListInterface* pWatchlist;

    Q_PROPERTY(double downloadSpeed READ getDownloadSpeed)
    Q_PROPERTY (QVariantMap status READ getStatus)
public:
    ScanAucThread(QObject* parent = NULL);
    ~ScanAucThread();
    bool        isBNetConnected();
    AucLogin    loggedAs();
    QDateTime   getScanStartTime();
    bool        setWatchList(QVariant);
    QVariant    getWatchList();
    int         curItemId();
    int         itemsDone();
    int         itemsLeft();
public slots:
    void stopScan();
    void startScan();
    void pauseScan();
    void resumeScan();
signals:
    void nextAucStarted();
private slots:
    void start()
    {
        QThread::start ();
    }
    void run();
private:
    int aucsTaken;
    double getDownloadSpeed();

    QVariantMap getStatus();
    void        nextAuc();
    bool        parse (QString data, QDateTime scantime, GroupID grid, int &start, int &end, int &total, int &pageSize);
};

#endif // SCANAUCTHREAD_H
