#include "scanstate.hxx"

#include <QxtLogger>
#include <QCoreApplication>

ScannerState::ScannerState(ScanState st, QObject* pController, QState* parent)
    :QState(parent)
{
    state = st;
    pCont = pController;
    assignProperty (pCont, "state", statename ());
}

void ScannerState::onEntry (QEvent *event)
{
    Q_UNUSED(event)
    qxtLog->debug() << QString(tr("enter state: %1")).arg (statename());
}

void ScannerState::onExit (QEvent *event)
{
    Q_UNUSED(event)
    qxtLog->debug() << QString(tr("exit state: %1")).arg (statename());
}

QString getStateNameString(ScanState state)
{
    QString res="";
    switch (state)
    {
        case Stopped : res = QCoreApplication::translate("getStateNameString","Stopped");  break;
        case Paused:   res = QCoreApplication::translate("getStateNameString","Paused");   break;
        case Idle:     res = QCoreApplication::translate("getStateNameString","Idle");     break;
        case Scanning: res = QCoreApplication::translate("getStateNameString","Scanning"); break;
        case Starting: res = QCoreApplication::translate("getStateNameString","Starting"); break;
        case Stopping: res = QCoreApplication::translate("getStateNameString","Stopping"); break;
        case Pausing:  res = QCoreApplication::translate("getStateNameString","Pausing");  break;
        case Resuming: res = QCoreApplication::translate("getStateNameString","Resuming"); break;
        default:       res = QCoreApplication::translate("getStateNameString","unknown state");   break;
    }
    return res;
}

QString ScannerState::statename()
{
    return getStateNameString(state);
}
