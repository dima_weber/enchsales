#ifndef SCANSTATE_H
#define SCANSTATE_H

#include <QState>
#include <QString>

enum ScanState
{
      Stopped  = 0x01, /*!   stopScan.    :  startScan */
      Paused   = 0x02, /*!   pauseScan.   : stopScan, resumeScan */
      Idle     = 0x04, /*!  ,    .  : startScan */
      Scanning = 0x08, /*!   .  : stopScan, pauseScan */
      Starting = 0x10,
      Stopping = 0x20,
      Pausing  = 0x40,
      Resuming = 0x80
} ;

class ScannerState : public QState
{
    Q_OBJECT
    Q_PROPERTY(QString statename READ statename)

    QObject* pCont;
public:
    ScannerState(ScanState st, QObject* pController, QState* parent = NULL);
private slots:
    void onEntry (QEvent *event);
    void onExit (QEvent *event);
private:
    ScanState state;
    QString statename();
};

#endif // SCANSTATE_H
