#include "scanstorageinterface.hxx"
#ifdef USE_DBUS
#   include <QDBusConnection>
#   include "dbus/scanstorageadaptor.h"
#   include <QDBusServiceWatcher>
#endif

ScanStorageInterface::ScanStorageInterface(QObject* parent)
    :QObject(parent)
{
    initialized = false;
#ifdef USE_DBUS
    new ScanStorageAdaptor (this);
    QDBusConnection connection = QDBusConnection::sessionBus ();
    connection.registerObject ("/Storage", this);
#endif
}

ScanStorageInterface::~ScanStorageInterface()
{
#ifdef USE_DBUS
    QDBusConnection connection = QDBusConnection::sessionBus ();
    connection.unregisterObject ("/Storage");
#endif
}

QStringList ScanStorageInterface::getStatesList ()
{
    QStringList stateNamesList;
    for (int st=static_cast<int>(Undefined); st <= static_cast<int>(Corrupted); st++)
    {
        stateNamesList.append(groupStateToString(static_cast<GroupState>(st)));
    }
    return stateNamesList;
}

QMap<QString, int> ScanStorageInterface::getCounts ()
{
    QMap<QString, int> countsMap;
    if (initialized)
        foreach(QString st, getStatesList ())
        {
            countsMap[st] = getCount (st);
        }
    return countsMap;
}

bool ScanStorageInterface::initialize()
{
    initialized = true;
    return initialized;
}
