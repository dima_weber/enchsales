#ifndef SCANSTORAGEINTERFACE_HPP
#define SCANSTORAGEINTERFACE_HPP

#include <QObject>
#include "datatypes.hxx"
#include <QStringList>
#include <QMap>

class ScanStorageInterface : public QObject
{
    Q_OBJECT
    bool initialized;
protected:
                            ScanStorageInterface(QObject* =0);
    virtual GroupID         getFirstAvaliableId() =0;          /// return first avaliable valid group id
    virtual    void         clearCommited() =0;
public:
                            ~ScanStorageInterface();
    virtual bool            initialize() = 0;
    /* GroupAPI */
    /* Input API */
    virtual GroupID         startGroup(MetaType metadata) =0;
    virtual bool            addItemToGroup(GroupID id, DataType data) = 0;
    virtual bool            finishGroup(GroupID id) = 0;
    virtual bool            corruptGroup(GroupID id) = 0;
    /* Output API */
    virtual GroupID         lockGroup (GroupID = GroupID::null) =0;
    virtual bool            getNextGroupItem(GroupID id, DataType& item) =0;
    virtual bool            releaseGroup(GroupID ) =0;
    virtual bool            rollbackGroup(GroupID) =0;

    virtual int             size() =0;
public slots:
    QMap<QString, int>      getCounts();
    virtual int             getCount(QString state) =0;
    static QStringList      getStatesList();
signals:
            void            groupStarted(GroupID);
            void            groupItemAdded(GroupID, DataType);
            void            groupFinished(GroupID);
            void            groupCorrupted(GroupID);
            void            groupLocked(GroupID);
            void            groupReleased(GroupID);
            void            groupRolledBack(GroupID);
};
extern ScanStorageInterface*  scan_map;
Q_DECLARE_INTERFACE(ScanStorageInterface, "ScanStorageInterface")
#endif // SCANSTORAGEINTERFACE_HPP
