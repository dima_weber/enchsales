#include "service.hxx"

QwbrService::QwbrService(int argc, char** argv, const QString& name) :
    QtService<Core>(argc, argv, name)
{
    setServiceFlags (QtServiceBase::CanBeSuspended);
}

void QwbrService::resume ()
{
}

void QwbrService::start ()
{
    dynamic_cast<Core*>(application ())->runServer();
}

void QwbrService::stop ()
{
    dynamic_cast<Core*>(application ())->shutdown ();
}

void QwbrService::pause ()
{

}
