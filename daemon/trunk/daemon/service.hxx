#ifndef SERVICE_HXX
#define SERVICE_HXX

#include <qtservice.h>
#include "core.hxx"

class QwbrService : public QtService<Core>
{
public:
    explicit QwbrService(int argc, char** argv, const QString& name);
protected:
    virtual void start();
    virtual void stop();
    virtual void pause();
    virtual void resume();
};

#endif // SERVICE_HXX
