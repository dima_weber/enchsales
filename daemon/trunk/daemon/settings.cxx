#include "settings.hxx"
#include <stdlib.h>
#ifdef USE_DBUS
#    include "dbus/settingsadaptor.h"
#endif
#include <QMutex>
#include <QxtLogger>
#include <QxtCommandOptions>
#include <QRegExp>
#include <QDir>

Config* Config::p_instance = NULL;
int Config::refcount = 0;

QMutex create_instance_mutex;
Config* Config::instance ()
{
    if (!Config::p_instance)
    {
       Config::p_instance = new Config ();
        atexit (Config::destroyInstance );
    }
    return Config::p_instance;
}

Config::Config(QObject *parent)
    :QObject(parent)
{
    p_settings = new QSettings;
#ifdef  USE_DBUS
    new ConfigAdaptor (this);
    QDBusConnection connection = QDBusConnection::sessionBus ();
    connection.registerObject ("/Config", this);
#endif
    buildParamList();
    readConfig ();
}

#include <iostream>
Config::~Config ()
{
#ifdef  USE_DBUS
    QDBusConnection connection = QDBusConnection::sessionBus ();
    connection.unregisterObject ("/Config");
#endif
    delete p_settings;
}

void Config::readConfig ()
{
    ConfParam param;
    foreach(param, getConfParams ())
    {
        settings_map[param.name] = p_settings->value (param.name, param.defValue);
    }
}

void Config::writeConfig ()
{
    foreach (QString key, settings_map.keys ())
    {
        p_settings->setValue (key, settings_map[key]);
    }
    p_settings->sync ();
}

QVariant& Config::operator [] (const QString & key)
{
    return settings_map[key];
}

QString Config::getParam (QString key) const
{
    return settings_map[key].toString();
}

QString Config::fileName ()
{
    return p_settings->fileName ();
}

void Config::setParam (QString key, QString value)
{
    settings_map[key] = value;
    emit paramChanged (key);
}

void Config::destroyInstance ()
{
    if (Config::p_instance)
    {
        delete Config::p_instance;
        Config::p_instance = NULL;
    }
}

void Config::aquire ()
{
    Config::refcount++;
}

void Config::release ()
{
    Config::refcount--;
    if (Config::refcount == 0)
    {
        Config::destroyInstance ();
    }
}

ConfigClient::ConfigClient()
{
    pConfig = Config::instance ();
    pConfig->aquire();
}

ConfigClient::~ConfigClient ()
{
    pConfig->release();
}

QVariant& ConfigClient::operator [] (const QString& key)
{
    return (*pConfig)[key];
}

QString ConfigClient::fileName ()
{
    return pConfig->fileName ();
}

void ConfigClient::readConfig ()
{
    pConfig->readConfig ();
}

void ConfigClient::writeConfig ()
{
    pConfig->writeConfig ();
}

int ConfigClient::processCommandLine (int argc, char *argv[])
{
    return pConfig->processCommandLine (argc, argv);
}

QString ConfigClient::getParam (QString key) const
{
    return pConfig->getParam (key);
}

void ConfigClient::setParams (QString key, QString value)
{
    pConfig->setParam (key, value);
}

void Config::buildParamList ()
{
    ConfParam param;
    paramList.clear ();
    param.name = "port";
    param.defValue = 3306;
    param.commandLineAliase = "p";
    param.commandLineDesc = QString ("database port. Default: %def%");
    param.commandLineKey = "port";
    param.commandLineHasValue = QxtCommandOptions::Required;


    paramList.append (param);

    paramList.append (ConfParam("host","db.example.com","host","database host address. Default: %def%",QString::null,QxtCommandOptions::Required));
    paramList.append (ConfParam("user", "dbuser","user","database user. Default: %def%", QString::null,  QxtCommandOptions::Required));
    paramList.append (ConfParam("password","dbpass","password","database password. Default: %def%",QString::null,QxtCommandOptions::Required));
    paramList.append (ConfParam("dbname", "dbname","dbname","database name. Default: %def%",QString::null,QxtCommandOptions::Required));

    paramList.append (ConfParam("DB/batchsize",100));
    paramList.append (ConfParam("no-inserts", true, "no-inserts","do not insert new data to DB. Default: %def%",QString::null,QxtCommandOptions::Optional));
    paramList.append (ConfParam("datadir", ".", "datadir", "data directory (%def%)", QString::null,QxtCommandOptions::Required));
    paramList.append (ConfParam("i18ndir", ".", "i18ndir", "translations directory (%def%)",QString::null,QxtCommandOptions::Required));
    QString msg = "Log level. Possible values are: ";
    QString allowedValRx = "";
    QString val;
    for (int i = 0; i <= 7; i++)
    {
        val = QxtLogger::logLevelToString (static_cast<QxtLogger::LogLevel>(1 << i));
        msg = QString ("%1 %2").arg(msg).arg (val);
        allowedValRx = QString("%1%2%3").arg (allowedValRx).arg (allowedValRx.isEmpty ()?"":"|").arg (val);
    }
    msg = QString (msg + ". Default: %def%");
    paramList.append (ConfParam("loglevel", "InfoLevel", "loglevel", msg,QString::null, QxtCommandOptions::Required,allowedValRx));
    paramList.append (ConfParam("consolelog", false, "consolelog", "Enable console log output. Default: %def%", QString::null, QxtCommandOptions::Optional));
    paramList.append (ConfParam("logfile", "/tmp/daemon.log", "logfile", "filename of logfile. Default: %def%.", QString::null, QxtCommandOptions::Required));
    paramList.append (ConfParam("trunclogfile", false, "trunclogfile", "Truncate log file at start. Default: %def%", QString::null, QxtCommandOptions::Optional));
    paramList.append (ConfParam("daemon", false, "daemon", "Run as daemon"));
    paramList.append (ConfParam("takeover", false, "takeover", "Take over existing daemon. Default: %def%",QString::null,QxtCommandOptions::Optional));

    paramList.append (ConfParam("BNet/BNetLogin", "email@example.com","bnlogin","BNet login. Default: %def%",QString::null,QxtCommandOptions::Required));
    paramList.append (ConfParam("BNet/BNetPassword", "********", "bnpassword", "BattleNet password",QString::null,QxtCommandOptions::Required));
    paramList.append (ConfParam("BNet/BNetEnabled", true, "bnetenabled","Connect to BattleNet and scan auction. Default: %def%",QString::null,QxtCommandOptions::Optional));

    paramList.append (ConfParam("mail/use_mail_log", true, "usemaillog", "Email Errors.", QString::null, QxtCommandOptions::Optional));
    paramList.append (ConfParam("mail/server", "smtp.example.com"));
    paramList.append (ConfParam("mail/useSSL", false));
    paramList.append (ConfParam("mail/port", 25));
    paramList.append (ConfParam("mail/startTLS", true));
    paramList.append (ConfParam("mail/login", "mail@server.com"));
    paramList.append (ConfParam("mail/password", "*******"));
    paramList.append (ConfParam("mail/from", "from@mail.com"));
    paramList.append (ConfParam("mail/to", "to@mail.com"));

    paramList.append (ConfParam("Timers/page_sleep", 1));
    paramList.append (ConfParam("Timers/auc_sleep", 1));
    paramList.append (ConfParam("Timers/scan_sleep", 900));
    paramList.append (ConfParam("Timers/db_sleep", 2));
    paramList.append (ConfParam("Timers/icon_sleep", 2700));
}

int Config::processCommandLine (int argc, char *argv[])
{
    QxtCommandOptions opt;

    opt.addSection ("general");
    opt.add ("save", QString ("save settings to ini file (%1)").arg (fileName ()),QxtCommandOptions::NoValue);

    opt.add ("help", "show this help text",QxtCommandOptions::NoValue);
    opt.alias ("help", "h");

    opt.addSection ("settings");
    foreach(ConfParam param, Config::instance ()->getConfParams ())
    {
        if (!param.commandLineKey.isEmpty ())
        {
            QString msg = param.commandLineDesc.replace ("%def%", getParam (param.name));
            opt.add (param.commandLineKey, msg, param.commandLineHasValue);
            if (!param.commandLineAliase.isEmpty())
                opt.alias (param.name, param.commandLineAliase);
        }
    }

    opt.parse (argc, argv);

    if (opt.count ("help") || opt.showUnrecognizedWarning ())
    {
        opt.showUsage ();
        return -1;
    }

    foreach(ConfParam param, Config::instance ()->getConfParams ())
    {
        if (!param.commandLineKey.isEmpty ())
        {
            if (opt.count (param.commandLineKey))
            {
                if (param.commandLineHasValue)
                {
                    QVariant val = opt.value (param.commandLineKey);
                    if (val.toString ().isEmpty ())
                        setParam (param.name, "true");
                    else
                    {
                        if (!param.validValuesRegExp.isNull ())
                        {
                            QRegExp rx(param.validValuesRegExp, Qt::CaseInsensitive);
                            if (!rx.exactMatch (val.toString ()))
                            {
                                qxtLog->warning () << QString("Invalid value (%1) for parameter %2. Default value (%3) will be used").arg(val.toString ()).arg(param.name).arg(param.defValue.toString ());
                                val = param.defValue;
                            }
                        }
                        setParam(param.name, val.toString ());
                    }
                }
                else
                    setParam (param.name, "true");
            }
        }
    }

    if (opt.count ("save"))
    {
        writeConfig ();
        return -1;
    }
    return 0;
}
