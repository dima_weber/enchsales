#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QxtCommandOptions>

struct ConfParam
{
    QString name;
    QVariant defValue;
    QString commandLineKey;
    QString commandLineDesc;
    QString commandLineAliase;
    QxtCommandOptions::ParamType    commandLineHasValue;
    QString validValuesRegExp;

    ConfParam(QString _name = QString::null, QVariant _defValue = QVariant(),
              QString _commandLineKey = QString::null, QString _commandLineDesc = QString::null,
              QString _commandLineAliase = QString::null,
              QxtCommandOptions::ParamType _commandLineHasValue = QxtCommandOptions::NoValue,
              QString regExp = QString::null)
    {
        name = _name;
        defValue = _defValue;
        commandLineKey = _commandLineKey;
        commandLineDesc = _commandLineDesc;
        commandLineAliase = _commandLineAliase;
        commandLineHasValue = _commandLineHasValue;
        validValuesRegExp = regExp;
    }

    ConfParam(const ConfParam& p)
    {
        name = p.name;
        defValue = p.defValue;
        commandLineKey = p.commandLineKey;
        commandLineDesc = p.commandLineDesc;
        commandLineAliase = p.commandLineAliase;
        commandLineHasValue = p.commandLineHasValue;
        validValuesRegExp = p.validValuesRegExp;
    }

    ConfParam& operator=(const ConfParam& p)
    {
        name = p.name;
        defValue = p.defValue;
        commandLineKey = p.commandLineKey;
        commandLineDesc = p.commandLineDesc;
        commandLineAliase = p.commandLineAliase;
        commandLineHasValue = p.commandLineHasValue;
        validValuesRegExp = p.validValuesRegExp;
        return *this;
    }

};

class Config : public QObject
{
    Q_OBJECT
private:
    static Config* p_instance;
    QSettings * p_settings;
    QList<ConfParam> paramList;
    QVariantMap settings_map;
    Config(QObject* parent = NULL);
    ~Config();
    static void destroyInstance();
private:
    static Config* instance();
    QVariant& operator[](const QString& key);
    QString fileName();
    Q_PROPERTY(QVariantMap config READ getParams)
private slots:
    int processCommandLine(int argc, char *argv[]);
    void readConfig ();
    void writeConfig ();
    QString getParam(QString key) const;
    void setParam(QString key, QString value);
signals:
    void paramChanged(QString key);
private:

    QVariantMap getParams()
    {return settings_map;}

    QList<ConfParam> getConfParams()
    {return paramList;}
    void buildParamList();

    static int  refcount;
    void aquire();
    void release();

    friend class ConfigClient;
    friend class ConfigAdaptor;
    friend int command_line_handle (int argc, char *argv[]);
};

class ConfigClient : public QObject
{
    Q_OBJECT
private:
    Config* pConfig;
public:
    ConfigClient();
    ~ConfigClient();

    QVariant& operator[](const QString& key);
    QString fileName();
public slots:
    int processCommandLine(int argc, char *argv[]);
    void readConfig ();
    void writeConfig ();
    QString getParam(QString key) const;
    void setParams(QString key, QString value);
};

#endif // SETTINGS_H
