#ifndef SIGNALHANDLER_H
#define SIGNALHANDLER_H
#include <QObject>
#include <QThread>

class QSocketNotifier;
class SignalHandler : public QObject
{
    Q_OBJECT

      public:
        SignalHandler(QObject *parent = 0);
        ~SignalHandler();

        // Unix signal handlers.
        static void hupSignalHandler(int unused);
        static void intSignalHandler(int unused);
        static void termSignalHandler(int unused);

        static int setup_unix_signal_handlers();

    public slots:
        // Qt signal handlers.
        void handleSigHup();
        void handleSigInt();
        void handleSigTerm();

      private:
        static int sighupFd[2];
        static int sigtermFd[2];
        static int sigintFd[2];

        QSocketNotifier *snHup;
        QSocketNotifier *snTerm;
        QSocketNotifier *snInt;

signals:
    void CtrlCPressed();
};

class SignalHandlerThread : public QThread
{
    SignalHandler* p_handle;
protected:
    virtual void run();
public:
    SignalHandlerThread(QObject* parent = NULL);
    ~SignalHandlerThread();
};

#endif // SIGNALHANDLER_H
