#include "storagelistener.hxx"
#include <QxtLogger>

StorageListener::StorageListener(ScanStorageInterface* pStorage, QObject *parent) :
    QObject(parent)
{
    this->pStorage = pStorage;
    connect (pStorage, SIGNAL(groupStarted(GroupID)), this, SLOT(onGroupAdd(GroupID)));
    connect (pStorage, SIGNAL(groupFinished(GroupID)), this, SLOT(onGroupFinish(GroupID)));
    connect (pStorage, SIGNAL(groupItemAdded(GroupID,DataType)), this,SLOT(onAddItem(GroupID,DataType)));
}

void StorageListener::onGroupAdd(GroupID groupID)
{
    QString str = QString(tr("Group Add: %1")).arg(groupID.toString());

    qxtLog->debug(str);

    count = 0;
    avgPrice = 0;
    bidCount = 0;
    avgBidPrice = 0;
}

void StorageListener::onGroupFinish(GroupID groupID)
{
    QString str = QString(tr("Group Finish: %1")).arg(groupID.toString());

    qxtLog->debug(str);

    if (count)
    {
        avgPrice /= count;
        str = QString(tr("Buyout count: %1. Avg Buyout Price: %2")).arg (count).arg (formatMoney (avgPrice));
        qxtLog->info () << str;
    }
    if (bidCount)
    {
        avgBidPrice /= bidCount;
        str = QString(tr("Bid count: %1. Avg Bid Price: %2")).arg (bidCount).arg (formatMoney (avgBidPrice));
        qxtLog->info () << str;
    }
}

void StorageListener::onAddItem (GroupID groupID, DataType data)
{
    Q_UNUSED(groupID)
    if (data.buy > 0)
    {
        count += data.quan;
        avgPrice += data.buy;
    }
    bidCount += data.quan;
    avgBidPrice += data.ppuBid;
}

QString StorageListener::formatMoney (long val)
{
    QString str("%1g %2s %3c");
    str = str.arg (val / 10000).arg (val % 10000 / 100).arg(val%100);
    return str;
}
