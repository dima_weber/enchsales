#ifndef STORAGELISTENER_HXX
#define STORAGELISTENER_HXX

#include <QObject>
#include "scanstorageinterface.hxx"

class StorageListener : public QObject
{
    Q_OBJECT
    ScanStorageInterface* pStorage;
public:
    explicit StorageListener(ScanStorageInterface* pStorage, QObject *parent = 0);
    QString  formatMoney(long);
signals:

private:
     long    avgPrice;
      int    count;
      int    bidCount;
     long    avgBidPrice;
public slots:
    void onGroupAdd     (GroupID groupID);
    void onGroupFinish  (GroupID groupID);
    void onAddItem      (GroupID groupID, DataType data);
};

#endif // STORAGELISTENER_HXX
