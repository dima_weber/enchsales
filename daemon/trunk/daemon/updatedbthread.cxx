#include "updatedbthread.hxx"
//#include "scanmap.hxx"
#include "qtscanstorage.hxx"

#include <QSqlError>
#include <QTextCodec>
#include <QDateTime>

#include <QxtLogger>
#include <QFile>
#include <QTextStream>

#ifdef USE_DBUS
#   include "dbus/updatedbadaptor.h"
#endif

#include "db.hxx"

UpdateDBThread::iconinfo::iconinfo ()
{
    path = "";
    alreadyInserted = false;
}

UpdateDBThread::UpdateDBThread ( QObject * parent):QThread (parent), config()
{
    connected  = false;
    quit_update = false;
#ifdef USE_DBUS
    new UpdateDBThreadAdaptor(this);
    QDBusConnection connection = QDBusConnection::sessionBus ();
    connection.registerObject ("/DB", this);
#endif
}

UpdateDBThread::~UpdateDBThread()
{
#ifdef USE_DBUS
    QDBusConnection connection = QDBusConnection::sessionBus ();
    connection.unregisterObject ("/DB");
#endif
    if (QSqlDatabase::contains ("update_conn"))
        QSqlDatabase::removeDatabase ("update_conn");
    qxtLog->trace() << tr("UpdateDBThread destroyed");
}

QString UpdateDBThread::get_next_sql (AucData scan)
{
    QString sql_statement = QString ("call mw_newWatch('%1', '%2', '%3', %4, '%5', %6, "
                                     " %7, '%8', %9, %11, %10, %12,  " " 1, 0, 'qt')")
/*1*/ .arg (QTextCodec::codecForName ("UTF8")->
                    toUnicode (scan.srv.toAscii ()))
/*2*/ .arg (QTextCodec::codecForName ("UTF8")->
                    toUnicode (scan.faction.toAscii ()))
/*3*/ .arg (scan.timestmp.toString ("yyyyMMddHHmmss"))
/*4*/ .arg (scan.itemid)
/*5*/ .arg (scan.itemmods)
/*6*/ .arg (scan.auc)
/*7*/ .arg (scan.quan)
/*8*/ .arg (QTextCodec::codecForName ("UTF8")->
                    toUnicode (scan.seller.toAscii ()))
/*9*/ .arg (scan.buy)
/*10*/ .arg (scan.nbid)
/*11*/ .arg (scan.ppuBid)
/*12*/ .arg (scan.time);
    return sql_statement;
}

bool UpdateDBThread::process_map ()
{
    qxtLog->trace() << tr("process_map started");
    start_time = QDateTime::currentDateTime ();
    bool ok = false;
    dbg_QSqlDatabase db = QSqlDatabase::database ("update_conn");
    if (db.isValid () && db.open ())
    {
        connected = true;
        itemsSent = 0;
        qxtLog->trace() << tr("DB connection  opened");
        GroupID gr_id = GroupID::null;
        while ( !quit_update && (gr_id = scan_map->lockGroup ()) != GroupID::null)
        {
            qxtLog->debug () << QString(tr("new data group[%1] in storage locked")).arg (gr_id.id);
            AucData scan;
            qxtLog->debug () << tr("begin transaction");
            //ok = q.exec ("START TRANSACTION");
            ok = db.transaction ();
            dbg_QSqlQuery q (db);
            while (ok && scan_map->getNextGroupItem (gr_id, scan) && !quit_update )
            {
                QString str = (QString)scan;
                QString msg = QString(tr("new item retrieved: %1")).arg (str);
                qxtLog->trace () <<  msg;
                icons_map[scan.itemid].path = scan.icon;
                QString sql = get_next_sql(scan);
                qxtLog->trace () << QString("SQL: %1").arg (sql);
                if (config["no-inserts"].toBool ())
                {
                    ok = true;
                    QFile file("./dummy.txt");
                    file.open (QFile::Append);
                    QTextStream st(&file);
                    st << sql << "\n";
                    file.close ();
                    msleep (100);
                }
                else
                    ok = q.exec (sql);


                if (!ok)
                {
                    qxtLog->error () << QString("Database Error [%1]: %2")
                                            .arg (q.lastError ().number ())
                                            .arg (q.lastError ().text ());
                    qxtLog->debug (q.lastQuery ());
                    //db->close ();
                }
                else
                {
                    itemsSent++;
                    qxtLog->trace () << QString (tr("%1 inserts. done")).arg (q.numRowsAffected ());
                }
            }

            if (!ok || quit_update)
            {
                qxtLog->error () << tr("ROLLBACK transaction");
                //q.exec ("ROLLBACK");
                db.rollback ();
                scan_map->rollbackGroup(gr_id);
                //break;
            }
            else
            {
                qxtLog->debug () << tr("Commit transaction");
                //ok = q.exec ("COMMIT");
                ok = db.commit ();
                if (!ok)
                {
                    qxtLog->error() << tr("Fail to commit transaction");
                    scan_map->rollbackGroup (gr_id);
                }
                else
                {
                    qxtLog->debug () << tr("Transaction commited");
                    scan_map->releaseGroup (gr_id);
                }
            }
        }

        if (ok && !quit_update)
        {
            foreach (int itemid, icons_map.keys ())
            {
                if (!icons_map[itemid].alreadyInserted)
                {
                    dbg_QSqlQuery query (db);
                    query.
                        prepare ("INSERT INTO iteminfo (itemid, icon_path) values (:itemid, :icon_path)"
                                 "ON DUPLICATE KEY UPDATE  icon_path=VALUES(icon_path)");
                    query.bindValue (":itemid", itemid);
                    query.bindValue (":icon_path",
                                     QString ("http://eu.wowarmory.com/wow-icons/_images/43x43/%1.png").
                                     arg (icons_map[itemid].path));
                    icons_map[itemid].alreadyInserted = query.exec ();
                }
            }
        }
        db.close ();
        connected = false;
    }
    else
    {
        qxtLog->error (db.lastError ().text ());
        ok = false;
    }

    return ok;
}


void UpdateDBThread::run ()
{
    qxtLog->debug() << tr("UpdateBD thread is started");
    quit_update = false;
    dbg_QSqlDatabase db = QSqlDatabase::addDatabase ("QMYSQL", "update_conn");
    db.setHostName (config["host"].toString ());
    db.setDatabaseName (config["dbname"].toString ());
    db.setUserName (config["user"].toString ());
    db.setPassword (config["password"].toString ());
    db.setPort (config["port"].toInt ());
    connected = false; // overkill, actually, but looks nice to set it here.

    //forever
    itemsSent = 0;
    while (!quit_update)
    {
        process_map ();
        msleep (1000 * config["Timers/db_sleep"].toInt ());
    }

    if (db.isValid ())
    {
        if (db.isOpen ())
            db.close();

    }
    connected = false;
    qxtLog->debug() << tr("UpdateBD thread is finished");
}

QVariantMap UpdateDBThread::getStatus()
{
    QVariantMap map;
    map["connected"] = isConnected();
    map["upload speed (aucs per sec)"] = getUploadSpeed ();
    map["last DB connect time"] = start_time.toString ();
    return map;
}

bool UpdateDBThread::isConnected()
{
    return connected;
    /*
    QSqlDatabase db = QSqlDatabase::database ("update_conn");
    return db.isValid () && db.isOpen () && !db.isOpenError ();
    */
}

void UpdateDBThread::stop()
{
    qxtLog->debug() << tr("holy crap! someone want to stop us!");
    quit_update = true;
}

double UpdateDBThread::getUploadSpeed ()
{
    double res = 0;
    if (isConnected ())
    {
        res = itemsSent * 1.0 / start_time.secsTo (QDateTime::currentDateTime ());
    }
    return res;
}
