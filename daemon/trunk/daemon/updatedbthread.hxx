#ifndef UPDATEDBTHREAD_H
#define UPDATEDBTHREAD_H

#include <QThread>
#include <QMap>
#include <QString>
#include <QVariantMap>
#include "settings.hxx"
#include <QDateTime>
#include "datatypes.hxx"

class UpdateDBThread :public  QThread
{
    Q_OBJECT
    Q_PROPERTY(bool connected READ isConnected)
    Q_PROPERTY(QVariantMap status READ getStatus)
    Q_PROPERTY(double uploadSpeed READ getUploadSpeed)

    struct iconinfo
    {
        QString path;
        bool alreadyInserted;

        iconinfo();
    };
    QMap<int, iconinfo> icons_map;
public:
    UpdateDBThread( QObject* parent = NULL);
    ~UpdateDBThread();

    void stop();
private:
    bool quit_update;
    bool connected;
    QString get_next_sql(AucData scan);
    bool process_map();
    void backup_to_disk();
    void run();

    QVariantMap getStatus();
    bool isConnected();

    int itemsSent;
    QDateTime start_time;
    double getUploadSpeed();

    ConfigClient config;
};

#endif // UPDATEDBTHREAD_H
