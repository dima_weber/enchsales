#include "updateiteminfothread.hxx"
#include <QTimer>
#include <QSqlError>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QxtLogger>
#include "db.hxx"

UpdateItemInfoThread::UpdateItemInfoThread(QObject* parent)
    : QThread(parent), config ()
{
    timer = NULL;
}

UpdateItemInfoThread::~UpdateItemInfoThread()
{
    if (QSqlDatabase::contains ("update_info_conn"))
        QSqlDatabase::removeDatabase ("update_info_conn");
    qxtLog->trace() << tr("UpdateItemInfoThread destroyed");
}

void UpdateItemInfoThread::run ()
{
    createDBConnection ();
    startTimer ();

    updateInfo ();
    exec ();

    stopTimer ();
    deleteDBConnection ();
}


void UpdateItemInfoThread::updateInfo ()
{
    pathes.clear ();
    dbg_QSqlDatabase db = QSqlDatabase::database ("update_info_conn");
    if (db.open ())
    {
        dbg_QSqlQuery query (db);
        QString sql = "SELECT itemid, icon_path FROM iteminfo WHERE icon IS NULL";
        if (!query.exec (sql))
        {
            qxtLog->error (query.lastError ().text ());
            return;
        }
        while (query.next ())
        {
            pathes.append (query.value (1).toString ());
        }
        currentIndex = 0;

        downloadNextIcon ();
    }
    else
    {
        qxtLog->error (db.lastError ().text ());
    }
}

void UpdateItemInfoThread::downloadNextIcon ()
{
    if (currentIndex < pathes.size ())
    {
        QUrl url (pathes[currentIndex]);
        QNetworkRequest req (url);
        connect (manager.get (req), SIGNAL (finished ()), SLOT (onDownloadFinished ()));
    }
}

void UpdateItemInfoThread::onDownloadFinished ()
{
    QNetworkReply *reply = dynamic_cast < QNetworkReply * >(sender ());
    QByteArray buff = reply->readAll ();

    QString path = pathes[currentIndex];
    dbg_QSqlDatabase db = QSqlDatabase::database ("update_info_conn");
    dbg_QSqlQuery query (db);
    query.prepare ("UPDATE iteminfo SET icon=:icon where icon_path=:path");
    query.bindValue (":icon", buff);
    query.bindValue (":path", path);
    query.exec ();

    currentIndex++;
    if (currentIndex < pathes.size ())
        downloadNextIcon ();
    else
        db.close ();

    reply->deleteLater ();
}

void UpdateItemInfoThread::stop ()
{
    quit();
}

bool UpdateItemInfoThread::createDBConnection ()
{
    dbg_QSqlDatabase db = QSqlDatabase::addDatabase ("QMYSQL", "update_info_conn");
    db.setHostName (config["host"].toString ());
    db.setDatabaseName (config["dbname"].toString ());
    db.setUserName (config["user"].toString ());
    db.setPassword (config["password"].toString ());
    db.setPort (config["port"].toInt ());

    return true;
}

bool UpdateItemInfoThread::deleteDBConnection ()
{
    QSqlDatabase db = QSqlDatabase::database ("update_info_conn");
    if (db.isValid ())
    {
        if (db.isOpen())
            db.close();
    }
    return true;
}

bool UpdateItemInfoThread::startTimer ()
{
    // stopTimer ();
    timer = new QTimer;
    timer->start (config["Timers/icon_sleep"].toInt () * 1000);
    connect (timer, SIGNAL (timeout ()), SLOT (updateInfo ()));

    return true;
}

bool UpdateItemInfoThread::stopTimer ()
{
    if (timer)
        timer->stop ();
    delete timer;
    timer = NULL;

    return true;
}
