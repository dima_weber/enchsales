#ifndef CLASSES_H
#define CLASSES_H

#include <QNetworkAccessManager>
#include <QThread>

#include "settings.hxx"

class QTimer;

class UpdateItemInfoThread : public QThread
{
    Q_OBJECT
    void run();
public:
    UpdateItemInfoThread(QObject* parent = NULL);
    ~UpdateItemInfoThread();
public slots:
    void stop();
private slots:
    void updateInfo();
    void downloadNextIcon();
    void onDownloadFinished();
private:
    QTimer* timer;
    QNetworkAccessManager manager;
    QList<QString> pathes;
    int currentIndex;
    ConfigClient config;

    bool createDBConnection();
    bool deleteDBConnection();
    bool startTimer();
    bool stopTimer();
};

#endif // MANAGER_H
