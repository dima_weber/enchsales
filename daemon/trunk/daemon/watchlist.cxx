#include "watchlist.hxx"
#include <QVariant>
#include "exception.hxx"

#include <QxtLogger>
#include "db.hxx"
#include <QSqlError>

#include <QSqlRecord>
#include <QSqlField>
#include <QSqlDriver>
#include <QDebug>

#include <QDir>
#include <QDataStream>
#include <QDateTime>

WatchList::WatchList(QObject* parent )
    :QObject(parent), config ()
{
    dbg_QSqlDatabase db = QSqlDatabase::addDatabase ("QMYSQL", "watch_list_db");
    db.setHostName (config["host"].toString ());
    db.setDatabaseName (config["dbname"].toString ());
    db.setUserName (config["user"].toString ());
    db.setPassword (config["password"].toString ());
    db.setPort (config["port"].toInt ());

    clear ();
}

WatchList::~WatchList ()
{
    dbg_QSqlDatabase db = QSqlDatabase::database ("watch_list_db");
    if (db.isValid ())
    {
        if (db.isOpen ())
            db.close();
        QSqlDatabase::removeDatabase ("watch_list_db");
    }
}

WatchItem WatchList::value()
{
    WatchItem wi;
    if (isValidIndex (current_index))
        wi = data[current_index];
    else
    {
        THROW_EXCEPTION(tr("Index out of range"));
    }
    return wi;
}

bool WatchList::next()
{
    current_index++;
    return isValidIndex( currentIndex() );
}

bool WatchList::prev()
{
    current_index--;
    return isValidIndex( currentIndex() );
}

int WatchList::currentIndex()
{
    return current_index;
}

int WatchList::size()
{
    return data.size();
}

bool WatchList::isValidIndex (int idx)
{
    return idx < size() && idx >= 0;
}

bool WatchList::atEnd()
{
    return !isValidIndex (current_index);
}

bool WatchList::isEmpty()
{
    return data.isEmpty ();
}

bool WatchList::addItem (WatchItem wi)
{
    data.append (wi);
    return true;
}

bool WatchList::removeItem (int index)
{
    if (isValidIndex (index))
    {
        data.removeAt (index);
        return true;
    }
    return false;
}

void WatchList::clear ()
{
    data.clear ();
    resetIndex();
}

void WatchList::resetIndex ()
{
    current_index = -1;
}

QVariant WatchList::toVariant()
{
    QVariantList list;
    foreach(WatchItem item, data)
    {
        list.append(item.toQVariant());
    }
    QVariantMap map;
    map["current_item"] = currentIndex ();
    map["item_list"] = list;
    return map;
}

bool WatchList::fromVariant(QVariant m)
{
    if (!m.canConvert<QVariantMap>())
    {
        return false;
    }
    QVariantMap map = m.toMap ();
    if (!map.contains("current_item") || !map.contains("item_list"))
        return false;
    current_index = map["current_item"].toInt();
    QVariantList item_list = map["item_list"].toList();
    foreach (QVariant v, item_list)
    {
        WatchItem wi;
        wi.fromQVariant (v);
        data.append (wi);
    }
    return true;
}

void WatchList::buildItemsList ()
{
    bool ok = true;

    dbg_QSqlDatabase db = QSqlDatabase::database ("watch_list_db");
    if (db.isValid () && db.open ())
    {
        qxtLog->debug () << tr("get scan list from DB");
        dbg_QSqlQuery query (db);
        if (query.exec (" select wi.itemid, wi.srv, ch.chname from watcheditems wi "
                        " RIGHT JOIN characters ch ON wi.faction = ch.faction AND wi.srv=ch.srv"
                        " order by srv,chname,itemid"))
        {
            clear ();
            while (query.next ())
            {
                addItem (query.value (1).toString (), query.value (2).toString (),
                         query.value (0).toInt ());
            }
        }
        else
        {
            qxtLog->error (query.lastError ().text ());
            ok = false;
        }
        db.close ();
    }
    else
    {
        ok = false;
        qxtLog->error (db.lastError ().text ());
    }
    if (!ok)
    {
        if (isEmpty ())
        {
            addItem (QString::fromUtf8 ("Азурегос"), QString::fromUtf8 ("Сведко"),
                     34054);
            qxtLog->error () << tr("DB server is down and scan list is empty -- use default list");
        }
        else
        {
            qxtLog->info () << tr("DB server is down -- use previous retrieved scan list");
        }
    }
}

void WatchList::addItem (QString srv, QString chname, int itemid)
{
    WatchItem wi;

    wi.itemid = itemid;
    wi.srv = srv;
    wi.chname = chname;

    addItem (wi);
}

void WatchList::toEnd()
{
    current_index = size();
}

#ifdef DBWATCHLISTPLUGIN_LIBRARY
#include <QtPlugin>

Q_EXPORT_PLUGIN2(dbwatchlistplugin, WatchList)
#endif
