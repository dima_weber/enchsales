#ifndef WATCHLISTINTERFACE_HXX
#define WATCHLISTINTERFACE_HXX

class WatchListInterface
{
    public:
        virtual WatchItem value() =0;
        virtual bool    addItem(WatchItem) =0;
        virtual void    addItem(QString srv, QString chname, int itemid) =0;
        virtual bool    removeItem(int index=-1) =0; // if index == -1 -- remove current Item
        virtual void    clear() =0;

        virtual void    resetIndex() =0;
        virtual void    toEnd() =0;
        virtual bool    next() =0;
        virtual bool    prev() =0;
        virtual int     currentIndex() =0;
        virtual int     size() =0;
        virtual bool    atEnd() =0;
        virtual bool    isEmpty() =0;

        virtual QVariant toVariant() =0;
        virtual bool    fromVariant(QVariant) =0;

        virtual void    buildItemsList() = 0;
};
Q_DECLARE_INTERFACE(WatchListInterface, "WatchListInterface")
#endif // WATCHLISTINTERFACE_HXX
