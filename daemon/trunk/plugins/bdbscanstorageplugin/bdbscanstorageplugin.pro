#-------------------------------------------------
#
# Project created by QtCreator 2010-11-28T04:01:20
#
#-------------------------------------------------

BASEDIR = "../../daemon"

QT       += core
QT -= gui
CONFIG += qxt plugin
QXT += core
TARGET = $$qtLibraryTarget(bdbscanstorageplugin)
TEMPLATE = lib
DESTDIR = $$BASEDIR/../bin/plugins

DEFINES += BDBSCANSTORAGEPLUGIN_LIBRARY
DEFINES += USE_BDB
DEFINES += IS_PLUGIN

SOURCES += \
    $$BASEDIR/bdbserializable.cxx \
    $$BASEDIR/datatypes.cxx \
    $$BASEDIR/bdbscanstorage.cxx \
    $$BASEDIR/exception.cxx \
    $$BASEDIR/scanstorageinterface.cxx \

HEADERS +=\
    $$BASEDIR/bdbserializable.hxx \
    $$BASEDIR/scanstorageinterface.hxx \
    $$BASEDIR/datatypes.hxx \
    $$BASEDIR/bdbscanstorage.hxx \
    $$BASEDIR/exception.hxx

INCLUDEPATH += $$BASEDIR
INCLUDEPATH += /opt/bdb/include
LIBS+= -L/opt/bdb/lib -ldb_cxx

symbian {
    #Symbian specific definitions
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE0063D41
    TARGET.CAPABILITY =
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = bdbscanstorageplugin.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/local/lib
    }
    INSTALLS += target
}
