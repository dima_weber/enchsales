/****************************************************************************
** Meta object code from reading C++ file 'bdbscanstorage.hxx'
**
** Created: Wed Aug 3 11:43:18 2011
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../daemon/bdbscanstorage.hxx"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'bdbscanstorage.hxx' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_BDBScanStorage[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       1,   19, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   20,   16,   15, 0x0a,

 // properties: name, type, flags
      52,   44, 0x0a095003,

       0        // eod
};

static const char qt_meta_stringdata_BDBScanStorage[] = {
    "BDBScanStorage\0\0int\0state\0getCount(QString)\0"
    "QString\0datafolder\0"
};

const QMetaObject BDBScanStorage::staticMetaObject = {
    { &ScanStorageInterface::staticMetaObject, qt_meta_stringdata_BDBScanStorage,
      qt_meta_data_BDBScanStorage, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &BDBScanStorage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *BDBScanStorage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *BDBScanStorage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_BDBScanStorage))
        return static_cast<void*>(const_cast< BDBScanStorage*>(this));
    if (!strcmp(_clname, "ScanStorageInterface"))
        return static_cast< ScanStorageInterface*>(const_cast< BDBScanStorage*>(this));
    return ScanStorageInterface::qt_metacast(_clname);
}

int BDBScanStorage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ScanStorageInterface::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: { int _r = getCount((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        default: ;
        }
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = getDataFolder(); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setDataFolder(*reinterpret_cast< QString*>(_v)); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
