/****************************************************************************
** Meta object code from reading C++ file 'scanstorageinterface.hxx'
**
** Created: Wed Aug 3 11:43:16 2011
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../daemon/scanstorageinterface.hxx"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'scanstorageinterface.hxx' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ScanStorageInterface[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: signature, parameters, type, tag, flags
      22,   21,   21,   21, 0x05,
      46,   44,   21,   21, 0x05,
      79,   21,   21,   21, 0x05,
     102,   21,   21,   21, 0x05,
     126,   21,   21,   21, 0x05,
     147,   21,   21,   21, 0x05,
     170,   21,   21,   21, 0x05,

 // slots: signature, parameters, type, tag, flags
     213,   21,  195,   21, 0x0a,
     235,  229,  225,   21, 0x0a,
     265,   21,  253,   21, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ScanStorageInterface[] = {
    "ScanStorageInterface\0\0groupStarted(GroupID)\0"
    ",\0groupItemAdded(GroupID,DataType)\0"
    "groupFinished(GroupID)\0groupCorrupted(GroupID)\0"
    "groupLocked(GroupID)\0groupReleased(GroupID)\0"
    "groupRolledBack(GroupID)\0QMap<QString,int>\0"
    "getCounts()\0int\0state\0getCount(QString)\0"
    "QStringList\0getStatesList()\0"
};

const QMetaObject ScanStorageInterface::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ScanStorageInterface,
      qt_meta_data_ScanStorageInterface, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ScanStorageInterface::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ScanStorageInterface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ScanStorageInterface::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ScanStorageInterface))
        return static_cast<void*>(const_cast< ScanStorageInterface*>(this));
    return QObject::qt_metacast(_clname);
}

int ScanStorageInterface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: groupStarted((*reinterpret_cast< GroupID(*)>(_a[1]))); break;
        case 1: groupItemAdded((*reinterpret_cast< GroupID(*)>(_a[1])),(*reinterpret_cast< DataType(*)>(_a[2]))); break;
        case 2: groupFinished((*reinterpret_cast< GroupID(*)>(_a[1]))); break;
        case 3: groupCorrupted((*reinterpret_cast< GroupID(*)>(_a[1]))); break;
        case 4: groupLocked((*reinterpret_cast< GroupID(*)>(_a[1]))); break;
        case 5: groupReleased((*reinterpret_cast< GroupID(*)>(_a[1]))); break;
        case 6: groupRolledBack((*reinterpret_cast< GroupID(*)>(_a[1]))); break;
        case 7: { QMap<QString,int> _r = getCounts();
            if (_a[0]) *reinterpret_cast< QMap<QString,int>*>(_a[0]) = _r; }  break;
        case 8: { int _r = getCount((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 9: { QStringList _r = getStatesList();
            if (_a[0]) *reinterpret_cast< QStringList*>(_a[0]) = _r; }  break;
        default: ;
        }
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void ScanStorageInterface::groupStarted(GroupID _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ScanStorageInterface::groupItemAdded(GroupID _t1, DataType _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ScanStorageInterface::groupFinished(GroupID _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ScanStorageInterface::groupCorrupted(GroupID _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ScanStorageInterface::groupLocked(GroupID _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void ScanStorageInterface::groupReleased(GroupID _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void ScanStorageInterface::groupRolledBack(GroupID _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_END_MOC_NAMESPACE
