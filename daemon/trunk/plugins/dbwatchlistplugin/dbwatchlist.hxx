class atchList : public QObject, public WatchListInterface
{
    Q_OBJECT
    Q_INTERFACES(WatchListInterface)

    QList<WatchItem> data;
    int              current_index;
    bool             isValidIndex(int idx);
    ConfigClient          config;
public:
    WatchList(QObject* parent = NULL);
    ~WatchList();
    WatchItem value();
    bool addItem(WatchItem);
    void addItem(QString srv, QString chname, int itemid);
    bool removeItem(int index=-1); // if index == -1 -- remove current Item
    void clear();

    void resetIndex();
    void toEnd();
    bool next();
    bool prev();
    int currentIndex();
    int size();
    bool atEnd();
    bool isEmpty();

    QVariant toVariant();
    bool     fromVariant(QVariant);

    void buildItemsList ();
};
