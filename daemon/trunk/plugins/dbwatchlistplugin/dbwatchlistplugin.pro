BASEDIR = "../../daemon"

QT       += core sql
QT -= gui
CONFIG += qxt plugin
QXT += core
TARGET = $$qtLibraryTarget(dbwatchlistplugin)
TEMPLATE = lib
DESTDIR = $$BASEDIR/../bin/plugins

DEFINES += DBWATCHLISTPLUGIN_LIBRARY
INCLUDEPATH += $$BASEDIR

SOURCES += \
    $$BASEDIR/datatypes.cxx \
    ../../daemon/watchlist.cxx \
    ../../daemon/settings.cxx \
    ../../daemon/db.cxx

HEADERS +=\
        $$BASEDIR/watchlist.hxx \
        $$BASEDIR/watchlistinterface.hxx \
        $$BASEDIR/datatypes.hxx \
    ../../daemon/settings.hxx \
    ../../daemon/db.hxx
