TEMPLATE = subdirs
DESTDIR = ../bin/plugins

qt.subdir = qtscanstorageplugin
SUBDIRS += qt

bdb.subdir = bdbscanstorageplugin
SUBDIRS += bdb

dbwl.subdir = dbwatchlistplugin
SUBDIRS += dbwl
