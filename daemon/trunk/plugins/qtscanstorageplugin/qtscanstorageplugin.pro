#-------------------------------------------------
#
# Project created by QtCreator 2010-11-28T04:09:04
#
#-------------------------------------------------

BASEDIR = "../../daemon"

QT       += core
QT -= gui
CONFIG += qxt plugin
QXT += core
TARGET = $$qtLibraryTarget(qtscanstorageplugin)
TEMPLATE = lib
DESTDIR = $$BASEDIR/../bin/plugins

DEFINES += QTSCANSTORAGEPLUGIN_LIBRARY
DEFINES += IS_PLUGIN

INCLUDEPATH += $$BASEDIR

SOURCES += \
    $$BASEDIR/qtscanstorage.cxx \
    $$BASEDIR/datatypes.cxx \
    $$BASEDIR/exception.cxx \
    $$BASEDIR/scanstorageinterface.cxx

HEADERS +=\
    $$BASEDIR/qtscanstorage.hxx \
    $$BASEDIR/datatypes.hxx \
    $$BASEDIR/exception.hxx \
    $$BASEDIR/scanstorageinterface.hxx \

symbian {
    #Symbian specific definitions
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE4D1997E
    TARGET.CAPABILITY =
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = qtscanstorage.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/local/lib
    }
    INSTALLS += target
}
