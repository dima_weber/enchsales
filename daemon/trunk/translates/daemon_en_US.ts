<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>BDBScanStorage</name>
    <message>
        <location filename="bdbscanstorage.cxx" line="178"/>
        <source>%1 Env Version mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bdbscanstorage.cxx" line="216"/>
        <source>Fail to upgrade catalog.db</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bdbscanstorage.cxx" line="248"/>
        <source>Fail to upgrade vault.db</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bdbscanstorage.cxx" line="269"/>
        <source>fail to open DB Enviropment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bdbscanstorage.cxx" line="295"/>
        <source>Check for %1 groups.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bdbscanstorage.cxx" line="304"/>
        <source>%1 invalid groups found. Please don&apos;t Ctrl-C / kill programm to prevent this. Use DBus shutdown call instead. Delete groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bdbscanstorage.cxx" line="318"/>
        <source>Done. %1 group(s) purged.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BNetConnection</name>
    <message>
        <location filename="bnetconnection.cxx" line="72"/>
        <source>getPage timed out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="73"/>
        <source>BNetConnection::getPage timed out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="88"/>
        <source>connecting to BNet: start</source>
        <translation>connecting to BNet: start</translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="119"/>
        <source>Server maintenance. Wait a bit.</source>
        <translation>Server maintenance. Wait a bit.</translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="136"/>
        <source>connecting to BNet: done</source>
        <translation>connecting to BNet: done</translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="165"/>
        <source>Logging character %1, server %2</source>
        <translation>Logging character %1, server %2</translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="186"/>
        <source>BNetConnection::logAuc timed out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="196"/>
        <source>character logged</source>
        <translation>character logged</translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="202"/>
        <source>Unable to log character. See trace for details</source>
        <translation>Unable to log character. See trace for details</translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="208"/>
        <source>empty result. Dunno if we logged. let&apos;s hope we did</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="214"/>
        <source>Error while trying to log character.</source>
        <translation>Error while trying to log character.</translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="243"/>
        <source>%1  byte(s) read</source>
        <translation>%1 byte(s) read</translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="258"/>
        <source>Network is back now. Reconnecting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="263"/>
        <source>Network is down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="bnetconnection.cxx" line="287"/>
        <source>Request timed out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Command</name>
    <message>
        <location filename="main.cxx" line="29"/>
        <source></source>
        <comment>Line Options Use ./enchsales_daemon --help to see options</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>CommandLine</name>
    <message>
        <location filename="main.cxx" line="34"/>
        <source>database connection</source>
        <translation>database connection</translation>
    </message>
    <message>
        <location filename="main.cxx" line="35"/>
        <source>database host address. Default: %1</source>
        <translation>database host adress.Default: %1</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="core.cxx" line="61"/>
        <source>Daemon is already running: take over.</source>
        <translation>Daemon is alreadyrunning: take over.</translation>
    </message>
    <message>
        <location filename="core.cxx" line="64"/>
        <source>NewOne: pause him</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="70"/>
        <location filename="core.cxx" line="95"/>
        <source>NewOne: crap... he&apos;s ignore me... exit now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="74"/>
        <source>NewOne: ok he is paused now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="76"/>
        <source>NewOne: we&apos;ve got secret key: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="80"/>
        <source>NewOne: and all his bases belong to us!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="86"/>
        <source>NewOne: Fail to retrieve data: exit now.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="88"/>
        <source>Core::Core broken data recieved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="90"/>
        <source>NewOne: shut him down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="96"/>
        <source>Core::Core timeouted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="98"/>
        <source>NewOne: his dead now!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="102"/>
        <source>Fail to acquire dbus: exit now.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="105"/>
        <source>I&apos;m the One!</source>
        <translation>I&apos;m the One!</translation>
    </message>
    <message>
        <location filename="core.cxx" line="109"/>
        <source>Daemon is already running and no --takeover : exit now.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="138"/>
        <source>Fail to cast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="143"/>
        <source>Fail to instancenate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="148"/>
        <source>Fail to load plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="153"/>
        <source>Not library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="235"/>
        <source>scan is in progress -- stop it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="237"/>
        <source>and wait for scanStoppedsignal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="239"/>
        <source>scanStopped recieved, proceed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="242"/>
        <source>call manager shutdown slot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="249"/>
        <source>Manager is stopped now.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="252"/>
        <source>Stop UpDBThread now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="319"/>
        <source>Fail to read shared memory: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="323"/>
        <source>Data from SharedMemmory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="337"/>
        <source>resuming previous scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="342"/>
        <source>starting new scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="core.cxx" line="348"/>
        <source>Bnet connection disabled. Will only send data from cash to DB.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="main.cxx" line="179"/>
        <source>quit.</source>
        <translatorcomment>quit from application</translatorcomment>
        <translation>Quit.</translation>
    </message>
</context>
<context>
    <name>Manager</name>
    <message>
        <location filename="manager.cxx" line="112"/>
        <source>Manager::shutdown() called</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="manager.cxx" line="114"/>
        <source>timer stopped. Send shutdownRequest signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="manager.cxx" line="136"/>
        <source>machine stopped</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScanAucThread</name>
    <message>
        <location filename="scanaucthread.cxx" line="39"/>
        <source>request page with url %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanaucthread.cxx" line="42"/>
        <source>parse page content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanaucthread.cxx" line="77"/>
        <source>Empty data recieved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanaucthread.cxx" line="91"/>
        <source>error when parsing [%1]: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanaucthread.cxx" line="95"/>
        <source>Connection to BNet lost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanaucthread.cxx" line="99"/>
        <source>sleep for 1 minute to prevent extra spam to server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanaucthread.cxx" line="119"/>
        <source>New scan. item: %7 (%1) srv: %5 faction:%6 start: %2 end: %3 total: %4 </source>
        <translation>New scan. item: %7 (%1) srv: %5 faction:%6 start: %2 end: %3 total: %4 </translation>
    </message>
    <message>
        <location filename="scanaucthread.cxx" line="163"/>
        <source>ScanAucThread destroyed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanaucthread.cxx" line="257"/>
        <source>online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanaucthread.cxx" line="257"/>
        <source>offline</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScannerState</name>
    <message>
        <location filename="scanstate.cxx" line="17"/>
        <source>enter state: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanstate.cxx" line="23"/>
        <source>exit state: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StorageListener</name>
    <message>
        <location filename="storagelistener.cxx" line="14"/>
        <source>Group Add: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="storagelistener.cxx" line="21"/>
        <source>Group Finish: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateDBThread</name>
    <message>
        <location filename="updatedbthread.cxx" line="50"/>
        <source>UpdateDBThread destroyed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="77"/>
        <source>process_map started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="85"/>
        <source>DB connection  opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="89"/>
        <source>new data group[%1] in storage locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="92"/>
        <source>begin transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="97"/>
        <source>new item retrieved: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="127"/>
        <source>%1 inserts. done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="133"/>
        <source>ROLLBACK transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="140"/>
        <source>Commit transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="144"/>
        <source>Fail to commit transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="149"/>
        <source>Transaction commited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="188"/>
        <source>UpdateBD thread is started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="204"/>
        <source>UpdateBD thread is finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="updatedbthread.cxx" line="227"/>
        <source>holy crap! someone want to stop us!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateItemInfoThread</name>
    <message>
        <location filename="updateiteminfothread.cxx" line="24"/>
        <source>UpdateItemInfoThread destroyed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchList</name>
    <message>
        <location filename="watchlist.cxx" line="43"/>
        <source>Index out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="watchlist.cxx" line="152"/>
        <source>get scan list from DB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="watchlist.cxx" line="183"/>
        <source>DB server is down and scan list is empty -- use default list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="watchlist.cxx" line="187"/>
        <source>DB server is down -- use previous retrieved scan list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>getStateNameString</name>
    <message>
        <location filename="scanstate.cxx" line="31"/>
        <source>Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanstate.cxx" line="32"/>
        <source>Paused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanstate.cxx" line="33"/>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanstate.cxx" line="34"/>
        <source>Scanning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanstate.cxx" line="35"/>
        <source>Starting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanstate.cxx" line="36"/>
        <source>Stopping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanstate.cxx" line="37"/>
        <source>Pausing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanstate.cxx" line="38"/>
        <source>Resuming</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="scanstate.cxx" line="39"/>
        <source>unknown state</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
