#ifndef DBINTERFACE_HXX
#define DBINTERFACE_HXX

#include <QObject>
#include <QDateTime>
#include <QPixmap>
#include <QStringList>
#include <QMap>

struct CandleData
{
    float min;
    float max;
    float open;
    float close;
    QDate date;
};

class DBInterface : public QObject
{
    Q_OBJECT
protected:
    DBInterface(QObject* parent = 0)
        :QObject(parent)
    {}
public:
    struct price_type
    {
        int cnt;
        int ea;
    };

public slots:

//    virtual QMap<int, QString>          mw_getItemIDs() =0;
      virtual QMap<int, QString>          mw_getItemIDs(const QString& _srv, const QString& _faction) =0;
      virtual QMap<QString, QStringList>  mw_getServers() =0;
//    virtual QPixmap                     mw_getItemIcon(int id = -1)=0;
//    virtual const QList<QDateTime>&     mw_Days()=0;
//    virtual float                       mw_quantile(QString name, QDateTime date)=0;
//    virtual float                       mw_getMaxQuantileValue(QString name)=0;
//    virtual float                       mw_getMinQuantileValue(QString name)=0;
//    virtual QDate                       mw_getMinDate()=0;
//    virtual QDate                       mw_getMaxDate()=0;
//    //virtual QSqlQuery                   mw_getDataQuery()=0;
//    //virtual QSqlQuery                   mw_getDetailDataQuery(QDateTime scantime)=0;
//    virtual void                        mw_ignoreAuc(int)=0;
//    virtual QList<QDateTime>            mw_lastUpdatesTime()=0;
//    virtual void                        mw_getUpdateData()=0;
//    virtual QMap<QDate, CandleData>     mw_getCandlesData()=0;

//    virtual void                        setServer(QString s)=0;
//    virtual void                        setFaction(QString f)=0;
//    virtual void                        setFromDate(QDate from)=0;
//    virtual void                        setToDate(QDate to)=0;
//    virtual void                        setItemId(int id)=0;
//signals:
//              void                        newServerAdded(const QString& );
//              void                        newFactionAdded(const QString&, const QString&  );
//              void                        newItemAdded(EnchItem::ItemInfo, const QString&, const QString& );
};
#endif // DBINTERFACE_HXX
