#include "enchtreewidget.hpp"
#include "enchitem.hpp"
#include "qwbrdb.h"
#include <QVariant>
#include <QBrush>
#include <math.h>
#include <QPainter>

QColor getColor(float pct)
{
    if (pct < 0.5)
        return QColor(0x33, 0x99, 0xff, 0xff);
    if (pct < 0.8)
        return QColor(0x33, 0xff, 0x44, 0xff);
    if (pct < 1.1)
        return QColor(0xFF, 0xFF, 0x00, 0xff);
    if (pct < 1.3)
        return QColor(0xff, 0x99, 0x00, 0xff);
    return QColor(0xff, 0,0, 0xff);
}

EnchItemsModel::EnchItemsModel(QObject* parent)
    :QAbstractItemModel(parent)
{
    p_rootItem = new EnchItem("/");
    server_icon.load(":/icons/icons/wow.png");
    horde_icon.load(":/icons/icons/horde.png");
    ally_icon.load(":/icons/icons/ally.png");
}

void EnchItemsModel::clear()
{
    delete p_rootItem;
    p_rootItem = NULL;
}


QModelIndex EnchItemsModel::index ( int row, int column, const QModelIndex & parent ) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    EnchItem* parentItem;

    if (!parent.isValid())
        parentItem = p_rootItem;
    else
        parentItem = static_cast<EnchItem*>(parent.internalPointer());

    EnchItem* childItem =  parentItem->child(row);
    if (childItem)
        return createIndex(row,column, childItem);
    else
        return QModelIndex();
}

QModelIndex	EnchItemsModel::parent ( const QModelIndex & index ) const
{
    if (!index.isValid())
        return QModelIndex();

    EnchItem *childItem = static_cast<EnchItem*>(index.internalPointer());
    EnchItem *parentItem = childItem->parent();

    if (parentItem == p_rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int	EnchItemsModel::rowCount ( const QModelIndex & parent ) const
{
    if (parent.isValid())
        return static_cast<EnchItem*>(parent.internalPointer())->childCount();
    else
        return p_rootItem->childCount();
}

int EnchItemsModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<EnchItem*>(parent.internalPointer())->columnCount();
    else
        return p_rootItem->columnCount();
}

QVariant EnchItemsModel::data(const QModelIndex &index, int role) const
{
    QVariant ret = QVariant();
    if (!index.isValid())
            return ret;

    QVariant d = static_cast<EnchItem*>(index.internalPointer())->data(index.column());
    switch(role)
    {
           case  Qt::DisplayRole:
                    if (d.canConvert<QVariantMap>())
                        ret = d.toMap()["name"];
                    else
                        ret = d;
                    break;
            case Qt::ToolTipRole:
                    if (d.canConvert<QVariantMap>())
                        ret =  QString("%1\nbyout each: %2\nLast scan: %3").arg(d.toMap()["name"].toString()).arg(d.toMap()["auc_price"].toFloat()).arg(d.toMap()["lastScanTime"].toDateTime().toString());
                    else
                        return d;
                    break;
            case Qt::UserRole:
                    if( d.canConvert<QVariantMap>())
                    {
                        EnchItem* p_item = static_cast<EnchItem*>(index.internalPointer());
                        ret = p_item->getValue();
                    }
                    break;
            case Qt::DecorationRole:
                    if(d.canConvert<QVariantMap>() )
                    {
                        if(d.toMap()["type"]=="item")
                            ret = QwbrDB::instance()->mw_getItemIcon(d.toMap()["itemid"].toInt());
                        if(d.toMap()["type"]=="server")
                            ret = server_icon;
                        if (d.toMap()["type"] == "faction")
                        {
                            // HACK!!!!!! noob!!!! who cares ?
                            if (d.toMap()["faction"].toString().length() < 6)
                                ret = horde_icon;
                            else
                                ret = ally_icon;
                        }
                    }
                    break;
            case Qt::ForegroundRole:
//                    ret = QBrush(Qt::white);
//                    break;
            case Qt::BackgroundRole:
                    {
                        QColor color = role == Qt::ForegroundRole ? Qt::white : Qt::black;
                        if(d.canConvert<QVariantMap>() )
                        {
                            QVariantMap map = d.toMap();
                            if (map.contains("auc_price") && map.contains("trashhold"))
                            {
                                float auc_price = map["auc_price"].toFloat();
                                float my_price  = map["trashhold"].toFloat();
                                float percent = auc_price/my_price;
                                /*
                                int r=0;
                                int g=0;
                                int b=0;
                                if (percent > 1)
                                {
                                    percent = 1/percent;
                                    b = 0;
                                    r = percent > 0.5 ? 155 : round( percent * 512);
                                    g = percent < 0.5 ? 155 : round( 256 - (percent-0.5) * 512);
                                }
                                else
                                {
                                    r = 0;
                                    g = percent > 0.5 ? 155 : round( percent * 512);
                                    b = percent < 0.5 ? 155 : round( 256 - (percent-0.5) * 512);
                                }
                                color = QColor(r,g,b)
                                */
                                color = getColor(percent);
                                if (role == Qt::BackgroundRole)
                                    color.setAlphaF(0.1);
                            }
                        }
                        ret  = QBrush (color);
                    }
                    break;
            default:
                    break;
    };
    return ret;

}

void EnchItemsModel::addServer(const QString& srv)
{
    if (!p_rootItem)
        return;

    if (p_rootItem->rowChildWithName(srv) < 0)
    {
        QModelIndex indx = QModelIndex();
        QVariantMap value;
        value["type"] = "server";
        value["name"] = srv;
        value["srv"]=srv;
        int row = p_rootItem->addChild(value);
        beginInsertRows(indx, row, row);
        endInsertRows();
    }
}

void EnchItemsModel::addFaction(const QString& faction, const QString& srv)
{
    if (!p_rootItem)
        return;

    int srv_row = p_rootItem->rowChildWithName(srv);
    if ( srv_row >= 0)
    {
        EnchItem* p_srvItem = p_rootItem->child(srv_row);
        int faction_row = p_srvItem->rowChildWithName(faction);
        if (faction_row < 0)
        {
            QModelIndex indx = index(p_srvItem->row(),0, QModelIndex());
            QVariantMap value;
            value["type"] = "faction";
            value["srv"] =  srv;
            value["faction"] = faction;
            value["name"] = faction;
            int row = p_srvItem->addChild(value);
            beginInsertRows(indx, row, row);
            endInsertRows();
        }
    }
}

void EnchItemsModel::addItem(const EnchItem::ItemInfo& info, const QString& faction, const QString& srv)
{
    if (!p_rootItem)
        return;

    int srv_row = p_rootItem->rowChildWithName(srv);
    if ( srv_row >= 0)
    {
        EnchItem* p_srvItem = p_rootItem->child(srv_row);
        int faction_row = p_srvItem->rowChildWithName(faction);
        if (faction_row >= 0)
        {
            QModelIndex faction_indx = index(p_srvItem->row(),0, QModelIndex());
            EnchItem* p_factionItem = p_srvItem->child(faction_row);
            int item_row = p_factionItem->rowChildWithName(info.name);
            if (item_row < 0)
            {
                QModelIndex item_indx = index(p_factionItem->row(),0, faction_indx);
                int row = p_factionItem->addChild(info);
                beginInsertRows(item_indx, row, row);
                endInsertRows();
            }
            else
            {
                EnchItem* p_item = p_factionItem->child(item_row);
                EnchItem::ItemInfo old_info(p_item->getValue());
                EnchItem::ItemInfo new_info = info;
                if (info.auc_price != old_info.auc_price)
                    new_info.pct_price_change = (old_info.auc_price - info.auc_price) * 100 / info.auc_price;
                else
                    new_info.pct_price_change = old_info.pct_price_change;
                p_item->setValue(new_info);
                QModelIndex item_index = index(item_row, 0, faction_indx);
                emit dataChanged(item_index, item_index);
            }
        }
    }
}

bool EnchItemsModel::hasChildren(const QModelIndex &index) const
{
    if (!index.isValid())
        return true;
    if (index.isValid())
    {
        EnchItem* p_item = static_cast<EnchItem*>(index.internalPointer());
        if(p_item->getValue().canConvert<QVariantMap>())
        {
            QVariantMap map = p_item->getValue().toMap();
            if (map["type"].toString() == "server" || map["type"].toString() == "faction")
                return true;
        }
    }
    return false;
}

bool EnchItemsModel::canFetchMore(const QModelIndex &parent) const
{
    if (parent.isValid())
    {
        EnchItem* p_item = static_cast<EnchItem*>(parent.internalPointer());
        if(p_item->getValue().canConvert<QVariantMap>())
        {
            QVariantMap map = p_item->getValue().toMap();
            if (map["type"].toString() == "server" || map["type"].toString() == "faction")
                if(p_item->childCount() == 0)
                    return true;
        }
    }
    return false;
}

void EnchItemsModel::fetchMore(const QModelIndex &parent)
{
    if (parent.isValid())
    {
        EnchItem* p_item = static_cast<EnchItem*>(parent.internalPointer());
        if(p_item->getValue().canConvert<QVariantMap>())
        {
            QVariantMap map = p_item->getValue().toMap();
            emit wantMoreData(map["srv"].toString(), map["faction"].toString());
        }
    }
}

EnchItemDelegate::EnchItemDelegate(QWidget* parent)
    : QStyledItemDelegate(parent)
{
    up_arrow.load(":/icons/icons/up_arrow.png");
    up_arrow = up_arrow.scaledToHeight(16).scaledToWidth(16);
    down_arrow.load(":/icons/icons/down_arrow.png");
    down_arrow = down_arrow.scaledToHeight(16).scaledToWidth(16);
}

void EnchItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyledItemDelegate::paint(painter, option, index);

    EnchItem* p_item = static_cast<EnchItem*>(index.internalPointer());
    if (p_item)
    {
        QVariantMap map = p_item->getValue().toMap();
        if (!map.isEmpty() && map["type"].toString() == "item")
        {
            painter->setRenderHint(QPainter::Antialiasing, true);
            QRect rect = option.rect;

            painter->save();

            int x,y,h,w;
            rect.getRect(&x, &y, &w, &h);
            w = up_arrow.width();
            h = up_arrow.height();
            //x = x + 48 - w;
            //y = y + 48 - h;
            rect.setRect(x+3,y+3,w,h);
            if (map["pct_price_change"].toInt() > 0)
                painter->drawPixmap(rect, up_arrow);
            if (map["pct_price_change"].toInt() < 0)
                painter->drawPixmap(rect, down_arrow);

            painter->restore();
            return;
        }
    }
}

QSize EnchItemDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QStyledItemDelegate::sizeHint(option, index);
}
