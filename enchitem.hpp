#ifndef ENCHITEM_HPP
#define ENCHITEM_HPP
#include <QVariant>
#include <QDateTime>
#include <QDebug>
class EnchItem
{
public:
    enum ItemType{Unknown = -1, Server, Faction, Item};
    struct ItemInfo
    {
        ItemType type;
        int itemid;
        QString name;
        QDateTime lastScanTime;
        float auc_price;
        float trashhold;
        QString srv;
        QString faction;
        int pct_price_change;

        ItemInfo()
        {
            type = Unknown;
            itemid = -1;
            name = "<unknow>";
            lastScanTime = QDateTime::currentDateTime();
            auc_price = 0;
            trashhold = 0;
            srv = "<unknown>";
            faction = "<unknown>";
            pct_price_change = 0;
        }

        ItemInfo(const ItemInfo& c)
        {
            type = c.type;
            itemid = c.itemid;
            pct_price_change = c.pct_price_change;
            name = c.name;
            lastScanTime = c.lastScanTime;
            auc_price = c.auc_price;
            trashhold = c.trashhold;
            srv = c.srv;
            faction = c.faction;
        }

        ItemInfo& operator=(const ItemInfo& c)
        {
            type = c.type;
            itemid = c.itemid;
            pct_price_change = c.pct_price_change;
            name = c.name;
            lastScanTime = c.lastScanTime;
            auc_price = c.auc_price;
            trashhold = c.trashhold;
            srv = c.srv;
            faction = c.faction;

            return *this;
        }

        ItemInfo(const QVariant& v)
        {
            *this = v;
        }

        ItemInfo& operator=(const QVariant& v)
        {
            if (v.canConvert<QVariantMap>())
            {
                QVariantMap map = v.toMap();
                if(map["type"] == "server")
                    type = Server;
                else if (map["type"] == "faction")
                    type = Faction;
                else if (map["type"] == "item")
                    type = Item;
                else
                    type = Unknown;

                name = map["name"].toString();
                itemid = map["itemid"].toInt();
                srv = map["srv"].toString();
                faction = map["faction"].toString();
                auc_price = map["auc_price"].toFloat();
                trashhold = map["trashhold"].toFloat();
                lastScanTime = map["lastScanTime"].toDateTime();
                pct_price_change = map["pct_price_change"].toFloat();
            }
            else if (v.canConvert<int>())
            {
                type = Unknown;
                itemid = v.toInt();
            }
            else if (v.canConvert<QString>())
            {
                type = Unknown;
                name = v.toString();
            }
            else
                type = Unknown;
            return *this;
        }

        operator QVariant () const
        {
            QVariantMap map;
            map["name"] = name;
            map["itemid"] = itemid;
            map["srv"] = srv;
            map["faction"] = faction;
            map["auc_price"] = auc_price;
            map["pct_price_change"] = pct_price_change;
            if (trashhold > 0)
                map["trashhold"] = trashhold;
            map["lastScanTime"] = lastScanTime;
            if (type == Unknown)
                map["type"] = QString("");
            else if (type == Server)
                map["type"] = QString("server");
            else if (type == Faction)
                map["type"] = QString("faction");
            else  if (type == Item)
                map["type"] = QString("item");

            return map;
        }
    };

    EnchItem(QVariant v,  EnchItem* parent = NULL)
    {
        parentItem = parent;
        setValue(v);
    }

    ~EnchItem()
    {
        foreach(int indx, children.keys())
            delete children[indx];
    }

    EnchItem * child(int row)
    {
        return children[row];
    }

    int childCount() const
    {
        return children.size();
    }

    int columnCount() const
    {
        return 1;
    }

    QVariant data(int column) const
    {
        if (value.canConvert<QVariantList>())
            return value.toList()[column];
        return value;
    }

    int row()
    {
        return parentItem->children.key(this);
    }
    EnchItem* parent()
    {
        return parentItem;
    }

    int addChild(EnchItem* p_child)
    {
        int index = childCount();
        children[index] = p_child;
        return index;
    }

    int addChild(QVariant v)
    {
        EnchItem* p_child = new EnchItem(v, this);
        return addChild(p_child);
    }

    int rowChildWithName(const QString& _name)
    {
        if (childCount() == 0)
            return -1;

        foreach(int index, children.keys())
        {
            EnchItem* p_child = children[index];
            QString name;
            if (p_child->value.canConvert<QVariantMap>())
                name = p_child->value.toMap()["name"].toString();
            else if (p_child->value.canConvert<QString>())
                name = p_child->value.toString();
            else if (p_child->value.canConvert<QVariantList>())
                name = p_child->value.toList()[0].toString();
            else
                name = QString::null;

            if (name == _name)
                return index;
        }
        return -1;
    }
    QVariant getValue()
    {
        return value;
    }
    void setValue(const QVariant& _v)
    {
        value = _v;
    }

private:
    EnchItem *parentItem;
    QMap<int, EnchItem*> children;
    QVariant value;
};

#endif // ENCHITEM_HPP
