#include "enchtreewidget.hpp"
#include <QTreeView>
#include <QHeaderView>
#include <QVBoxLayout>

EnchTreeWidget::EnchTreeWidget(QWidget *parent) :
    QWidget(parent)
{
    p_layout = new QVBoxLayout(this);

    p_treeview = new QTreeView(this);
    p_treeview->setMinimumWidth(500);
    p_treeview->setMaximumWidth(900);
    QFont font = p_treeview->font();
    QPalette palette = p_treeview->palette();
    palette.setColor(QPalette::Base, Qt::black);
    palette.setColor(QPalette::Text, Qt::white);
    palette.setColor(QPalette::BrightText, Qt::white);
    font.setPointSize(12);
    p_treeview->setFont(font);
    p_treeview->header()->hide();
    p_treeview->setPalette(palette);
    p_treeview->setItemDelegate(new EnchItemDelegate(p_treeview));
    connect(p_treeview, SIGNAL(clicked(QModelIndex)), SLOT(onEnchItemClick(QModelIndex)));

    p_enchmodel = new EnchItemsModel();
    p_treeview->setModel(p_enchmodel);

    connect(p_enchmodel, SIGNAL(wantMoreData(QString,QString)), SIGNAL(wantMoreData(QString,QString)));

    p_layout->addWidget(p_treeview);
}

void EnchTreeWidget::addServer(const QString & srv)
{
    p_enchmodel->addServer(srv);
}

void EnchTreeWidget::addFaction(const QString & faction, const QString& srv)
{
    p_enchmodel->addFaction(faction, srv);
}

void EnchTreeWidget::addItem(EnchItem::ItemInfo info, const QString & faction, const QString & srv)
{
    p_enchmodel->addItem(info, faction, srv);
}

void EnchTreeWidget::onEnchItemClick(const QModelIndex& index)
{
    EnchItem* p_item = static_cast<EnchItem*>(index.internalPointer());
    QVariant v = p_item->getValue();
    if (v.canConvert<QVariantMap>())
    {
        QVariantMap map = v.toMap();
        if (map["type"] == "server")
        {
            emit serverSelected(map["srv"].toString());
        }
        if (map["type"] == "item")
        {
            emit itemSelected(EnchItem::ItemInfo(v));
        }
        if (map["type"] == "faction")
        {
            emit factionSelected(map["srv"].toString(), map["faction"].toString());
        }
    }
}
