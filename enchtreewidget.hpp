#ifndef ENCHTREEWIDGET_HPP
#define ENCHTREEWIDGET_HPP

#include <QWidget>
#include "enchitem.hpp"
#include <QAbstractItemModel>
#include <QList>
#include <QPixmap>
#include <QModelIndex>
#include <QStyledItemDelegate>

class QTreeView;
class QVBoxLayout;

class EnchItemsModel;

class EnchTreeWidget : public QWidget
{
    Q_OBJECT
public:
    explicit EnchTreeWidget(QWidget *parent = 0);

signals:
    void serverSelected(const QString& );
    void factionSelected(const QString& srv, const QString& faction);
    void itemSelected(EnchItem::ItemInfo);
    void wantMoreData(QString,QString);

public slots:
    void addServer(const QString&);
    void addFaction(const QString&, const QString&);
    void addItem(EnchItem::ItemInfo,const QString&, const QString&);

private slots:
    void onEnchItemClick(const QModelIndex& index);
private:
    EnchItemsModel* p_enchmodel;
    QTreeView* p_treeview;

    QVBoxLayout* p_layout;
};

class EnchItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    EnchItemDelegate(QWidget* parent = NULL);
    void paint (QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
private:
    QPixmap up_arrow;
    QPixmap down_arrow;
};

class EnchItemsModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    EnchItemsModel(QObject* parent = NULL);
    virtual QModelIndex	index ( int row, int column, const QModelIndex & parent = QModelIndex() ) const;
    virtual QModelIndex	parent ( const QModelIndex & index ) const;
    virtual int	rowCount ( const QModelIndex & parent = QModelIndex() ) const;
    virtual int columnCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual bool hasChildren(const QModelIndex &parent) const;
    virtual bool canFetchMore(const QModelIndex &parent) const;

public slots:
    void addServer(const QString& srv);
    void addFaction(const QString& faction, const QString& srv);
    void addItem(const EnchItem::ItemInfo&,  const QString& faction, const QString& srv);
    virtual void fetchMore(const QModelIndex &parent);

//    void setItemsData();
    void clear();
private:
    void clear(const QModelIndex& index);
    EnchItem* p_rootItem;

    QPixmap horde_icon;
    QPixmap ally_icon;
    QPixmap server_icon;
signals:
    void wantMoreData(const QString& srv, const QString& faction);
};


#endif // ENCHTREEWIDGET_HPP
