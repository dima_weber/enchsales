#-------------------------------------------------
#
# Project created by QtCreator 2010-10-26T15:38:52
#
#-------------------------------------------------

QT       += core gui svg dbus

TARGET = esDaemonStatus
TEMPLATE = app

CONFIG += debug

SOURCES += main.cpp\
        esdstatus.cpp \
    proxy.cpp \
    qled.cpp \
    thrproxy.cpp \
    configproxy.cpp

HEADERS  += esdstatus.h \
    proxy.h \
    qled.h \
    thrproxy.h \
    configproxy.h

FORMS    += esdstatus.ui

RESOURCES += \
    qled.qrc
