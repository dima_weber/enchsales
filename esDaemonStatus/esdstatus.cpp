#include "esdstatus.h"
#include "ui_esdstatus.h"
#include "qled.h"
#include <QString>

#define SERVICE_NAME "net.softwarium.weber.EnchSaleDaemon"

esdStatus::esdStatus(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::esdStatus)
{
    p_watcher = new QDBusServiceWatcher(SERVICE_NAME, QDBusConnection::sessionBus(), QDBusServiceWatcher::WatchForOwnerChange, this);
    connect (p_watcher, SIGNAL(serviceRegistered(QString)), SLOT(onServiceUp()));
    connect (p_watcher, SIGNAL(serviceUnregistered(QString)), SLOT(onServiceDown()));

    ui->setupUi(this);
    createActions();
    createTrayIcon();

    proxy = NULL;
    dbproxy=NULL;
    timer = NULL;
    dbtimer = NULL;
    configproxy = NULL;
    onServiceDown();
    onServiceUp();
}

esdStatus::~esdStatus()
{
    delete ui;
    delete proxy;
}

void esdStatus::onBNetConnect(bool ok)
{
    Q_UNUSED(ok);
    //    ui->qLed->setValue(ok);
}

void esdStatus::onScanStarted()
{
    ui->curServ_Label->show();
    ui->curChar_Label->show();
    ui->curItem_Label->show();
    ui->DLSpeed_Label->show();
    ui->progressBar_2->hide();
    ui->groupBox->setTitle(QString("Current scan (%1)").arg (proxy->state ()));
}

void esdStatus::onScanFinished()
{
    ui->curServ_Label->hide();
    ui->curChar_Label->hide();
    ui->curItem_Label->hide();
    ui->DLSpeed_Label->hide();
    ui->progressBar_2->show();

    ui->groupBox->setTitle(QString("Time to next scan (%1)").arg (proxy->state ()));
    QVariantMap map = proxy->status();
    QTime time = proxy->timeToNextScan();
    int cur = QTime(0,0).secsTo(time);
    int max = QString(configproxy->getParam ("Timers/scan_sleep")).toInt();
    ui->progressBar_2->setRange(0, max);
    ui->progressBar_2->setValue(cur);
    ui->progressBar_2->setInvertedAppearance(false);
    if (timer)
        delete timer;
    timer = new QTimer(this);
    timer->setInterval(1000);
    timer->start();
    connect(timer, SIGNAL(timeout()), SLOT(onTimer()));
}

void esdStatus::onTimer()
{
    int value = ui->progressBar_2->value() - 1;
    ui->progressBar_2->setValue(value);
    QString text = QTime(0,0).addSecs(value).toString("m'm' ss's'");
    ui->progressBar_2->setFormat(text);
    if (value == 0)
    {
        delete timer;
        timer = NULL;
    }
}

void esdStatus::onDBTimer ()
{
    bool connected = dbproxy->connected ();
    ui->DB_label->setText(QString("Connected to DB: %1").arg(connected));

    int mem_size = -1; //dbproxy->dataMemorySize ();
    int cach_size = -1; //dbproxy->dataCashSize ();

    ui->Mem_label->setText(QString("Mem items: %1").arg(mem_size));
    ui->Disk_Label->setText(QString("Disk items: %1").arg(cach_size));

    double upload_speed = dbproxy->uploadSpeed();
    ui->ULSpeed_Label->setText(QString("Upload speed: %1 aps").arg(upload_speed));
}

void esdStatus::onNextAuc()
{
    QVariantMap map = proxy->status();

    int pos = map["aucs done"].toInt();
    int max = map["aucs left"].toInt() + pos - 1;

    ui->curServ_Label->setText(QString("Current server: %1").arg(map["current Srv"].toString()));
    ui->curChar_Label->setText(QString("Current character: %1").arg(map["current Character"].toString()));
    ui->curItem_Label->setText(QString("Current item: %1").arg(map["current Item"].toInt()));

    ui->DLSpeed_Label->setText(QString("Download speed: %1 aps").arg(map["download speed"].toDouble()));

    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(max);
    ui->progressBar->setValue(pos);
}

bool esdStatus::initDBus()
{
    bool  ok = false;
    if (proxy)
        delete proxy;

    proxy = new Proxy(SERVICE_NAME, "/Manager", QDBusConnection::sessionBus(), this);

    if (proxy->isValid())
    {
        dbproxy=new DBProxy(SERVICE_NAME, "/DB", QDBusConnection::sessionBus(), this);
        dbtimer = new QTimer;
        connect(dbtimer, SIGNAL(timeout()), SLOT(onDBTimer()));
        dbtimer->setInterval (5000);
        dbtimer->start ();

        configproxy = new ConfigProxy(SERVICE_NAME, "/Config", QDBusConnection::sessionBus (), this);

        ok = true;
        connect (proxy, SIGNAL(connectedBNet(bool)), SLOT(onBNetConnect(bool)));
        connect (proxy, SIGNAL(started()), SLOT(onScanStarted()));
        connect (proxy, SIGNAL(stopped()), SLOT(onScanFinished()) );
        connect (proxy, SIGNAL(nextAucStarted()), SLOT(onNextAuc()));

        if (proxy->isScanning ())
        {
            onScanStarted();
        }
        else
        {
            onScanFinished();
        }
    }
    return ok;
}

void esdStatus::onServiceUp()
{
    if(initDBus())
    {
        QIcon icon(QPixmap(":/resources/circle_green.svg"));
        trayIcon->setIcon(icon);
        ui->qLed->setValue(true);
    }
}

void esdStatus::onServiceDown()
{
    ui->qLed->setValue(false);
    QIcon icon(QPixmap(":/resources/circle_red.svg"));
    trayIcon->setIcon(icon);

    ui->progressBar->setRange(0,0);
    ui->progressBar->setValue(0);
    ui->curServ_Label->hide();
    ui->curChar_Label->setText("No daemon online...");
    ui->curItem_Label->hide();
    ui->DLSpeed_Label->hide();
    ui->progressBar_2->hide();
}

void esdStatus::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(maximizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
}

void esdStatus::createActions()
{
    minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

    maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(maximizeAction, SIGNAL(triggered()), this, SLOT(showMaximized()));

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
}
