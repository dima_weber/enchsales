#ifndef ESDSTATUS_H
#define ESDSTATUS_H

#include <QWidget>
#include "proxy.h"
#include "configproxy.h"
#include "thrproxy.h"
#include <QDBusServiceWatcher>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QAction>

namespace Ui {
    class esdStatus;
}

class QTimer;
class esdStatus : public QWidget
{
    Q_OBJECT

public:
    explicit esdStatus(QWidget *parent = 0);
    ~esdStatus();

private slots:
    void onBNetConnect(bool ok=true);
    void onScanStarted();
    void onScanFinished();
    void onNextAuc();
    void onTimer();
    void onDBTimer();
    void onServiceDown();
    void onServiceUp();
private:
    void createActions();
    void createTrayIcon();
    bool initDBus();

    QDBusServiceWatcher* p_watcher;
    Proxy* proxy;
    DBProxy* dbproxy;
    ConfigProxy* configproxy;
    Ui::esdStatus *ui;

    QAction *minimizeAction;
    QAction *maximizeAction;
    QAction *restoreAction;
    QAction *quitAction;

    QTimer* timer;
    QTimer* dbtimer;

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
};

#endif // ESDSTATUS_H
