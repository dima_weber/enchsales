/*
 * This file was generated by qdbusxml2cpp version 0.7
 * Command line was: qdbusxml2cpp -p proxy -c Proxy
 *
 * qdbusxml2cpp is Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#ifndef PROXY_H_1288880965
#define PROXY_H_1288880965

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>

/*
 * Proxy class for interface net.softwarium.weber.EnchSaleDaemon.Manager
 */
class Proxy: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    static inline const char *staticInterfaceName()
    { return "net.softwarium.weber.EnchSaleDaemon.Manager"; }

public:
    Proxy(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = 0);

    ~Proxy();

    Q_PROPERTY(bool isScanning READ isScanning)
    inline bool isScanning() const
    { return qvariant_cast< bool >(property("isScanning")); }

    Q_PROPERTY(QString state READ state WRITE setState)
    inline QString state() const
    { return qvariant_cast< QString >(property("state")); }
    inline void setState(const QString &value)
    { setProperty("state", qVariantFromValue(value)); }

    Q_PROPERTY(QVariantMap status READ status)
    inline QVariantMap status() const
    { return qvariant_cast< QVariantMap >(property("status")); }

    Q_PROPERTY(QTime timeToNextScan READ timeToNextScan)
    inline QTime timeToNextScan() const
    { return qvariant_cast< QTime >(property("timeToNextScan")); }

    Q_PROPERTY(QVariantMap watchitems READ watchitems WRITE setWatchitems)
    inline QVariantMap watchitems() const
    { return qvariant_cast< QVariantMap >(property("watchitems")); }
    inline void setWatchitems(const QVariantMap &value)
    { setProperty("watchitems", qVariantFromValue(value)); }

public Q_SLOTS: // METHODS
    inline QDBusPendingReply<> pauseScan()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("pauseScan"), argumentList);
    }

    inline QDBusPendingReply<> resumeScan()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("resumeScan"), argumentList);
    }

    inline QDBusPendingReply<> shutdown()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("shutdown"), argumentList);
    }

    inline QDBusPendingReply<> startScan()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("startScan"), argumentList);
    }

    inline QDBusPendingReply<> stopScan()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("stopScan"), argumentList);
    }

Q_SIGNALS: // SIGNALS
    void machineStopped();
    void nextAucStarted();
    void pauseRequest();
    void resumeRequest();
    void shutdownRequest();
    void startRequest();
    void started();
    void stopRequest();
    void stopped();
    void waiting();
};

namespace net {
  namespace softwarium {
    namespace weber {
      namespace EnchSaleDaemon {
        typedef ::Proxy Manager;
      }
    }
  }
}
#endif
