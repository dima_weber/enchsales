#include "logindialog.hpp"
#include "ui_logindialog.h"
#include <QSqlDatabase>
#include <QMessageBox>
#include <QSqlError>

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    settings = new QSettings();

    ui->setupUi(this);
    setLayout(ui->verticalLayout);

    ui->hostLE->setText(settings->value("database/host","host").toString());
    ui->dbnameLE->setText(settings->value("database/dbname","enchsales").toString());
    ui->userLE->setText(settings->value("database/user","user").toString());
    ui->pwdLE->setText(settings->value("database/password","password").toString());
    ui->portLE->setText(settings->value("database/port","3306").toString());
/*
    ui->label_4->hide();
    ui->label_5->hide();
    ui->dbnameLE->hide();
    ui->portLE->hide();
   */
}

LoginDialog::~LoginDialog()
{
    delete ui;
    delete settings;
}

void LoginDialog::onTestConnection()
{
    QSqlDatabase db = QSqlDatabase(QSqlDatabase::addDatabase("QMYSQL", "ping_connection"));
    db.setHostName(ui->hostLE->text());
    db.setDatabaseName(ui->dbnameLE->text());
    db.setUserName(ui->userLE->text());
    db.setPassword(ui->pwdLE->text());
    db.setPort(ui->portLE->text().toInt());

    bool ok = db.open();
    db.close();
    QSqlDatabase::removeDatabase("ping_connection");

    if (ok)
        QMessageBox::information(this, "Test Connection", "Test connection finished succesfully", QMessageBox::Ok);
    else
    {
        QString err_msg = db.lastError().databaseText();
        QMessageBox::warning(this, "Test Connection", "Error during test connection: " + err_msg, QMessageBox::Ok);
    }
}

void LoginDialog::onConnect()
{
    QwbrDB::instance()->disconnect();
    if (QwbrDB::instance()->connect(ui->hostLE->text(), ui->userLE->text(),ui->pwdLE->text(),ui->dbnameLE->text(),ui->portLE->text().toInt()))
    {
        settings->setValue("database/host",ui->hostLE->text());
        settings->setValue("database/dbname",ui->dbnameLE->text());
        settings->setValue("database/user",ui->userLE->text());
        settings->setValue("database/password",ui->pwdLE->text());
        settings->setValue("database/port",ui->portLE->text());
        accept();
    }
    else
    {
        //QString err_msg = db.lastError().databaseText();
        QMessageBox::warning(this, "Database error", "Error when connecting database.", QMessageBox::Ok);
    }
}
