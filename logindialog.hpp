#ifndef LOGINDIALOG_HPP
#define LOGINDIALOG_HPP

#include <QDialog>
#include "qwbrdb.h"
#include <QSettings>

namespace Ui {
    class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();
public  slots:
    void onTestConnection();
    void onConnect();
private:
    Ui::LoginDialog *ui;
    QSettings* settings;
};

#endif // LOGINDIALOG_HPP
