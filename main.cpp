#include <QtGui/QApplication>
#include "mainwindow.h"
#include "qwbrdb.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("dimaweber");
    QCoreApplication::setOrganizationDomain("dimaweber.homeip.net");
    QCoreApplication::setApplicationName("enchsales");

    QApplication a(argc, argv);
    MainWindow w;

    w.show();
    return a.exec();
}
