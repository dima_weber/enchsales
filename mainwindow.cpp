#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qwbrdb.h"
#include "qwbrplot.h"
#include "qwt_plot_price.h"
#include "qwt_price_move.h"
#include <qwt_symbol.h>
#include <QMap>
#include <QMessageBox>
#include <QSqlError>
#include <qwt_plot_marker.h>
#include "logindialog.hpp"
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QPalette>
#include <QSortFilterProxyModel>
#include <QPainter>
#include <QProgressIndicator.h>

#include "enchtreewidget.hpp"

#include <QxtLogger>
#include <QPushButton>

Plot::Plot(QWidget* parent)
    : QwtPlot(parent)
{
    itemid = 0;
    setTitle(QString("Price chart"));

    p_grid = NULL;
    p_marker = NULL;
    p_zoomer = NULL;

    p_legend = new QwtLegend();
    p_legend->setItemMode(QwtLegend::CheckableItem);
    insertLegend(p_legend, QwtPlot::TopLegend);
    // Set axis titles
    setAxisTitle(xBottom, "Date");
    setAxisTitle(yLeft, "Price");
    setCanvasBackground(Qt::white);

    p_draw = new DateDraw();
    setAxisScaleDraw(QwtPlot::xBottom, p_draw);
    //setAxisLabelRotation(QwtPlot::xBottom, -90.0);
    connect(this, SIGNAL(legendChecked(QwtPlotItem *, bool)),
        SLOT(showCurve(QwtPlotItem *, bool)));

    connect (this, SIGNAL(legendChecked(QwtPlotItem*, bool)), SIGNAL(legendClicked()));
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->centralWidget->setLayout(ui->verticalLayout);

    ui->horizontalLayout_3->setDirection(QBoxLayout::RightToLeft);

    p_treewidget = new EnchTreeWidget(this);
    connect(p_treewidget, SIGNAL(serverSelected(QString)), SLOT(onServerSelected(QString)));
    connect(p_treewidget, SIGNAL(factionSelected(QString,QString)), SLOT(onFactionSelected(QString, QString)));
    connect(p_treewidget, SIGNAL(itemSelected(EnchItem::ItemInfo)), SLOT(onItemSelected(EnchItem::ItemInfo)));

    connect( QwbrDB::instance(), SIGNAL(newServerAdded(QString)), p_treewidget, SLOT(addServer(QString)) );
    connect( QwbrDB::instance(), SIGNAL(newFactionAdded(QString, QString)), p_treewidget, SLOT(addFaction(QString,QString)) );
    connect( QwbrDB::instance(), SIGNAL(newItemAdded(EnchItem::ItemInfo, QString,QString)), p_treewidget, SLOT(addItem(EnchItem::ItemInfo,QString,QString)));
    connect( p_treewidget,       SIGNAL(wantMoreData(QString,QString)), SLOT(getItemList(QString,QString)));

    QVBoxLayout* plots_layout = new QVBoxLayout(ui->centralWidget);

    d_plot = new Plot(ui->centralWidget);
   // d_plot->setMinimumSize(800, 650);
    d_plot->setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::Expanding);

    d_plot->addCurve(Plot::plot_item ("min_bo", "Minimal",     Qt::cyan,   2, false));
    d_plot->addCurve(Plot::plot_item ("q25_bo", "Low Quantile",Qt::blue,   2, false));
    d_plot->addCurve(Plot::plot_item ("med_bo", "Median",      Qt::green,  2, true));
    d_plot->addCurve(Plot::plot_item ("avg_bo", "Average",     Qt::darkBlue,2,true));
    d_plot->addCurve(Plot::plot_item ("q75_bo", "Hi Quantile", Qt::red,    2, false));
    d_plot->addCurve(Plot::plot_item ("max_bo", "Maximal",     Qt::magenta,2, false));

    d_candle_plot = new QwtPlot(ui->centralWidget);
    d_candle_plot->setAxisTitle(QwtPlot::yLeft, "Price");
   // d_candle_plot->setMinimumSize(800, 650);
    d_candle_plot->hide();
    price = new QwtPlotPrice();
    price->setStyle(QwtPlotPrice::Candlestick);
    price->setPen(QPen(Qt::black));
    price->setUpBrush(Qt::green);
    price->setDownBrush(Qt::red);

    QPushButton* hide_tree = new QPushButton(this);
    hide_tree->setMinimumWidth(10);
    hide_tree->setMinimumHeight(100);
    hide_tree->setMaximumWidth(10);
    connect (hide_tree, SIGNAL(clicked()), SLOT(onHideShowTree()));

    QPushButton* hide_table = new QPushButton(this);
    hide_table->setMinimumWidth(10);
    hide_table->setMinimumHeight(100);
    hide_table->setMaximumWidth(10);
    connect (hide_table, SIGNAL(clicked()), SLOT(onHideShowTable()));
    ui->tableView->hide();

    QPushButton* switch_plot = new QPushButton(this);
    switch_plot->setMinimumSize(100,10);
    switch_plot->setMaximumSize(100,10);
    connect(switch_plot, SIGNAL(clicked()), SLOT( onSwitchPlot() ));

    ui->horizontalLayout_3->addWidget(hide_table);
    ui->horizontalLayout_3->addLayout(plots_layout);
    plots_layout->addWidget(d_plot);
    plots_layout->addWidget(switch_plot);
    plots_layout->setAlignment(switch_plot,Qt::AlignHCenter);
    plots_layout->addWidget(d_candle_plot);
    ui->horizontalLayout_3->addWidget(hide_tree);
    ui->horizontalLayout_3->addWidget(p_treewidget);


    p_model = new QSqlQueryModel;
    updateTableView();
    connect (d_plot, SIGNAL(legendClicked()), SLOT(updateTableView()));

    connect (ui->tableView->verticalHeader(), SIGNAL(sectionClicked(int)), SLOT(onTableHeaderClicked(int)));

    ui->dateFromEdit->setEnabled(false);
    ui->dateToEdit->setEnabled(false);

    p_indicator = new QProgressIndicator(this);
    p_indicator->setAnimationDelay(200);
    p_indicator->setDisplayedWhenStopped(false);
    p_indicator->resize(100,100);
    p_indicator->move(width()/2-50,height()/2-50);
    p_indicator->lower();


    disableList << ui->mainToolBar
                << ui->menuBar
                << ui->centralWidget;

    QwbrDB::instance()->setProgressIndicator(p_indicator);
    QwbrDB::instance()->setDisableList(disableList);

    QTimer* timer = new QTimer(this);
    connect (timer, SIGNAL(timeout()), SLOT(onCheckUpdateTimer()));
    timer->start(1000 * 30);
}

MainWindow::~MainWindow()
{
    delete ui;
    QwbrDB::instance()->disconnect();
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::onFromDateChange()
{
    QDate date = ui->dateFromEdit->date();
    QwbrDB::instance()->setFromDate(date);

    d_plot->showData();
    updateTableData();
    updateTableView();
}

void MainWindow::onToDateChange()
{
    QDate date = ui->dateToEdit->date();
    QwbrDB::instance()->setToDate(date);

    d_plot->showData();
    updateTableData();
    updateTableView();
}

void MainWindow::updateTableData()
{
    QSqlQuery query = QwbrDB::instance()->mw_getDataQuery();
    p_model->setQuery(query);
    ui->tableView->setModel(p_model);
}

void MainWindow::updateTableView()
{
    int col=0;
    p_model->setHeaderData(col++, Qt::Horizontal, tr("Scan time"));
    foreach(Plot::plot_item* item, d_plot->items)
    {
        p_model->setHeaderData(col, Qt::Horizontal, item->title);
        ui->tableView->setColumnHidden(col, !item->p_curve->isVisible());
        col++;
    }
    ui->tableView->resizeRowsToContents ();
    ui->tableView->resizeColumnsToContents();
}

void MainWindow::setMarker(int indx , int col_indx)
{
    QwtPlotMarker*& p_marker = d_plot->p_marker;
    if(p_marker)
        delete p_marker;
    p_marker = new QwtPlotMarker();
    QwtText label;
    double x = d_plot->items[1]->p_data->sample(indx).x();
    if (col_indx >= 0 && col_indx <d_plot->items.size() )
    {
        double y = d_plot->items[col_indx]->p_data->sample(indx).y();
        QString label_date  = d_plot->p_draw->label(x).text();
        label = QString("%1: %2").arg(label_date).arg(y);
        label.setBackgroundBrush(QColor(225,225,250, 127));
        label.setBackgroundPen(QPen(Qt::black));
        p_marker->setLabelAlignment(Qt::AlignRight | Qt::AlignTop);
        p_marker->setLabelOrientation(Qt::Horizontal);
        p_marker->setSymbol(new QwtSymbol(QwtSymbol::Star2, QColor(Qt::yellow), QColor(Qt::black), QSize(15,15)));
        p_marker->setYValue(y);
    }
    else
    {
        label = d_plot->p_draw->label(x);
        p_marker->setLabelAlignment(Qt::AlignLeft | Qt::AlignBottom);
        p_marker->setLabelOrientation(Qt::Vertical);
        p_marker->setLineStyle(QwtPlotMarker::VLine);
        p_marker->setLinePen(QPen(Qt::black, 3, Qt::DashDotLine));
    }
    p_marker->setLabel(label);
    p_marker->setXValue(x);
    p_marker->attach(d_plot);
    p_marker->show();
    d_plot->replot();
}

void MainWindow::onTableItemClicked(const QModelIndex& index)
{
    setMarker(index.row(), index.column()-1);
}

void QwbrColorDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
   QStyleOptionViewItem opt = option;
   QModelIndex aucid_index = p_model->index(index.row(), 6);
   int ignore = p_model->data(aucid_index).toInt();

   if (ignore == 1)
   {
        painter->fillRect(option.rect, QColor(255,200,200));
       //opt.palette.setColor(QPalette::Base, QColor(255,200,200));
   }
   QStyledItemDelegate::paint(painter, opt, index);
}

QwbrTableViewWithSort::QwbrTableViewWithSort(QWidget *parent)
    : QTableView(parent)
{
}

QwbrTableViewWithSort::~QwbrTableViewWithSort()
{
}

void QwbrTableViewWithSort::onHeaderClick(int indx)
{
    Qt::SortOrder so = sortOrders[indx];
    sortOrders[indx] = so==Qt::AscendingOrder?Qt::DescendingOrder : Qt::AscendingOrder;
    sortByColumn(indx, so);
}

void QwbrTableViewWithSort::onDoubleClickItem(const QModelIndex &index)
{
    QAbstractItemModel* p_model= model();
    QModelIndex aucid_index = p_model->index(index.row(), 5);
    int aucid = p_model->data(aucid_index).toInt();
    QwbrDB::instance()->mw_ignoreAuc(aucid);
}

void MainWindow::onTableItemDoubleClicked(const QModelIndex& index)
{
    QDateTime   scantime = QwbrDB::instance()->mw_Days()[index.row()];
    QwbrTableViewWithSort* p_view = new QwbrTableViewWithSort(NULL);
    QSqlQuery  query = QwbrDB::instance()->mw_getDetailDataQuery(scantime);
    QSqlQueryModel* p_detail_model = new QSqlQueryModel(p_view);
    p_detail_model->setQuery(query);
    QSortFilterProxyModel *p_proxyModel = new QSortFilterProxyModel(p_view);
    p_proxyModel->setSourceModel(p_detail_model);
    p_view->setModel(p_proxyModel);
    p_view->resizeColumnsToContents();
    p_view->hideColumn(5);
    p_view->hideColumn(6);
    QPalette pal = p_view->palette();
    pal.setColor(QPalette::AlternateBase, QColor(225, 250, 250));
    p_view->setSortingEnabled(true);
    p_view->setPalette(pal);
    p_view->setAlternatingRowColors(true);
    p_view->resizeRowsToContents();
    p_view->setWindowFlags(Qt::Tool);
    p_view->setGeometry(QCursor::pos().x(),QCursor::pos().y(), 500, 500);
    //p_view->setWindowTitle(QString("Detail price for %1 on %2").arg(ui->comboBox->currentText()).arg(scantime.toString()));
    p_view->setItemDelegate(new QwbrColorDelegate( p_view->model(), p_view));

    connect (p_view->horizontalHeader(), SIGNAL(sectionClicked(int)),
             p_view,                    SLOT(onHeaderClick(int)));
    connect (p_view, SIGNAL(doubleClicked(QModelIndex)), p_view, SLOT(onDoubleClickItem(QModelIndex)));
    p_view->show();
}

void MainWindow::onTableHeaderClicked(int row_index)
{
    setMarker(row_index, -1);
}

void MainWindow::onConnectionDialog()
{
    LoginDialog diag;
    if (diag.exec() == QDialog::Accepted)
    {
        QMap<QString, QStringList> srv_list = QwbrDB::instance()->mw_getServers();
        //p_enchmodel->clear();
    }
}

void MainWindow::onRefresh()
{
    if (QwbrDB::instance()->reconnect())
    {
        d_plot->showData();
        updateTableData();
        updateTableView();
    }
}

void MainWindow::onLastWeekClick()
{
    QDate max_date = QwbrDB::instance()->mw_getMaxDate();
    QDate min_date = QwbrDB::instance()->mw_getMinDate();

    min_date = max_date.addDays(-7) < min_date ? min_date : max_date.addDays(-7);

    QwbrDB::instance()->setToDate(max_date);
    QwbrDB::instance()->setFromDate(min_date);
    ui->dateFromEdit->setDate(  min_date);
    ui->dateToEdit->setDate(  max_date);

    d_plot->showData();
    updateTableData();
    updateTableView();
}

void MainWindow::onLastTwoWeeksClick()
{
    QDate max_date = QwbrDB::instance()->mw_getMaxDate();
    QDate min_date = QwbrDB::instance()->mw_getMinDate();
    min_date = max_date.addDays(-14) < min_date ? min_date : max_date.addDays(-14);
    QwbrDB::instance()->setToDate(max_date);
    QwbrDB::instance()->setFromDate(min_date);
    ui->dateFromEdit->setDate(  min_date);
    ui->dateToEdit->setDate(  max_date);

    d_plot->showData();
    updateTableData();
    updateTableView();
}

void MainWindow::onLastMonthClick()
{
    QDate max_date = QwbrDB::instance()->mw_getMaxDate();
    QDate min_date = QwbrDB::instance()->mw_getMinDate();
    min_date = max_date.addMonths(-1) < min_date ? min_date : max_date.addMonths(-1);
    QwbrDB::instance()->setToDate(max_date);
    QwbrDB::instance()->setFromDate(min_date);
    ui->dateFromEdit->setDate(  min_date);
    ui->dateToEdit->setDate(  max_date);

    d_plot->showData();
    updateTableData();
    updateTableView();
}

const int bar_width = 20;
QwtScaleDiv buildDivs(const QDate& startDate, const QDate& endDate)
{
    double begin = (startDate.dayOfYear()) * bar_width;
    double end   = (endDate.dayOfYear()+1)* bar_width ;

    int days_range = startDate.daysTo(endDate);
    QList<double> ticks[3];
    QDate date = startDate;
    if (days_range > 7 && days_range < 7 * 8)
    {
        while (date <= endDate)
        {
            /*
            if (date.day() == 1)
                ticks[2].append( date.dayOfYear() * bar_width);
            else
            */
            if (date.dayOfWeek() == 1)
                ticks[2].append(date.dayOfYear() * bar_width);
            else
                ticks[0].append( date.dayOfYear() * bar_width);
            date = date.addDays(1);
        }
    }
    else if (days_range >=7 * 8)
    {
        while (date <= endDate)
        {
            /*
            if (date.day() == 1)
                ticks[2].append( date.dayOfYear() * bar_width);
            else
            */
            if (date.day() == 1)
                ticks[2].append(date.dayOfYear() * bar_width);
            else if (date.dayOfWeek()== 1)
                ticks[0].append( date.dayOfYear() * bar_width);
            date = date.addDays(1);
        }
    }

    QwtScaleDiv _div( begin,  end, ticks);
    return _div;
}

class CandleDateDraw : public QwtScaleDraw
{
    virtual QwtText label(double v) const
    {
        QDate date(QDate::currentDate().year(), 1, 1);
        date = date.addDays(v / bar_width - 1);
        QString res;
        res =  date.toString("dd MMM");
        return QwtText(res);
    }
};

class CandleDateDrawTop : public QwtScaleDraw
{
    virtual QwtText label(double v) const
    {
        QDate date(QDate::currentDate().year(), 1, 1);
        date = date.addDays(v / bar_width - 1);
        QString res;
        res =  date.toString("ddd");
        return QwtText(res);
    }
};

void MainWindow::onFullScreen()
{
    QVector<QwtPriceMove> sample(0);
    QMap<QDate, CandleData> data = QwbrDB::instance()->mw_getCandlesData();
    QDate startDate =data.begin().key();
    QDate endDate = data.begin().key();

    foreach(QDate key, data.keys())
    {
        QwtPriceMove move;
        move.interval = QwtDoubleInterval(key.dayOfYear()*bar_width + 2,key.dayOfYear() * bar_width + bar_width - 2);
        move.open = data[key].open;
        move.close = data[key].close;
        move.high = data[key].max;
        move.low = data[key].min;
        sample.append(move);

        if (key > endDate)
            endDate = key;
        if (key < startDate)
            startDate = key;
    }

        //d_candle_plot->setAxisScale(QwtPlot::yLeft, 289, 500);
//        d_candle_plot->setAxisScale(QwtPlot::xBottom, 1237302000-100, 1237159800+100);

    price->detach();
    d_candle_plot->setTitle("Candles");
    price->setData(sample);
    price->attach(d_candle_plot);


        QwtPlotGrid* p_grid;
        p_grid = new QwtPlotGrid;
        p_grid->enableXMin(true);
        p_grid->enableYMin(true);
        p_grid->setMajPen(QPen(Qt::gray, 1, Qt::SolidLine));
        p_grid->setMinPen(QPen(Qt::gray, 0 , Qt::DotLine));
        p_grid->attach(d_candle_plot);
        QwtScaleDiv div = buildDivs(startDate, endDate);
        //p_grid->setXDiv(div);
        QwtScaleDraw* p_candle_draw = new CandleDateDraw();
        QwtScaleDraw* p_candle_draw2 = new CandleDateDrawTop();
        d_candle_plot->setAxisScaleDraw(QwtPlot::xBottom, p_candle_draw);
        d_candle_plot->setAxisScaleDraw(QwtPlot::xTop, p_candle_draw2);
        d_candle_plot->setAxisScaleDiv(QwtPlot::xBottom, div);
        d_candle_plot->replot();

//        refreshPlotItems(sample, "Demo : Google Inc. (GOOG)");
        /* *** */

}

void MainWindow::onCheckUpdateTimer()
{
    QList<QDateTime> uptimes = QwbrDB::instance()->mw_lastUpdatesTime();
    QString msg = "Update Data";
    if (!uptimes.isEmpty())
    {
        msg = QString("%1 updates ready\nLast: %2").arg(uptimes.size()).arg(uptimes.last().toString());
    }
    else
    {
        msg = QString("No new updates.");
    }
    ui->actionUpdateData->setToolTip(msg);
    ui->actionUpdateData->setDisabled(uptimes.isEmpty());

    QwbrDB::instance()->mw_getItemIDs();
}

void MainWindow::onUpdateDataClick()
{
    qxtLog->debug() << "Update data";
    QwbrDB::instance()->mw_getUpdateData();

    d_plot->showData();
    updateTableData();
    updateTableView();
}

void MainWindow::onServerSelected(const QString &)
{
    // do nothing. Who cares about servers ?
}

void MainWindow::onFactionSelected(const QString & srv, const QString & faction)
{
    getItemList(srv, faction);
}

void MainWindow::onItemSelected(EnchItem::ItemInfo info)
{
    int id = info.itemid;

    QwbrDB::instance()->setServer(info.srv);
    QwbrDB::instance()->setFaction(info.faction);
    QwbrDB::instance()->setItemId(id);

    QDate min_date = QwbrDB::instance()->mw_getMinDate();
    QDate max_date = QwbrDB::instance()->mw_getMaxDate();

    ui->dateFromEdit->setDateRange(min_date, max_date);
    ui->dateFromEdit->setDate(  min_date);

    ui->dateToEdit->setDateRange(min_date, max_date);
    ui->dateToEdit->setDate(  max_date);

    d_plot->setItemId(id);
    updateTableData();
    updateTableView();

    ui->dateFromEdit->setEnabled(true);
    ui->dateToEdit->setEnabled(true);
}

void MainWindow::getItemList(const QString &srv, const QString &faction)
{
    QwbrDB::instance()->mw_getItemIDs(srv, faction);
    QwbrDB::instance()->mw_getItemIcon();
}


void MainWindow::onHideShowTree()
{
    p_treewidget->setVisible(!p_treewidget->isVisible());
}

void MainWindow::onHideShowTable()
{
    ui->tableView->setVisible(!ui->tableView->isVisible());
}

void MainWindow::refreshPlotItems(const QVector<QwtPriceMove>& sample, const QString plotTitle)
{
    price->detach();
    d_candle_plot->setTitle(plotTitle);
    price->setData(sample);
    price->attach(d_candle_plot);
    d_candle_plot->replot();
}

void MainWindow::onSwitchPlot()
{
    d_plot->setVisible( !d_plot->isVisible() );
    d_candle_plot->setVisible( !d_candle_plot->isVisible() );
}
