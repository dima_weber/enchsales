#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDate>
#include <QSqlQueryModel>
#include <QTableView>
#include "qwbrplot.h"
#include <QStyledItemDelegate>
#include <QAbstractItemModel>
#include <QSslError>
#include <QtNetwork>
#include <QList>
#include <QWidgetList>
#include "enchitem.hpp"
#include "qwt_price_move.h"
namespace Ui {
    class MainWindow;
}

class QwtPlotZoomer;
class Plot;
class QwtPlot;
class QHttp;
class QProgressIndicator;
class QwtPlotPrice;
class EnchTreeWidget;

class QwbrTableViewWithSort : public QTableView
{
    Q_OBJECT
public:
    QwbrTableViewWithSort(QWidget* paret = NULL);
    ~QwbrTableViewWithSort();
public slots:
    void onHeaderClick(int indx);
    void onDoubleClickItem(const QModelIndex& index);
private:
    QMap<int, Qt::SortOrder> sortOrders;
};

class QwbrColorDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    QwbrColorDelegate(QAbstractItemModel* model, QWidget *parent = NULL) : QStyledItemDelegate(parent)
    {
        p_model = model;
    }
    void paint(QPainter *painter, const QStyleOptionViewItem &option,
                   const QModelIndex &index) const;
private:
    QAbstractItemModel* p_model;
};

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void onFromDateChange();
    void onToDateChange();
    void updateTableData();
    void updateTableView();
    void onTableItemClicked(const QModelIndex& index);
    void onTableItemDoubleClicked(const QModelIndex& index);
    void onTableHeaderClicked(int row_index);
    void onConnectionDialog();
    void onRefresh();
    void onLastWeekClick();
    void onLastTwoWeeksClick();
    void onLastMonthClick();
    void onCheckUpdateTimer();
    void onUpdateDataClick();
    //void onEnchItemClick(const QModelIndex& index);
    void onServerSelected(const QString&);
    void onFactionSelected(const QString&, const QString&);
    void onItemSelected(EnchItem::ItemInfo);
protected:
    void changeEvent(QEvent *e);
protected slots:
    void setMarker(int indx, int col_indx);
    void onFullScreen();
    void getItemList(const QString& srv, const QString& faction);
    void onHideShowTree();
    void onHideShowTable();
    void onSwitchPlot();
    void refreshPlotItems(const QVector<QwtPriceMove>& sample, const QString plotTitle);
private:
    Ui::MainWindow *ui;
    QwtPlotZoomer *d_zoomer[2];
    Plot* d_plot;
    QwtPlot* d_candle_plot;
    QwtPlotPrice *price;
    QwtPlot* p_global_plot;
    QSqlQueryModel *p_model;
    //QSqlQueryModel*  p_detail_model;
    QProgressIndicator* p_indicator;
    QWidgetList disableList;

    EnchTreeWidget* p_treewidget;
};

#endif // MAINWINDOW_H
