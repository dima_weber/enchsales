/****************************************************************************
** Meta object code from reading C++ file 'enchitem.hpp'
**
** Created: Mon Oct 4 13:00:57 2010
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "enchitem.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'enchitem.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_EnchItemDelegate[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_EnchItemDelegate[] = {
    "EnchItemDelegate\0"
};

const QMetaObject EnchItemDelegate::staticMetaObject = {
    { &QStyledItemDelegate::staticMetaObject, qt_meta_stringdata_EnchItemDelegate,
      qt_meta_data_EnchItemDelegate, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EnchItemDelegate::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EnchItemDelegate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EnchItemDelegate::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EnchItemDelegate))
        return static_cast<void*>(const_cast< EnchItemDelegate*>(this));
    return QStyledItemDelegate::qt_metacast(_clname);
}

int EnchItemDelegate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QStyledItemDelegate::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_EnchItemsModel[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      28,   16,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      62,   58,   15,   15, 0x0a,
      93,   81,   15,   15, 0x0a,
     134,  121,   15,   15, 0x0a,
     185,  178,   15,   15, 0x0a,
     208,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_EnchItemsModel[] = {
    "EnchItemsModel\0\0srv,faction\0"
    "wantMoreData(QString,QString)\0srv\0"
    "addServer(QString)\0faction,srv\0"
    "addFaction(QString,QString)\0,faction,srv\0"
    "addItem(EnchItem::ItemInfo,QString,QString)\0"
    "parent\0fetchMore(QModelIndex)\0clear()\0"
};

const QMetaObject EnchItemsModel::staticMetaObject = {
    { &QAbstractItemModel::staticMetaObject, qt_meta_stringdata_EnchItemsModel,
      qt_meta_data_EnchItemsModel, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EnchItemsModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EnchItemsModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EnchItemsModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EnchItemsModel))
        return static_cast<void*>(const_cast< EnchItemsModel*>(this));
    return QAbstractItemModel::qt_metacast(_clname);
}

int EnchItemsModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractItemModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: wantMoreData((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 1: addServer((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: addFaction((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 3: addItem((*reinterpret_cast< const EnchItem::ItemInfo(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 4: fetchMore((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 5: clear(); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void EnchItemsModel::wantMoreData(const QString & _t1, const QString & _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
