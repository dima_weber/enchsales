#ifndef QWBRDATETIMESCALEENGINE_H
#define QWBRDATETIMESCALEENGINE_H

#include <qwt_scale_engine.h>
#include <qwt_scale_div.h>

class QwbrDateTimeScaleEngine : public QwtScaleEngine
{
public:
    QwbrDateTimeScaleEngine();
protected:
    virtual QwtScaleDiv divideScale(double x1,
                                    double x2,
                                    int maxMajSteps,
                                    int maxMinSteps,
                                    double stepSize) const;
    virtual void autoScale(int maxNumSteps,
                           double &x1,
                           double &x2,
                           double &stepSize) const;
    virtual QwtScaleTransformation* transformation() const;
};

#endif // QWBRDATETIMESCALEENGINE_H
