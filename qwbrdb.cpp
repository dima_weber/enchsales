#include "qwbrdb.h"
#include <QMessageBox>
#include <QSqlError>
#include <QTextCodec>
#include <QTextDecoder>
#include "threadexec.h"
#include <QMutex>

void dump_server_variables()
{
    QSqlQuery query;
    query.exec("SELECT @itemid, @itemmods, @srv, @faction, UNIX_TIMESTAMP(@from_date), UNIX_TIMESTAMP(@to_date),@watchid,@scanid");
    query.next();
    qDebug() << QString("------------------\nitemid: %1\nitemmods: %2\nsrv: %3\nfaction: %4\nfrom_date: %5\nto_date: %6\nwatchid: %7\nscanid: %8")
            .arg(query.value(0).toInt())
            .arg(query.value(1).toString())
            .arg(query.value(2).toString())
            .arg(query.value(3).toString())
            .arg(query.value(4).toInt())
            .arg(query.value(5).toInt())
            .arg(query.value(6).toInt())
            .arg(query.value(7).toInt());

}


QwbrDB::QwbrDB(QObject *parent) :
    DBInterface(parent)
{
    db = NULL;
    itemid = 0;
    from_date = QDate::currentDate();
    to_date = QDate::currentDate();
    srv = "";
    faction = "";

    p_indicator = NULL;
}

bool QwbrDB::connect(QString hostname,
                     QString username,
                     QString password,
                     QString dbname,
                     int port)
{
    if (!db)
        db = new QSqlDatabase(QSqlDatabase::addDatabase("QMYSQL"));
    if (!hostname.isEmpty())
    {
        db->setHostName(hostname);
        db->setDatabaseName(dbname);
        db->setUserName(username);
        db->setPassword(password);
        db->setPort(port);
        ThreadConnect::execConnect(db, disableList, p_indicator);
        return db->isOpen();
    }
    else
        return false;
}


QwbrDB* QwbrDB::qwbrdb = NULL;

QwbrDB* QwbrDB::instance()
{
    if (qwbrdb == NULL)
    {
        qwbrdb = new QwbrDB();
        qwbrdb->connect();
    }
    return qwbrdb;
}

QMap<int, QString> QwbrDB::mw_getItemIDs(const QString &_srv, const QString &_faction)
{
    setServer(_srv);
    setFaction(_faction);
    return mw_getItemIDs();
}

QMap<int, QString> QwbrDB::mw_getItemIDs()
{
    QMap<int, QString> res;
    QSqlQuery query;

    QString sql = "call mw_getItemNames()";
    //dump_server_variables();
    ThreadExec::execQuery(&query, sql, disableList, p_indicator);
    while (query.next())
    {
        int id = query.value(0).toInt();
        QString name = query.value(1).toString();
        QDateTime timestmp = query.value(2).toDateTime();
        float ea = query.value(3).toFloat();
        float avg_sell = query.value(4).toFloat();
        res[id] = name;

        EnchItem::ItemInfo info;
        info.itemid = id;
        info.name = name;
        info.trashhold = avg_sell;
        info.auc_price = ea;
        info.lastScanTime = timestmp;
        info.srv = srv;
        info.faction = faction;
        info.type = EnchItem::Item;

        emit newItemAdded(info, faction, srv);
    }
    return res;
}


const QList<QDateTime>& QwbrDB::mw_Days()
{
    if (!days_map.contains(itemid) && itemid > 0)
    {
        //dump_server_variables();
        QSqlQuery query;
        QString sql = "call mw_Days()";
        //dump_server_variables();
        ThreadExec::execQuery(&query, sql, disableList, p_indicator);
        while (query.next())
        {
            days_map[itemid].append( query.value(0).toDateTime() );
        }
    }
    return days_map[itemid];
}

float QwbrDB::mw_quantile(QString name, QDateTime date)
{
   float res;
   if (data.isEmpty())
   {
        QSqlQuery query = mw_getDataQuery();
        while(query.next())
        {
            QDateTime d = query.value(0).toDateTime();
            data[d]["min_bo"] = query.value(1).toFloat();
            data[d]["q25_bo"] = query.value(2).toFloat();
            data[d]["med_bo"] = query.value(3).toFloat();
            data[d]["avg_bo"] = query.value(4).toFloat();
            data[d]["q75_bo"] = query.value(5).toFloat();
            data[d]["max_bo"] = query.value(6).toFloat();
            data[d]["volume"] = query.value(7).toInt();
            data[d]["vendors"] = query.value(8).toInt();
        }
   }
   res = data[date][name];
   return res;
}

float QwbrDB::mw_getMaxQuantileValue(QString name)
{
    if (!itemid)
        return 1;
    float res;
    if (max_values.isEmpty())
    {
        QSqlQuery query;
        QString sql = "call mw_getMaxValues()";
        //dump_server_variables();
        ThreadExec::execQuery(&query, sql, disableList, p_indicator);
        if(query.next())
        {
            max_values["min_bo"] = query.value(0).toFloat();
            max_values["q25_bo"] = query.value(1).toFloat();
            max_values["med_bo"] = query.value(2).toFloat();
            max_values["avg_bo"] = query.value(3).toFloat();
            max_values["q75_bo"] = query.value(4).toFloat();
            max_values["max_bo"] = query.value(5).toFloat();
            max_values["volume"] = query.value(6).toInt();
            max_values["vendors"] = query.value(7).toInt();
        }
    }
    res =  max_values[name];
    return res;
}

float QwbrDB::mw_getMinQuantileValue(QString name)
{
    if (!itemid)
        return 0;
    float res;
    if (min_values.isEmpty())
    {
        QSqlQuery query;
        QString sql = "call enchsales.mw_getMinValues()";
        //dump_server_variables();
        ThreadExec::execQuery(&query, sql, disableList, p_indicator);
        if(query.next())
        {
            min_values["min_bo"] = query.value(0).toFloat();
            min_values["q25_bo"] = query.value(1).toFloat();
            min_values["med_bo"] = query.value(2).toFloat();
            min_values["avg_bo"] = query.value(3).toFloat();
            min_values["q75_bo"] = query.value(4).toFloat();
            min_values["max_bo"] = query.value(5).toFloat();
            min_values["volume"] = query.value(6).toInt();
            min_values["vendors"] = query.value(7).toInt();
        }
    }
    res =  min_values[name];
    return res;
}

QMap<QString, QStringList>  QwbrDB::mw_getServers()
{
    QMap<QString, QStringList> res;
    QSqlQuery query;
    QString sql = "call mw_getServers()";
    //dump_server_variables();
    ThreadExec::execQuery(&query, sql, disableList, p_indicator);

    while (query.next())
    {
        QString s = query.value(0).toString();
        QString f = query.value(1).toString();
        res[s].append(f);

        emit newServerAdded(s);
        emit newFactionAdded(f, s);
    }

    return res;
}

void QwbrDB::setServer(QString s)
{
    srv = s;

    QSqlQuery query;
    query.prepare("SET @srv=:srv");
    query.bindValue(":srv", srv);
    ThreadExec::execQuery(&query, disableList, p_indicator);
}
void QwbrDB::setFaction(QString f)
{
    faction = f;

    QSqlQuery query;
    query.prepare("SET @faction=:faction");
    query.bindValue(":faction", faction);
    ThreadExec::execQuery(&query, disableList, p_indicator);
}

QDate QwbrDB::mw_getMinDate()
{
    QDate res;
    QSqlQuery query;
    QString sql = " call mw_getMinDate()";
    //dump_server_variables();
    ThreadExec::execQuery(&query, sql, disableList, p_indicator);
    if (query.next())
        res = query.value(0).toDate();
    return res;
}

QDate QwbrDB::mw_getMaxDate()
{
    QDate res;
    QSqlQuery query;
    QString sql = " call mw_getMaxDate()";
    //dump_server_variables();
    ThreadExec::execQuery(&query, sql, disableList, p_indicator);
    if (query.next())
        res = query.value(0).toDate();
    return res;
}

void QwbrDB::setFromDate(QDate from)
{
    if (from_date != from)
    {
        from_date = from;
        data.clear();
        min_values.clear();
        max_values.clear();
        days_map.clear();
    }
    QSqlQuery query;
    query.prepare("SET @from_date=:from_date");
    query.bindValue(":from_date", QDate(from_date));
    ThreadExec::execQuery(&query, disableList, p_indicator);
    //dump_server_variables();
}

void QwbrDB::setToDate(QDate to)
{
    if  (to_date != to)
    {
        to_date = to;
        data.clear();
        min_values.clear();
        max_values.clear();
        days_map.clear();
    }
    QSqlQuery query;
    query.prepare("SET @to_date=:to_date");
    query.bindValue(":to_date", to_date);
    ThreadExec::execQuery(&query, disableList, p_indicator);
    //dump_server_variables();
}

void QwbrDB::setItemId(int id)
{
    itemid = id;
    data.clear();
    min_values.clear();
    max_values.clear();
    days_map.clear();

    QSqlQuery query;
    query.prepare("SET @itemid=:itemid");
    query.bindValue(":itemid", itemid);
    ThreadExec::execQuery(&query, disableList, p_indicator);

    query.prepare("SET @itemmods=:itemmods");
    query.bindValue(":itemmods", "0:0:0:0:0:0");
    ThreadExec::execQuery(&query, disableList, p_indicator);

    QString sql = "set @watchid=(SELECT watchid from watcheditems where srv=@srv and itemid=@itemid and faction=@faction and itemmods=@itemmods)";
    ThreadExec::execQuery(&query, sql, disableList, p_indicator);
    //dump_server_variables();

    setToDate(mw_getMaxDate());
    setFromDate(mw_getMinDate());

}

QSqlQuery  QwbrDB::mw_getDataQuery()
{
    QSqlQuery query;
    QString sql = "call mw_getGeneralData()";
    //dump_server_variables();
    ThreadExec::execQuery(&query, sql, disableList, p_indicator);
    return query;
}

QSqlQuery     QwbrDB::mw_getDetailDataQuery(QDateTime scantime)
{
    QSqlQuery query;
    query.prepare("SELECT scanid into @scanid from scantimes where watchid=@watchid and timestmp=:scantime");
    query.bindValue(":scantime", scantime);
    ThreadExec::execQuery(&query, disableList, p_indicator);

    QString sql = "call mw_getDetailData()";
    //dump_server_variables();
    ThreadExec::execQuery(&query, sql, disableList, p_indicator);
    return query;
}

bool QwbrDB::disconnect()
{
    db->close();
/*
    QSqlDatabase::removeDatabase(db->connectionName());
    delete db;
    db = NULL;
*/
    return true;
}

bool QwbrDB::reconnect()
{
    QString host = db->hostName();
    QString dbname = db->databaseName();
    QString user = db->userName();
    QString pass = db->password();
    int port = db->port();

    bool ok = true;
   ok = disconnect();
    if (ok)
        ok = connect(host, user, pass, dbname, port);
    if (ok)
    {
        int id = itemid;
        QDate f=from_date;
        QDate t=to_date;
        //setItemId(0);
        setServer(srv);
        setFaction(faction);
        setItemId(id);
        setFromDate(f);
        setToDate(t);
    }
    return ok;
}

void QwbrDB::mw_ignoreAuc(int aucid)
{
    QSqlQuery query;
    query.prepare(" UPDATE marketwatcher set ignore_bo = NOT ignore_bo where aucid=:aucid");
    query.bindValue(":aucid", aucid);
    //dump_server_variables();
    ThreadExec::execQuery(&query, disableList, p_indicator);
}

bool already_loading_icons = false;
QPixmap QwbrDB::mw_getItemIcon(int _id)
{
    QPixmap res;
    res.load(":/icons/icons/question.png");
    if(icon_cash.isEmpty() && !already_loading_icons)
    {
        already_loading_icons = true;
        if (_id == -1)
            _id = itemid;
        QSqlQuery query;
        query.prepare("SELECT itemid, icon from iteminfo");
        //dump_server_variables();
        ThreadExec::execQuery(&query, disableList, p_indicator);
        if (query.isActive())
        {
            while(query.next())
            {
                int id = query.value(0).toInt();
                QByteArray buf = query.value(1).toByteArray();
                icon_cash[id].loadFromData(buf);
            }
        }
    }
    if (!icon_cash.isEmpty() && icon_cash.contains(_id))
        res = icon_cash[_id];
    return res;
}

void QwbrDB::setProgressIndicator(QProgressIndicator* p)
{
    p_indicator = p;
}
void QwbrDB::setDisableList(QWidgetList list)
{
    disableList = list;
}

QList<QDateTime> QwbrDB::mw_lastUpdatesTime()
{
    QList<QDateTime> res;
    if (itemid != 0)
    {
        QSqlQuery query;
        QString sql = "call mw_getUpdateTimes()";
        ThreadExec::execQuery(&query, sql, disableList, p_indicator);
        while( query.next())
        {
            res.append(query.value(0).toDateTime());
        }
    }
    return res;
}

void QwbrDB::mw_getUpdateData()
{
    QString sql = "call mw_getGeneralDataUpdate()";
    QSqlQuery query;
    ThreadExec::execQuery(&query, sql, disableList, p_indicator);
    while( query.next())
    {
        QDateTime d = query.value(0).toDateTime();
        data[d]["min_bo"] = query.value(1).toFloat();
        data[d]["q25_bo"] = query.value(2).toFloat();
        data[d]["med_bo"] = query.value(3).toFloat();
        data[d]["avg_bo"] = query.value(4).toFloat();
        data[d]["q75_bo"] = query.value(5).toFloat();
        data[d]["max_bo"] = query.value(6).toFloat();
        data[d]["volume"] = query.value(7).toInt();
        data[d]["vendors"] = query.value(8).toInt();
        days_map[itemid].append(d);
    }
    sql ="SELECT @from_date, @to_date";
    ThreadExec::execQuery(&query, sql, disableList, p_indicator);
    query.next();
    from_date = query.value(0).toDate();
    to_date= query.value(1).toDate();
    min_values.clear();
    max_values.clear();
}

QMap<QDate, CandleData> QwbrDB::mw_getCandlesData()
{
    QMap<QDate, CandleData> res;
    QString sql = "call mw_getCandlesData()";
    QSqlQuery query;
    ThreadExec::execQuery(&query, sql, disableList, p_indicator);
    while( query.next())
    {
        CandleData data;
        data.date = query.value(0).toDate();
        data.min = query.value(1).toFloat();
        data.max = query.value(2).toFloat();
        data.open = query.value(3).toFloat();
        data.close = query.value(4).toFloat();

        res[data.date]=data;
    }
    return res;
}
