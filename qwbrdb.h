#ifndef QWBRDB_H
#define QWBRDB_H

#include "enchitem.hpp"
#include "dbinterface.hxx"
#include <QObject>
#include <QString>
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>
#include <QVariant>
#include <QList>
#include <QPair>


class QProgressIndicator;
#include <QWidgetList>

class QwbrDB : public DBInterface
{

Q_OBJECT
    QSqlDatabase* db;
    static QwbrDB* qwbrdb;
    explicit QwbrDB(QObject *parent = 0);
public:
    static QwbrDB* instance();
public slots:
    bool         connect(QString hostname="",
                         QString username="",
                         QString password="",
                         QString dbname="",
                         int port = 3306);
    bool        disconnect();
    bool        reconnect();

    QMap<int, QString>          mw_getItemIDs();
    QMap<int, QString>          mw_getItemIDs(const QString& _srv, const QString& _faction);
    QMap<QString, QStringList>  mw_getServers();

    QPixmap                     mw_getItemIcon(int id = -1);

    //int                       mw_DaysCount();
    const QList<QDateTime>&     mw_Days();
    float                       mw_quantile(QString name, QDateTime date);
    float                       mw_getMaxQuantileValue(QString name);
    float                       mw_getMinQuantileValue(QString name);
    QDate                       mw_getMinDate();
    QDate                       mw_getMaxDate();
    QSqlQuery                   mw_getDataQuery();
    QSqlQuery                   mw_getDetailDataQuery(QDateTime scantime);
    void                        mw_ignoreAuc(int);
    QList<QDateTime>            mw_lastUpdatesTime();
    void                        mw_getUpdateData();

    QMap<QDate, CandleData> mw_getCandlesData();

    void setServer(QString s);
    void setFaction(QString f);
    void setFromDate(QDate from);
    void setToDate(QDate to);
    void setItemId(int id);

    void setProgressIndicator(QProgressIndicator* p);
    void setDisableList(QWidgetList list);
signals:
    void newServerAdded(const QString& );
    void newFactionAdded(const QString&, const QString&  );
    void newItemAdded(EnchItem::ItemInfo, const QString&, const QString& );
private:
    typedef  QMap<QString, float> data_item;
    QMap<QDateTime, data_item> data;
    QMap<int, QPixmap> icon_cash;

    data_item min_values;
    data_item max_values;

    QMap<int, QList<QDateTime> >        days_map;

    QString srv;
    QString faction;

    QDate from_date;
    QDate to_date;

    int itemid;

    QProgressIndicator* p_indicator;
    QWidgetList disableList;
};


#endif // QWBRDB_H
