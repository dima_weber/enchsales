#ifndef QWBRPLOT_H
#define QWBRPLOT_H
#include <qwt_plot.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_legend.h>
#include <qwt_legend_item.h>
#include <qwt_series_data.h>
#include <qwt_text.h>
#include <qwt_math.h>
#include <qwt_scale_draw.h>
#include <QDateTime>
#include "qwbrdb.h"
//#include <algorithm>

/*
class Zoomer: public QwtPlotZoomer
{
public:
    Zoomer(int xAxis, int yAxis, QwtPlotCanvas *canvas):
        QwtPlotZoomer(xAxis, yAxis, canvas)
    {
        setSelectionFlags(QwtPicker::DragSelection | QwtPicker::CornerToCorner);
        setTrackerMode(QwtPicker::ActiveOnly);
        setRubberBand(QwtPicker::NoRubberBand);

        // RightButton: zoom out by 1
        // Ctrl+RightButton: zoom out to full size

        setMousePattern(QwtEventPattern::MouseSelect2,
            Qt::RightButton, Qt::ControlModifier);

        setMousePattern(QwtEventPattern::MouseSelect3,
            Qt::RightButton);
    }
};
*/

class DateDraw: public QwtScaleDraw
{
public:
    DateDraw()
    {
    }
    virtual QwtText label(double v) const
    {
        const QList<QDateTime>& days = QwbrDB::instance()->mw_Days();
        QString res;
        if (!days.empty())
            res =  days.first().addSecs(v).toString();
        return res;
    }
};

#ifdef DBDATA
class DBData: public QwtData
{
public:
    DBData(const DBData& c)
    {
        name = c.name;
    }

    DBData(QString n)
    {
        name = n;
    }

    virtual QwtData *copy() const
    {
        return new DBData(*this);
    }

    virtual size_t size() const
    {
        return QwbrDB::instance()->mw_Days().size();
    }

    virtual double x(size_t i) const
    {
        return QwbrDB::instance()->mw_Days()[0].secsTo(QwbrDB::instance()->mw_Days()[i]);
    }

    virtual double y(size_t i) const
    {
        if (i<size())
        {
            QDateTime day = QwbrDB::instance()->mw_Days()[i];
            return QwbrDB::instance()->mw_quantile(name, day);
        }
        else
            return 0;
    }

private:
    QString name;
};
#endif

class Plot : public QwtPlot
{
    Q_OBJECT
public:
    Plot(QWidget* parent);
    void setItemId(int id)
    {
       // if (id>0 and id != itemid)
        {
            /*
            if(p_zoomer)
            {
                p_zoomer->zoom(0);
                delete p_zoomer;
            }
            p_zoomer = NULL;
            */
            itemid = id;
//            p_draw->setItemId(id);
            if (p_marker)
            {
                delete p_marker;
                p_marker = NULL;
            }
            showData();
        }
    }
    struct plot_item
    {
        QwtPlotCurve* p_curve;
        QString title;
        QColor color;
        QwtPointArrayData* p_data;
        bool show;
        QString name;
        int width;

        plot_item(QString _n, QString _t="", QColor _c=Qt::red, int _w = 1, bool _s=true)
        {
            name = _n;
            title = _t;
            color = _c;
            show  = _s;
            p_curve = NULL;
            p_data = NULL;
            width = _w;
        }
    };
    DateDraw* p_draw;
    QwtPlotMarker* p_marker;
    QList<plot_item*> items;
signals:
    void legendClicked();
public slots:
    void showData()
    {
        foreach (plot_item* item, items)
        {
           if (item->p_data)
           {
               //delete item->p_data;
               //item->p_data = NULL;
           }
           if (!QwbrDB::instance()->mw_Days().isEmpty())
           {

#ifndef DBDATA
               QDateTime start;
               start = QwbrDB::instance()->mw_Days()[0];
               QVector<double> x;
               QVector<double> y;
               for (int i=0;i< QwbrDB::instance()->mw_Days().size();i++)
               {
                   QDateTime day = QwbrDB::instance()->mw_Days()[i];
                   x.append(start.secsTo(day));
                   y.append(QwbrDB::instance()->mw_quantile(item->name, day));
               }

               item->p_data = new QwtPointArrayData(x,y);
#else
               item->p_data = new DBData(item->name);
#endif
               item->p_curve->setData(item->p_data);
            }
        }

        rescale();
    }
    void showCurve(QwtPlotItem *item, bool on)
    {
        item->setVisible(on);
        QWidget *w = legend()->find(item);
        if ( w && w->inherits("QwtLegendItem") )
            dynamic_cast<QwtLegendItem *>(w)->setChecked(on);
        rescale();
    }

    void rescale()
    {
        float max_v = 0;
        float min_v = 0xFFFFFFFF;
        foreach (plot_item* i, items)
        {
           if (i->p_curve && i->p_curve->isVisible())
            {
                max_v = std::max(max_v, QwbrDB::instance()->mw_getMaxQuantileValue(i->name));
                min_v = std::min(min_v, QwbrDB::instance()->mw_getMinQuantileValue(i->name));
            }
        }


        setAxisScale(yLeft, min_v * 0.95, max_v * 1.05 );
        if (!QwbrDB::instance()->mw_Days().isEmpty())
        {
            QDateTime first_day = QwbrDB::instance()->mw_Days().first();
            QDateTime last_day = QwbrDB::instance()->mw_Days().last();
            setAxisScale(xBottom, 0, first_day.secsTo(last_day) );
        }


        /*
        setAxisAutoScale(yLeft);
        setAxisAutoScale(xBottom);
        */

        if (p_grid)
            delete p_grid;
        p_grid = new QwtPlotGrid;
        p_grid->enableXMin(true);
        p_grid->enableYMin(true);
        p_grid->setMajPen(QPen(Qt::black, 0, Qt::DotLine));
        p_grid->setMinPen(QPen(Qt::gray, 0 , Qt::DotLine));
        p_grid->attach(this);

        if(p_zoomer)
            delete p_zoomer;
        p_zoomer = new QwtPlotZoomer( QwtPlot::xBottom, QwtPlot::yLeft, this->canvas());
        //p_zoomer->setRubberBand(QwtPicker::HLineRubberBand);
        p_zoomer->setRubberBand(QwtPicker::RectRubberBand);
        p_zoomer->setRubberBandPen(QColor(Qt::black));
        p_zoomer->setTrackerMode(QwtPicker::ActiveOnly);
        //p_zoomer->setTrackerMode(QwtPicker::AlwaysOff);
        //p_zoomer->setTrackerMode(QwtPicker::AlwaysOn);
        p_zoomer->setTrackerPen(QColor(Qt::darkBlue));
        p_zoomer->setResizeMode(QwtPicker::Stretch);
        //p_zoomer->setSelectionFlags(QwtPicker::DragSelection | QwtPicker::RectSelection);

        // RightButton: zoom out by 1
        // Ctrl+RightButton: zoom out to full size

        p_zoomer->setMousePattern(QwtEventPattern::MouseSelect2,
            Qt::RightButton, Qt::ControlModifier);

        p_zoomer->setMousePattern(QwtEventPattern::MouseSelect3,
            Qt::RightButton);

        replot();
    }
    void addCurve(plot_item item)
    {
        plot_item* new_curve = new plot_item(item.name, item.title, item.color, item.width, item.show);
        new_curve->p_curve = new QwtPlotCurve(new_curve->title);
        new_curve->p_curve->setRenderHint(QwtPlotItem::RenderAntialiased);
        new_curve->p_curve->setPen(QPen(new_curve->color, new_curve->width));
        new_curve->p_curve->attach(this);
        showCurve(new_curve->p_curve, new_curve->show);

        items.append(new_curve);
    }
private:
   int itemid;
   QwtLegend *p_legend;
   QwtPlotGrid *p_grid;
   QwtPlotZoomer* p_zoomer;
};

#endif // QWBRPLOT_H
