/* -*- mode: C++ ; c-file-style: "stroustrup" -*- *****************************
 * Qwt Widget Library
 * Copyright (C) 1997   Josef Wilgen
 * Copyright (C) 2002   Uwe Rathmann
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Qwt License, Version 1.0
 *****************************************************************************/

#include "qwt_math.h"
#include <qwt_series_data.h>
#include "qwt_move_series_data.h"

/*!
  \brief Calculate the bounding rect of a series

  Slow implementation, that iterates over the series.

  \param series Series
  \return Bounding rectangle
*/
QRectF qwtBoundingRect(const QwtSeriesData<QwtPriceMove>& series)
{
    double minX, maxX, minY, maxY;
    minX = maxX = minY = maxY = 0.0;

    bool isValid = false;

    const size_t sz = series.size();
    for ( size_t i = 0; i < sz; i++ )
    {
        const QwtPriceMove price = series.sample(i);

        if ( !price.interval.isValid() )
            continue;

        if ( !isValid )
        {
            minX = price.interval.minValue();
            maxX = price.interval.maxValue();
            minY = price.low;
            maxY = price.high;
            
            isValid = true;
        }
        else
        {
            if ( price.interval.minValue() < minX )
                minX = price.interval.minValue();
            if ( price.interval.maxValue() > maxX )
                maxX = price.interval.maxValue();

            if ( price.low < minY )
                minY = price.low;
            if ( price.high > maxY )
                maxY = price.high;
        }
    }
    if ( !isValid )
        return QRectF(1.0, 1.0, -2.0, -2.0); // invalid

    return QRectF(minX, minY, maxX - minX, maxY - minY);
}


/*! 
   Constructor
   \param samples Samples
*/
QwtPriceMoveSeriesData::QwtPriceMoveSeriesData(const QVector<QwtPriceMove> &samples)
    : QwtArraySeriesData<QwtPriceMove>(samples)
{
    qDebug() << "1";
}   

QwtSeriesData<QwtPriceMove> *QwtPriceMoveSeriesData::copy() const
{
    return new QwtPriceMoveSeriesData(d_samples);
}

QRectF QwtPriceMoveSeriesData::boundingRect() const
{
    return qwtBoundingRect(*this);
}


