/* -*- mode: C++ ; c-file-style: "stroustrup" -*- *****************************
 * Qwt Widget Library
 * Copyright (C) 1997   Josef Wilgen
 * Copyright (C) 2002   Uwe Rathmann
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Qwt License, Version 1.0
 *****************************************************************************/

// vim: expandtab

#ifndef QWT_MOVE_SERIES_DATA_H
#define QWT_MOVE_SERIES_DATA_H 1

#include "qwt_global.h"
#include "qwt_price_move.h"
#include <qwt_series_data.h>
#include <qwt_double_range.h>
//! Interface for iterating over an array of price moves
class QWT_EXPORT QwtPriceMoveSeriesData: public QwtArraySeriesData<QwtPriceMove>
{
    public:
        QwtPriceMoveSeriesData(const QVector<QwtPriceMove> & = QVector<QwtPriceMove>());

        virtual QwtSeriesData<QwtPriceMove> *copy() const;
        virtual QRectF boundingRect() const;
};


QWT_EXPORT QRectF qwtBoundingRect(
    const QwtSeriesData<QwtPriceMove> &);

#if defined(QWT_TEMPLATEDLL)
// MOC_SKIP_BEGIN
template class QWT_EXPORT QwtArray<QwtPriceMove>;
// MOC_SKIP_END
#endif

#endif // !QWT_SERIES_DATA_H
