#include "bdbclient.h"
#include <QDebug>

BDBClient::BDBClient(DataStorage& d, QObject *parent) :
    DBInterface(parent), ds(d)
{
}

QMap<QString, QStringList> BDBClient::mw_getServers()
{
    QMap<QString, QStringList> res;
    Dbc* pCursor =0;
    DbTxn* pTrans =0;
    int ret;

    ret = ds.pEnv->txn_begin(0, &pTrans, 0);
    ret = ds.dbMap["watchitem"]->secDb["server:faction"]->cursor(pTrans, &pCursor, 0);

    Dbt skey;
    Dbt pkey;
    Dbt data;

    pkey.set_flags(DB_DBT_MALLOC);
    data.set_flags(DB_DBT_MALLOC);
    memset(&skey, 0, sizeof(skey));

    QVariant var;
    QVariantMap varMap;
    while (pCursor->pget(&skey, &pkey, &data, DB_NEXT_NODUP) == 0)
    {
        var = dbtToVariant(data);
        free (pkey.get_data());
        free (data.get_data());

        varMap = var.toMap();
        QString server = varMap["server"].toString();
        QString faction = varMap["faction"].toString();
        res[server].append(faction);
        qDebug() << QString("server: %1, faction: %2").arg(server).arg(faction);
    }
    pCursor->close();
    pTrans->commit(0);

    return res;
}

QMap<int, QString> BDBClient::mw_getItemIDs(const QString &_srv, const QString &_faction)
{
    QMap<int, QString> res;
    Dbc* pCursor = 0;
    DbTxn* pTrans = 0;
    int ret;


    qxtLog->debug() << QString("Get Items for %1/%2").arg(_srv).arg(_faction);

    Dbt key;
    Dbt pkey;
    Dbt data;

    qulonglong watchid;
    memset(&key,0, sizeof(key));
    data.set_flags(DB_DBT_MALLOC);

    pkey.set_flags(DB_DBT_USERMEM);
    pkey.set_data(&watchid);
    pkey.set_size(sizeof(watchid));
    pkey.set_ulen(sizeof(watchid));

    QVariantMap filter;
    filter["server"] = _srv;
    filter["faction"] = _faction;

    key = variantToDbt(filter);
    QVariant var;
    QVariantMap varMap;

    try
    {
        ret = ds.pEnv->txn_begin(0, &pTrans, 0);
        ret = ds.dbMap["watchitem"]->secDb["server:faction"]->cursor(pTrans, &pCursor, 0);
        ret = pCursor->pget(&key, &pkey, &data, DB_FIRST);
        while(!ret)
        {
            var = dbtToVariant(data);
            free (data.get_data());
            varMap = var.toMap();
            int itemid = varMap["itemid"].toInt();
            QString itemname = varMap["item"].toString();
            res[itemid] = itemname;
            qDebug() << QString("id: %1, name: %2 (%3/%4 %5)").arg(itemid).arg(itemname).arg(varMap["server"].toString()).arg(varMap["faction"].toString()).arg(watchid);
            ret = pCursor->pget(&key, &pkey, &data, DB_NEXT_DUP);
        }
        pCursor->close();
        pTrans->commit(0);
        qDebug() << QString("Total count: %1").arg(res.count());
    }
    catch(DbException& e)
    {
        qxtLog->error() << e.what();
        if(pTrans)
            pTrans->abort();
    }

    return res;
}
