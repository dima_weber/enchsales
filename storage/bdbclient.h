#ifndef BDBCLIENT_H
#define BDBCLIENT_H

#include "../dbinterface.hxx"
#include "convert.hxx"
#include "main.h"

class BDBClient : public DBInterface
{
    Q_OBJECT
    DbEnv* pEnv;
    DataStorage& ds;
public:
    explicit BDBClient(DataStorage& d, QObject *parent = 0);

signals:

public slots:
    /**
      Список всех серверов и фраккий в базе.  Если в базе нету ни одного сервера -- пустой результат.
      @return ключ - сервер. значение - все имеющиеся для этого сервера фракции
    */
    virtual QMap<QString, QStringList>  mw_getServers();
    /**
      Список всех предметов, имеющихся для указанного сервера за указнную фракцию.
      @arg _srv строка с именем сервера.
      @arg _faction строка с именем фракции.
      @return ключ - id предмета, значение -- имя предмета
      */
    virtual QMap<int, QString>          mw_getItemIDs(const QString &_srv, const QString &_faction);
};

#endif // BDBCLIENT_H
