#include "convert.hxx"
#include <stdlib.h>
#include <QDebug>

ICrypter* Converter::crypter = 0;
ICrypter dummy;

bool ICrypter::decrypt(const QByteArray &cipher, QByteArray &plain)
{
    plain = cipher;
    plain.clear();
    int size = cipher.size();
    for(int i=0;i<cipher.size();i++)
        plain.append(cipher.at(size - i - 1));
    return true;
}

bool ICrypter::encrypt(const QByteArray &plain, QByteArray &cipher)
{
    cipher = plain;
    cipher.clear();
    int size = plain.size();
    for(int i=0;i<plain.size();i++)
        cipher.append(plain.at(size - i - 1));
    return true;
}

QDomElement Converter::variantToDomXml(const QVariant& var, QDomDocument& doc)
{
    QDomElement n;
    if (var.canConvert<QVariantList>())
    {
        n = doc.createElement("valuelist");
        foreach(QVariant item, var.toList())
        {
            n.appendChild( variantToDomXml(item, doc));
        }
    }
    else if (var.canConvert<QVariantMap>())
    {
        QVariantMap varMap = var.toMap();
        n = doc.createElement("valuemap");
        foreach(QString key, varMap.keys())
        {
            QDomElement mapitem = doc.createElement("item");
            mapitem.setAttribute("name", key);
            mapitem.appendChild(variantToDomXml(varMap[key], doc));
            n.appendChild(mapitem);
        }
    }
    else
    {
        n = doc.createElement("value");
        n.setAttribute("type", var.typeName());
        if (var.canConvert<QString>())
        {
            QString str = var.toString();
            QDomText text = doc.createTextNode(str);
            n.appendChild(text);
            n.setAttribute("binary", false);
            n.setAttribute("encode", "utf8");
        }
        else
        {
            n.setAttribute("binary", true);
            n.setAttribute("encode", "base64");
            QByteArray originalData;
            QDataStream str(&originalData, QIODevice::WriteOnly);
            str << var;
            QByteArray encodedData = originalData.toBase64();
            QDomText text = doc.createTextNode(QString(encodedData));
            n.appendChild(text);
        }
    }
    return n;
}

QString Converter::variantToXml(QVariant var)
{
    QDomDocument doc;
    QDomElement root = doc.createElement("variant");
    doc.appendChild(root);
    root.appendChild( variantToDomXml(var, doc) );
    return doc.toString();
}

QVariant Converter::xmlDomToVariant(QDomElement e)
{
    QVariant var;
    if (e.tagName() == "value")
    {
        QString binaryAttr = e.attribute("binary", "0");
        if (binaryAttr.toInt() == 1)
        {
            QString encode = e.attribute("encode", "base64");
            QByteArray encodedVal = e.text().toAscii();
            QByteArray decodedVal;
            if (encode == "base64")
            {
                decodedVal = QByteArray::fromBase64(encodedVal);
            }
            else if (encode == "hex")
            {
                decodedVal = QByteArray::fromHex(encodedVal);
            }
            QDataStream str(&decodedVal, QIODevice::ReadOnly);
            str >> var;
        }
        else
        {
            // is text
            QString text = e.text();
            var = QString::fromUtf8(text.toAscii());
        }
        QString typeName = e.attribute("type", "QString");
        QVariant::Type type = QVariant::nameToType(typeName.toAscii());
        if (var.canConvert(type))
            var.convert(type);
    }
    else if (e.tagName() == "valuemap")
    {
        QVariantMap varMap;
        QDomElement itemElem = e.firstChildElement("item");
        for( ; !itemElem.isNull(); itemElem = itemElem.nextSiblingElement("item"))
        {
            QString key = itemElem.attribute("name", "default");
            varMap[key] = xmlDomToVariant(itemElem.firstChildElement());
        }
        var = varMap;
    }
    else if (e.tagName() == "valuelist")
    {
        QVariantList varList;
        QDomElement itemElem = e.firstChildElement("value");
        for( ; !itemElem.isNull(); itemElem = itemElem.nextSiblingElement())
        {
            varList.append(xmlDomToVariant(itemElem));
        }
        var = varList;
    }
    else if (e.tagName() == "variant")
    {
        var = xmlDomToVariant(e.firstChild().toElement());
    }
    return var;
}

QVariant Converter::xmlToVariant(QString xml)
{
    QVariant var;
    QDomDocument doc;
    doc.setContent(xml);
    QDomNodeList lst = doc.elementsByTagName("variant");
    if (!lst.isEmpty())
    {
        if (lst.count() > 1)
        {
            QVariantList varList = QVariantList();
            for(int i=0; i<lst.size(); i++)
                varList.append(xmlDomToVariant(lst.at(i).toElement()));
            var = varList;
        }
        else
            var = xmlDomToVariant(lst.at(0).toElement());
    }
    return var;
}

QVariant Converter::dbtToVariant(const Dbt& dbt, DataFormat format)
{
    QVariant var;
    QByteArray buf;
    u_int8_t compressed;
    DataFormat reportedFormat;
    DataFormat usedFormat = format;

    buf = dbtToByteArray (dbt, compressed, reportedFormat);
    if (format == Auto)
        usedFormat = reportedFormat;

    switch (usedFormat)
    {
        case Xml:
            var = xmlToVariant( QString(buf));
            break;
        case Binary:
            var = byteArrayToVariant (buf);
            break;
    }
//    if (dbt.get_flags() & DB_DBT_MALLOC)
//        free (dbt.get_data());
    return var;
}

Dbt Converter::variantToDbt(const QVariant& var, u_int8_t compressionLevel, DataFormat format)
{
    DataFormat useFormat = format;
    if (format == Auto)
        useFormat = Xml;
    Dbt data;
    QByteArray buf;
    switch (useFormat)
    {
        case Binary:
            buf = variantToByteArray (var);
            break;
        case Xml:
            buf = variantToXml(var).toUtf8();
            break;
    }
    data = byteArrayToDbt (buf, compressionLevel, useFormat);
    return data;
}

QVariant Converter::byteArrayToVariant(const QByteArray& mem)
{
    QVariant var;
    QDataStream stream(mem);
    stream >> var;

    return var;
}

QByteArray   Converter::variantToByteArray(const QVariant& var)
{
    QByteArray buff;
    QDataStream stream(&buff, QIODevice::WriteOnly);
    stream << var;

    return buff;
}

Dbt Converter::byteArrayToDbt(QByteArray& ba, u_int8_t compressionLevel, DataFormat format)
{
    ICrypter* decoder = &dummy;
    Dbt data;
    QByteArray buff;
    QByteArray decrypted_buff;
    QByteArray encrypted_buff;
    binaryHeader header;

    buff = qCompress(ba, compressionLevel);
    header.compressionLevel = compressionLevel;
    header.format = static_cast<u_int8_t>(format);

    decrypted_buff.append(reinterpret_cast<const char*>(&header), binaryHeaderSize);
    decrypted_buff.append(buff);
    if (crypter)
        decoder = crypter;
    decoder->encrypt(decrypted_buff, encrypted_buff);

    int size = encrypted_buff.size();
    void* pBegin = malloc(size);
    memcpy(pBegin, encrypted_buff.data(), size);

    memset (&data, 0, sizeof(data));
    data.set_data (pBegin);
    data.set_ulen (size);
    data.set_size (size);
    data.set_flags (DB_DBT_APPMALLOC);

    return data;
}

QByteArray Converter::dbtToByteArray(const Dbt& data, u_int8_t& compressed, DataFormat& format)
{
    ICrypter* decoder = &dummy;
    QByteArray ba;
    QByteArray decrypted_data;
    QByteArray encrypted_data;
    binaryHeader header;

    uchar* pdata = static_cast<uchar*>(data.get_data());
    int size = data.get_size ();

    encrypted_data.setRawData(reinterpret_cast<char*>(pdata), size);

    if (crypter)
        decoder = crypter;
    decoder->decrypt(encrypted_data, decrypted_data);

    pdata = reinterpret_cast<uchar*>(decrypted_data.data());
    size = decrypted_data.size();

    memcpy(&header, pdata, binaryHeaderSize);
    pdata = pdata + binaryHeaderSize;
    ba = qUncompress(pdata, size - binaryHeaderSize);
    compressed = header.compressionLevel;
    format = static_cast<DataFormat>(header.format);
    return ba;
}

ICrypter* Converter::setCrypter(ICrypter * newCrypter)
{
    ICrypter* prevCrypter = Converter::crypter;
    Converter::crypter = newCrypter;
    return prevCrypter;
}
