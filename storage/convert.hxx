#ifndef CONVERT_HXX
#define CONVERT_HXX

#include <QDomDocument>
#include <QVariant>
#include <db_cxx.h>

struct ICrypter
{
public:
    virtual bool decrypt(const QByteArray& cipher, QByteArray& plain);
    virtual bool encrypt(const QByteArray& plain,  QByteArray& cipher);
};


class Converter
{
public:
    enum DataFormat{Xml, Binary, Auto};
    static QVariant        dbtToVariant(const Dbt& dbt, DataFormat format = Auto);
    static Dbt             variantToDbt(const QVariant& var, u_int8_t compressionLevel = 0, DataFormat format = Auto);

    static ICrypter* setCrypter(ICrypter* );

private:
    static ICrypter* crypter;

    struct binaryHeader
    {
        u_int8_t compressionLevel;
        u_int8_t format;
        u_int8_t reserved2;
        u_int8_t reserved3;
        binaryHeader()
        {memset(this, 0, sizeof(*this));}
        binaryHeader(const binaryHeader& o)
        {memcpy(this, &o, sizeof(o));}
        binaryHeader& operator=(const binaryHeader& o)
        {return *static_cast<binaryHeader*>(memcpy(this, &o, sizeof(o)));}
    };
    static const int binaryHeaderSize = sizeof(binaryHeader);


    static QString         variantToXml(QVariant var);
    static QByteArray      variantToByteArray(const QVariant& var);
    static QVariant        xmlToVariant(QString xml);
    static QByteArray      dbtToByteArray(const Dbt& data, u_int8_t& compressed, DataFormat& format);
    static QVariant        byteArrayToVariant(const QByteArray& mem);
    static Dbt             byteArrayToDbt (QByteArray& ba, u_int8_t compression = 0, DataFormat format = Auto);

    static QDomElement     variantToDomXml(const QVariant& var, QDomDocument& doc);
    static QVariant        xmlDomToVariant(QDomElement e);
};

#endif // CONVERT_HXX
