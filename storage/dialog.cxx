#include "dialog.hxx"
#include "ui_dialog.h"
#include "qled.h"
#include "bdbclient.h"
#include <QxtStars>

#include "main.h"
Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    //setLayout( ui->verticalLayout);
    QTimer* cacheTimer = new QTimer(this);

    connect (cacheTimer, SIGNAL(timeout()), SLOT(onCacheProgress()));
    ui->cacheProgress->setMinimum(0);
    ui->cacheProgress->setFormat("%v / %m");

    cacheTimer->start(100);
    onStart ();
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::onStart ()
{
    openDB();
}

void Dialog::onExit ()
{
    closeDB();
}

void Dialog::onStartStopCheckBackThread ()
{
    if (isCheckBackRunning ())
    {
        delete pCheckedBackLed;
        pCheckedBackLed = 0;
        stopCheckBackThread ();
    }
    else
    {
        pCheckedBackLed = new QLed(this);
        pCheckedBackLed->setOnColor(QLed::Green);
        pCheckedBackLed->setOffColor(QLed::Yellow);
        pCheckedBackLed->setShape(QLed::Circle);
        pCheckedBackLed->setValue(false);
        ui->checkBackLayout->addWidget(pCheckedBackLed);
        pCheckedBackLed->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        pCheckedBackLed->setMinimumSize(50,50);
        connect(pChecker, SIGNAL(processStarted()), pCheckedBackLed, SLOT(toggleValue()));
        connect(pChecker, SIGNAL(processFinished()), pCheckedBackLed, SLOT(toggleValue()));
        startCheckBackThread ();
    }
}

void Dialog::onStartStopReadThreads ()
{
    if (isReadRunning ())
        stopReadThreads ();
    else
        startReadThreads ();
}

void Dialog::onStartStopWriteThreads ()
{
    if (isWriteRunning ())
    {
        disconnect(this, SLOT(onStackReady(int)));
        stopWriteThreads ();
    }
    else
    {
        connect (pWriter, SIGNAL(stackReady(int)),      SLOT(onStackReady(int)));
        connect (pWriter, SIGNAL(taskStarted(int,int)), SLOT(onProgressRangeChange(int,int)));
        connect (pWriter, SIGNAL(itemAdded(int,int)),   SLOT(onProgressValueChange(int, int)));
        connect (pWriter, SIGNAL(taskAborted(int)),     SLOT(onTaskAborted(int)));
        connect (pWriter, SIGNAL(childStarted(int)),    SLOT(onWriterStarted(int)));
        connect (pWriter, SIGNAL(childStopped(int)),    SLOT(onWriterStopped(int)));

        startWriteThreads ();
    }
}


void Dialog::on_writeButton_clicked()
{
    onStartStopWriteThreads ();
    ui->writeButton->setDisabled(true);
}

void Dialog::on_readButton_clicked()
{
    onStartStopReadThreads ();
}

void Dialog::on_checkButton_clicked()
{
    onStartStopCheckBackThread ();
}

void Dialog::on_exitButton_clicked()
{
    onExit ();
    accept ();
}

void Dialog::onProgressRangeChange(int id,int max)
{
    if (pbList.contains(id))
        pbList[id]->setRange (0, max);
    if(sList.contains(id))
    {
        sList[id]->setMaximum(10);
        sList[id]->setValue(10);
        sList[id]->setMinimum(0);
    }
}

void Dialog::onProgressValueChange(int id, int val)
{
    if (pbList.contains(id))
        pbList[id]->setValue (val);
}

void Dialog::on_pushButton_clicked()
{
    BDBClient client(ds);
    sfMap = client.mw_getServers();
    ui->serverEdit->clear();
    ui->serverEdit->addItems(sfMap.keys());
    ui->serverEdit->setCurrentIndex(0);
}

void Dialog::on_getItems_clicked()
{
    BDBClient client(ds);
    QString server = ui->serverEdit->currentText();
    QString faction = ui->factionEdit->currentText();
    client.mw_getItemIDs(server, faction);
}

void Dialog::onWriterStarted(int id)
{
    if (pbList.contains(id))
        return;
    QProgressBar* pBar = new QProgressBar(this);
    //QxtStars* pStars = new QxtStars(this);
    QLayout* pLayout = new QHBoxLayout(this);

    pBar->setFormat(QString("thread %1 %v / %m (%p%)").arg(id));
    //pStars->setReadOnly(true);
    //pLayout->addWidget(pStars);
    pLayout->addWidget(pBar);
    ui->progressBarsLayout->addLayout(pLayout);
    pbList[id] = pBar;
    //sList[id] = pStars;
    lList[id] = pLayout;
}

void Dialog::onWriterStopped(int id)
{
    if (!pbList.contains(id))
        return;
    QProgressBar* pBar = pbList.take(id);
    QLayout* pLayout = lList.take(id);
    //QxtStars* pStars = sList.take(id);
    delete pBar;
    delete pLayout;
    //delete pStars;

    if (pbList.isEmpty())
        ui->writeButton->setEnabled(true);
}

void Dialog::onTaskAborted(int id)
{
    if (sList.contains(id))
        sList[id]->setValue( sList[id]->value() - 2);
}

void Dialog::onStackReady(int size)
{
    ui->writeButton->setEnabled(true);
    ui->progressBar->setMinimum(0);
    ui->progressBar->setValue(0);
    ui->progressBar->setMaximum(size);
    ui->progressBar->setFormat("%v / %m (%p%");

}

void Dialog::on_serverEdit_currentIndexChanged(const QString &arg1)
{
    ui->factionEdit->clear();
    ui->factionEdit->addItems(sfMap[arg1]);
    ui->factionEdit->setCurrentIndex(0);
}

void Dialog::onCacheProgress()
{
    int v;
    int m;
    pWriter->getCacheSize(v, m);
    ui->cacheProgress->setMaximum(m);
    ui->cacheProgress->setValue(v);
    pWriter->getCacheHitMiss(v,m);
    ui->cacheHitMissLabel->setText(QString("Hits %1 / Misses %2").arg(v).arg(m));
    pWriter->getProgress(v,m);
    ui->progressBar->setMaximum(m);
    ui->progressBar->setValue(v);
}
