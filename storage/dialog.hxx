#ifndef DIALOG_HXX
#define DIALOG_HXX

#include <QDialog>
#include <QMap>
namespace Ui {
    class Dialog;
}

class QProgressBar;
class QLed;
class QxtStars;
class Dialog : public QDialog
{
    Q_OBJECT
public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
public slots:
    void onStart();
    void onStartStopWriteThreads();
    void onStartStopReadThreads();
    void onStartStopCheckBackThread();
    void onExit();

private slots:
    void onProgressRangeChange(int,int);
    void onProgressValueChange(int,int);
   // void onTaskFinished(int);
    void onTaskAborted(int);
    void onStackReady(int);
    void on_writeButton_clicked();
    void on_readButton_clicked();
    void on_checkButton_clicked();
    void on_exitButton_clicked();
    void onWriterStarted(int);
    void onWriterStopped(int);
    void onCacheProgress();
    void on_pushButton_clicked();

    void on_getItems_clicked();

    void on_serverEdit_currentIndexChanged(const QString &arg1);

private:

    Ui::Dialog *ui;
    QMap<int, QProgressBar*> pbList;
    QMap<int, QxtStars*> sList;
    QMap<int, QLayout*> lList;

    QLed*   pCheckedBackLed;
    QMap<QString, QStringList> sfMap;
};

#endif // DIALOG_HXX
