#include "thread.hxx"
#include "main.h"

ItemWriter ::ItemWriter(int id, DataStorage& d, QList<watchscanid>& st, QMap<watchscanid, QVariantList>& cm, QObject* parent)
    : QObject(parent), stack(st), ds(d), threadId(id), sqlCache(cm)
{
}

ItemWriter::~ItemWriter ()
{
}

void ItemWriter::processItem(watchscanid rec)
{
    QVariantList list;
    int ret;
    if (sqlCache.contains(rec))
        list = sqlCache.take(rec);
    if (!list.isEmpty() && !stopReq)
    {
        bool need_restart = false;
        int total = list.size();
        emit taskStarted(threadId, total);
        do
        {
            need_restart = false;
            DbTxn* pTrans = 0;
            doneInserts[rec].first = false;
            bool commit = false;
            ret = ds.pEnv->txn_begin (0, &pTrans, 0);
            try
            {
                int counter = 0;
                foreach(QVariant var,list)
                {
                    if (stopReq)
                        break;
                    counter++;
                    ret = put_MW_record(var.toMap(), pTrans);
                    doneInserts[rec].second.append (var.toMap()["aucid"].toULongLong());
                    emit itemAdded(threadId,counter);
                }
                if (!stopReq)
                    commit = true;
            }
            catch (DbDeadlockException& e)
            {
                qDebug () << "dead lock encountered and killed us. restart";
                need_restart = true;
            }
            catch(DbException& e)
            {
                qxtLog->error() << QString("error: %1").arg(e.what());
            }

            if (commit)
            {
                doneInserts[rec].first = true;
                if (pTrans)
                    ret = pTrans->commit (0);
            }
            else
            {
                doneInserts[rec].first = false;
                doneInserts[rec].second.clear();
                emit taskAborted(threadId);
                if (pTrans)
                    ret = pTrans->abort ();
            }
        } while (need_restart);
    }
    if (!stopReq)
        emit taskFinished(threadId);
    else
        stopIt();
    state = 0;
}

void ItemWriter::onDataReady(int id, int watchid, int scanid)
{
    if (id == threadId)
    {
        state = 1;
        watchscanid rec;
        rec.scanid = scanid;
        rec.watchid = watchid;
        processItem(rec);
    }
}

void ItemWriter::stop ()
{
    if (QThread::currentThread() != thread())
        QMetaObject::invokeMethod(this, "stop", Qt::QueuedConnection);
    else
    {
        stopReq = true;
        if (state == 0)
            stopIt();
    }
}

void ItemWriter::stopIt()
{
    //disconnect(0,this,SLOT(onDataReady(int,int,int)));
    emit stopped(threadId);
    QSqlDatabase::removeDatabase (conn_name);
    //thread()->quit();
}

void ItemWriter::start()
{
    stopReq = false;
    conn_name = QString("conn_item_%1").arg (QThread::currentThreadId ());
    QSqlDatabase* pDb = new QSqlDatabase(QSqlDatabase::addDatabase ("QMYSQL", conn_name));
    pDb->setHostName (db_host);
    pDb->setPort (db_port);
    pDb->setUserName (db_user);
    pDb->setPassword (db_pwd);
    pDb->setDatabaseName (db_name);
    delete pDb;

    connect (this, SIGNAL(started(int)),        SIGNAL(needData(int)));
    connect (this, SIGNAL(taskFinished(int)),   SIGNAL(needData(int)));

    state = 0;
    emit started(threadId);
}
