#include <QApplication>
#include "main.h"
#include "dialog.hxx"

#include "convert.hxx"
#include <QxtLogger>
#include <QString>
#include <QDateTime>
#include <QThread>

#define READERS_CNT 5
#define WRITERS_CNT 10

const QString db_host = "192.168.1.2";
const int db_port = 3306;
const QString db_name = "enchsales";
const QString db_user = "remote";
const QString db_pwd = "remote";

DataStorage ds;

const QString ENV_FOLDER("/home/weber/projects/enchsales/storage/");
const QString MW_FILE("mw.dat");
const QString DATA_FOLDER("data");
const QString BACKUP_FOLDER("back_%1");

QMap<watchscanid, QPair<bool, QList<qulonglong>  > > doneInserts;

template <class Key>
int insertRecord(Database* d, const Key& k, const QVariant& rec, u_int32_t flags =0, DbTxn* parentTrans = 0)
{
    Dbt data;
    Dbt key;
    DbTxn* transaction =0;
    int ret;
    ret  = d->primDb->get_env()->txn_begin(parentTrans, &transaction, 0);
    data = variantToDbt(rec);
    key.set_data((void*)&k);
    key.set_size(sizeof(k));
    key.set_ulen(sizeof(k));
    key.set_flags(DB_DBT_USERMEM);
    try
    {
        ret = d->primDb->put(transaction, &key, &data, flags);
        transaction->commit(0);
    }
    catch(DbDeadlockException& e)
    {
        qxtLog->debug() << "fail to insert: deadlock";
        transaction->abort();
        throw e;
    }
    catch(DbException& e)
    {
        qxtLog->error() << QString("Fail to insert record to %1: %2").arg(d->databaseName).arg(e.what());
        transaction->abort();
    }
    return ret;
}


//template< >
int insertRecord /*<QVariant>*/ (Database* d, const QVariant& k, const QVariant& rec, u_int32_t flags =0, DbTxn* parentTrans = 0)
{
    Dbt data;
    Dbt key;
    DbTxn* transaction =0;
    int ret;
    ret  = d->primDb->get_env()->txn_begin(parentTrans, &transaction, 0);
    data = variantToDbt(rec);
    key  = variantToDbt(k, 0, Binary);
    try
    {
        ret = d->primDb->put(transaction, &key, &data, flags);
        transaction->commit(0);
    }
    catch(DbDeadlockException& e)
    {
        qxtLog->debug() << "fail to insert: deadlock";
        transaction->abort();
        throw e;
    }
    catch(DbException& e)
    {
        qxtLog->error() << QString("Fail to insert record to %1: %2").arg(d->databaseName).arg(e.what());
        transaction->abort();
    }
    return ret;
}

template<class Key>
bool mw_key_exists(const QString& dbName, const Key& keyVal, DbTxn* parentTrans = 0)
{
    Dbt key;
    key.set_data((void*)&keyVal);
    key.set_size(sizeof(keyVal));
    key.set_ulen(sizeof(keyVal));
    key.set_flags(DB_DBT_USERMEM);

    int ret = ds.dbMap[dbName]->primDb->exists(parentTrans, &key, DB_READ_COMMITTED);
    return ret == 0;
}

int put_MW_record(QVariantMap rec, DbTxn* pTrans)
{
    int ret;

    qulonglong watchid = rec["watchid"].toULongLong();
    if (!mw_key_exists("watchitem", watchid, pTrans))
    {
        QVariantMap wiRec;
        wiRec["server"] = rec["server"];
        wiRec["faction"]= rec["faction"];
        wiRec["itemid"] = rec["itemid"];
        wiRec["item"] = rec["item"];
        wiRec["itemmodes"] = rec["itemmods"];
        ret = insertRecord(ds.dbMap["watchitem"], watchid, wiRec, DB_NOOVERWRITE, pTrans);
    }

    QVariantMap stRec;
    stRec["timestmp"] = rec["timestmp"];
    stRec["timestmp_int"] = rec["timestmp_int"];
    stRec["watchid"] = rec["watchid"];
    qulonglong scanid = rec["scanid"].toULongLong();
    ret = insertRecord(ds.dbMap["scantime"], scanid, stRec, DB_NOOVERWRITE, pTrans);

    QVariantMap mwRec;
    //mwRec["aucid"] = rec["aucid"];
    mwRec["bidamount"] = rec["bidamount"];
    mwRec["boprice"] = rec["boprice"];
    mwRec["cnt"] = rec["cnt"];
    mwRec["ignore_bo"] = rec["ignore_bo"];
    mwRec["instances"] = rec["instances"];
    mwRec["minbid"] = rec["minbid"];
    mwRec["seller"] = rec["seller"];
    mwRec["source"] = rec["source"];
    mwRec["timeleft"] = rec["timeleft"];
    mwRec["uid"] = rec["uid"];
    qulonglong aucid = rec["aucid"].toULongLong();
    ret = insertRecord(ds.dbMap["marketwatcher"], aucid, mwRec, DB_NOOVERWRITE);

    switch(ret)
    {
        case DB_KEYEXIST:
            qxtLog->error() << QString("record %1 already exists").arg(aucid);
            break;
        case 0:
            //qxtLog->trace() << QString("record %1 added").arg(aucid);
            break;
    }

    return ret;
}

inline
QString getIndent(int depth)
{
    QString str;
    str.fill (' ', depth * 2);
    return str;
}

QString dumpVariant(const QVariant& var, int depth)
{
    QString str;
    if (var.canConvert<QVariantMap> ())
    {
        QVariantMap map = var.toMap ();
        str += getIndent (depth) + "{\n";
        foreach(QString key, map.keys())
            str += getIndent (depth+1) + key + " : " + dumpVariant (map[key], depth + 1) + ".\n";
        str += getIndent (depth) + "}\n";
    }
    else if (var.canConvert<QVariantList> ())
    {
        QVariantList list = var.toList ();
        str += getIndent(depth) + "[\n";
        foreach(QVariant v, list)
            str  += getIndent (depth+1) + dumpVariant(v, depth +1) + ",\n";
        str +=  getIndent(depth) + "]\n";
    }
    else
        str = var.toString ();

    return str;
}


const int readThreadsCnt = READERS_CNT;
int readThreadRealCnt=0;
Reader* readArray[readThreadsCnt];
void startReadThreads()
{
    if (!isReadRunning ())
        for(int i=0;i<readThreadsCnt;i++)
        {
            readThreadRealCnt++;
            readArray[i] = new Reader(ds);
            readArray[i]->start ();
        }
    qDebug() << "read threads started";
}

void stopReadThreads(bool async)
{
    if (isReadRunning ())
        for(int i=0;i<readThreadRealCnt;i++)
            readArray[i]->stop();
    if (!async)
        for(int i=0;i<readThreadRealCnt;i++)
        {
            readArray[i]->wait ();
            delete readArray[i];
            readArray[i] = 0;
        }
    readThreadRealCnt = 0;
}


Checker* pChecker =0;
QThread* pCheckerThread =0;

void startCheckBackThread()
{
   // delete pChecker;
   // delete pCheckerThread;

   // pChecker = new Checker(ds);
   // pCheckerThread = new QThread();

    pCheckerThread->start ();
}

void stopCheckBackThread(bool async )
{
    if (pCheckerThread && pCheckerThread->isRunning() && pChecker)
        pChecker->stop();
    //pCheckerThread->wait();
    /*
    if (!async)
        pCheckerThread->wait();
    */
}

QThread* pWriterThread = 0;
Writer* pWriter =0;

void startWriteThreads()
{
   pWriterThread->start();
}

void stopWriteThreads()
{
    pWriter->stop ();
    //pWriterThread->wait();
}

bool isWriteRunning()
{
    return pWriterThread->isRunning ();
}


bool isReadRunning()
{
    bool isAnyRunning = false;
    for(int i=0;i<readThreadRealCnt;i++)
        if (readArray[i])
                isAnyRunning |= readArray[i]->isRunning ();
    return isAnyRunning;
}

bool isCheckBackRunning()
{
    return pCheckerThread && pCheckerThread->isRunning ();
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qxtLog->enableAllLogLevels ();


    pChecker = new Checker(ds);
    pChecker->setObjectName("pChecker");
    pCheckerThread = new QThread();
    pCheckerThread->setObjectName("pCheckerThread");
    pChecker->moveToThread(pCheckerThread);
    pChecker->connect(pCheckerThread, SIGNAL(started()), SLOT(start()) );
    pCheckerThread->connect(pChecker, SIGNAL(finished()), SLOT(quit()));

    pWriter = new Writer(ds, WRITERS_CNT);
    pWriter->setObjectName("pWriter");
    pWriterThread = new QThread();
    pWriterThread->setObjectName("pWriterThread");
    pWriter->moveToThread(pWriterThread);
    pWriter->connect(pWriterThread, SIGNAL(started()), SLOT(start()));
    pWriterThread->connect(pWriter, SIGNAL(finished()), SLOT(quit()));


    qsrand(QDateTime::currentDateTime().time().msec());
    Dialog d;
    d.show();

    return a.exec();

    delete pChecker;
    delete pCheckerThread;
}
