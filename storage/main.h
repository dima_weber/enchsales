#ifndef MAIN_H
#define MAIN_H
#include "storage.hxx"
#include "thread.hxx"
#include <QThread>
#include <db_cxx.h>
#include <QMap>
#include <QList>
#include <QVariant>
#include <QStack>
#include <QStringList>

extern const QString db_host;
extern const int db_port;
extern const QString db_name;
extern const QString db_user;
extern const QString db_pwd;

extern DataStorage ds;

extern const QString ENV_FOLDER;
extern const QString MW_FILE;
extern const QString DATA_FOLDER;
extern const QString BACKUP_FOLDER;

extern Writer* pWriter;
extern Checker* pChecker;
extern QThread* pWriterThread;
extern QThread* pCheckerThread;


extern QMap<watchscanid, QPair<bool, QList<qulonglong> > > doneInserts;

#define log_entry_header QString("[%1] %2 (%3:%4)").arg(QThread::currentThreadId ()).arg(__func__).arg(__FILE__).arg(__LINE__)

QString dumpVariant(const QVariant& var, int depth = 0);
int put_MW_record(QVariantMap rec, DbTxn* pTrans = 0);


void startReadThreads();
void stopReadThreads(bool async = true);
void startCheckBackThread();
void stopCheckBackThread(bool async = true);
void startWriteThreads();
void stopWriteThreads();
bool isWriteRunning();
bool isReadRunning();
bool isCheckBackRunning();

#endif // MAIN_H
