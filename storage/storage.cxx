#include "storage.hxx"
//#include "main.h"
#include <errno.h>
#include "convert.hxx"
#include <QxtLogger>
#include <QDir>
#include <QThread>
#include <db_cxx.h>
#include <QMessageBox>

#define log_entry_header QString("[%1] %2 (%3:%4)").arg(QThread::currentThreadId ()).arg(__func__).arg(__FILE__).arg(__LINE__)

void DataStorage::errCallback(const DbEnv* env, const char*
                 prefix, const char* errMsg)
{
    Q_UNUSED(env);
    qxtLog->error () << QString("BDB error: %1 %2 [%3]").arg (prefix).arg (errMsg).arg(log_entry_header);
}

int get_index( Db* pSecondaryDb,
                    const Dbt* pPKey,
                    const Dbt* pPData,
                          Dbt* pSKey)
{
    Q_UNUSED(pPKey)

    const char* dbName;
    const char* fileName;
    int ret;

    ret = pSecondaryDb->get_dbname(&fileName, &dbName);
    QStringList indexNameList = QString(dbName).split(":");
    QVariant data = Converter::dbtToVariant (*pPData);
    QVariant indexVal;
    if (indexNameList.count() == 1)
        indexVal = data.toMap ()[indexNameList[0]];
    else
    {
        QVariantMap indexMap;
        foreach(QString index, indexNameList)
        {
            indexMap[index] = data.toMap ()[index];
        }
        indexVal = indexMap;
    }
    *pSKey = Converter::variantToDbt(indexVal);
    return 0;
}

int wi_server_faction_bt_compare(DB*, const DBT* dbt1,  const DBT* dbt2)
{
    const Dbt* one = Dbt::get_const_Dbt(dbt1);
    const Dbt* two = Dbt::get_const_Dbt(dbt2);
    QVariantMap var1;
    QVariantMap var2;
    var1 = Converter::dbtToVariant(*one).toMap();
    var2 = Converter::dbtToVariant(*two).toMap();
    QString serverfaction1 = var1["server"].toString() + var1["faction"].toString();
    QString serverfaction2 = var2["server"].toString() + var2["faction"].toString();

    return (serverfaction1).compare(serverfaction2);
}

void DataStorage::db_feedback_func(Db* dbp, int opcode, int percent)
{
    Q_UNUSED(dbp)

    QString proc_name;
    switch(opcode)
    {
    case DB_UPGRADE:
        proc_name = "Upgrade"; break;
    case DB_VERIFY:
        proc_name = "Verify"; break;
    default:
        proc_name = "Unknown"; break;
    }
    qxtLog->info() << QString("%1 %2: %3% done").arg(log_entry_header).arg(proc_name).arg(percent);
}

void DataStorage::env_feedback_func(DbEnv* dbenv, int opcode, int percent)
{
    Q_UNUSED (dbenv)

    QString proc_name;
    switch(opcode)
    {
    case DB_RECOVER:
        proc_name = "Recover"; break;
    default:
        proc_name = "Unknown"; break;
    }
    qxtLog->info() << QString("%1 %2: %3% done").arg(log_entry_header).arg(proc_name).arg(percent);
}

DbEnv* DataStorage::initEnv(QString datafolder, const QString& passphrase, u_int32_t flags)
{
    DbEnv* env = 0;
    int ret;
    u_int32_t envOpenFlags =  flags;
    QDir dir (datafolder);
    if (!dir.exists ("DATA_FOLDER"))
        dir.mkpath ("DATA_FOLDER");
    QDir envFolder (dir.absoluteFilePath ("DATA_FOLDER"));
    qxtLog->trace ("env setup");
    bool retryOpen = false;
    do
    {
        retryOpen = false;
        try
        {
            env = new DbEnv(0);
            ret = env->set_cachesize (1,0,0);

            ret = env->set_lk_max_lockers (10000);
            ret = env->set_lk_max_locks ( 10000);
            ret = env->set_lk_max_objects (10000);

            env->set_errcall ( errCallback );
            env->set_errpfx ("Berkeley DB Envirompment");

            ret = env->set_lk_detect (DB_LOCK_DEFAULT);

            ret = env->set_feedback (env_feedback_func);
            if (!passphrase.isNull())
            {
                ret = env->set_encrypt(passphrase.toUtf8().data(), DB_ENCRYPT_AES);
                envOpenFlags |= DB_ENCRYPT;
            }

            ret = env->open (envFolder.absolutePath ().toAscii(), envOpenFlags, 0600);

            if (ret)
            {
                switch (ret)
                {
                    case ENOENT:
                        qxtLog->error() << "Not enveronmept directory"; break;
                }
            }
        }
        catch(DbException& e)
        {
            ret = env->close (0);
            if (    e.get_errno() == DB_VERSION_MISMATCH
                 || e.get_errno() == DB_RUNRECOVERY
               )
            {
                qDebug("Version mismatch or recovery needed");
                delete env;
                envOpenFlags |= DB_RECOVER | DB_CREATE;
                retryOpen = true;
            }
            else
            {
                qxtLog->error() << QString ("We can't handle it here: %1").arg(e.what());
                throw e;
            }
        }
    }while(retryOpen);
    qxtLog->trace ("env setup done");

    return env;
}

Db* DataStorage::initDb(DbEnv* pEnv, QString filename, QString dbname, DBTYPE type, u_int32_t openFlags, u_int32_t extraFlags, bt_compare_fcn_type func, DbTxn* parentTrans)
{
    int ret;
    Db* pDb = 0;
    u_int32_t dbOpenFlags = openFlags;
    qxtLog->trace() << QString("open %1 database (%2)").arg(dbname).arg(filename);

    const char* pEnvHome;
    ret = pEnv->get_home(&pEnvHome);
    QDir envDir;
    envDir.setPath(pEnvHome);

    DbTxn* pTrans = 0;
    bool retryOpen;
    do
    {
        retryOpen = false;
        try
        {
            pDb = new Db(pEnv, 0);
            ret = pEnv->txn_begin (parentTrans, &pTrans, 0);
            ret = pDb->set_flags(extraFlags);
            ret = pDb->set_feedback(db_feedback_func);
//            {
//                Db db(pEnv, 0);
//                ret = db.set_feedback(db_feedback_func);
//                ret = db.verify(filename.toAscii().constData(),dbname.toAscii().constData(),0,0);
//                if (ret)
//                    qxtLog->warning() << QString("database %1 (%2) corrupted").arg(dbname).arg(filename);
//            }
            if (func)
                ret = pDb->set_bt_compare(func);
            u_int32_t encFlags;
            pEnv->get_encrypt_flags(&encFlags);
            if (encFlags & DB_ENCRYPT_AES)
                dbOpenFlags |= DB_ENCRYPT;
//            if (!passphrase.isNull())
//            {
////                ret = pDb->set_encrypt(passphrase.toUtf8().data(), DB_ENCRYPT_AES);
//                dbOpenFlags |= DB_ENCRYPT;
//            }
            ret = pDb->open(pTrans, filename.toAscii (), dbname.toAscii(), type, dbOpenFlags, 0600);
            if (pTrans)
                ret = pTrans->commit (0);
        }
        catch (DbException& e)
        {
            if (pTrans)
                ret = pTrans->abort();
            if (e.get_errno() == DB_OLD_VERSION)
            {
                delete pDb;

                Db db(NULL, 0);
                db.set_feedback(db_feedback_func);

                ret = db.upgrade(envDir.absoluteFilePath (filename).toAscii (), 0);
                if (ret)
                 qxtLog->fatal() << log_entry_header << QString("Fail to upgrade %1").arg (filename);

                retryOpen = true;
            }
            else
                throw e;
        }
    } while (retryOpen);

    qxtLog->trace() << QString("%1 database ready").arg(dbname);
    return pDb;
}


Db* DataStorage::initIndex(Db* pDb, Index index, DbTxn* parentTrans)
{
    const char* filename;
    const char* primDbName;
    int ret;

    DbEnv* pEnv;
    u_int32_t primDbOpenFlags;
    ret = pDb->get_dbname(&filename, &primDbName);
    qxtLog->trace() << QString("setup index %1 for database %2").arg(index.name).arg(primDbName);
    pEnv = pDb->get_env();
    Db* pIndexDb = 0;
    ret = pDb->get_open_flags(&primDbOpenFlags);
    pIndexDb = initDb(pEnv, filename, index.name, DB_BTREE, primDbOpenFlags, index.unique?0:DB_DUP|DB_DUPSORT, index.func, parentTrans);
    ret = pDb->associate (parentTrans, pIndexDb, index.index_func, DB_CREATE);
    qxtLog->trace() << QString("setup index %1 for database %2 done").arg(index.name).arg(primDbName);

    return pIndexDb;
}

void DataStorage::close()
{
    foreach(QString primName, dbMap.keys())
    {
        foreach(Index index, dbMap[primName]->indexList)
        {
            if (dbMap[primName]->secDb.contains(index.name) && dbMap[primName]->secDb[index.name])
            {
                dbMap[primName]->secDb[index.name]->close (0);
                delete dbMap[primName]->secDb[index.name];
                dbMap[primName]->secDb[index.name] = 0;
            }
        }

        if (dbMap[primName]->primDb)
        {
            dbMap[primName]->primDb->close (0);
            delete dbMap[primName]->primDb;
            dbMap[primName]->primDb = 0;
        }
    }

    if (pEnv)
    {
        pEnv->close (0);
        pEnv = 0;
    }

    delete pEnv;
}

bool Database::initialize(DbEnv *pEnv, DbTxn* parentTrans)
{
    if (initialized)
        return true;
    bool ok = true;
    DbTxn* pTrans = 0;
    try
    {
        pEnv->txn_begin(parentTrans, &pTrans, 0);
        primDb = DataStorage::initDb(pEnv, fileName, databaseName, accessMethod, dbOpenFlags, 0, 0, pTrans);
        foreach(Index index, indexList)
        {
            secDb[index.name] = DataStorage::initIndex(primDb, index, pTrans);
        }
        pTrans->commit(0);
    }
    catch(DbException e)
    {
        qxtLog->error() << QString("Fail to open database %1: %2").arg(databaseName).append(e.what());
        pTrans->abort();
        ok = false;
    }
    initialized = ok;
    return ok;
}

Database::Database(const QString& _fileName, const QString& _dbName, DBTYPE _acc, u_int32_t flags)
    : fileName(_fileName),
      databaseName(_dbName),
      accessMethod(_acc),
      dbOpenFlags(flags),
      initialized(false),
      primDb(NULL)
{}

bool Database::addIndex(const QString &indexName, bool unique, create_index_func indx_f, bt_compare_fcn_type func, bool autoInit)
{
    Index idx(indexName, unique, indx_f, func);
    indexList.append(idx);
    if (autoInit && initialized)
        return DataStorage::initIndex(primDb, idx);
    else
        return true;
}

DataStorage::DataStorage(const QString& d, u_int32_t f)
    :initialized(false),
      dataFolder(d),
      envOpenFlags(f),
      pEnv(NULL)
{
}

bool DataStorage::addDatabase(const QString& fileName, const QString& dbName, DBTYPE accessMethod, u_int32_t dbOpenFlags, bool autoInit)
{
    Database* db = new Database(fileName, dbName, accessMethod, dbOpenFlags);
    dbMap[dbName] = db;
    if (initialized && autoInit)
        return db->initialize(pEnv);
    else
        return true;
}

bool DataStorage::initialize( passphrase_cb func )
{
    if (dataFolder.isNull() || envOpenFlags == 0)
    {
        qxtLog->fatal("Environment datafolder / openflags are not set");
        return false;
    }
    bool ok = true;
    DbTxn* pTrans = 0;
    try
    {
        QString pass = QString::null;
        if (func)
            pass = func("Enter password for database please.", "This password is used to encrypt all database files.\nNone files can be opened without it.");
        bool retry;
        do
        {
            retry = false;
            try
            {
                pEnv = initEnv(dataFolder, pass, envOpenFlags);
            }
            catch (DbException& e)
            {
                if(func && QMessageBox::critical(0, "Error", QString("Wrong password.\nRetry ?").arg(e.get_errno()).arg(e.what()), QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
                {
                    retry = true;
                    pass= func("Enter password for database please.", "This password is used to encrypt all database files.\nNone files can be opened without it.");
                }
                else
                    return false;
            }
        }while (retry);
        pEnv->txn_begin(0, &pTrans, 0);
        foreach(Database* p, dbMap)
        {
            if(p && !p->initialized)
                ok = p->initialize(pEnv, pTrans);
            if (!ok)
                break;
        }
    }
    catch (DbException& e)
    {
        qxtLog->error() << QString("Fail to initialize environment  %1: %2").arg(dataFolder).append(e.what());
        ok = false;
    }
    if(pTrans)
    {
        if (ok )
            pTrans->commit(0);
        else
            pTrans->abort();
    }
    initialized = ok;
    return ok;
}

bool DataStorage::addIndex(const QString &dbName, const QString &indexName, bool unique, create_index_func indx_f, bt_compare_fcn_type func, bool autoInit)
{
    if (dbMap.contains(dbName))
        return dbMap[dbName]->addIndex(indexName, unique, indx_f, func, autoInit);
    else
        return false;
}

DataStorage::~DataStorage()
{
    close();
}
