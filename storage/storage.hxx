#ifndef STORAGE_HXX
#define STORAGE_HXX
#include <db_cxx.h>
#include <QMap>
#include <QString>


typedef int (*create_index_func)(Db* secKey, const Dbt* primKey, const Dbt* data, Dbt* result);
int get_index( Db* pSecondaryDb,
                    const Dbt* pPKey,
                    const Dbt* pPData,
                          Dbt* pSKey);

struct Index
{
    Index(QString n, bool uniq = false, create_index_func inx_f = get_index, bt_compare_fcn_type f=0)
    {name=n;func=f;index_func = inx_f;unique = uniq;}
    QString name;
    create_index_func index_func;
    bt_compare_fcn_type func;
    bool unique;
};

struct Database
{
    QString             fileName;
    QString             databaseName;
    QString             passphrase;
    DBTYPE              accessMethod;
    u_int32_t           dbOpenFlags;

    bool                initialized;
    Db*                 primDb;
    QList<Index>        indexList;
    QMap<QString, Db*>  secDb;

    bool addIndex(const QString& indexName, bool unique = false, create_index_func indx_f = get_index, bt_compare_fcn_type func = 0, bool autoInit = false);
    bool initialize(DbEnv *pEnv, DbTxn* pTrans = 0);

    Database(const QString& _fileName, const QString& _dbName = "", DBTYPE _acc = DB_BTREE, u_int32_t flags = DB_CREATE);
};

struct DataStorage
{
    typedef QString (*passphrase_cb)(const QString&, const QString&);

    bool initialized;
    QString dataFolder;
    u_int32_t envOpenFlags;
    DbEnv* pEnv;
    QMap<QString, Database*> dbMap;

    ~DataStorage();
    bool addDatabase(const QString& _fileName, const QString& _dbName = "", DBTYPE _acc = DB_BTREE, u_int32_t flags = DB_CREATE, bool autoInit = false);
    bool addIndex(const QString& dbName, const QString& indexName, bool unique = false, create_index_func indx_f = get_index, bt_compare_fcn_type func=0, bool autoInit = false);
    DataStorage(const QString& _dataFolder = QString::null, u_int32_t flags = 0);
    bool initialize( passphrase_cb func);

    void close();

    static DbEnv* initEnv(QString datafolder, const QString& pass, u_int32_t flags = 0);
    static Db* initDb(DbEnv* pEnv, QString filename, QString dbname = "", DBTYPE type = DB_BTREE, u_int32_t openFlags = 0, u_int32_t extraFlags = 0, bt_compare_fcn_type func = 0, DbTxn* parentTrans=0);
    static Db* initIndex(Db* pDb, Index index, DbTxn* parentTrans =0);

    static void db_feedback_func(Db* dbp, int opcode, int percent);
    static void env_feedback_func(DbEnv* dbenv, int opcode, int percent);
    static void errCallback(const DbEnv* env, const char* prefix, const char* errMsg);

};
#endif // STORAGE_HXX
