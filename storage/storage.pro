#-------------------------------------------------
#
# Project created by QtCreator 2011-07-04T21:40:01
#
#-------------------------------------------------

QT       += core sql xml svg

QT       += gui

TARGET = storage
CONFIG   += console qxt # debug
QXT += core gui
CONFIG   -= app_bundle # release

TEMPLATE = app

INCLUDEPATH += /opt/bdb/include
LIBS+= -L/opt/bdb/lib -ldb_cxx

SOURCES += main.cxx \
    dialog.cxx \
    thread.cxx \
    convert.cxx \
    bdbclient.cpp \
    storage.cxx \
    qled.cpp \
    itemwriter.cxx
HEADERS += thread.hxx \
    dialog.hxx \
    main.h \
    convert.hxx \
    ../dbinterface.hxx \
    bdbclient.h \
    storage.hxx \
    qled.h

FORMS += \
    dialog.ui

RESOURCES += \
    qled.qrc \
    ../resource.qrc




