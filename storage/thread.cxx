#include "thread.hxx"
#include "convert.hxx"

#include "main.h"
#include <db_cxx.h>
#include <QxtLogger>
#include <QThread>
#include <QTimer>
#include <QDir>
#include <QDateTime>
#include <QDebug>
#include <stdlib.h>
#include <QStack>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QSqlField>

void Checker::start ()
{
    qDebug() << QString("check/backup [%1] started").arg (QThread::currentThreadId ());
    pCheckTimer = new QTimer();
    pBackupTimer = new QTimer();
    connect(pCheckTimer, SIGNAL(timeout()), SLOT(onCheckTimer()));
    connect(pBackupTimer, SIGNAL(timeout()),SLOT(onBackupTimer()));
    pCheckTimer->start(60 * 1000  );
    pBackupTimer->start (60 * 60  * 1000 );
}

void Checker::stop ()
{
    if (QThread::currentThread() != thread())
        QMetaObject::invokeMethod(this, "stop", Qt::QueuedConnection);
    else
    {
        qDebug() << QString("check/backup [%1] done").arg (QThread::currentThreadId ());
        if (pCheckTimer)
        {
            pCheckTimer->stop ();
            delete pCheckTimer;
            pCheckTimer = 0;
        }
        if (pBackupTimer)
        {
            pBackupTimer->stop ();
            delete pBackupTimer;
            pBackupTimer = 0;
        }

        emit finished ();
    }
}

void Checker::onCheckTimer()
{
    mutex.lock ();
    if (ds.pEnv)
    {
        try
        {
            qxtLog->info () << QString("Checkpoint [%1] start").arg(QThread::currentThreadId ());
            emit processStarted();
            ds.pEnv->txn_checkpoint (512,0,0);
            emit processFinished();
            qxtLog->info () << "Checkpoint done";
        }
        catch(DbException e)
        {
            qxtLog->error () << QString("checkpoint thread exception: %1").arg (e.what());
        }
    }
    mutex.unlock ();
}
void Checker::onBackupTimer()
{
    mutex.lock ();
    qxtLog->info () <<QString("backup [%1] started").arg (QThread::currentThreadId ());
    emit processStarted();
    try
    {
        ds.pEnv->txn_checkpoint (512,0,0);
        archiveHelper (DB_ARCH_REMOVE);
        QStringList dataList  = archiveHelper (DB_ARCH_ABS | DB_ARCH_DATA);
        QDir envDir = QDir(ENV_FOLDER);
        QString backFolderName = BACKUP_FOLDER.arg (QDateTime::currentDateTime ().toString ("yyMMddhhmmss"));
        if (!envDir.exists (backFolderName))
            envDir.mkpath (backFolderName);
        QString backDirPath = envDir.absoluteFilePath (backFolderName);
        QDir backDir = QDir(backDirPath);
        foreach(QString file, dataList)
        {
            QFileInfo fi(file);
            QString newFileName = backDir.absoluteFilePath (fi.fileName ());
            QFile::copy (file, newFileName);
            //db_copy(ds.pEnv->get_DB_ENV(), file.toAscii(), backDirPath.toAscii(), 0);
        }

        QStringList logList  = archiveHelper (DB_ARCH_ABS | DB_ARCH_LOG);
        foreach(QString file, logList)
        {
            QFileInfo fi(file);
            QString newFileName = backDir.absoluteFilePath (fi.fileName ());
            QFile::copy (file, newFileName);
        }
    }
    catch(DbException e)
    {
        qxtLog->error(QString("Error while doing backup: %1").arg (e.what()));
    }
    emit processFinished();
    mutex.unlock ();
}


QStringList Checker::archiveHelper(u_int32_t flags)
{
    QStringList list;
    char** file_list;
    int ret;
    if (ds.pEnv)
    {
        ret = ds.pEnv->log_archive (&file_list, flags);
        if (ret)
            qxtLog->error () << "archive helper failed";
        else if (file_list)
        {
            char** listBegin = file_list;
            for(;*file_list;file_list++)
            {
                list.append (QString(*file_list));
            }
            qFree(listBegin);
        }
    }
    return list;
}

Checker::Checker( DataStorage& d, QObject* parent)
    :QObject(parent), ds(d)
{
}

Checker::~Checker()
{
}

///////////////////////

Reader::Reader(DataStorage& d, QObject *parent)
    :QObject(parent), pThread(0), ds(d)
{
    pThread = new QThread();
    moveToThread (pThread);
    connect (pThread, SIGNAL(started()), SLOT(_start()));
    connect (this, SIGNAL(finished()), pThread, SLOT(quit()));

}

Reader::~Reader ()
{
    delete pThread;
}

void Reader::readData()
{
    //qDebug() << QString("Read data from thread %1").arg(QThread::currentThreadId ());
    while (doneInserts.isEmpty ())
        return;
    watchscanid wsi = doneInserts.keys ().at (qrand() % doneInserts.keys ().count ());
    while (doneInserts[wsi].second.isEmpty () || !doneInserts[wsi].first)
        return;
    qulonglong aucid   = doneInserts[wsi].second.at(qrand() % doneInserts[wsi].second.count());

    //qDebug() << QString("read data for aucid %1").arg(aucid);
    DbTxn* pTrans = 0;
    ds.pEnv->txn_begin (0, &pTrans, 0);
    Dbt data;
    data.set_flags (DB_DBT_MALLOC);
    Dbt key;
    key.set_data (&aucid);
    key.set_size (sizeof(aucid));
    key.set_ulen (sizeof(aucid));
    key.set_flags (DB_DBT_USERMEM);

    try
    {
        int ret = ds.dbMap["marketwatcher"]->primDb->get (pTrans, &key, &data, DB_READ_UNCOMMITTED);
        if (!ret)
        {
            QVariant res = dbtToVariant (data);
            free (data.get_data ());
            //qDebug() << dumpVariant (res);
        }
        else if (ret == DB_NOTFOUND)
        {
            qDebug() << "not found";
        }
        if(pTrans)
            pTrans->commit (0);
    }
    catch(DbException e)
    {
        if (pTrans)
            pTrans->abort ();
        qDebug() << QString("reader thread exception: %1").arg (e.what());
    }
}

void Reader::_start()
{
    qDebug() << QString("reader %1 started").arg(QThread::currentThreadId ());
    pTimer = new QTimer();
    connect(pTimer, SIGNAL(timeout()), SLOT(readData ()));
    pTimer->start (100);
}

void Reader::stopReq ()
{
    QMetaObject::invokeMethod(this, "_stop", Qt::QueuedConnection);
}

void Reader::_stop ()
{
    if (pTimer)
        pTimer->stop ();
    delete pTimer;
    pTimer = 0;
    emit finished ();
    pThread->quit ();
    qDebug() << QString("reader %1 done").arg(QThread::currentThreadId ());
}

void Reader::start()
{
    if (!pThread->isRunning ())
        pThread->start ();
}

void Reader::stop (bool async)
{
    if (pThread->isRunning ())
        stopReq ();
    if (!async)
        pThread->wait ();
}

bool Reader::isRunning ()
{
    return pThread->isRunning ();
}

void Reader::wait ()
{
    pThread->wait ();
}



QVariant sqlToVariant(const QSqlQuery& q)
{
    QSqlRecord rec;
    rec = q.record ();
    QVariantMap dataVar;
    for(int i=0;i<rec.count ();i++)
        dataVar[rec.field (i).name ()] = q.value (i);
    return dataVar;
}


void Writer ::stop()
{
    //if (pThread->isRunning ())
    if (QThread::currentThread() != thread())
        QMetaObject::invokeMethod(this, "stop", Qt::QueuedConnection);
    else
    {
        emit stopRequested();

//        if (pCacheFiller)
//            pCacheFiller->stop();

//        foreach(int id, writers.keys())
//            writers[id].second->stop();
    }
}

writerSqlCacheFill::~writerSqlCacheFill()
{

}

void Writer::fillIdStack()
{
//    qxtLog->debug() << QString("%1: %2").arg(__func__).arg(QThread::currentThreadId());
    if (idStack.isEmpty())
    {
        QList<watchscanid> list;
        QString conn_name = QString("conn_watchreader");

        QSqlDatabase* pDb = new QSqlDatabase(QSqlDatabase::addDatabase ("QMYSQL", conn_name));
        pDb->setHostName (db_host);
        pDb->setPort (db_port);
        pDb->setUserName (db_user);
        pDb->setPassword (db_pwd);
        pDb->setDatabaseName (db_name);


        if(pDb->open ())
        {
            QSqlQuery watchid_query(*pDb);
            if(watchid_query.exec("select watchid,scanid from scantimes"))
            {
                while(watchid_query.next ())
                {
                    watchscanid rec;
                    rec.watchid = watchid_query.value (0).toUInt ();
                    rec.scanid  = watchid_query.value (1).toUInt ();
                    int i = 0;
                    if (idStack.size() > 0)
                        i = qrand() % idStack.size();
                    idStack.insert(i, rec);
                }

            }
            pDb->close ();
        }
        delete pDb;
        QSqlDatabase::removeDatabase (conn_name);

        totalProgress = idStack.size();
    }
    qxtLog->debug() << "Stack ready";
    emit stackReady(idStack.size());
}


void Writer::onStackReady()
{
    pCacheFiller = new writerSqlCacheFill(idStack, sqlCache);
    pCacheFillerThread = new QThread();

    pCacheFiller->moveToThread(pCacheFillerThread);

    connect (pCacheFillerThread, SIGNAL(started()), pCacheFiller, SLOT(start()));
    connect (pCacheFiller, SIGNAL(done()), pCacheFillerThread, SLOT(quit()));

    connect (pCacheFiller, SIGNAL(cacheAdded(int,int)), SLOT(onCacheAdded(int,int)));
    connect (pCacheFillerThread, SIGNAL(finished()), SLOT(onCacheFillerStop()));

    connect (this, SIGNAL(stopRequested()), pCacheFiller, SLOT(stop()));

    pCacheFillerThread->start();
 }

void Writer::start()
{
    fillIdStack ();
    for(int i=0; i<thrdCnt; i++)
    {
        QThread*  pChildWriterThread = new QThread();
        pChildWriterThread->setObjectName(QString("pChildWriterThread_%1").arg(i));
        ItemWriter* pChildWriter = new ItemWriter(i, ds, idStack, sqlCache);
        pChildWriter->setObjectName(QString("pChildWriter_%1").arg(i));

        pChildWriter->moveToThread(pChildWriterThread);

        pChildWriter->connect(pChildWriterThread, SIGNAL(started()), SLOT(start()));
        //connect(pChildWriter, SIGNAL(done()), pChildWriterThread, SLOT(quit()), Qt::QueuedConnection);

        connect (pChildWriter, SIGNAL(taskStarted(int,int)), SIGNAL(taskStarted(int, int)));
        connect (pChildWriter, SIGNAL(taskAborted(int)),     SIGNAL(taskAborted(int)));
        connect (pChildWriter, SIGNAL(taskFinished(int)),    SLOT(onProgressInc(int)));
        connect (pChildWriter, SIGNAL(itemAdded(int,int)),   SIGNAL(itemAdded(int,int)));

        connect (this, SIGNAL(dataReady(int,int, int)), pChildWriter, SLOT(onDataReady(int, int,int)));
        //connect (pW, SIGNAL(started(int)),         SIGNAL(childStarted(int)));
        connect (pChildWriter, SIGNAL(needData(int)),             SLOT(childNeedData(int)));

        connect (pChildWriter, SIGNAL(stopped(int)),              SLOT(onChildStop(int)));
        connect (pChildWriter, SIGNAL(started(int)),              SLOT(onChildStart(int)));

        pChildWriter->connect (this, SIGNAL(stopRequested()), SLOT(stop()));

        writers[i]  = QPair<QThread*, ItemWriter*>(pChildWriterThread, pChildWriter);
        pChildWriterThread->start();
    }
}

Writer::Writer(DataStorage& d, int cnt, QObject* parent)
    :QObject(parent), thrdCnt(cnt), ds(d), pCacheFiller(0),cachehit(0),cachemiss(0)
{

    connect (this, SIGNAL(stackReady(int)), SLOT(onStackReady()));
    connect (this, SIGNAL(dataForInsertsReady(int,int)), SLOT(onDataForInsertReady(int,int)));
}

Writer::~Writer()
{
}

int Writer::getStackSize()
{
    return idStack.size();
}

void writerSqlCacheFill::buildCache()
{
    conn_name = QString("conn_cachefiller");

    QSqlDatabase* pDb = new QSqlDatabase(QSqlDatabase::addDatabase ("QMYSQL", conn_name));
    pDb->setHostName (db_host);
    pDb->setPort (db_port);
    pDb->setUserName (db_user);
    pDb->setPassword (db_pwd);
    pDb->setDatabaseName (db_name);

    if (!stopReq)
    {
        QSqlDatabase db = QSqlDatabase::database(conn_name);
        if(db.open ())
        {
            for(int i=0; i<idStack.size() && !stopReq; i++)
            {
                QVariantList list;
                watchscanid rec;
                if (firstPriority.isEmpty())
                    rec = idStack[i];
                else
                    rec = firstPriority.pop();
                if (sqlCache.contains(rec))
                    continue;
                QSqlQuery q(db);
                q.prepare("select mv.*,g.name as item,wi.srv as server, wi.faction "
                          "     from marketwatcher_view  mv "
                          "          left join goods  g  on mv.itemid = g.itemid "
                          "          left join watcheditems wi on wi.watchid=mv.watchid "
                          "where mv.scanid=:sid and mv.watchid=:wid");
                q.bindValue (":sid", rec.scanid);
                q.bindValue (":wid", rec.watchid);
                if (q.exec ())
                {
                    while(q.next () && !stopReq)
                    {
                        QVariant var = sqlToVariant(q);
                        list.append(var);
                    }
                    if (!stopReq)
                    {
                        //qxtLog->debug() << QString("new item added to cache. total cache size: %1").arg(sqlCache.size());
                        sqlCache[rec] = list;
                        emit cacheAdded(rec.watchid, rec.scanid);
                    }
                }
                else
                {
                    qxtLog->debug() << q.lastError().text();
                }
            }
            db.close ();
        }
        else
        {
            qxtLog->error() << db.lastError().text();
        }
    }

    delete pDb;

    QSqlDatabase::removeDatabase (conn_name);

    emit done();
}

void writerSqlCacheFill::stop()
{
    stopReq = true;
}

writerSqlCacheFill::writerSqlCacheFill(QList<watchscanid>& s, QMap<watchscanid, QVariantList>& c)
    : idStack(s), sqlCache(c)
{
}

void writerSqlCacheFill::addFirstPriority(int watchid, int scanid)
{
    watchscanid id;
    id.watchid = watchid;
    id.scanid = scanid;
    firstPriority.push(id);
}

void Writer::getDataForInserts(int watchid, int scanid)
{
    watchscanid id;
    id.watchid = watchid;
    id.scanid = scanid;
    if (sqlCache.contains(id))
    {
        cachehit++;
        emit dataForInsertsReady(watchid, scanid);
    }
    else
    {
        cachemiss++;
        if (pCacheFiller)
            pCacheFiller->addFirstPriority(watchid, scanid);
    }
}

void Writer::onCacheAdded(int watchid, int scanid)
{
    //qxtLog->debug() << QString("%1: %2").arg(__func__).arg(QThread::currentThreadId());
    emit cacheAdded(sqlCache.size(), idStack.size());
    emit dataForInsertsReady(watchid, scanid);
}

void Writer::onDataForInsertReady(int watchid, int scanid)
{
    if (idleChildrenId.isEmpty())
        return;
    emit dataReady(idleChildrenId.takeFirst(), watchid, scanid);
}

void writerSqlCacheFill::start()
{
    stopReq = false;
    buildCache();
}

void Writer::childNeedData(int id)
{
    idleChildrenId.append(id);
    //qDebug() << QString("%1: %2").arg(__func__).arg(QThread::currentThreadId());
    bool newData = false;
    watchscanid idToProcess;
    while (!idStack.isEmpty())
    {
        stackAccessMutex.lock ();
        if (!idStack.isEmpty ())
        {
             //idToProcess = stack.takeAt( qrand() % stack.size() );
            idToProcess = idStack.takeFirst();
             newData = true;
        }
        stackAccessMutex.unlock ();
        qulonglong scanid = idToProcess.scanid;
        Dbt key;
        key.set_flags(DB_DBT_USERMEM);
        key.set_data(&scanid);
        key.set_size(sizeof(scanid));
        DbTxn* pTrans =0;
        try
        {
            ds.pEnv->txn_begin(0, &pTrans, 0);
            int ret = ds.dbMap["scantime"]->primDb->exists(pTrans, &key, DB_READ_COMMITTED);

            if (pTrans)
                pTrans->commit(0);

            if (!ret)
            {
                qxtLog->debug() << QString("scan %1 already inserted -- get next").arg(scanid);
                newData = false;
                emit taskFinished(id);
            }
        }
        catch(DbException& e)
        {
            qxtLog->error() << e.what();
            if (pTrans)
                pTrans->abort();
        }
        if(newData)
        {
            getDataForInserts(idToProcess.watchid, idToProcess.scanid);
            break;
        }
    }
}

void Writer::getCacheSize(int &cur, int &total)
{
    cur = sqlCache.size();
    total = idStack.size();
}

void Writer::getCacheHitMiss(int &hit, int &miss)
{
    hit = cachehit;
    miss = cachemiss;
}

void Writer::getProgress(int &c, int &t)
{
    c = currentProgress;
    t = totalProgress;
}

void Writer::onProgressInc(int id)
{
    qxtLog->debug() << QString("%1: sender: %2 [%3]. reciever: %4 [%5]")
                       .arg(__func__)
                       .arg(sender()->objectName()).arg(sender()->thread()->objectName())
                       .arg(this->objectName()).arg(thread()->objectName());
    currentProgress++;
    emit taskFinished(id);
}

void Writer::onChildStop(int id)
{
    qxtLog->debug() << QString("%1: sender: %2 [%3]. reciever: %4 [%5]")
                       .arg(__func__)
                       .arg(sender()->objectName()).arg(sender()->thread()->objectName())
                       .arg(this->objectName()).arg(thread()->objectName());


    emit childStopped(id);

    if (writers.contains(id) && writers[id].first)
    {
        writers[id].first->quit();
        writers[id].first->wait();
        writers[id].second->deleteLater();
        writers[id].first->deleteLater();
    }


    writers.remove(id);

    if (writers.isEmpty() && !pCacheFiller)
        emit finished();


}

void Writer::onChildStart(int id)
{
    qxtLog->debug() << QString("%1: sender: %2 [%3]. reciever: %4 [%5]")
                       .arg(__func__)
                       .arg(sender()->objectName()).arg(sender()->thread()->objectName())
                       .arg(this->objectName()).arg(thread()->objectName());
    emit childStarted(id);
}

void Writer::onCacheFillerStop()
{
    delete pCacheFiller;
    delete pCacheFillerThread;

    if (writers.isEmpty() && !pCacheFiller)
        emit finished();
}

void openDB()
{
    u_int32_t  dbOpenFlags =  DB_CREATE
                            | DB_THREAD
                            ;
    u_int32_t envOpenFlags =  DB_CREATE
                            | DB_INIT_MPOOL
                            | DB_THREAD
                            | DB_INIT_TXN
                            | DB_INIT_LOG
                            | DB_INIT_LOCK
                            ;
    try
    {
        ds.envOpenFlags = envOpenFlags;
        ds.dataFolder = ENV_FOLDER;

        bool ok;
        ok = ds.addDatabase(MW_FILE,"marketwatcher", DB_BTREE, dbOpenFlags);
        ok = ds.addIndex("marketwatcher","seller");
        ok = ds.addIndex("marketwatcher","timestmp_int");
        ok = ds.addIndex("marketwatcher","server:faction");

        ok = ds.addDatabase("wi.dat", "watchitem", DB_BTREE, dbOpenFlags);
        ok = ds.addIndex("watchitem","server:faction", wi_server_faction_bt_compare);
        ok = ds.addIndex("watchitem","itemid");
        ok = ds.addIndex("watchitem","server:faction:itemid");

        ok = ds.addDatabase("st.dat", "scantime", DB_BTREE, dbOpenFlags);
        ok = ds.addIndex("scantime", "watchid");

        ok = ds.initialize();
    }
    catch(DbException& e)
    {
        qxtLog->fatal() << log_entry_header << "fail to open DB Enviropment";
        qxtLog->fatal() << log_entry_header << QString("[%1]: %2").arg (e.get_errno()).arg (e.what());
    }

    qxtLog->info ("Db configured");
}

void closeDB()
{
    stopWriteThreads ();
    stopReadThreads (false);
    stopCheckBackThread (false);

    pWriterThread->wait();
    pCheckerThread->wait();

    foreach(QString primName, ds.dbMap.keys())
    {
        foreach(Index index, ds.dbMap[primName]->indexList)
        {
            if (ds.dbMap[primName]->secDb.contains(index.name) && ds.dbMap[primName]->secDb[index.name])
            {
                ds.dbMap[primName]->secDb[index.name]->close (0);
                delete ds.dbMap[primName]->secDb[index.name];
                ds.dbMap[primName]->secDb[index.name] = 0;
            }
        }

        if (ds.dbMap[primName]->primDb)
        {
            //marketwatcher->compact (NULL, NULL,NULL,NULL,0,NULL);
            ds.dbMap[primName]->primDb->close (0);
            delete ds.dbMap[primName]->primDb;
            ds.dbMap[primName]->primDb = 0;
        }
    }

    if (ds.pEnv)
        ds.pEnv->close (0);

    delete ds.pEnv;
}
