#ifndef THREAD_HXX
#define THREAD_HXX

//#include "main.h"
#include "storage.hxx"
#include <db_cxx.h>
#include <QxtLogger>
#include <QThread>
#include <QTimer>
#include <QDir>
#include <QDateTime>
#include <QDebug>
#include <stdlib.h>
#include <QList>
#include <QMessageBox>
#include <QSqlQuery>
#include <QMutex>
#include <QStack>
#include <QPointer>

struct watchscanid
{
    uint watchid;
    uint scanid;
    bool operator<(const watchscanid& o) const
    {
        return watchid < o.watchid || (watchid == o.watchid && scanid < o.scanid);
    }
    bool operator == (const watchscanid& o) const
    {
        return watchid == o.watchid && scanid == o.scanid;
    }
};

class Checker : public QObject
{
    Q_OBJECT
    QMutex mutex;
public:
    Checker(DataStorage& d, QObject* parent = 0);
    ~Checker();
public slots:
    void start();
    void stop();
private slots:
    void onCheckTimer();
    void onBackupTimer();
signals:
    void finished();
    void processStarted();
    void processFinished();
protected:
    QStringList archiveHelper(u_int32_t flags);
    DataStorage& ds;
    QTimer* pCheckTimer;
    QTimer* pBackupTimer;
};

class Reader : public QObject
{
    Q_OBJECT
public:
    Reader(DataStorage& d, QObject* parent = 0);
    ~Reader();
    bool isRunning();
    void wait();
public slots:
    void start();
    void stop(bool async = true);
private slots:
    void readData();
    void _start();
    void _stop();
signals:
    void finished();
protected:
    QThread* pThread;
    DataStorage& ds;
    QTimer* pTimer;
    void stopReq();
    void run();
};

class ItemWriter : public QObject
{
    Q_OBJECT

    QString conn_name;
    QList<watchscanid>& stack;
    bool stopReq;
    DataStorage& ds;
    int threadId;
    QMap<watchscanid, QVariantList>& sqlCache;
    watchscanid idToProcess;
    int state; /// 0 - idle, 1 - processing item
signals:
    void itemAdded(int id, int itemNum);
    void taskFinished(int id);
    void taskStarted(int id, int totalCount);
    void taskAborted(int id);
    void needData(int id);
    //void done();
    void stopped(int id);
    void started(int id);
private slots:
    void processItem(watchscanid wi);
    void stopIt();
public slots:
    void stop();
    void start();
    void onDataReady(int id, int watchid, int scanid);
public:
    ItemWriter(int id, DataStorage& d, QList<watchscanid>& st, QMap<watchscanid, QVariantList>& cm, QObject* parent =0);
    ~ItemWriter();
};

class writerSqlCacheFill : public QObject
{
    Q_OBJECT

    QList<watchscanid>& idStack;
    QMap<watchscanid, QVariantList>& sqlCache;
    QStack<watchscanid> firstPriority;
    bool stopReq;
    QString conn_name;
public slots:
    void buildCache();
    void addFirstPriority(int watchid, int scanid);
    void stop();
    void start();
signals:
    void cacheAdded(int watchid, int scanid);
    void done();
public:
    writerSqlCacheFill(QList<watchscanid>& s, QMap<watchscanid, QVariantList>& c);
    ~writerSqlCacheFill();
};

class Writer : public QObject
{
    Q_OBJECT

    QMap<int, QPair<QThread*,ItemWriter*> > writers;
    int thrdCnt;
    DataStorage& ds;
    QList<watchscanid> idStack;
    QMap<watchscanid, QVariantList> sqlCache;
    QPointer<writerSqlCacheFill> pCacheFiller;
    QPointer<QThread> pCacheFillerThread;
    QList<int> idleChildrenId;
    int cachehit;
    int cachemiss;
    int totalProgress;
    int currentProgress;
    QMutex stackAccessMutex;

    void stopRequest();
    void fillIdStack();
    void run();
public:
    Writer(DataStorage& d, int cnt, QObject* parent = 0);
    ~Writer();
    int getStackSize();
    void getCacheSize(int& cur, int& total);
    void getCacheHitMiss(int& hit, int& miss);
    void getProgress(int& c, int& t);
public slots:
    void getDataForInserts(int watchid, int scanid);
    void start();
    void stop();
signals:
    void itemAdded(int threadNum, int itemNum);
    void taskFinished(int threadNum);
    void taskStarted(int threadNum, int totalCount);
    void taskAborted(int threadNum);
    void childStarted(int threadNum);
    void childStopped(int threadNum);
    void stackReady(int);
    void cacheAdded(int, int);
    void dataForInsertsReady(int watchid, int scanid);
    void dataReady(int, int, int);
    void finished();
    void stopRequested();
private slots:
    void onChildStop(int id);
    void onChildStart(int id);
    void childNeedData(int id);
    void onStackReady();
    void onCacheFillerStop();
    void onCacheAdded(int watchid, int scanid);
    void onDataForInsertReady(int, int);
    void onProgressInc(int);
};

#endif // THREAD_HXX

