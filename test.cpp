#include <QVariantMap>
#include <QDebug>

void print_map(QVariant map, QString tab="")
{
    if (map.toMap().isEmpty())
        qDebug () << QString("%1%2").arg(tab).arg(map.toString());
    else
    {
        foreach(QString key, map.toMap().keys())
        {
                qDebug() << QString("%1%2").arg(tab).arg(key);;
                print_map(map.toMap()[key], tab+"\t");
        }
    }
}

void f()
{
    QMap<QString, QMap<QString, QMap<QString, QVariant> > > real_map;
    real_map["Argent Dawn"]["Alliance"]["Infinite Dust"] = 34054;
    real_map["Argent Dawn"]["Alliance"]["Abyss Crystal"] = 34056;
    real_map["Argent Dawn"]["Horde"]["Infinite Dust"] = 34054;

    real_map["Eathern Ring"]["Alliance"]["Infinite Dust"] = 34054;
    real_map["Eathern Ring"]["Horde"]["Infinite Dust"] = 34054;

    QVariantMap map;
    foreach(QString key, real_map.keys())
    {
        QVariantMap v;
        foreach(QString key2, real_map[key].keys())
        {
            QVariantMap v2;
            foreach(QString key3, real_map[key][key2].keys())
            {
                v2[key3] = real_map[key][key2][key3];
            }

            v[key2] = v2;
        }

        map[key] = v;
    }

    print_map(map);
}
