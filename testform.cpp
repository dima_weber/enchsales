#include "testform.h"
#include "ui_testform.h"
#include <qwt_series_data.h>
#include <qwt_plot_curve.h>
#include <QDialog>
#include <QtSql>
#include <qwt_scale_draw.h>
#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_zoomer.h>
#include <QMessageBox>
#include <QFont>

class datedraw: public QwtScaleDraw
{
public:
    datedraw(QDateTime startdate) : QwtScaleDraw()
    {
        startDate = startdate;
    }
    virtual QwtText label(double v) const
    {
        return startDate.addSecs(v).toString();
    }
private:
    QDateTime startDate;
};



TestForm::TestForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestForm)
{
    ui->setupUi(this);
    setLayout(ui->verticalLayout);
    setAutoDisplay(false);
    display();

    p_fulltime_plot = new QwtPlot(this);
    ui->verticalLayout->addWidget(p_fulltime_plot);
    p_fulltime_plot->setMaximumHeight(100);
    p_fulltime_plot->setMinimumHeight(100);
}

TestForm::~TestForm()
{
    delete ui;
}
void TestForm::addInfo(int itemid, QStringList values)
{
    info[itemid] = values;
    if (autodisplay)
        display();
}

void TestForm::addInfo(int itemid, QString value)
{
    info[itemid].append(value);
    if (autodisplay)
        display();
}

void TestForm::clear()
{
    info.clear();
    if (autodisplay)
        display();
}

void TestForm::display()
{
    ui->qwtPlot->detachItems();
    if (info.isEmpty())
        return;
    QVector<int> ids;
    foreach(int id, info.keys())
        ids.append(id);

    QVector<QVector<double> > y;
    QVector<double> x;
    QDateTime start;
    QDateTime finish;
    double min_val = 0xFFFFFF;
    double max_val = 0;

    QSqlQuery query;
    QString sql = QString("Select min(scantime), max(scantime) "
                  "    from mw_bo_stat "
                  "    where itemid in (%1").arg(ids[0]);

    for (int i=1;i<ids.size();i++)
    {
        sql = QString(sql+", %1").arg(ids[i]);
    }
    sql = sql + ")";
    query.exec(sql);
    query.next();
    start = query.value(0).toDateTime();
    finish = query.value(1).toDateTime();

    QStringList factions;
    sql = "SELECT DISTINCT srv, faction from mw_bo_stat";
    query.exec(sql);
    while(query.next())
    {
        QString f = query.value(1).toString();
        QString s = query.value(0).toString();
        factions.append(f);
    }

    int cnt=0;
    foreach(QString faction, factions)
    {
        foreach(int id, ids)
        {
            QString sql = "select scantime";
            x.clear();
            y.clear();
            int sz = info[id].size();
            foreach(QString param, info[id])
            {
                sql = sql + ", " + param + "/10000";
                y.append(QVector<double>());
            }
             sql = sql + " from mw_bo_stat "
                              " where itemid=:itemid and faction=:faction"
                             " order by scantime;";
            query.prepare(sql);
            query.bindValue(":itemid", id);
            query.bindValue(":faction", faction);
            query.exec();

            QDateTime   cur;
            while (query.next())
            {
                cur = query.value(0).toDateTime();
                for(int i=1;i<= sz;i++)
                {
                    double  val = query.value(i).toDouble();
                    y[i-1].append(val);
                    min_val = std::min(min_val, val);
                    max_val = std::max(max_val, val);
                }

                x.append( start.secsTo(cur));
            }

            for(int i=0;i<y.size();i++)
            {
                QwtSeriesData<QPointF>* p_data   = new  QwtPointArrayData(x,  y[i]);
                QwtPlotCurve* p_curve = new QwtPlotCurve(QString("%2 %3 (%1)").arg(faction).arg(id).arg(info[id][i]));
                p_curve->setRenderHint(QwtPlotItem::RenderAntialiased);
                p_curve->setPen(QPen(QColor(QColor(100+cnt*10, 100+cnt*10, 150+cnt*10)), 2));
                p_curve->attach(ui->qwtPlot);
                p_curve->setData(p_data);

                QwtPlotCurve* p_fulltime_curve = new QwtPlotCurve(QString("%2 %3 (%1)").arg(faction).arg(id).arg(info[id][i]));
                p_fulltime_curve->attach(p_fulltime_plot);
                p_fulltime_curve->setRenderHint(QwtPlotItem::RenderAntialiased);
                p_fulltime_curve->setData(p_data);

                cnt++;
            }
        }
    }

    //ui->qwtPlot->setAxisScaleDraw(QwtPlot::xBottom, new datedraw(start));

    QwtLegend* p_legend = new QwtLegend();
    p_legend->setItemMode(QwtLegend::CheckableItem);
    ui->qwtPlot->insertLegend(p_legend, QwtPlot::TopLegend);

    p_fulltime_plot->setAxisScale(QwtPlot::xBottom, 0, start.secsTo(finish));
    p_fulltime_plot->setAxisScale(QwtPlot::yLeft, min_val, max_val);

    ui->qwtPlot->setAxisScale(QwtPlot::xBottom, 0, start.secsTo(finish));
    ui->qwtPlot->setAxisScale(QwtPlot::yLeft, min_val, max_val);

    QwtPlotGrid* p_grid = new QwtPlotGrid;
    p_grid->enableXMin(true);
    p_grid->enableYMin(true);
    p_grid->setMajPen(QPen(Qt::black, 0, Qt::DotLine));
    p_grid->setMinPen(QPen(Qt::gray, 0 , Qt::DotLine));
    p_grid->attach(ui->qwtPlot);

    // Set axis titles
   // ui->qwtPlot->setAxisTitle(QwtPlot::xBottom, "Date");
   // ui->qwtPlot->setAxisTitle(QwtPlot::yLeft, "Price");

    QwtPlotZoomer* p_zoomer;

    p_zoomer = new QwtPlotZoomer( QwtPlot::xBottom, QwtPlot::yLeft, ui->qwtPlot->canvas());
    //p_zoomer->setRubberBand(QwtPicker::HLineRubberBand);
    p_zoomer->setRubberBand(QwtPicker::RectRubberBand);
    p_zoomer->setRubberBandPen(QColor(Qt::black));
    p_zoomer->setTrackerMode(QwtPicker::ActiveOnly);
    //p_zoomer->setTrackerMode(QwtPicker::AlwaysOff);
    //p_zoomer->setTrackerMode(QwtPicker::AlwaysOn);
    p_zoomer->setTrackerPen(QColor(Qt::darkBlue));
    p_zoomer->setResizeMode(QwtPicker::Stretch);
    //p_zoomer->setSelectionFlags(QwtPicker::DragSelection | QwtPicker::RectSelection);

    // RightButton: zoom out by 1
    // Ctrl+RightButton: zoom out to full size

    p_zoomer->setMousePattern(QwtEventPattern::MouseSelect2,
        Qt::RightButton, Qt::ControlModifier);

    p_zoomer->setMousePattern(QwtEventPattern::MouseSelect3,
        Qt::RightButton);
    ui->qwtPlot->replot();
}

void TestForm::setAutoDisplay(bool a)
{
    autodisplay= a;
}
