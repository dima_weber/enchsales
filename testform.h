#ifndef TESTFORM_H
#define TESTFORM_H

#include <QWidget>
#include <QMap>
#include <QStringList>

namespace Ui {
    class TestForm;
}

class QwtPlot;

class TestForm : public QWidget
{
    Q_OBJECT

public:
    typedef QMap<int, QStringList> showInfo; // key - item id, values - names of curves to show

    explicit TestForm(QWidget *parent = 0);

    void addInfo(int, QStringList);
    void addInfo(int, QString);
    void clear();
    void setAutoDisplay(bool a=true);
    ~TestForm();
private:
    void display();
    Ui::TestForm *ui;
    showInfo info;
    bool autodisplay;

    QwtPlot * p_fulltime_plot;
};

#endif // TESTFORM_H
