#include "threadexec.h"

#include <QMessageBox>
#include "QProgressIndicator.h"
#include <qxtsignalwaiter.h>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlDatabase>

#include <QxtLogger>
#include <QMutex>

QMutex mutex2;
void ThreadExec::run()
{
    mutex2.lock();
    try
    {
        if (sql.isNull())
        {
            query->exec();
        }
        else
        {
            query->exec(sql);
        }
    }
    catch(...)
    {
        qxtLog->critical() <<"Exception while process query";
    }
    mutex2.unlock();
}

void ThreadConnect::run()
{
    db->open();
}

bool ThreadExec::execQuery(QSqlQuery* p_query, QWidgetList disableList, QProgressIndicator* p_indicator)
{
    foreach(QWidget* p_widget, disableList)
    {
        p_widget->setEnabled(false);
    }

    if (p_indicator)
    {
        p_indicator->raise();
        p_indicator->startAnimation();
    }

    ThreadExec p_thread(p_query);
    p_thread.start();
    QxtSignalWaiter waiter(&p_thread, SIGNAL(finished()));
    bool res = waiter.wait();

    if (p_indicator)
    {
        p_indicator->lower();
        p_indicator->stopAnimation();
    }

    foreach(QWidget* p_widget, disableList)
    {
        p_widget->setEnabled(true);
    }

    if(!p_query->isActive())
    {
        QMessageBox::critical(NULL, "Database Error", p_query->lastError().text(), QMessageBox::Ok);
    }
    return res;
}

bool ThreadExec::execQuery(QSqlQuery* p_query, QString sql,QWidgetList disableList, QProgressIndicator* p_indicator)
{
    foreach(QWidget* p_widget, disableList)
    {
        p_widget->setEnabled(false);
    }

    if (p_indicator)
    {
        p_indicator->raise();
        p_indicator->startAnimation();
    }

    ThreadExec p_thread(p_query, sql);
    p_thread.start();
    QxtSignalWaiter waiter(&p_thread, SIGNAL(finished()));
    bool res = waiter.wait();

    if (p_indicator)
    {
        p_indicator->lower();
        p_indicator->stopAnimation();
    }

    foreach(QWidget* p_widget, disableList)
    {
        p_widget->setEnabled(true);
    }

    if(!p_query->isActive())
    {
        QMessageBox::critical(NULL, "Database Error", p_query->lastError().text(), QMessageBox::Ok);
    }
    return res;
}

bool ThreadConnect::execConnect(QSqlDatabase* p_db, QWidgetList disableList, QProgressIndicator* p_indicator)
{
    foreach(QWidget* p_widget, disableList)
    {
        p_widget->setEnabled(false);
    }

    if (p_indicator)
    {
        p_indicator->raise();
        p_indicator->startAnimation();
    }

    ThreadConnect p_thread(p_db);
    p_thread.start();
    bool res = QxtSignalWaiter::wait(&p_thread, SIGNAL(finished()));

    if (p_indicator)
    {
        p_indicator->lower();
        p_indicator->stopAnimation();
    }

    foreach(QWidget* p_widget, disableList)
    {
        p_widget->setEnabled(true);
    }

    if(!p_db->isOpen())
    {
        QMessageBox::critical(NULL, "Database Error", p_db->lastError().text(), QMessageBox::Ok);
    }
    return res;

    return res;
}
