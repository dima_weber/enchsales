#ifndef THREADEXEC_H
#define THREADEXEC_H

#include <QThread>
#include <QWidgetList>

class QProgressIndicator;
class QSqlQuery;
class QSqlDatabase;

class ThreadExec : public QThread
{
    Q_OBJECT
public:
    ThreadExec(QSqlQuery* p,QString _sql=QString::null)
    {
        query=p;
        sql = _sql;
    }

    static bool execQuery(QSqlQuery* p_query, QWidgetList disableList, QProgressIndicator* p_indicator = NULL);
    static bool execQuery(QSqlQuery* p_query, QString, QWidgetList disableList, QProgressIndicator* p_indicator = NULL);
private:
    void run();
    QSqlQuery* query;
    QString sql;
};

class ThreadConnect : public QThread
{
    Q_OBJECT
public:
    ThreadConnect(QSqlDatabase* p_db)
    {
        db=p_db;
    }

    static bool execConnect(QSqlDatabase* p_db, QWidgetList disableList, QProgressIndicator* p_indicator = NULL);
private:
    void run();
    QSqlDatabase* db;
};

#endif // THREADEXEC_H
