/* Copyright (C) 2002 MySQL AB

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

/*
** example file of UDF (user definable functions) that are dynamicly loaded
** into the standard mysqld core.
**
** The functions name, type and shared library is saved in the new system
** table 'func'.  To be able to create new functions one must have write
** privilege for the database 'mysql'.	If one starts MySQL with
** --skip-grant, then UDF initialization will also be skipped.
**
** Syntax for the new commands are:
** create function <function_name> returns {string|real|integer}
**		  soname <name_of_shared_library>
** drop function <function_name>
**
** Each defined function may have a xxxx_init function and a xxxx_deinit
** function.  The init function should alloc memory for the function
** and tell the main function about the max length of the result
** (for string functions), number of decimals (for double functions) and
** if the result may be a null value.
**
** If a function sets the 'error' argument to 1 the function will not be
** called anymore and mysqld will return NULL for all calls to this copy
** of the function.
**
** All strings arguments to functions are given as string pointer + length
** to allow handling of binary data.
** Remember that all functions must be thread safe. This means that one is not
** allowed to alloc any global or static variables that changes!
** If one needs memory one should alloc this in the init function and free
** this on the __deinit function.
**
** Note that the init and __deinit functions are only called once per
** SQL statement while the value function may be called many times
**
** A dynamicly loadable file should be compiled shared.
** (something like: gcc -shared -o my_func.so myfunc.cc).
** You can easily get all switches right by doing:
** cd sql ; make udf_example.o
** Take the compile line that make writes, remove the '-c' near the end of
** the line and add -shared -o udf_example.so to the end of the compile line.
** The resulting library (udf_example.so) should be copied to some dir
** searched by ld. (/usr/lib ?)
** If you are using gcc, then you should be able to create the udf_example.so
** by simply doing 'make udf_example.so'.
qqq**
** After the library is made one must notify mysqld about the new
** functions with the commands:
**
** CREATE AGGREGATE FUNCTION median RETURNS REAL SONAME "udf_median.so";
** CREATE AGGREGATE FUNCTION lowquantile RETURNS REAL SONAME "udf_median.so";
** CREATE AGGREGATE FUNCTION hiquantile RETURNS REAL SONAME "udf_median.so";
**
** After this the functions will work exactly like native MySQL functions.
** Functions should be created only once.
**
** The functions can be deleted by:
**
** DROP FUNCTION median;
** DROP FUNCTION lowquantile;
** DROP FUNCTION hiquantile;
**
** The CREATE FUNCTION and DROP FUNCTION update the func@mysql table. All
** Active function will be reloaded on every restart of server
** (if --skip-grant-tables is not given)
**
** If you ge problems with undefined symbols when loading the shared
** library, you should verify that mysqld is compiled with the -rdynamic
** option.
**
** If you can't get AGGREGATES to work, check that you have the column
** 'type' in the mysql.func table.  If not, run 'mysql_fix_privilege_tables'.
**
*/

#ifdef STANDARD
/* STANDARD is defined, don't use any mysql functions */
#include <stdlib.h>
//#include <stdio.h>
#include <string.h>
#ifdef __WIN__
typedef unsigned __int64 ulonglong;	/* Microsofts 64 bit types */
typedef __int64 longlong;
#else
typedef unsigned long long ulonglong;
typedef long long longlong;
#endif /*__WIN__*/
#else
#include <my_global.h>
#include <my_sys.h>
#if defined(MYSQL_SERVER)
#include <m_string.h>		/* To get strmov() */
#else
/* when compiled as standalone */
#include <string.h>
#define strmov(a,b) stpcpy(a,b)
#define bzero(a,b) memset(a,0,b)
#define memcpy_fixed(a,b,c) memcpy(a,b,c)
#endif
#endif
#include <mysql.h>
#include <ctype.h>

#ifdef HAVE_DLOPEN


/* These must be right or mysqld will not find the symbol! */

my_bool median_init( UDF_INIT* initid, UDF_ARGS* args, char* message );
void median_deinit( UDF_INIT* initid );
void median_reset( UDF_INIT* initid, UDF_ARGS* args, char* is_null, char *error );
void median_clear( UDF_INIT* initid, char* is_null, char *error );
void median_add( UDF_INIT* initid, UDF_ARGS* args, char* is_null, char *error );
double median( UDF_INIT* initid, UDF_ARGS* args, char* is_null, char *error );

my_bool lowquantile_init( UDF_INIT* initid, UDF_ARGS* args, char* message );
void lowquantile_deinit( UDF_INIT* initid );
void lowquantile_reset( UDF_INIT* initid, UDF_ARGS* args, char* is_null, char *error );
void lowquantile_clear( UDF_INIT* initid, char* is_null, char *error );
void lowquantile_add( UDF_INIT* initid, UDF_ARGS* args, char* is_null, char *error );
double lowquantile( UDF_INIT* initid, UDF_ARGS* args, char* is_null, char *error );

my_bool hiquantile_init( UDF_INIT* initid, UDF_ARGS* args, char* message );
void hiquantile_deinit( UDF_INIT* initid );
void hiquantile_reset( UDF_INIT* initid, UDF_ARGS* args, char* is_null, char *error );
void hiquantile_clear( UDF_INIT* initid, char* is_null, char *error );
void hiquantile_add( UDF_INIT* initid, UDF_ARGS* args, char* is_null, char *error );
double hiquantile( UDF_INIT* initid, UDF_ARGS* args, char* is_null, char *error );
/*************************************************************************
** Example of init function
** Arguments:
** initid	Points to a structure that the init function should fill.
**		This argument is given to all other functions.
**	my_bool maybe_null	1 if function can return NULL
**				Default value is 1 if any of the arguments
**				is declared maybe_null.
**	unsigned int decimals	Number of decimals.
**				Default value is max decimals in any of the
**				arguments.
**	unsigned int max_length  Length of string result.
**				The default value for integer functions is 21
**				The default value for real functions is 13+
**				default number of decimals.
**				The default value for string functions is
**				the longest string argument.
**	char *ptr;		A pointer that the function can use.
**
** args		Points to a structure which contains:
**	unsigned int arg_count		Number of arguments
**	enum Item_result *arg_type	Types for each argument.
**					Types are STRING_RESULT, REAL_RESULT
**					and INT_RESULT.
**	char **args			Pointer to constant arguments.
**					Contains 0 for not constant argument.
**	unsigned long *lengths;		max string length for each argument
**	char *maybe_null		Information of which arguments
**					may be NULL
**
** message	Error message that should be passed to the user on fail.
**		The message buffer is MYSQL_ERRMSG_SIZE big, but one should
**		try to keep the error message less than 80 bytes long!
**
** This function should return 1 if something goes wrong. In this case
** message should contain something usefull!
**************************************************************************/

/*
** Syntax for the new aggregate commands are:
** create aggregate function <function_name> returns {string|real|integer}
**		  soname <name_of_shared_library>
**
** Syntax for median: median( t.quantity, t.price )
**	with t.quantity=integer, t.price=double
** (this example is provided by Andreas F. Bobak <bobak@relog.ch>)
*/


struct median_data
{
  double* data;
  longlong data_len;
  longlong count;
};

inline void* get_memory (struct median_data* p_data)
{
  longlong len = p_data->data_len;
  double* data = p_data->data;
  p_data->data = realloc (data, len * sizeof(double));
  
  return p_data->data;
}

inline void set_error_text(char* buf, char* text)
{
    strncpy(buf,text, MYSQL_ERRMSG_SIZE );
	buf[MYSQL_ERRMSG_SIZE - 1]= '\0';
}

void sort(double* buf, int cnt)
{
	int i,j;
        double t;
	for(i=0;i<cnt-1;i++)
		for(j=i+1;j<cnt;j++)
			if (buf[i] > buf[j])
			{
				t = buf[j];
				buf[j] = buf[i];
				buf[i] = t;
			}
}

int compare_double(const void* a, const void* b)
{
  double a1 = *((double*)a);
  double b1 = *((double*)b);
  return (int)(a1 - b1);
}

/*
** Median Aggregate Function.
*/
my_bool
median_init( UDF_INIT* initid, UDF_ARGS* args, char* message )
{
    struct median_data*	data;

  if (args->arg_count != 2)
  {
    set_error_text(message, "error: MEDIAN() requires two arguments" );
    return 1;
  }

  if (args->arg_type[0] != INT_RESULT) 
  {
    set_error_text(message, "error: MEDIAN() requires an INT as first argument .");
    return 1;
  }
  if	(args->arg_type[1] != REAL_RESULT && args->arg_type[1] != INT_RESULT)
  {
      if (args->arg_type[1]== DECIMAL_RESULT)
      {
          args->arg_type[1] = REAL_RESULT;
      }
      else
      {
          set_error_text(message, "error: MEDIAN() requires a REAL or INT as second argument.");
          return 1;
      }
  }

  initid->maybe_null	= 0;		/* The result may be null */
  initid->decimals	= 4;		/* We want 4 decimals in the result */
  initid->max_length	= 20;		/* 6 digits + . + 10 decimals */

  
  if (!(data = (struct median_data*) malloc(sizeof(struct median_data))))
  {
    set_error_text(message, "Couldn't allocate memory");
    return 1;
  }
  data->data	 = NULL;
  data->data_len = 10;
  data->count    = 0;

  if (!get_memory(data))
  {
	set_error_text(message, "Couldn't allocate memory");
	free(data);  
	return 1;
  }
  initid->ptr = (char*)data;
  return 0;
}

void
median_deinit( UDF_INIT* initid )
{
  struct median_data* data = (struct median_data*)initid->ptr;
  free(data->data);
  free(data);
}

/* This is only for MySQL 4.0 compability */
void
median_reset(UDF_INIT* initid, UDF_ARGS* args, char* is_null, char* message)
{
  struct median_data* data = (struct median_data*)initid->ptr;
  median_clear(initid, is_null, message);
  median_add(initid, args, is_null, message);
}

/* This is needed to get things to work in MySQL 4.1.1 and above */

void
median_clear(UDF_INIT* initid, char* is_null __attribute__((unused)),
              char* message __attribute__((unused)))
{
  struct median_data* data = (struct median_data*)initid->ptr;
  data->count=	0;
  data->data_len =	10;
  get_memory(data);
}


void
median_add(UDF_INIT* initid, UDF_ARGS* args,
            char* is_null __attribute__((unused)),
            char* message __attribute__((unused)))
{
  struct median_data* data	= (struct median_data*)initid->ptr;
  if (args->args[0] && args->args[1])
  {
    
    longlong quantity		    = *((longlong*)args->args[0]);
    longlong newquantity	    = data->count + quantity;
    double price = 0;
	int i=0;
	
	if (args->arg_type[1] == REAL_RESULT)
	{
		price = *((double*)args->args[1]);
	}
	else
	{
		if (args->arg_type[1] == INT_RESULT)
		{
			price = (double)*((longlong*)args->args[1]);
		}
	}
	

	if (newquantity >= data->data_len)
	{
		data->data_len = newquantity + 20;
		get_memory(data);
	}

	for(i=0;i<quantity;i++)
	{
	  data->data[data->count] = price;
	  data->count++;
	}
  }
}

double
median( UDF_INIT* initid, UDF_ARGS* args __attribute__((unused)),
        char* is_null, char* error __attribute__((unused)))
{
  double res = 0.0;
  struct median_data* data = (struct median_data*)initid->ptr;
  if (!data->count)
  {
    *is_null = 1;
  }
  else
  {
	*is_null = 0;
        qsort(data->data, data->count, sizeof(double), compare_double );
        //sort(data->data, data->count);
	if (data->count % 2 == 0)
	{
	  res = (data->data[data->count / 2] + data->data[data->count/2 - 1]) / 2;
	}
	else
	{
	  res = data->data[data->count / 2 - 1];
	}
  }
  return res;
}

my_bool lowquantile_init( UDF_INIT* initid, UDF_ARGS* args, char* message )
{
  return median_init(initid, args, message);
}

void lowquantile_deinit( UDF_INIT* initid )
{
  median_deinit(initid);
}


/* This is only for MySQL 4.0 compability */
void lowquantile_reset(UDF_INIT* initid, UDF_ARGS* args, char* is_null, char* message)
{
  median_reset(initid, args, is_null, message);
}

/* This is needed to get things to work in MySQL 4.1.1 and above */

void lowquantile_clear(UDF_INIT* initid, char* is_null __attribute__((unused)),
              char* message __attribute__((unused)))
{
  median_clear(initid, is_null, message);
}


void lowquantile_add(UDF_INIT* initid, UDF_ARGS* args,
            char* is_null __attribute__((unused)),
            char* message __attribute__((unused)))
{
  median_add(initid, args, is_null,message);
}

double lowquantile( UDF_INIT* initid, UDF_ARGS* args __attribute__((unused)),
        char* is_null, char* error __attribute__((unused)))
{
  double res = 0.0;
  struct median_data* data = (struct median_data*)initid->ptr;
  if (!data->count)
  {
    *is_null = 1;
  }
  else
  {
	*is_null = 0;
        qsort(data->data, data->count, sizeof(double), compare_double );
        //sort(data->data, data->count);
	if (data->count % 4 == 0)
	{
	  res = (data->data[data->count / 4] + data->data[data->count/4 - 1]) / 2;
	}
	else
	{
	  res = data->data[data->count / 4 - 1];
	}
  }
  return res;
}

my_bool hiquantile_init( UDF_INIT* initid, UDF_ARGS* args, char* message )
{
  return median_init(initid, args, message);
}

void hiquantile_deinit( UDF_INIT* initid )
{
  median_deinit(initid);
}


/* This is only for MySQL 4.0 compability */
void hiquantile_reset(UDF_INIT* initid, UDF_ARGS* args, char* is_null, char* message)
{
  median_reset(initid, args, is_null, message);
}

/* This is needed to get things to work in MySQL 4.1.1 and above */

void hiquantile_clear(UDF_INIT* initid, char* is_null __attribute__((unused)),
              char* message __attribute__((unused)))
{
  median_clear(initid, is_null, message);
}


void hiquantile_add(UDF_INIT* initid, UDF_ARGS* args,
            char* is_null __attribute__((unused)),
            char* message __attribute__((unused)))
{
  median_add(initid, args, is_null,message);
}

double hiquantile( UDF_INIT* initid, UDF_ARGS* args __attribute__((unused)),
        char* is_null, char* error __attribute__((unused)))
{
  double res = 0.0;
  struct median_data* data = (struct median_data*)initid->ptr;

  if (!data->count)
  {
    *is_null = 1;
  }
  else
  {
	*is_null = 0;
    qsort(data->data, data->count, sizeof(double), compare_double );
    //sort(data->data, data->count);
	if (3 * data->count % 4 == 0)
	{
	  res = (data->data[(3 * data->count) / 4] + data->data[(3 * data->count) / 4 - 1]) / 2;
	}
	else
	{
	  res = data->data[3 * data->count / 4 - 1];
	}
  }
  return res;
}

#endif /* HAVE_DLOPEN */
