/********************************************************************************
** Form generated from reading UI file 'logindialog.ui'
**
** Created: Sun Jul 3 17:36:26 2011
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINDIALOG_H
#define UI_LOGINDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFormLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoginDialog
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_6;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *hostLE;
    QLabel *label_2;
    QLineEdit *userLE;
    QLabel *label_3;
    QLineEdit *pwdLE;
    QLabel *label_4;
    QLineEdit *dbnameLE;
    QLabel *label_5;
    QLineEdit *portLE;
    QHBoxLayout *horizontalLayout;
    QPushButton *testConnectionButton;
    QPushButton *connectButton;
    QPushButton *cancelButton;

    void setupUi(QDialog *LoginDialog)
    {
        if (LoginDialog->objectName().isEmpty())
            LoginDialog->setObjectName(QString::fromUtf8("LoginDialog"));
        LoginDialog->setWindowModality(Qt::ApplicationModal);
        LoginDialog->resize(374, 222);
        verticalLayoutWidget = new QWidget(LoginDialog);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 0, 371, 243));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(8);
        verticalLayout->setContentsMargins(10, 10, 10, 10);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_6 = new QLabel(verticalLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMaximumSize(QSize(64, 64));
        label_6->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/database-accept-icon 48x48.png")));

        horizontalLayout_3->addWidget(label_6);

        formLayout = new QFormLayout();
        formLayout->setContentsMargins(10, 10, 10, 10);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label->setMargin(5);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        hostLE = new QLineEdit(verticalLayoutWidget);
        hostLE->setObjectName(QString::fromUtf8("hostLE"));

        formLayout->setWidget(0, QFormLayout::FieldRole, hostLE);

        label_2 = new QLabel(verticalLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_2->setMargin(5);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        userLE = new QLineEdit(verticalLayoutWidget);
        userLE->setObjectName(QString::fromUtf8("userLE"));

        formLayout->setWidget(1, QFormLayout::FieldRole, userLE);

        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_3->setMargin(5);

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        pwdLE = new QLineEdit(verticalLayoutWidget);
        pwdLE->setObjectName(QString::fromUtf8("pwdLE"));
        pwdLE->setEchoMode(QLineEdit::Password);

        formLayout->setWidget(2, QFormLayout::FieldRole, pwdLE);

        label_4 = new QLabel(verticalLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setEnabled(false);
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_4->setMargin(5);

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        dbnameLE = new QLineEdit(verticalLayoutWidget);
        dbnameLE->setObjectName(QString::fromUtf8("dbnameLE"));
        dbnameLE->setEnabled(false);
        dbnameLE->setReadOnly(true);

        formLayout->setWidget(3, QFormLayout::FieldRole, dbnameLE);

        label_5 = new QLabel(verticalLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setEnabled(false);
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_5->setMargin(5);

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        portLE = new QLineEdit(verticalLayoutWidget);
        portLE->setObjectName(QString::fromUtf8("portLE"));
        portLE->setEnabled(false);
        portLE->setReadOnly(true);

        formLayout->setWidget(4, QFormLayout::FieldRole, portLE);


        horizontalLayout_3->addLayout(formLayout);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(10, -1, 10, 10);
        testConnectionButton = new QPushButton(verticalLayoutWidget);
        testConnectionButton->setObjectName(QString::fromUtf8("testConnectionButton"));

        horizontalLayout->addWidget(testConnectionButton);

        connectButton = new QPushButton(verticalLayoutWidget);
        connectButton->setObjectName(QString::fromUtf8("connectButton"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/connect.png"), QSize(), QIcon::Normal, QIcon::Off);
        connectButton->setIcon(icon);

        horizontalLayout->addWidget(connectButton);

        cancelButton = new QPushButton(verticalLayoutWidget);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));

        horizontalLayout->addWidget(cancelButton);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(LoginDialog);
        QObject::connect(testConnectionButton, SIGNAL(clicked()), LoginDialog, SLOT(onTestConnection()));
        QObject::connect(connectButton, SIGNAL(clicked()), LoginDialog, SLOT(onConnect()));
        QObject::connect(cancelButton, SIGNAL(clicked()), LoginDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(LoginDialog);
    } // setupUi

    void retranslateUi(QDialog *LoginDialog)
    {
        LoginDialog->setWindowTitle(QApplication::translate("LoginDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        label_6->setText(QString());
        label->setText(QApplication::translate("LoginDialog", "Host", 0, QApplication::UnicodeUTF8));
        hostLE->setText(QApplication::translate("LoginDialog", "dimaweber.homeip.net", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("LoginDialog", "User", 0, QApplication::UnicodeUTF8));
        userLE->setText(QApplication::translate("LoginDialog", "remote", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("LoginDialog", "Password", 0, QApplication::UnicodeUTF8));
        pwdLE->setText(QApplication::translate("LoginDialog", "remote", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("LoginDialog", "Database", 0, QApplication::UnicodeUTF8));
        dbnameLE->setText(QApplication::translate("LoginDialog", "enchsales", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("LoginDialog", "Port", 0, QApplication::UnicodeUTF8));
        portLE->setText(QApplication::translate("LoginDialog", "3306", 0, QApplication::UnicodeUTF8));
        testConnectionButton->setText(QApplication::translate("LoginDialog", "Test Connection", 0, QApplication::UnicodeUTF8));
        connectButton->setText(QApplication::translate("LoginDialog", "Connect", 0, QApplication::UnicodeUTF8));
        cancelButton->setText(QApplication::translate("LoginDialog", "Cancel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class LoginDialog: public Ui_LoginDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINDIALOG_H
