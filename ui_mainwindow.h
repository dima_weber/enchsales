/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Sun Jul 3 17:36:26 2011
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDateEdit>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTableView>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionConnect;
    QAction *actionExit;
    QAction *actionFull_Screen;
    QAction *actionRefrech;
    QAction *actionUpdateData;
    QWidget *centralWidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *lastWeekButton;
    QPushButton *lastTwoWeeksButton;
    QPushButton *lastMonthButton;
    QLabel *label_3;
    QDateEdit *dateFromEdit;
    QLabel *label_4;
    QDateEdit *dateToEdit;
    QHBoxLayout *horizontalLayout_3;
    QTableView *tableView;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuView;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1200, 900);
        MainWindow->setMaximumSize(QSize(1200, 900));
        actionConnect = new QAction(MainWindow);
        actionConnect->setObjectName(QString::fromUtf8("actionConnect"));
        actionConnect->setEnabled(true);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/icons/database-accept-icon 48x48.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionConnect->setIcon(icon);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icons/icons/remove-icon 48x48.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionExit->setIcon(icon1);
        actionFull_Screen = new QAction(MainWindow);
        actionFull_Screen->setObjectName(QString::fromUtf8("actionFull_Screen"));
        actionFull_Screen->setCheckable(true);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/icons/computer-icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionFull_Screen->setIcon(icon2);
        actionRefrech = new QAction(MainWindow);
        actionRefrech->setObjectName(QString::fromUtf8("actionRefrech"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icons/icons/database-down-icon 48x48.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionRefrech->setIcon(icon3);
        actionUpdateData = new QAction(MainWindow);
        actionUpdateData->setObjectName(QString::fromUtf8("actionUpdateData"));
        actionUpdateData->setEnabled(false);
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icons/icons/OnLamp-icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon4.addFile(QString::fromUtf8(":/icons/icons/OnLamp-icon.png"), QSize(), QIcon::Normal, QIcon::On);
        icon4.addFile(QString::fromUtf8(":/icons/icons/OffLamp-icon.png"), QSize(), QIcon::Disabled, QIcon::Off);
        icon4.addFile(QString::fromUtf8(":/icons/icons/OffLamp-icon.png"), QSize(), QIcon::Disabled, QIcon::On);
        actionUpdateData->setIcon(icon4);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 0, 1410, 882));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetMaximumSize);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lastWeekButton = new QPushButton(verticalLayoutWidget);
        lastWeekButton->setObjectName(QString::fromUtf8("lastWeekButton"));

        horizontalLayout->addWidget(lastWeekButton);

        lastTwoWeeksButton = new QPushButton(verticalLayoutWidget);
        lastTwoWeeksButton->setObjectName(QString::fromUtf8("lastTwoWeeksButton"));

        horizontalLayout->addWidget(lastTwoWeeksButton);

        lastMonthButton = new QPushButton(verticalLayoutWidget);
        lastMonthButton->setObjectName(QString::fromUtf8("lastMonthButton"));

        horizontalLayout->addWidget(lastMonthButton);

        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout->addWidget(label_3);

        dateFromEdit = new QDateEdit(verticalLayoutWidget);
        dateFromEdit->setObjectName(QString::fromUtf8("dateFromEdit"));
        dateFromEdit->setCalendarPopup(true);

        horizontalLayout->addWidget(dateFromEdit);

        label_4 = new QLabel(verticalLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout->addWidget(label_4);

        dateToEdit = new QDateEdit(verticalLayoutWidget);
        dateToEdit->setObjectName(QString::fromUtf8("dateToEdit"));
        dateToEdit->setCalendarPopup(true);

        horizontalLayout->addWidget(dateToEdit);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        tableView = new QTableView(verticalLayoutWidget);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tableView->sizePolicy().hasHeightForWidth());
        tableView->setSizePolicy(sizePolicy1);
        tableView->setMinimumSize(QSize(300, 800));
        tableView->setMaximumSize(QSize(400, 16777215));
        tableView->setMouseTracking(true);
        tableView->setStyleSheet(QString::fromUtf8("QTableView::item {\n"
"    border: 1px solid #d9d9d9;\n"
"    border-top-color: transparent;\n"
"    border-bottom-color: transparent;\n"
"}\n"
" \n"
"QTableView::item:hover {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #e7effd, stop: 1 #cbdaf1);\n"
"    border: 1px solid #bfcde4;\n"
"}\n"
" \n"
"QTableView::item:selected {\n"
"    border: 1px solid #567dbc;\n"
"}\n"
" \n"
"QTableView::item:selected:active{\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #6ea1f1, stop: 1 #567dbc);\n"
"}\n"
" \n"
"QTableView::item:selected:!active {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #6b9be8, stop: 1 #577fbf);\n"
"}\n"
""));
        tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView->setProperty("showDropIndicator", QVariant(false));
        tableView->setAlternatingRowColors(true);
        tableView->setSelectionBehavior(QAbstractItemView::SelectItems);

        horizontalLayout_3->addWidget(tableView);


        verticalLayout->addLayout(horizontalLayout_3);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1200, 23));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QString::fromUtf8("menuView"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        mainToolBar->setEnabled(true);
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuFile->addAction(actionConnect);
        menuFile->addAction(actionRefrech);
        menuFile->addAction(actionUpdateData);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        menuView->addAction(actionFull_Screen);
        mainToolBar->addAction(actionConnect);
        mainToolBar->addAction(actionRefrech);
        mainToolBar->addAction(actionExit);
        mainToolBar->addAction(actionFull_Screen);
        mainToolBar->addAction(actionUpdateData);

        retranslateUi(MainWindow);
        QObject::connect(dateFromEdit, SIGNAL(editingFinished()), MainWindow, SLOT(onFromDateChange()));
        QObject::connect(dateToEdit, SIGNAL(editingFinished()), MainWindow, SLOT(onToDateChange()));
        QObject::connect(actionExit, SIGNAL(triggered()), MainWindow, SLOT(close()));
        QObject::connect(tableView, SIGNAL(clicked(QModelIndex)), MainWindow, SLOT(onTableItemClicked(QModelIndex)));
        QObject::connect(actionConnect, SIGNAL(triggered()), MainWindow, SLOT(onConnectionDialog()));
        QObject::connect(actionFull_Screen, SIGNAL(toggled(bool)), MainWindow, SLOT(onFullScreen()));
        QObject::connect(tableView, SIGNAL(doubleClicked(QModelIndex)), MainWindow, SLOT(onTableItemDoubleClicked(QModelIndex)));
        QObject::connect(actionRefrech, SIGNAL(triggered()), MainWindow, SLOT(onRefresh()));
        QObject::connect(actionUpdateData, SIGNAL(triggered()), MainWindow, SLOT(onUpdateDataClick()));
        QObject::connect(lastWeekButton, SIGNAL(clicked()), MainWindow, SLOT(onLastWeekClick()));
        QObject::connect(lastTwoWeeksButton, SIGNAL(clicked()), MainWindow, SLOT(onLastTwoWeeksClick()));
        QObject::connect(lastMonthButton, SIGNAL(clicked()), MainWindow, SLOT(onLastMonthClick()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "EnchSales DB Client 0.56", 0, QApplication::UnicodeUTF8));
        actionConnect->setText(QApplication::translate("MainWindow", "Connect...", 0, QApplication::UnicodeUTF8));
        actionConnect->setShortcut(QApplication::translate("MainWindow", "Ctrl+C", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0, QApplication::UnicodeUTF8));
        actionFull_Screen->setText(QApplication::translate("MainWindow", "Full Screen", 0, QApplication::UnicodeUTF8));
        actionFull_Screen->setShortcut(QApplication::translate("MainWindow", "Ctrl+F", 0, QApplication::UnicodeUTF8));
        actionRefrech->setText(QApplication::translate("MainWindow", "Reconnect", 0, QApplication::UnicodeUTF8));
        actionRefrech->setShortcut(QApplication::translate("MainWindow", "F5", 0, QApplication::UnicodeUTF8));
        actionUpdateData->setText(QApplication::translate("MainWindow", "Update Data", 0, QApplication::UnicodeUTF8));
        lastWeekButton->setText(QApplication::translate("MainWindow", "Last Week", 0, QApplication::UnicodeUTF8));
        lastTwoWeeksButton->setText(QApplication::translate("MainWindow", "Last 2 weeks", 0, QApplication::UnicodeUTF8));
        lastMonthButton->setText(QApplication::translate("MainWindow", "Last Month", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "From date:", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "To date:", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
        menuView->setTitle(QApplication::translate("MainWindow", "View", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
